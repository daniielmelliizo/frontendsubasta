/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        'custom-fondo': "url('/assets/Images/Fondo-new.svg')",
        'custom-fondo-login': "url('/assets/Images/login_New.svg')",
        'custom-fondo-time': "url('/assets/Images/img time.svg')",
        'custom-fondo-subasta': "url('/assets/Images/fondosubasta.svg')",
        'custom-red': '#D8354C',
        'custom-yellow': '#F6EBDA',
        'custom-overlay': 'linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5))',
        'custom-overlay-two': 'linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7))',
      },
      height: {
        'screen-75': '75vh',
        'screen-70': '70vh',
        'screen-65': '65vh',
        'screen-60': '60vh',
        'screen-50': '50vh',
        'screen-45': '45vh',
        'screen-40': '40vh',
        'screen-35': '35vh',
        'screen-30': '30vh',
        'screen-25': '25vh',
        'screen-15': '15vh',
        'screen-10': '10vh',
        'screen-5': '5vh',
      },
      keyframes: {
        breathe: {
          '0%, 100%': { backgroundColor: 'rgba(144, 238, 144, 0.5)' },
          '50%': { backgroundColor: 'rgba(144, 238, 144, 0.1)' },
        },
      },
      animation: {
        breathe: 'breathe 2s ease-in-out infinite',
      },
    },
  },
  plugins: [],
};
