# Utilizar la imagen oficial de Node.js versión 20.12.2
FROM node:20.12.2

# Establecer el directorio de trabajo en el contenedor
WORKDIR /app

# Copiar los archivos 'package.json' y 'package-lock.json'
COPY package*.json ./

# Instalar las dependencias del proyecto
RUN npm install

# Copiar todos los archivos del proyecto al contenedor
COPY . .

# Construir la aplicación
RUN npm run build

# Definir el puerto que será expuesto por el contenedor
EXPOSE 3000

# Comando para ejecutar la aplicación en modo de producción
CMD ["npm", "run", "start"]
