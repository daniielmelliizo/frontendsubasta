/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  /* images: {
    domains: ['yourdomain.com'], // Agrega los dominios de donde estás sirviendo imágenes
  }, */
  webpack: (config, { isServer }) => {
    // Configuración adicional de Webpack si es necesaria
    if (!isServer) {
      config.resolve.fallback.fs = false;
    }
    return config;
  },
  async redirects() {
    return [
      {
        source: '/admin',
        destination: '/admin/LoginAdmin',
        permanent: true,
      },
      {
        source: '/user',
        destination: '/user/Home',
        permanent: true,
      },
    ];
  },
};

export default nextConfig;
