import React from "react";
import Image from "../Inviduals/Image";
import Text from "../Inviduals/Text";

const AuctionCard = ({ auction, userRol }) => (
  <div className="bg-[#2F3542] rounded-lg shadow-lg p-4 min-w-[250px] max-w-sm min-h-[400px] flex flex-col">
    <Image
      src={auction.product?.imagen || "placeholder.jpg"}
      alt="Product"
      className="w-full h-48 object-cover rounded-t-lg"
    />
    <div className="p-4 flex flex-col gap-2">
      <Text color="text-white-700" className="text-sm font-semibold">
        {auction.product?.nombre || "N/A"}
      </Text>
      <Text color="text-white-700" className="text-sm">
        Lot: {auction.product?.lote || "N/A"}
      </Text>
      <Text color="text-white-700" className="text-sm">
        Process: {auction.product?.process || "N/A"}
      </Text>
      <Text color="text-white-700" className="text-sm">
        Score: {auction.product?.score || "N/A"}
      </Text>
      <Text color="text-white-700" className="text-sm">
        Weight: {auction.product?.peso || "N/A"} {auction.product?.unidadPeso || ""}
      </Text>
      <Text color="text-white-700" className="text-sm">
        Final price:{" "}
        {auction.product?.precioFinal
          ? Number(auction.product?.precioFinal).toLocaleString("de-DE", {
              minimumFractionDigits: 0,
              maximumFractionDigits: 0,
            })
          : "N/A"}{" "}
        USD
      </Text>
      <Text color="text-white-700" className="text-sm">
        Increment: {auction.product?.increment || "N/A"}
      </Text>
      <Text color="text-white-700" className="text-sm">
        Status: {auction.estado || "N/A"}
      </Text>
      <Text color="text-white-700" className="text-sm">
        Winner:{" "}
        {userRol
          ? auction.winningUser?.nombre + " " + auction.winningUser?.apellido
          : auction.highestBidParticipantNum || "N/A"}
      </Text>
      <Text color="text-white-700" className="text-sm">
        Total:{" "}
        {auction.product?.precioFinal
          ? Number(auction.product?.precioFinal).toLocaleString("de-DE", {
              minimumFractionDigits: 0,
              maximumFractionDigits: 0,
            })
          : "N/A"}{" "}
        USD
      </Text>
      <Text color="text-white-700" className="text-sm">
        Total / Weight:{" "}
        {auction.product?.precioFinal
          ? Number(
              auction.product?.precioFinal * auction.product?.peso
            ).toLocaleString("de-DE", {
              minimumFractionDigits: 0,
              maximumFractionDigits: 0,
            })
          : "N/A"}{" "}
        USD
      </Text>
    </div>
  </div>
);

export default AuctionCard;
