import React from "react";
import AuctionRow from "./AuctionRow";
import AuctionCard from "./AuctionCard";

const AuctionTable = ({ auctionDetails, updatedRows, userRol }) => {
  return (
    <div className="flex-grow overflow-auto">
      {Object.entries(auctionDetails).map(([muestraTitle, muestraData], index) => (
        <div key={index} className="mb-8">
          <h1 className="text-2xl font-bold mb-4">{muestraTitle.toUpperCase()}</h1>
          {Object.entries(muestraData).map(([subTitle, auctions]) => (
            <div key={subTitle} className="mb-8">
              <div className="bg-[#2F3542] rounded-lg p-4">
                <h2 className="text-xl font-bold mb-4">{subTitle.toUpperCase()}</h2>
                
                {/* Tabla: Visible en pantallas medianas y grandes */}
                <div className="hidden md:block">
                  <table className="w-full table-auto border-separate border-spacing-0">
                    <thead>
                      <tr>
                        <th className="w-1/11 px-2 py-1 text-center text-white">Lot</th>
                        <th className="w-1/12 px-2 py-1 text-center text-white">Product</th>
                        <th className="w-1/12 px-2 py-1 text-center text-white">Process</th>
                        <th className="w-1/12 px-2 py-1 text-center text-white">Score</th>
                        <th className="w-1/12 px-2 py-1 text-center text-white">Weight</th>
                        <th className="w-1/12 px-2 py-1 text-center text-white">Bid</th>
                        <th className="w-1/12 px-2 py-1 text-center text-white">Increment</th>
                        <th className="w-1/12 px-2 py-1 text-center text-white">Status</th>
                        <th className="w-1/12 px-2 py-1 text-center text-white">
                          {userRol ? "Ganador" : "#1"}
                        </th>
                        <th className="w-1/12 px-2 py-1 text-center text-white">Total</th>
                        <th className="w-1/12 px-2 py-1 text-center text-white">Price</th>
                      </tr>
                    </thead>
                    <tbody>
                      {auctions.map((auction, subIndex) => (
                        <AuctionRow
                          key={subIndex}
                          auction={auction}
                          isHighlighted={updatedRows[auction.id]}
                          userRol={userRol}
                        />
                      ))}
                    </tbody>
                  </table>
                </div>

                {/* Cards: Visible en pantallas pequeñas */}
                <div className="block md:hidden">
                  <div className="grid gap-4 grid-cols-1 sm:grid-cols-2">
                    {auctions.map((auction, subIndex) => (
                      <AuctionCard
                        key={subIndex}
                        auction={auction}
                        userRol={userRol}
                      />
                    ))}
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      ))}
    </div>
  );
};

export default AuctionTable;
