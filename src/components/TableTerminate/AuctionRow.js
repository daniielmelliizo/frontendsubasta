import React from "react";

const AuctionRow = ({ auction, isHighlighted, userRol }) => (
  <tr className={isHighlighted ? "bg-red-500 bg-opacity-70 animate-pulse rounded-lg" : ""}>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.lote || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.nombre || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.process || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.score || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.peso || "N/A"} {auction.product?.unidadPeso || ""}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.precioFinal
        ? Number(auction.product.precioFinal).toLocaleString("de-DE", {
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
          })
        : "N/A"}{" "}
      USD
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.increment || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.estado || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {userRol ? auction.winningUser?.nombre || "N/A" + " "+  auction.winningUser?.apellido || "N/A"
      : (auction.highestBidParticipantNum || "N/A")}
  </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.precioInicial
        ? Number(auction.product?.precioFinal).toLocaleString("de-DE", {
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
          })
        : "N/A"}{" "}
      USD
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.precioInicial
        ? Number(auction.product?.precioFinal * auction.product?.peso).toLocaleString("de-DE", {
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
          })
        : "N/A"}{" "}
      USD
    </td>
  </tr>
);

export default AuctionRow;
