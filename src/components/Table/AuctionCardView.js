import React from "react";
import AuctionCard from "./AuctionCard";
import Link from 'next/link';

const AuctionCardView = ({ auctionDetails, handlePreconfigClick, username }) => {
  return (
    <div className="grid grid-cols-1 gap-4">
      {Object.entries(auctionDetails).map(([muestraTitle, muestraData], index) => {

        return (
          <div key={index} className="mb-8">
            <Link
              href={{
                pathname: '/VistaSubasta',
                query: {
                  muestraId:JSON.stringify(auctionDetails),
                  username, // Como string directamente
                },
              }}
            >
              <h1 className="text-2xl text-white font-bold mb-4">{muestraTitle.toUpperCase()}</h1>
           </Link>

            {Object.entries(muestraData).map(([title, auctions]) => {


              return (
                <div key={title} className="bg-black rounded-lg shadow-lg p-4">
                  <h2 className="text-xl font-bold mb-4 text-white">{title.toUpperCase()}</h2>
                  <div className="flex gap-4 overflow-x-auto">
                    {auctions.map((auction, subIndex) => (
                      <AuctionCard
                        key={subIndex}
                        auction={auction}
                        handlePreconfigClick={handlePreconfigClick}
                      />
                    ))}
                  </div>
                </div>
              );
            })}
          </div>
        );
      })}
    </div>
  );
};

export default AuctionCardView;
