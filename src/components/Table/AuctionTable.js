import React, { useState, useEffect } from "react";
import AuctionRow from "./AuctionRow";
import CountdownTimer from "../Body/Cliente/Auction/CountDownTimer";
import secureStorage from "src/utils/secureStorage";
import CountdownDisplay from "../Body/Cliente/Auction/CountdownDisplay";

const AuctionTable = ({ auctionDetails, fetchAuctionDetails, handleDropdownChange, handlePreconfigClick, updatedRows }) => {
    const [showTable, setShowTable] = useState(false); // Estado para controlar la visibilidad de la tabla
    const [username, setUsername] = useState('');

    const handleTimerEnd = () => {
        setShowTable(true); // Muestra la tabla cuando el temporizador finalice
    };

    useEffect(() => {
        const storedUsername = secureStorage.getItem("user");
        const parsedUser = storedUsername ? JSON.parse(storedUsername) : null;
        setUsername(parsedUser?.id || '');
    }, []);

    return (
        <div className="flex-grow overflow-auto">
            {Object.entries(auctionDetails).map(([muestraTitle, muestraData], index) => (
                <div key={index} className="mb-8">
                    {Object.entries(muestraData).map(([subTitle, auctions]) => (
                        <div key={subTitle} className="mb-8">
                            {/* Verifica si la subasta está en estado 'Inactivo' y el temporizador no ha terminado */}
                            {auctions.some(auction => auction.estado === 'Inactiva' || auction.estado === 'Activo') && !showTable ? (
                                <CountdownTimer
                                    NombreMuestra={muestraTitle || 'N/A'} // Nombre general de la muestra
                                    startTime={auctions[0]?.horaSubasta} // Toma el tiempo de subasta de la primera entrada inactiva
                                    auctions={auctions[0]} // Pasa la primera subasta inactiva al CountdownTimer
                                    username={username}
                                    fetchAuctionDetails={fetchAuctionDetails}
                                    onTimerEnd={handleTimerEnd}
                                />
                            ) : (
                                <>
                                    {/* Solo muestra los encabezados cuando la tabla es visible */}
                                    <div className="flex items-center justify-between mb-4">
                                        <h1 className="text-2xl font-bold">{muestraTitle.toUpperCase()}</h1>
                                        <CountdownDisplay endTime={auctions[0]?.horaFin} />
                                    </div>

                                    <div className="bg-[#2F3542] rounded-lg p-4">
                                        <h2 className="text-xl font-bold mb-4">{subTitle.toUpperCase()}</h2>
                                        <table className="w-full table-auto mb-2">
                                            <thead>
                                                <tr>
                                                    <th className="w-1/11 px-2 py-1 text-center text-white">N#</th>
                                                    <th className="w-1/11 px-2 py-1 text-center text-white">Lot</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Product</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Process</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Score</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Weight</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Bid</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Increment</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Auto/Manual</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Status</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">#1</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Total</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Price</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {auctions.map((auction, subIndex) => (
                                                    <AuctionRow
                                                        key={subIndex}
                                                        auction={auction}
                                                        isHighlighted={updatedRows[auction.id]}
                                                        fetchAuctionDetails={fetchAuctionDetails}
                                                        handleDropdownChange={handleDropdownChange}
                                                        handlePreconfigClick={handlePreconfigClick}
                                                    />
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </>
                            )}
                        </div>
                    ))}
                </div>
            ))}
        </div>
    );
};

export default AuctionTable;
