import React from "react";
import CustomButton from "../Inviduals/CustomButton";

const AuctionRow = ({ auction, isHighlighted, fetchAuctionDetails, handleDropdownChange, handlePreconfigClick }) => (
  <tr className={isHighlighted ? "bg-red-500 bg-opacity-70 animate-pulse rounded-lg" : ""}>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.bidUsers[0]?.participantNum || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.lote || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.nombre || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.process || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.score || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.peso || "N/A"} {auction.product?.unidadPeso || ""}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.precioFinal
        ? Number(auction.product.precioFinal).toLocaleString("de-DE", {
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
          })
        : "N/A"}{" "}
      USD
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.increment || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.estado !== "Finalizada" &&
        auction.suscriptions?.length > 0 &&
        auction.bidUsers?.map((bidUser) => {
          const matchingSubscription = auction.suscriptions.find(
            (suscription) =>
              suscription.auctionId === auction.id &&
              suscription.id === bidUser.suscribedId
          );

          if (matchingSubscription) {
            let selectedOption = `x${bidUser.valorPor}`;
            const filteredOptions = [
              { value: "x1", label: "x1" },
              { value: "x2", label: "x2" },
            ];

            return (
              <div key={bidUser.id} className="flex items-center space-x-1">
                {bidUser.manual ? (
                  <select
                    value={selectedOption}
                    onChange={(e) =>
                      handleDropdownChange(e.target.value, auction, bidUser)
                    }
                    className="px-12 py-1 m-5 text-center text-white bg-gray-800 border border-gray-700 rounded text-sm"
                  >
                    {filteredOptions.map((option) => (
                      <option key={option.value} value={option.value}>
                        {option.label}
                      </option>
                    ))}
                  </select>
                ) : (
                  <CustomButton
                    onClick={() => handleDropdownChange("Manual", auction, bidUser)}
                    className="text-sm bg-gray-800 text-white"
                  >
                    Manually
                  </CustomButton>
                )}
              </div>
            );
          }
          return null;
        })}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.estado || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.highestBidParticipantNum || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.precioInicial
        ? Number(auction.product?.precioFinal).toLocaleString("de-DE", {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })
        : "N/A"}{" "}
      USD
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.precioInicial
        ? Number(auction.product?.precioFinal * auction.product?.peso).toLocaleString("de-DE", {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })
        : "N/A"}{" "}
      USD
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
    {auction.estado !== "Finalizada" && (
      auction.bidUsers?.some((bidUser) => 
        auction.suscriptions?.some((suscription) => 
          suscription.auctionId === auction.id && 
          suscription.id === bidUser.suscribedId && 
          suscription.status === true
        )
      ) ? (
        // Si existe una suscripción activa con un bidUser, muestra el botón "Actualizar bid"
        auction.bidUsers.map((bidUser) => {
          const matchingSubscription = auction.suscriptions.find(
            (suscription) =>
              suscription.auctionId === auction.id &&
              suscription.id === bidUser.suscribedId &&
              suscription.status === true
          );
          return (
            matchingSubscription && (
              <CustomButton
                key={bidUser.id}
                onClick={() => handlePreconfigClick(auction)}
                className="text-sm rounded bg-red-500 text-white font-bold"
              >
                Upgrade bid
              </CustomButton>
            )
          );
        })
      ) : (
        // Si no hay ningún bidUser asociado a una suscripción activa, muestra el botón "configurar bid"
        auction.suscriptions?.some((suscription) => suscription.auctionId === auction.id && suscription.status === true) && (
          <CustomButton
            onClick={() => handlePreconfigClick(auction)}
            className="text-sm rounded bg-red-500 text-white font-bold"
          >
            configure bid
          </CustomButton>
        )
      )
    )}
    </td>
  </tr>
);

export default AuctionRow;
