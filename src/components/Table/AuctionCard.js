import React from "react";
import Image from "../Inviduals/Image";
import CustomButton from "../Inviduals/CustomButton";
import Text from "../Inviduals/Text";

const AuctionCard = ({ auction, handlePreconfigClick }) => (
  <div className="bg-[#2F3542] rounded-lg shadow-lg mb-4 p-4 flex-shrink-0 min-w-[300px]">
    <Image
      src={auction.product?.imagen || "placeholder.jpg"}
      alt="Product"
      className="w-full h-48 object-cover rounded-t-lg"
    />
    <div className="p-4">
      <Text color="text-white-700" className="text-sm font-semibold">
        {auction.product?.nombre || "N/A"}
      </Text>
      <Text color="text-white-700" className="text-sm">
        Lot: {auction.product?.lote || "N/A"}
      </Text>
      <Text color="text-white-700" className="text-sm">
        Process: {auction.product?.process || "N/A"}
      </Text>
      <Text color="text-white-700" className="text-sm">
        Score: {auction.product?.score || "N/A"}
      </Text>
      <Text color="text-white-700" className="text-sm">
        Weight: {auction.product?.peso || "N/A"} {auction.product?.unidadPeso || ""}
      </Text>
      <Text color="text-white-700" className="text-sm">
        Final price:{" "}
        {auction.product?.precioFinal
          ? Number(auction.product?.precioFinal).toLocaleString("de-DE", {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })
          : "N/A"}{" "}
        USD
      </Text>
      <Text color="text-white-700" className="text-sm">
        Increment: {auction.product?.increment || "N/A"}
      </Text>
      <Text color="text-white-700" className="text-sm">
        Status: {auction.estado || "N/A"}
      </Text>
      <Text color="text-white-700" className="text-sm">
        N# Participant: {auction.bidUsers[0]?.participantNum || "N/A"}
      </Text>
      <Text color="text-white-700" className="text-sm">
      N# highest bid: {auction.highestBidParticipantNum || "N/A"}
      </Text>
      <Text color="text-white-700" className="text-sm">
        Total:{" "}
        {auction.product?.precioFinal
          ? Number(auction.product?.precioFinal).toLocaleString("de-DE", {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })
          : "N/A"}{" "}
        USD
      </Text>
      <Text color="text-white-700" className="text-sm">
        Total / Wieght:{" "}
        {auction.product?.precioFinal
          ? Number(auction.product?.precioFinal * auction.product?.peso).toLocaleString("de-DE", {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })
          : "N/A"}{" "}
        USD
      </Text>
      {auction.estado !== "Finalizada" && (
      auction.bidUsers?.some((bidUser) => 
        auction.suscriptions?.some((suscription) => 
          suscription.auctionId === auction.id && 
          suscription.id === bidUser.suscribedId && 
          suscription.status === true
        )
      ) ? (
        // Si existe una suscripción activa con un bidUser, muestra el botón "Actualizar bid"
        auction.bidUsers.map((bidUser) => {
          const matchingSubscription = auction.suscriptions.find(
            (suscription) =>
              suscription.auctionId === auction.id &&
              suscription.id === bidUser.suscribedId &&
              suscription.status === true
          );
          return (
            matchingSubscription && (
              <CustomButton
                key={bidUser.id}
                onClick={() => handlePreconfigClick(auction)}
                className="text-sm rounded bg-red-500 text-white font-bold"
              >
                Update bid
              </CustomButton>
            )
          );
        })
      ) : (
        // Si no hay ningún bidUser asociado a una suscripción activa, muestra el botón "configurar bid"
        auction.suscriptions?.some((suscription) => suscription.auctionId === auction.id && suscription.status === true) && (
          <CustomButton
            onClick={() => handlePreconfigClick(auction)}
            className="text-sm rounded bg-red-500 text-white font-bold"
          >
            Configure bid
          </CustomButton>
        )
      )
    )}
    </div>
  </div>
);

export default AuctionCard;
