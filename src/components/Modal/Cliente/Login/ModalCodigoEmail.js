import React, { useState } from "react";
import PropTypes from "prop-types";
import CustomButton from "../../../Inviduals/CustomButton";
import Image from "../../../Inviduals/Image";
import Tittle from "../../../Inviduals/Tittle";
import Text from "../../../Inviduals/Text";
import UserService from "src/aplication/services/userService";
import UserController from "src/controllers/userController";
import ModalAlertsConfir from "../../ModalAlertsConfir";
import ModalAlertsError from "../../ModalAlertsError";

const CodigoCorreoModal = ({ isOpen, onClose, email, onCloseRegister, onCloseLogin }) => {
  const [codigo, setCodigo] = useState(Array(6).fill(""));
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);
  
  const userService = new UserService();
  const userController = new UserController(userService);

  const handleInputChange = (e, index) => {
    const { value } = e.target;
    if (/^\d*$/.test(value)) {
      const newCodigo = [...codigo];
      newCodigo[index] = value;
      setCodigo(newCodigo);

      if (value && index < 5) {
        document.getElementById(`codigo-${index + 1}`).focus();
      }
    }
  };

  const handleKeyDown = (e, index) => {
    if (e.key === "Backspace" && !codigo[index] && index > 0) {
      document.getElementById(`codigo-${index - 1}`).focus();
    }
  };

  const handleSubmit = async () => {
    const fullCodigo = codigo.join("");
    if (fullCodigo.length === 6) {
      try {
        const form = {
          email: email,
          verificationCode: fullCodigo,
        };

        const data = await userController.handleValidationComplet(form);
        
        if (data.status === 409 || data.status === 400) {
          handleErrorConfiModal("Invalid code");
        } else {
          handleOpenConfiModal("Valid code");
        }

      } catch (error) {}
    } else {
      handleErrorConfiModal("The code must be 6 digits long");
    }
  };

  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };
  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };
  const handleClose = () => {
    onClose();
    onCloseRegister();
    onCloseLogin();
    setConfirModalOpen(false);
  };

  return isOpen ? (
    <div className="fixed inset-0 bg-gray-50 bg-opacity-75 flex justify-center items-center z-50">
      <div className="bg-black p-5 rounded-lg w-full sm:w-2/3 md:w-1/2 lg:w-1/3 max-w-lg mx-auto">
        {" "}
        {/* Responsivo */}
        <CustomButton
          title="Close"
          onClick={onClose}
          className="text-white bg-black mb-4"
        />
        <div className="flex justify-center items-center mb-4">
          <div className="w-20 h-10">
            {" "}
            {/* Tamaño fijo del logo */}
            <Image src="/assets/Images/logo.svg" alt="Logo" />
          </div>
        </div>
        <Tittle
          level="h1"
          size="text-2xl md:text-3xl"
          color="text-white"
          align="text-center"
          className="mb-4"
        >
          Enter Validation Code
        </Tittle>
        <Text
          size="text-sm md:text-base"
          color="text-white"
          align="text-center"
          className="mb-4"
        >
          Please enter the 6-digit code you received.
        </Text>
        <div className="flex justify-center items-center mb-4 space-x-2">
          {codigo.map((char, index) => (
            <input
              key={index}
              type="text"
              id={`codigo-${index}`}
              value={char}
              onChange={(e) => handleInputChange(e, index)}
              onKeyDown={(e) => handleKeyDown(e, index)}
              maxLength={1}
              className="bg-black text-center text-white w-10 h-12 px-2 py-2 border border-white rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          ))}
        </div>
        <div className="flex justify-center">
          <CustomButton
            title="Valid code"
            onClick={handleSubmit}
            className="text-white bg-[#D8354C]"
          />
        </div>
      </div>
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={handleClose}
        customMessage={confirMessage}
      />
    </div>
  ) : null;
};

CodigoCorreoModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onCloseRegister: PropTypes.func,
  onCloseLogin: PropTypes.func,
  handleCompleteValidation: PropTypes.func,
  registeredEmail: PropTypes.string,
};

export default CodigoCorreoModal;
