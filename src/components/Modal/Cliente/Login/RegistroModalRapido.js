import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import CustomButton from "../../../Inviduals/CustomButton";
import Input from "../../../Inviduals/Input";
import Tittle from "../../../Inviduals/Tittle";
import Image from "../../../Inviduals/Image";
import Text from "../../../Inviduals/Text";
import UserInvitedService from "src/aplication/services/userInvitedService";
import UserInvitedController from "src/controllers/UserInvitedController";
import ModalAlertsConfir from "../../ModalAlertsConfir";
import { FaTimes, FaEye, FaEyeSlash } from "react-icons/fa";
import ModalAlertsError from "../../ModalAlertsError";

const userInvitedService = new UserInvitedService();
const userInvitedController = new UserInvitedController(userInvitedService);

const RegisterModalRapido = ({ isOpen, onClose, code, onCloseLogin, onCloseValidCod }) => {
  console.log("que me llega",code );
  
  const [formData, setFormData] = useState({
    email: "",
    password: "",
    nombre: "",
    apellido: "",
    code: "", // Set the code received from props
    randomPart: ""
  });

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);
  const [isPasswordVisible, setPasswordVisible] = useState(false); // Estado para la visibilidad de la contraseña

  useEffect(() => {
    if (code) {
      setFormData({
        ...formData,
        code: code.slice(0, 6), // Set the first 6 characters
        randomPart: code.slice(6) // Set the remaining characters
      });
    }
  }, [code]); // Only run when code changes

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({ ...prevData, [name]: value }));
  };

  const validateEmail = (email) => {
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regex.test(email);
  };

  const validateRequiredFields = () => {
    const requiredFields = ["nombre", "email", "password", "apellido"];
    for (let field of requiredFields) {
      if (!formData[field]) {
        return false;
      }
    }
    return true;
  };

  const validatePassword = (password) => {
    // Expresión regular para al menos 8 caracteres, una letra mayúscula y un carácter especial
    const regex = /^(?=.*[A-Z])(?=.*[!@#$%^&*(),.?":{}|<>]).{8,}$/;
    return regex.test(password);
  };

  const handleRegistro = async (event) => {
    event.preventDefault();

    if (!validateRequiredFields()) {
      handleErrorConfiModal("Missing fields to be filled in");
      return;
    }

    if (!validateEmail(formData.email)) {
      handleErrorConfiModal("The e-mail is not valid.");
      return;
    }

    if (!validatePassword(formData.password)) {
      handleErrorConfiModal(
        "The password must be at least 8 characters long, include a capital letter and a special character.");
      return;
    }

    try {
      setLoading(true);
      console.log(" que me guarda", formData);
      
      const response = await userInvitedController.handleCreateUserInvited(
        formData
      );
      setLoading(false);
      if (response.status === 409 || response.status === 400) {
        handleErrorConfiModal(response.data.message);
      } else {
        handleOpenConfiModal(response.message);
      }
    } catch (error) {
      setLoading(false);
      setError(error.message);
    }
  };

  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  const handleClose = () => {
    setConfirModalOpen(false);
    onClose();
    onCloseLogin();
    onCloseValidCod();
  };

  const handleCloseRegisterRapido = () => {
    onClose();
    onCloseValidCod();
  };

  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 bg-gray-50 bg-opacity-75 flex justify-center items-center z-50">
      <div className="bg-black hidden md:flex justify-center h-screen-50 w-screen-50 rounded-lg">
        <div className="bg-black p-5 rounded-lg h-screen-50 w-96 relative flex flex-col justify-center items-center">
          <CustomButton
            title="Close"
            onClick={handleCloseRegisterRapido}
            className="bg-black absolute top-2 left-2 text-gray-500 hover:text-gray-500"
          />
          <Tittle
            level="h1"
            size="text-3x1"
            color="text-custom-red"
            align="text-center"
            className="p-5"
          >
            Register
          </Tittle>
          <form className="w-full px-5 " onSubmit={handleRegistro}>
            <div className="mb-4 text-black">
              <Input
                name="email"
                value={formData.email}
                onChange={handleChange}
                placeholder="E-mail"
                type="email"
                className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
              />
            </div>
            <div className="mb-4 text-black">
              <Input
                name="password"
                value={formData.password}
                onChange={handleChange}
                placeholder="Password"
                type={isPasswordVisible ? "text" : "password"} 
                className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
              />
               <div
                    className="absolute right-3 top-1/2 transform -translate-y-1/2 cursor-pointer"
                    onClick={() => setPasswordVisible(!isPasswordVisible)}
                  >
                    {isPasswordVisible ? <FaEyeSlash /> : <FaEye />}
                </div>
            </div>
            <div className="mb-4 text-black">
              <Input
                name="nombre"
                value={formData.nombre}
                onChange={handleChange}
                placeholder="Name"
                type="text"
                className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
              />
            </div>
            <div className="mb-4 text-black">
              <Input
                name="apellido"
                value={formData.apellido}
                onChange={handleChange}
                placeholder="Last name"
                type="text"
                className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
              />
            </div>

            <div className="col-span-3 flex justify-center items-center">
              <CustomButton
                type="submit"
                title="Register"
                className="bg-[#D8354C] w-full"
                disabled={loading}
              />
            </div>
          </form>
          {error && <p className="text-red-500 mt-4">{error}</p>}
        </div>
        <div className="relative h-screen-50 w-96">
          <div className="absolute inset-0 bg-custom-fondo-login bg-cover bg-center rounded-lg"></div>
          <div className="absolute inset-0 bg-black opacity-50 rounded-lg"></div>
          <div className="relative p-4 flex flex-col items-start justify-center h-full space-y-4">
            <div className="absolute top-2 left-2">
              <Image src="../assets/Images/logo.svg" width={20} height={20} />
            </div>
            <div className="flex flex-col items-start justify-center h-full space-y-4">
              <Tittle
                level="h1"
                size="text-3xl"
                color="text-custom-red"
                align="text-left"
              >
                Hello, friend!
              </Tittle>
              <Text
                size="text-sm"
                color="text-white-700"
                align="text-left"
                weight="font-light"
              >
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam
              </Text>
            </div>
          </div>
        </div>
      </div>
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={handleClose}
        customMessage={confirMessage}
      />
    </div>
  );
};

RegisterModalRapido.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  code: PropTypes.string.isRequired, // Ensure code prop is required
};

export default RegisterModalRapido;
