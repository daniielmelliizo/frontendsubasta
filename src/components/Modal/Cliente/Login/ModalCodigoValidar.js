import React, { useState } from "react";
import PropTypes from "prop-types";
import CustomButton from "../../../Inviduals/CustomButton";
import Image from "../../../Inviduals/Image";
import Tittle from "../../../Inviduals/Tittle";
import Text from "../../../Inviduals/Text";
import RegisterModalRapido from "./RegistroModalRapido";
import InvitationService from "src/aplication/services/InvitationService";
import InvitationController from "src/controllers/InvitationController";
import ModalAlertsConfir from "../../ModalAlertsConfir";

const CodigoValidacionModal = ({ isOpen, onClose, onCloseLogin }) => {
  const [error, setError] = useState("");
  const [isRegisterModalRapidoOpen, setRegisterModalRapidoOpen] = useState(false);
  const [codigo, setCodigo] = useState(Array(10).fill("")); // Adjusted to 10 digits
  const [confirMessage, setConfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);

  // 2. Service and Controller Instances
  const invitationService = new InvitationService();
  const invitationController = new InvitationController(invitationService);

  // 3. Input Handlers
  const handleInputChange = (e, index) => {
    const { value } = e.target;
    if (/^[a-zA-Z0-9]?$/.test(value)) {
      const newCodigo = [...codigo];
      newCodigo[index] = value;
      setCodigo(newCodigo);

      // Move focus to the next input
      if (value && index < 9) { // Adjusted to 9 for 10 total inputs
        document.getElementById(`codigo-${index + 1}`).focus();
      }
    }
  };

  const handleKeyDown = (e, index) => {
    if (e.key === "Backspace" && !codigo[index] && index > 0) {
      document.getElementById(`codigo-${index - 1}`).focus();
    }
  };

  // 4. Form Submission Handler
  const handleSubmit = async () => {
    const fullCodigo = codigo.join("");
    if (codigo.includes("")) {
      setError("Please complete all code fields.");
      return; // Detener el envío si hay campos vacíos
    }
    if (fullCodigo.length === 10) { // Updated to 10 digits
      try {
        const dataCode = {
          codeToValidate: fullCodigo.slice(0, 6), // Primeros 6 dígitos
          randomPart: fullCodigo.slice(6) // Últimos 4 dígitos
        };

        const data = await invitationController.handleCheckInvitation(dataCode);
        console.log("que me trae la data",data );
          if (data.status === 400) {
            setError(data.data.message);
          } else if (data.exists === true) {
            handleOpenConfiModal("Valid code now you can register");
            setRegisterModalRapidoOpen(true); // Open registration modal
          }
      } catch (error) {
        setError("Error de red: " + error);
      }
    }
  };

  // 5. Modal Control
  const handleOpenConfiModal = (message) => {
    setConfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleClose = () =>{
    setCodigo(Array(10).fill("")); // Restablece el código a un array vacío de 10 elementos
    setError(""); // Restablece el mensaje de error
    setConfirMessage(null); // Restablece el mensaje de confirmación
    onClose();
  }

  // 6. Render the component
  return isOpen ? (
    <div className="fixed inset-0 bg-gray-50 bg-opacity-75 flex justify-center items-center z-50">
      <div className="bg-black p-5 rounded-lg w-1/3">
        <CustomButton
          title="Close"
          onClick={handleClose}
          className="text-white bg-black mb-4"
        />
        <div
          className="flex justify-center items-center"
          style={{ width: "136px", height: "38px" }}
        >
          <Image src="/assets/Images/logo.svg" alt="Logo" />
        </div>
        <Tittle
          level="h1"
          size="text-3xl"
          color="text-white"
          align="text-center"
          className="mb-4"
        >
          Enter Validation Code
        </Tittle>
        <Text
          size="text-sm"
          color="text-white"
          align="text-center"
          className="mb-4"
        >
          Please enter the 10-digit code you received.
        </Text>
        <div className="flex justify-center items-center mb-4 space-x-2">
          {codigo.map((char, index) => (
            <input
              key={index}
              type="text"
              id={`codigo-${index}`}
              value={char}
              onChange={(e) => handleInputChange(e, index)}
              onKeyDown={(e) => handleKeyDown(e, index)}
              maxLength={1}
              className="bg-black text-center text-white w-10 h-12 px-2 py-2 border border-white rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          ))}
        </div>
        {error && <p className="text-red-500 text-center mb-4">{error}</p>}
        <div className="flex justify-center">
          <CustomButton
            title="Validate Code"
            onClick={handleSubmit}
            className="text-white bg-[#D8354C]"
          />
        </div>
      </div>

      {/* Registration Modal */}
      <RegisterModalRapido
        isOpen={isRegisterModalRapidoOpen}
        onClose={() => setRegisterModalRapidoOpen(false)}
        code={codigo.join("")} // Pass validation code to registration modal
        onCloseLogin={onCloseLogin}
        onCloseValidCod={onClose}
      />

      {/* Confirmation Modal */}
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={() => setConfirModalOpen(false)}
        customMessage={confirMessage}
      />
    </div>
  ) : null;
};

// PropTypes
CodigoValidacionModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onCloseLogin: PropTypes.func,
};

export default CodigoValidacionModal;
