import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CustomButton from '../../../Inviduals/CustomButton';
import { FaTimes } from "react-icons/fa";
import Icons from '../../../Inviduals/Icons';
import Tittle from '../../../Inviduals/Tittle';
import Text from '../../../Inviduals/Text';
import LoginModal from '../../../Header/Cliente/LoginModal';
import secureStorage from '../../../../utils/secureStorage';

const DesplegarSubasta = ({ isOpen, onClose, auctions }) => {
  const [isLoginModalOpen, setLoginModalOpen] = useState(false);
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  useEffect(() => {
    const storedUsername = secureStorage.getItem("user");
    const parsedUser = storedUsername ? JSON.parse(storedUsername) : null;
    if (parsedUser && parsedUser.id) {
      setIsAuthenticated(true);
    }
  }, []); // Verifica el estado de autenticación al cargar el componente

  const handleStarNowClick = () => {
    if (isAuthenticated) {
      // Redirigir inmediatamente si está autenticado
      window.location.href = '/user/ListaSubasta';
    } else {
      // Mostrar el modal si no está autenticado
      setLoginModalOpen(true);
    }
  };

  const handleLoginSuccess = (username) => {
    secureStorage.setItem('user', JSON.stringify({ username }));
    setIsAuthenticated(true); // Cambia el estado a autenticado
    setLoginModalOpen(false); // Cierra el modal
    window.location.href = '/user/ListaSubasta'; // Redirige automáticamente
  };

  const truncateText = (text, maxLength) => {
    return text.length > maxLength ? text.substring(0, maxLength) + '...' : text;
  };

  const limitedAuctions = auctions.slice(0, 2);

  return (
    <>
      {isOpen && auctions.length > 0 && (
        <div className="fixed inset-0 bg-[#090B11] bg-opacity-75 flex justify-center items-center z-50 overflow-hidden">
          <div className="relative bg-[#F1F2F6] p-5 rounded-lg w-full max-w-full sm:max-w-md md:max-w-lg lg:max-w-xl overflow-hidden h-100">
            <FaTimes
              title="Close"
              onClick={onClose}
              className="absolute top-2 right-2 text-[#D8354C] text-2xl"
            />
            <div className="space-y-4 sm:space-y-6">
              <Tittle
                level="h1"
                size="text-2xl sm:text-3xl"
                color="text-[#D8354C]"
                align="text-center"
              >
                Current Auctions
              </Tittle>

              <div className={`flex ${limitedAuctions.length === 1 ? 'justify-center' : 'justify-between'} space-x-4`}>
                {limitedAuctions.map((auction, index) => (
                  <div key={index} className="bg-white flex flex-col items-center p-4 border rounded-2xl shadow-sm shadow-gray-900 w-1/2">
                    <Icons src="/assets/icons/Trust.svg" divSize='60%' />
                    <div className="flex-1">
                      <Tittle
                        level="h2"
                        size="text-lg sm:text-xl"
                        color="text-black"
                        align="text-center"
                      >
                        {auction.nombre}
                      </Tittle>
                      <Text
                        size="text-sm sm:text-base"
                        align="text-center"
                        weight="font-light"
                        className="h-12 overflow-hidden overflow-ellipsis"
                      >
                        {truncateText(auction.descripcion, 60)}
                      </Text>
                    </div>
                    <CustomButton
                      title="Start now"
                      onClick={handleStarNowClick}
                      className="text-white bg-[#D8354C] w-full sm:w-auto"
                    />
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      )}
      {isLoginModalOpen && (
        <div className="fixed inset-0 z-60">
          <LoginModal
            isOpen={isLoginModalOpen}
            onClose={() => setLoginModalOpen(false)}
            onLoginSuccess={handleLoginSuccess} // Maneja el inicio de sesión exitoso
          />
        </div>
      )}
    </>
  );
};

DesplegarSubasta.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  auctions: PropTypes.array.isRequired,
};

export default DesplegarSubasta;
