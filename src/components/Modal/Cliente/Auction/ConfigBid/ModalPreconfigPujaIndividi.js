import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import CustomButton from "../../../../Inviduals/CustomButton";
import Input from "../../../../Inviduals/Input";
import Tittle from "../../../../Inviduals/Tittle";
import SubastaController from "src/controllers/SubastaController";
import SubastaService from "src/aplication/services/SubastaService";
import ModalAlertsConfir from "../../../ModalAlertsConfir";
import ModalAlertsError from "../../../ModalAlertsError";

const subastaService = new SubastaService();
const subastaController = new SubastaController(subastaService);

const ModalPreconfigPuja = ({
  isOpen,
  onClose,
  auction,
  username,
  fetchAuctionDetails,
}) => {
  const initialMuestraData = {
    maxLimit: "",
    manual: "",
  };

  const [muestraData, setMuestraData] = useState(initialMuestraData);
  const [errors, setErrors] = useState({});
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  const title =
    auction.bidUsers && auction.bidUsers.length > 0
      ? "Update your bid"
      : "Preset Bidding";

  const handleChange = (e) => {
    const { name, value } = e.target;
    setMuestraData({
      ...muestraData,
      [name]: value,
    });
  };

  const fetchBidUser = async () => {
    try {
      const matchingSubscription = auction.suscriptions.find(
        (sub) => sub.auctionId === auction.id
      );

      if (matchingSubscription) {
        const matchingBidUser = auction.bidUsers.find(
          (bidUser) => bidUser.suscribedId === matchingSubscription.id
        );
        setMuestraData({
          maxLimit: matchingBidUser.maxLimit,
          manual: matchingBidUser.manual,
        });
      }
    } catch (err) {
      console.error("Error fetching bid user data", err);
    }
  };

  const handleClose = () => {
    setConfirModalOpen(false);
    onClose();
  };

  const validateForm = () => {
    const newErrors = {};
    if (!muestraData.maxLimit) newErrors.maxLimit = "The maximum limit is required.";
    if (muestraData.manual === "" || muestraData.manual === null) newErrors.manual = "You must select the bidding mode.";
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };

  const handleSave = async (e) => {
    e.preventDefault();
    if (validateForm()) {
      try {
        const matchingSubscription = auction.suscriptions.find(
          (sub) => sub.auctionId === auction.id
        );

        if (matchingSubscription) {
          if (title === "Update your bid") {
            const matchingBidUser = auction.bidUsers.find(
              (bidUser) => bidUser.suscribedId === matchingSubscription.id
            );
            await subastaController.handleUpdateConfigBid(
              matchingBidUser.id,
              muestraData
            );
            handleOpenConfiModal("Your settings for this bid have been updated");
          } else {
            const dataSave = {
              ...muestraData,
              suscribedId: auction.suscriptions[0].id,
            };

            await subastaController.handleCreateConfigBid(dataSave);
            handleOpenConfiModal("The new bidding configuration was saved");
          }
        }
        fetchAuctionDetails();
      } catch (err) {
        console.error("Error updating configuration:", err);
        handleErrorConfiModal("There was an error saving the configuration");
      }
    }
  };

  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  const handleChangeBool = (e) => {
    const { name, value } = e.target;

    const updatedValue =
      value === ""
        ? null
        : value === "true"
        ? true
        : value === "false"
        ? false
        : value;

    setMuestraData({
      ...muestraData,
      [name]: updatedValue,
    });
  };

  useEffect(() => {
    if (isOpen) {
      fetchBidUser();
    }
  }, [username, auction]);

  return isOpen ? (
    <div className="fixed inset-0 bg-gray-50 bg-opacity-75 flex justify-center items-center z-50">
      <div className="bg-black p-5 rounded-lg h-auto w-full max-w-lg relative flex flex-col justify-center items-center">
        <button
          onClick={handleClose}
          className="absolute top-2 right-2 text-gray-500 hover:text-gray-700"
        >
          Close
        </button>
        <Tittle
          level="h1"
          size="text-3xl"
          color="text-custom-red"
          align="text-center"
          className="mb-5"
        >
          {title}
        </Tittle>
        <form className="w-full" onSubmit={handleSave}>
          <div className="mb-4">
            <Input
              title="Maximum Limit"
              name="maxLimit"
              placeholder="Maximum Limit"
              value={muestraData.maxLimit}
              onChange={handleChange}
              type="number"
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
            {errors.maxLimit && (
              <p className="text-red-500 text-sm mt-1">{errors.maxLimit}</p>
            )}
          </div>
          <div className="mb-4">
            <label className="block text-white mb-1">Bidding Mode</label>
            <select
              name="manual"
              value={muestraData.manual}
              onChange={handleChangeBool}
              className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
            >
              <option value="">Select your Bid</option>
              <option value="true">Automatic</option>
              <option value="false">Manual</option>
            </select>
            {errors.manual && (
              <p className="text-red-500 text-sm mt-1">{errors.manual}</p>
            )}
          </div>
          <div className="flex justify-center items-center">
            <CustomButton
              type="submit"
              title="Save"
              className="bg-[#D8354C] text-lg w-96"
            />
          </div>
        </form>
      </div>
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={handleClose}
        customMessage={confirMessage}
      />
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
    </div>
  ) : null;
};

ModalPreconfigPuja.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  auction: PropTypes.object.isRequired,
  username: PropTypes.string.isRequired,
  fetchAuctionDetails: PropTypes.func.isRequired,
};

export default ModalPreconfigPuja;
