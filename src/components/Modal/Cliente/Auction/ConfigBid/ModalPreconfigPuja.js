import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import CustomButton from "../../../../Inviduals/CustomButton";
import Input from "../../../../Inviduals/Input";
import Tittle from "../../../../Inviduals/Tittle";
import SubastaController from "src/controllers/SubastaController";
import SubastaService from "src/aplication/services/SubastaService";
import ModalAlertsConfir from "../../../ModalAlertsConfir";
import ModalAlertsError from "../../../ModalAlertsError";

const subastaService = new SubastaService();
const subastaController = new SubastaController(subastaService);

const ModalPreconfigPuja = ({
  isOpen,
  onClose,
  auction,
  username,
  fetchAuctionDetails,
}) => {
  const initialMuestraData = {
    suscribedId: "",
    maxLimit: "",
    manual: "",
  };

  const [configBid, setConfigBid] = useState([]);
  const [muestraData, setMuestraData] = useState(initialMuestraData);
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [errors, setErrors] = useState({});
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  const title =
    auction.bidUsers && auction.bidUsers.length > 0
      ? "Update your bid"
      : "Preset Bidding";

  const handleChange = (e) => {
    const { name, value } = e.target;
    setMuestraData({
      ...muestraData,
      [name]: value,
    });
  };

  const handleChangeBool = (e) => {
    const { name, value } = e.target;

    const updatedValue =
      value === ""
        ? null
        : value === "true"
        ? true
        : value === "false"
        ? false
        : value;

    setMuestraData({
      ...muestraData,
      [name]: updatedValue,
    });
  };
  const handleProductSelect = (e) => {
    const selectedAuction = configBid.find((sub) => sub.id === e.target.value);
    if (selectedAuction) {
      const productAlreadySelected = selectedProducts.some(
        (product) => product.id === selectedAuction.auction.product.id
      );

      if (!productAlreadySelected) {
        setMuestraData((prevData) => ({
          ...prevData,
          suscribedId: selectedAuction.id,
        }));
        setSelectedProducts((prevProducts) => [
          ...prevProducts,
          selectedAuction.auction.product,
        ]);
      }
    }
  };

  const handleRemoveProduct = (productId) => {
    setSelectedProducts((prevProducts) =>
      prevProducts.filter((product) => product.id !== productId)
    );
  };

  const fetchBidUser = async () => {
    try {
      const response = await subastaController.handleGetConfigBid(username);
      const filteredData = response.suscriptions.filter(
        (sub) => sub.muestraId === auction.muestraId
      );
      console.log("que me trae esto", filteredData);
      

      setConfigBid(filteredData);

      if (auction.bidUsers && auction.bidUsers.length > 0) {
        const bidUser = auction.bidUsers[0];
        setMuestraData({
          suscribedId: bidUser.suscribedId,
          maxLimit: bidUser.maxLimit,
          manual: bidUser.manual,
        });
        setSelectedProducts(filteredData.map((sub) => sub.auction.product));
      }
    } catch (err) {
      console.error("Error fetching bid user data", err);
    }
  };

  const handleClose = () => {
    setConfirModalOpen(false);
    onClose();
  };

  const validateForm = () => {
    const newErrors = {};
    if (selectedProducts.length === 0)
      newErrors.suscribedId = "Selecciona un producto.";
    if (!muestraData.maxLimit)
      newErrors.maxLimit = "El límite máximo es requerido.";
    if (muestraData.manual !== true && muestraData.manual !== false) {
      newErrors.manual = "Selecciona un modo de puja.";
    }
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };

  const handleSave = async (e) => {
    e.preventDefault();
    if (validateForm()) {
      try {
        for (let product of selectedProducts) {
          const matchedConfigBid = configBid.find(
            (sub) =>
              sub.auction.product && sub.auction.product.id === product.id
          );

          if (!matchedConfigBid) {
            console.error(
              "No se encontró una suscripción para el producto:",
              product
            );
            continue; // O puedes lanzar un error si quieres detener el proceso
          }

          const dataToSave = {
            ...muestraData,
            suscribedId: matchedConfigBid.id,
          };

          // Verificamos si ya existe un usuario de puja para este producto
          if (auction.bidUsers && auction.bidUsers.length > 0) {
            const existingBidUser = auction.bidUsers.find(
              (bidUser) => bidUser.suscribedId === matchedConfigBid.id
            );

            if (existingBidUser) {
              // Si se encuentra un usuario de puja existente, actualizamos la configuración

              await subastaController.handleUpdateConfigBid(
                existingBidUser.id,
                dataToSave
              );

              
              handleOpenConfiModal("Configuración exitosa");
            } else {
              // Si no hay usuario de puja para esa suscripción, creamos uno nuevo
              await subastaController.handleCreateConfigBid(dataToSave);
            }
          } else {
            // Si no hay usuarios de puja en absoluto, simplemente creamos uno nuevo
            const result = await subastaController.handleCreateConfigBid(dataToSave);
            if (result.status === 400) {
                handleErrorConfiModal(result.data.message);
              } else {
                handleOpenConfiModal(result.message);
              }
          }
        }
        fetchAuctionDetails();
      } catch (err) {
        console.error("Error guardando la configuración:", err);
      }
    }
  };
  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  useEffect(() => {
    fetchBidUser();
  }, [username]);

  useEffect(() => {
    if (auction.bidUsers && auction.bidUsers.length > 0) {
      const bidUserProducts = auction.bidUsers
        .map((bidUser) => {
          const matchedSubscription = configBid.find(
            (sub) => sub.id === bidUser.suscribedId
          );

          return matchedSubscription?.auction.product;
        })
        .filter((product) => product); // Filter out undefined products

      setSelectedProducts(bidUserProducts);
    } else {
      setSelectedProducts([]); // Reset if not updating
    }
  }, [auction, configBid]); // Ensure this effect only runs when auction or configBid changes

  return isOpen ? (
    <div className="fixed inset-0 bg-gray-50 bg-opacity-75 flex justify-center items-center z-50">
      <div className="bg-black p-5 rounded-lg h-auto w-full max-w-lg relative flex flex-col justify-center items-center">
        <button
          onClick={handleClose}
          className="absolute top-2 right-2 text-gray-500 hover:text-gray-700"
        >
          Close
        </button>
        <Tittle
          level="h1"
          size="text-3xl"
          color="text-custom-red"
          align="text-center"
          className="mb-5"
        >
          {title}
        </Tittle>
        <form className="w-full" onSubmit={handleSave}>
          <div className="mb-4">
            <select
              name="auctionId"
              value={muestraData.suscribedId}
              onChange={handleProductSelect}
              className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
            >
              <option value="">Select a product</option>
              {configBid.map((sub) => (
                <option
                  key={sub.id}
                  value={sub.id}
                  className="text-black"
                  disabled={selectedProducts.some(
                    (product) => product.id === sub.auction.product.id
                  )}
                >
                  {sub.auction.product.nombre}
                </option>
              ))}
            </select>
            {errors.suscribedId && (
              <p className="text-red-500 text-sm mt-1">{errors.suscribedId}</p>
            )}
          </div>
          <div className="mb-4">
            {selectedProducts.map((product) => (
              <div key={product.id} className="flex items-center mb-2">
                <span className="bg-gray-200 text-black px-2 py-1 rounded mr-2">
                  {product.nombre}
                </span>
                <button
                  type="button"
                  onClick={() => handleRemoveProduct(product.id)}
                  className="text-red-500"
                >
                  X
                </button>
              </div>
            ))}
          </div>
          <div className="mb-4">
            <Input
              title="Maximum Limit"
              name="maxLimit"
              placeholder="Maximum Limit"
              value={muestraData.maxLimit}
              onChange={handleChange}
              type="number"
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
            {errors.maxLimit && (
              <p className="text-red-500 text-sm mt-1">{errors.maxLimit}</p>
            )}
          </div>
          <div className="mb-4">
            <label className="block text-white mb-1">Bidding Mode</label>
            <select
              name="manual"
              value={muestraData.manual} // Mantenlo así para que el valor en el select sea correcto
              onChange={handleChangeBool}
              className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
            >
              <option value="">Select your Bid</option>
              <option value="true">Automatic</option>
              <option value="false">Manual</option>
            </select>
            {errors.manual && (
              <p className="text-red-500 text-sm mt-1">{errors.manual}</p>
            )}
          </div>

          <div className="flex justify-center items-center">
            <CustomButton
              type="submit"
              title="Save"
              className="bg-[#D8354C] text-lg w-96"
            />
          </div>
        </form>
      </div>
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={handleClose}
        customMessage={confirMessage}
      />
    </div>
  ) : null;
};

ModalPreconfigPuja.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  auction: PropTypes.object.isRequired,
  username: PropTypes.string.isRequired,
  fetchAuctionDetails: PropTypes.func.isRequired,
};

export default ModalPreconfigPuja;
