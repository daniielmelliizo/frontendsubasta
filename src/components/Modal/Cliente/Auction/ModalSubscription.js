import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import CustomButton from "../../../Inviduals/CustomButton";
import Text from "../../../Inviduals/Text";
import Tittle from "../../../Inviduals/Tittle";
import SuscripcionController from "src/controllers/SuscripcionController";
import SuscripcionService from "src/aplication/services/SuscripcionService";
import ModalAlertsConfir from "../../ModalAlertsConfir";
import ModalAlertsError from "../../ModalAlertsError";

const suscripcionService = new SuscripcionService();
const suscripcionController = new SuscripcionController(suscripcionService);

const Subscripcion = ({
  isOpen,
  onClose,
  username,
  auction,
  fetchAuctionDetails,
}) => {
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  useEffect(() => {
    if (isOpen && auction) {
        console.log("que me trae auction",auction );
        
    }
  }, [isOpen, auction, username]);

  const handleSubscription = async (event) => {
    event.preventDefault(); // Evitar la recarga de la página por defecto
    try {
      const data = {
        userId: username,
        muestraId: auction.muestraId,
      };
      const result = await suscripcionController.handleCreateSuscripcion(data);
      fetchAuctionDetails();
      if (result.status === 400) {
        handleErrorConfiModal(result.data.message);
      } else {
        handleOpenConfiModal(result.message);
      }
    } catch (error) {
      alert("Error al suscribirse: " + error.message);
    }
  };

  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  const handleClose = () => {
    setConfirModalOpen(false);
    onClose();
  };

  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 bg-gray-50 bg-opacity-75 flex justify-center items-center z-50">
      <div className="bg-white p-5 rounded-lg w-1/3 relative">
        <Tittle
          level="h1"
          size="text-3xl"
          color="text-black"
          align="text-center"
          className="p-5"
        >
          Subscription
        </Tittle>
        <div className="w-full px-5">
          {" "}
          {/* Removido el <form> */}
          <div className="mb-4 text-black">
            <Text>
            Welcome, would you like to participate in the auction of{" "}
              {auction.sample.nombre}?
            </Text>
          </div>
          <div className="flex justify-center items-center">
            <CustomButton
              onClick={handleSubscription}
              title="Accept"
              className="bg-[#D8354C] w-full"
            />
          </div>
          <div className="flex justify-center items-center mt-4">
            <CustomButton
              onClick={onClose}
              title="Cancel"
              className="bg-[#D8354C] w-full"
            />
          </div>
        </div>
      </div>
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={handleClose}
        customMessage={confirMessage}
      />
    </div>
  );
};

Subscripcion.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  fetchAuctionDetails: PropTypes.func.isRequired,
};

export default Subscripcion;
