import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import CustomButton from "../../../../Inviduals/CustomButton";
import Input from "../../../../Inviduals/Input";
import Tittle from "../../../../Inviduals/Tittle";
import BidService from "src/aplication/services/BidService";
import BidController from "src/controllers/BidController";
import ModalAlertsConfir from "../../../ModalAlertsConfir";

// Inicializar el servicio y el controlador
const bidService = new BidService();
const bidController = new BidController(bidService);

const BidManual = ({ isOpen, onClose, maxBid, IdAuction, fetchAuctionDetails }) => {
  
  const [formData, setFormData] = useState({
    bidUserId: maxBid,
    auctionId: IdAuction,
    value: "",
    tipo: "manual",
  });

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);

  useEffect(() => {
    if (isOpen) {
      setFormData((prevFormData) => ({
        ...prevFormData,
        bidUserId: maxBid,
        auctionId: IdAuction
      }));
    }
  }, [isOpen, maxBid, IdAuction]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const resetForm = () => {
    setFormData({
      bidUserId: "",
      value: "",
      tipo: "manual", // Mantener el tipo como "manual" al resetear
    });
  };
  const handleBidSubmit = async () => {
    try {
      setLoading(true);
      console.log("que mando en data", formData);
      
      const response = await bidController.handlePujaManual(formData);
      handleOpenConfiModal(response.message);
      fetchAuctionDetails();

      resetForm();
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };
  const handleClose = () => {
    setConfirModalOpen(false);
    onClose();
  };

  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 bg-gray-50 bg-opacity-75 flex justify-center items-center z-50">
      <div className="bg-white p-5 rounded-lg w-1/3 relative">
        <CustomButton
          title="Close"
          onClick={onClose}
          className="bg-black absolute top-2 right-2 text-gray-500 hover:text-gray-500"
        />
        <Tittle
          level="h1"
          size="text-3xl"
          color="text-custom-red"
          align="text-center"
          className="p-5"
        >
          New Bid
        </Tittle>
        <form className="w-full px-5">
          <div className="mb-4 text-black">
            <Input
              name="value"
              value={formData.value}
              onChange={handleChange}
              placeholder="Bid Value"
              type="number"
              className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="flex justify-center items-center">
            <CustomButton
              title="Submit Bid"
              type="button" // Cambiado de 'submit' a 'button'
              className="bg-[#D8354C] w-full"
              onClick={handleBidSubmit} // Manejo de la función en onClick
              disabled={loading}
            />
          </div>
        </form>
        {error && <p className="text-red-500 mt-4">{error}</p>}
      </div>
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={handleClose}
        customMessage={confirMessage}
      />
    </div>
  );
};

BidManual.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  fetchAuctionDetails: PropTypes.func.isRequired,
};

export default BidManual;
