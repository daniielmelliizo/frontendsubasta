import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import CustomButton from "../../../Inviduals/CustomButton";
import Input from "../../../Inviduals/Input";
import Tittle from "../../../Inviduals/Tittle";
import UserService from "src/aplication/services/userService";
import UserController from "src/controllers/userController";
import ModalAlertsConfir from "../../ModalAlertsConfir";
import ModalAlertsError from "../../ModalAlertsError";

const userService = new UserService();
const userController = new UserController(userService);

const EditUserModal = ({ isOpen, onClose, userData, onUpdateUser }) => {
  const [formState, setFormState] = useState({ ...userData });
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  useEffect(() => {
    setFormState({ ...userData });
  }, [userData]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    const updatedValue = name === "status" ? value === "true" : value;
    setFormState((prevState) => ({
      ...prevState,
      [name]: updatedValue,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    setError("");

    try {
      const data = {
        nombre: formState.nombre,
        apellido: formState.apellido,
        email: formState.email,
        phone: formState.phone,
        pais: formState.pais,
        typeIdentificacion: formState.typeIdentificacion,
        identificacion: formState.identificacion,
        cargo: formState.cargo,
        direccion: formState.direccion,
        codigoPostal: formState.codigoPostal,
      };
      const response = await userController.handleUpdateUsers(
        formState.id,
        data
      );
      onUpdateUser(formState);
      if (response.status === 400) {
        handleErrorConfiModal(response.data.message);
        return null;
      } else {
        handleOpenConfiModal(response.message);
      }
      return response;
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };
  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };
  const handleClose = () => {
    setConfirModalOpen(false);
    onClose();
  };

  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50">
      <div className="bg-white p-4 rounded shadow-lg max-w-md w-full max-h-screen overflow-y-auto">
        <Tittle title="Editar Usuario">Edit user</Tittle>
        {error && <p className="text-red-500">{error}</p>}
        <form className="w-full" onSubmit={handleSubmit}>
          <div className="mb-4">
            <Input
              title="Name"
              name="nombre"
              value={formState.nombre}
              onChange={handleChange}
              placeholder="Name"
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="mb-4">
            <Input
              title="Last name"
              name="apellido"
              value={formState.apellido}
              onChange={handleChange}
              placeholder="Last name"
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="mb-4">
            <Input
              title="E-mail"
              name="email"
              value={formState.email}
              onChange={handleChange}
              placeholder="E-mail"
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="mb-4">
            <Input
              title="Phone"
              name="phone"
              value={formState.phone}
              onChange={handleChange}
              placeholder="Phone"
              type="number"
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="mb-4">
            <Input
              title="Country"
              name="pais"
              value={formState.pais}
              onChange={handleChange}
              placeholder="Country"
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="mb-4">
            <select
              name="type Identificacion"
              value={formState.typeIdentificacion}
              onChange={handleChange}
              className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
            >
              <option value="">Type of identification</option>
              <option value="Cedula Ciudadana">Citizen Identification Card</option>
              <option value="Cedula Extranjera">Foreign Cedula</option>
              <option value="Otro">Otro</option>
            </select>
          </div>
          <div className="mb-4">
            <Input
              title="Identificación"
              name="identificacion"
              value={formState.identificacion}
              onChange={handleChange}
              placeholder="Identificación"
              type="number"
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="mb-4">
            <select
              name="status"
              value={formState.status}
              onChange={handleChange}
              className="block w-full px-4 py-2 mt-2 text-black bg-white border rounded-md focus:border-blue-500 focus:outline-none focus:ring"
            >
              <option value="">Foreign Cedula</option>
              <option value="true">Validate</option>
              <option value="false">Invalidate</option>
            </select>
          </div>
          <div className="mb-4">
            <select
              name="cargo"
              value={formState.cargo}
              onChange={handleChange}
              className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
            >
              <option value="">Select the position</option>
              <option value="Importador">Importer</option>
              <option value="Tostador">Roaster</option>
            </select>
          </div>
          <div className="mb-4">
            <Input
              title="Address"
              name="direccion"
              value={formState.direccion}
              placeholder="Address"
              onChange={handleChange}
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="mb-4">
            <Input
              title="Zip Code"
              name="codigoPostal"
              value={formState.codigoPostal}
              onChange={handleChange}
              placeholder="Zip Code"
              type="number"
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="flex justify-end space-x-4 mt-4">
            <CustomButton
              title="Cancel"
              onClick={onClose}
              disabled={loading}
              className="bg-[#2c2c2c] text-lg w-50"
            />
            <CustomButton
              title="Save"
              type="submit"
              disabled={loading}
              className="bg-[#D8354C] text-lg w-50"
            />
          </div>
        </form>
      </div>
      {isErrorModalOpen && (
        <ModalAlertsError
          isOpen={isErrorModalOpen}
          onClose={() => setErrorModalOpen(false)}
          customMessage={errorMessage}
        />
      )}
      {isConfirModalOpen && (
        <ModalAlertsConfir
          isOpen={isConfirModalOpen}
          onClose={handleClose}
          customMessage={confirMessage}
        />
      )}
    </div>
  );
};

EditUserModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  userData: PropTypes.object.isRequired,
  onUpdateUser: PropTypes.func.isRequired,
};

export default EditUserModal;
