import React from "react";
import AuctionRow from "./AuctionRow";


const AuctionTable = ({ auctionDetails }) => {

    return (
        <div className="flex-grow overflow-auto">
            {Object.entries(auctionDetails).map(([muestraTitle, muestraData], index) => (
                <div key={index} className="mb-8">
                    {Object.entries(muestraData).map(([subTitle, auctions]) => (
                        <div key={subTitle} className="mb-8">
                                <>
                                    {/* Solo muestra los encabezados cuando la tabla es visible */}
                                    <h1 className="text-2xl font-bold mb-4">{muestraTitle.toUpperCase()}</h1>
                                    <div className="bg-[#2F3542] rounded-lg p-4">
                                        <h2 className="text-xl font-bold mb-4">{subTitle.toUpperCase()}</h2>
                                        <table className="w-full table-auto mb-2">
                                            <thead>
                                                <tr>
                                                    <th className="w-1/11 px-2 py-1 text-center text-white">N#</th>
                                                    <th className="w-1/11 px-2 py-1 text-center text-white">Lot</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Product</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Process</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Score</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Weight</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Bid</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Increment</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Status</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">My last Bid</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Total</th>
                                                    <th className="w-1/12 px-2 py-1 text-center text-white">Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {auctions.map((auction, subIndex) => (
                                                    <AuctionRow
                                                        key={subIndex}
                                                        auction={auction}
                                                    />
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </>
                        </div>
                    ))}
                </div>
            ))}
        </div>
    );
};

export default AuctionTable;
