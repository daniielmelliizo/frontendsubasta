import React from "react";

const AuctionRow = ({ auction }) => (
  <tr className=  "">
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.bidUsers[0]?.participantNum || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.lote || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.nombre || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.process || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.score || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.peso || "N/A"} {auction.product?.unidadPeso || ""}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.precioFinal
        ? Number(auction.product.precioFinal).toLocaleString("de-DE", {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })
        : "N/A"}{" "}
      USD
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.increment || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.estado || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.highestBid || "N/A"}
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.precioInicial
        ? Number(auction.product?.precioFinal).toLocaleString("de-DE", {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })
        : "N/A"}{" "}
      USD
    </td>
    <td className="px-1 py-0 text-center text-white text-sm">
      {auction.product?.precioInicial
        ? Number(auction.product?.precioFinal * auction.product?.peso).toLocaleString("de-DE", {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })
        : "N/A"}{" "}
      USD
    </td>
  </tr>
);

export default AuctionRow;
