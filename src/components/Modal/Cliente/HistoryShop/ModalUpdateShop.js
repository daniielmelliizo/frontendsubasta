import React from 'react';
import CustomButton from '../../../Inviduals/CustomButton';

const ModalUpdateShop = ({ isOpen, onClose, onConfirm, message }) => {
  if (!isOpen) {
    return null;
  }

  return (
    <div className="fixed inset-0 z-50 overflow-auto bg-smoke-800 flex">
      <div className="relative p-8 bg-white boreder border-[#D8354C] w-full max-w-md m-auto flex-col flex rounded-lg items-center justify-center ">
        <h2 className="text-xl font-bold mb-4 text-black">Alert</h2>
        <p className="mb-6 text-black ">{message}</p>
        <div className="flex items-center justify-center space-x-4">
          <CustomButton title="Cancel" onClick={onClose} className="px-4 py-2 text-sm sm:text-base md:text-lg bg-[#D8354C] rounded-md" />
          <CustomButton title="Accept" onClick={onConfirm} className="px-4 py-2 text-sm sm:text-base md:text-lg bg-[#019267] rounded-md" />
        </div>
      </div>
    </div>
  );
};

export default ModalUpdateShop;
