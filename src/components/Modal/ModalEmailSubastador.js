import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CustomButton from '../Inviduals/CustomButton';
import Image from '../Inviduals/Image';
import Tittle from '../Inviduals/Tittle';
import Text from '../Inviduals/Text';
import UserService from 'src/aplication/services/userService';
import UserController from 'src/controllers/userController';

const CodigoSubastadorCorreoModal = ({ isOpen, onClose, handleCompleteValidation, registeredEmail }) => {
    const [codigo, setCodigo] = useState(Array(6).fill(''));

    const userService = new UserService();
    const userController = new UserController(userService);

    const handleInputChange = (e, index) => {
        const { value } = e.target;
        if (/^\d*$/.test(value)) {
            const newCodigo = [...codigo];
            newCodigo[index] = value;
            setCodigo(newCodigo);

            if (value && index < 5) {
                document.getElementById(`codigo-${index + 1}`).focus();
            }
        }
    };

    const handleKeyDown = (e, index) => {
        if (e.key === 'Backspace' && !codigo[index] && index > 0) {
            document.getElementById(`codigo-${index - 1}`).focus();
        }
    };

    const handleSubmit = async () => {
        const fullCodigo = codigo.join('');
        if (fullCodigo.length === 6) {
            try {
                const form = {
                    email: registeredEmail,
                    verificationCode: fullCodigo
                }
                const data = await userController.handleValidationComplet(form);
                if (data) {
                    alert('Código válido');
                    onClose();
                    handleCompleteValidation();
                } else {
                    alert('Código inválido');
                }
            } catch (error) {
                console.log('Error de red: ' + error);
            }
        } else {
            alert('El código debe tener 6 dígitos');
        }
    };

    return isOpen ? (
        <div className="fixed inset-0 bg-gray-50 bg-opacity-75 flex justify-center items-center z-50">
            <div className="bg-black p-5 rounded-lg w-1/3 ">
                <CustomButton title="Close" onClick={onClose} className="text-white bg-black mb-4" />
                <div className='flex justify-center items-center' style={{ width: '136px', height: '38px' }}>
                    <Image src='/assets/Images/logo.svg' alt='Logo'/>
                </div>
                <Tittle level="h1" size="text-3xl" color="text-white" align="text-center" className="mb-4">
                    Enter Validation Code
                </Tittle>
                <Text size="text-sm" color="text-white" align="text-center" className="mb-4">
                    Please enter the 6-digit code you received.
                </Text>
                <div className="flex justify-center items-center mb-4 space-x-2">
                    {codigo.map((char, index) => (
                        <input
                            key={index}
                            type="text"
                            id={`codigo-${index}`}
                            value={char}
                            onChange={(e) => handleInputChange(e, index)}
                            onKeyDown={(e) => handleKeyDown(e, index)}
                            maxLength={1}
                            className="bg-black text-center text-white w-10 px-4 py-2 border border-white rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                        />
                    ))}
                </div>
                <div className="flex justify-center">
                    <CustomButton title="Validate Code" onClick={handleSubmit} className="text-white bg-[#D8354C]" />
                </div>
            </div>
        </div>
    ) : null;
};

CodigoSubastadorCorreoModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    handleCompleteValidation: PropTypes.func.isRequired,
    registeredEmail: PropTypes.string.isRequired,
};

export default CodigoSubastadorCorreoModal;
