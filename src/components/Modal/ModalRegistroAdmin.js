import React, { useState } from "react";
import PropTypes from "prop-types";
import CustomButton from "../Inviduals/CustomButton";
import Input from "../Inviduals/Input";
import Tittle from "../Inviduals/Tittle";
import UserService from "src/aplication/services/userService";
import UserController from "src/controllers/userController";
const userService = new UserService();
const userController = new UserController(userService);

const RegisterAdminModal = ({ isOpen, onClose }) => {
  const [formData, setFormData] = useState({
    email: "",
    password: "",
    nombre: "",
    apellido: "",
    phone: "",
    pais: "",
    providencia: "",
    region: "",
    ubicacion: "",
    typeIdentificacion: "",
    identificacion: "",
    budget: "",
    budgetStatus: "",
    rol: "",
  });

  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [screen, setScreen] = useState(1);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const validateRequiredFields = (fields) => {
    for (let field of fields) {
      if (!formData[field]) {
        return false;
      }
    }
    return true;
  };

  const handleNext = (fields) => {
    if (validateRequiredFields(fields)) {
      setScreen(screen + 1);
    } else {
      alert("Faltan campos por llenar");
    }
  };

  const handleRegistro = async (event) => {
    event.preventDefault();

    if (!validateRequiredFields(Object.keys(formData))) {
      alert("Faltan campos por llenar");
      return;
    }

    try {
      setLoading(true);
      const response = await userController.handleCreateUser(formData);
      setLoading(false);
      alert(response.message);
      onClose();
    } catch (error) {
      setLoading(false);
    }
  };

  return isOpen ? (
    <>
      <div className="fixed inset-0 bg-gray-50 bg-opacity-75 flex justify-center items-center z-50">
        <div className="bg-black hidden md:flex justify-center h-screen-50 w-screen-50 rounded-lg">
          <div className="bg-black p-5 rounded-lg h-screen-50 w-96 relative flex flex-col justify-center items-center">
            <CustomButton
              title="Close"
              onClick={onClose}
              className="bg-black absolute top-2 left-2 text-gray-500 hover:text-gray-500"
            />
            <Tittle
              level="h1"
              size="text-3x1"
              color="text-custom-red"
              align="text-center"
              className="p-5"
            >
              Register
            </Tittle>
            {errorMessage && (
              <div className="mb-4 text-red-500">{errorMessage}</div>
            )}
            <form className="w-full px-5 " onSubmit={handleRegistro}>
              {screen === 1 && (
                <>
                  <div className="mb-4 text-black">
                    <Input
                      type="email"
                      placeholder="E-mail"
                      value={formData.email}
                      name="email"
                      onChange={handleChange}
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                    />
                  </div>
                  <div className="mb-4 text-black">
                    <Input
                      type="password"
                      placeholder="Password"
                      value={formData.password}
                      name="password"
                      onChange={handleChange}
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                    />
                  </div>
                  <div className="mb-4 text-black">
                    <Input
                      type="text"
                      placeholder="Name"
                      value={formData.nombre}
                      name="nombre"
                      onChange={handleChange}
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                    />
                  </div>
                  <div className="mb-4 text-black">
                    <Input
                      type="text"
                      placeholder="Last name"
                      value={formData.apellido}
                      name="apellido"
                      onChange={handleChange}
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                    />
                  </div>
                  <CustomButton
                    title="Next"
                    onClick={() =>
                      handleNext(["email", "password", "nombre", "apellido"])
                    }
                    className="bg-[#D8354C] w-full"
                  />
                </>
              )}
              {screen === 2 && (
                <>
                  <div className="mb-4 text-black">
                    <Input
                      type="text"
                      placeholder="Phone"
                      value={formData.phone}
                      name="phone"
                      onChange={handleChange}
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                    />
                  </div>
                  <div className="mb-4 text-black">
                    <Input
                      type="text"
                      placeholder="Country"
                      value={formData.pais}
                      name="pais"
                      onChange={handleChange}
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                    />
                  </div>
                  <div className="mb-4 text-black">
                    <Input
                      type="text"
                      placeholder="Providence"
                      value={formData.providencia}
                      name="providencia"
                      onChange={handleChange}
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                    />
                  </div>
                  <div className="mb-4 text-black">
                    <Input
                      type="text"
                      placeholder="Region"
                      value={formData.region}
                      name="region"
                      onChange={handleChange}
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                    />
                  </div>
                  <CustomButton
                    title="Next"
                    onClick={() =>
                      handleNext(["phone", "pais", "providencia", "region"])
                    }
                    className="bg-[#D8354C] w-full"
                  />
                </>
              )}
              {screen === 3 && (
                <>
                  <div className="mb-4 text-black">
                    <Input
                      type="text"
                      placeholder="Ubication"
                      value={formData.ubicacion}
                      name="ubicacion"
                      onChange={handleChange}
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                    />
                  </div>
                  <div className="mb-4 text-black">
                    <Input
                      type="text"
                      placeholder="Type of Identification"
                      value={formData.typeIdentificacion}
                      name="typeIdentificacion"
                      onChange={handleChange}
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                    />
                  </div>
                  <div className="mb-4 text-black">
                    <Input
                      type="text"
                      placeholder="Identification"
                      value={formData.identificacion}
                      name="identificacion"
                      onChange={handleChange}
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                    />
                  </div>
                  <div className="mb-4 text-black">
                    <Input
                      type="number"
                      placeholder="Budget"
                      value={formData.budget}
                      name="budget"
                      onChange={handleChange}
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                    />
                  </div>
                  <div className="mb-4 text-black">
                    <Input
                      type="text"
                      placeholder="Budget Statement"
                      value={formData.budgetStatus}
                      name="budgetStatus"
                      onChange={handleChange}
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                    />
                  </div>
                  <div className="mb-4 text-black">
                    <Input
                      type="text"
                      placeholder="Role"
                      value={formData.rol}
                      name="rol"
                      onChange={handleChange}
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                    />
                  </div>
                  <CustomButton
                    title="Register"
                    type="submit"
                    className="bg-[#D8354C] w-full"
                    disabled={loading}
                  />
                </>
              )}
            </form>
          </div>
        </div>
      </div>
    </>
  ) : null;
};

RegisterAdminModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default RegisterAdminModal;
