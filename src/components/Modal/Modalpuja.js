import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CustomButton from '../Inviduals/CustomButton';
import Input from '../Inviduals/Input';
import Tittle from '../Inviduals/Tittle';
import BidService from 'src/aplication/services/BidService';
import BidController from 'src/controllers/BidController';

// Inicializar el servicio y el controlador
const bidService = new BidService();
const bidController = new BidController(bidService);

const BidModal = ({ isOpen, onClose }) => {
    const [formData, setFormData] = useState({
        userId: "",
        bidValue: ""
    });

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleBidSubmit = async (event) => {
        event.preventDefault();

        if (!formData.userId || !formData.bidValue) {
            alert("All fields are required.");
            return;
        }

        try {
            setLoading(true);
            const response = await bidController.submitBid(formData);
            setLoading(false);

            if (response && response.message) {
                if (response.message === "OK") {
                    alert("Bid successfully registered");
                    onClose();
                } else {
                    alert(`Failed to register the bid: ${response.message}`);
                }
            } else {
                alert("Unexpected server response.");
            }
        } catch (error) {
            setLoading(false);
            setError(error.message);
            alert("Error registering the bid. Please try again.");
        }
    };

    if (!isOpen) return null;

    return (
        <div className="fixed inset-0 bg-gray-50 bg-opacity-75 flex justify-center items-center z-50">
            <div className="bg-white p-5 rounded-lg w-1/3 relative">
                <CustomButton title='Close' onClick={onClose} className='bg-black absolute top-2 right-2 text-gray-500 hover:text-gray-500' />
                <Tittle level="h1" size="text-3xl" color="text-custom-red" align="text-center" className="p-5">
                    New Bid
                </Tittle>
                <form className="w-full px-5" onSubmit={handleBidSubmit}>
                    <div className="mb-4 text-black">
                        <Input
                            type="text"
                            placeholder="User ID"
                            value={formData.userId}
                            name="userId"
                            onChange={handleChange}
                            className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                        />
                    </div>
                    <div className="mb-4 text-black">
                        <Input
                            type="number"
                            placeholder="Bid Value"
                            value={formData.bidValue}
                            name="bidValue"
                            onChange={handleChange}
                            className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                        />
                    </div>
                    <div className="flex justify-center items-center">
                        <CustomButton type="submit" title="Submit Bid" className="bg-[#D8354C] w-full" disabled={loading} />
                    </div>
                </form>
                {error && <p className="text-red-500 mt-4">{error}</p>}
            </div>
        </div>
    );
};

BidModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
};

export default BidModal;
