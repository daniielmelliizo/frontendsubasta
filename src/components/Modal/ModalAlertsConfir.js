import React from "react";
import PropTypes from "prop-types";
import CustomButton from "../Inviduals/CustomButton";
import { FaCheckCircle } from "react-icons/fa";

const ModalAlertsConfir = ({
  isOpen,
  onClose,
  customMessage, // Mensaje personalizado opcional
}) => {
  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 z-50 flex items-center justify-center bg-smoke-800 bg-opacity-70">
      <div className="relative p-8 bg-white w-full max-w-md m-auto flex flex-col items-center rounded-lg shadow-2xl">
        <div className="mb-4 text-green-500">
          <FaCheckCircle size={60} />
        </div>
        <p className="mb-6 text-black text-center">{customMessage}</p>
        <div className="flex justify-center">
          <CustomButton
            title="Accept"
            onClick={onClose}
            className="px-4 py-2 text-sm sm:text-base md:text-lg bg-[#2F3542] text-white rounded-md"
          />
        </div>
      </div>
    </div>
  );
};

ModalAlertsConfir.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  customMessage: PropTypes.string,
};

export default ModalAlertsConfir;
