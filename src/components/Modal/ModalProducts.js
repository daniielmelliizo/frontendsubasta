import React, { useState, useEffect } from 'react';
import ProductService from 'src/aplication/services/ProductService';
import ProductController from 'src/controllers/ProductController';

const productService = new ProductService();
const productController = new ProductController(productService);

const ModalProductSelect = ({ isOpen, onClose }) => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    if (isOpen) {
      const fetchProducts = async () => {
        try {
          const productData = await productController.getAllProducts();
          setProducts(productData);
        } catch (error) {
          console.error('Error fetching products:', error);
        }
      };
      fetchProducts();
    }
  }, [isOpen]);

  if (!isOpen) {
    return null;
  }

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50">
      <div className="bg-black p-4 rounded-lg shadow-lg w-2/3 overflow-auto">
        <h2 className="text-xl mb-4">Select a Product</h2>
        <table className="min-w-full border-collapse">
          <thead>
            <tr>
              {Object.keys(products[0] || {}).map((key) => (
                <th key={key} className="border px-4 py-2">{key}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {products.map(product => (
              <tr key={product.id} className="border-b">
                <td className="border px-4 py-2">{product.id}</td>
                <td className="border px-4 py-2">{product.nombre}</td>
                <td className="border px-4 py-2">{product.pais}</td>
                <td className="border px-4 py-2">{product.region}</td>
                <td className="border px-4 py-2">{product.fincaProductor}</td>
                <td className="border px-4 py-2">{product.especie}</td>
                <td className="border px-4 py-2">{product.variedad}</td>
                <td className="border px-4 py-2">{product.sabor}</td>
                <td className="border px-4 py-2">{product.aroma}</td>
                <td className="border px-4 py-2">{product.acidez}</td>
                <td className="border px-4 py-2">{product.cuerpo}</td>
                <td className="border px-4 py-2">{product.balance}</td>
                <td className="border px-4 py-2">{product.process}</td>
                <td className="border px-4 py-2">{product.precioInicial}</td>
                <td className="border px-4 py-2">
                  <img src={product.imagen} alt={product.nombre} className="w-20 h-20 object-cover" />
                </td>
                <td className="border px-4 py-2">{product.altitude}</td>
                <td className="border px-4 py-2">{product.humidity}</td>
                <td className="border px-4 py-2">{product.temperature}</td>
                <td className="border px-4 py-2">{product.lote}</td>
                {/* <td className="border px-4 py-2">{`${product.coordinates.latitude}, ${product.coordinates.longitude}`}</td>
                <td className="border px-4 py-2">{new Date(product.createdAt).toLocaleString()}</td>
                <td className="border px-4 py-2">{new Date(product.updatedAt).toLocaleString()}</td> */}
              </tr>
            ))}
          </tbody>
        </table>
        <button className="bg-red-500 text-white px-4 py-2 rounded mt-4" onClick={onClose}>Close</button>
      </div>
    </div>
  );
};

export default ModalProductSelect;
