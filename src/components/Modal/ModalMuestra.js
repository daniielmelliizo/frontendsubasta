import React, { useState, useEffect } from 'react';
import SampleService from 'src/aplication/services/SampleService';
import SampleController from 'src/controllers/SampleController';
import Tittle from '../Inviduals/Tittle';
import Text from '../Inviduals/Text';

const sampleService = new SampleService();
const sampleController = new SampleController(sampleService);

const ModalSampleSelect = ({ isOpen, onClose }) => {
  const [samples, setSamples] = useState([]);

  useEffect(() => {
    if (isOpen) {
      const fetchSamples = async () => {
        try {
          const sampleData = await sampleController.handleGetSample();
          setSamples(sampleData);
        } catch (error) {
          console.error('Error fetching samples:', error);
        }
      };
      fetchSamples();
    }
  }, [isOpen]);

  if (!isOpen) {
    return null;
  }

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50">
      <div className="bg-black p-4 rounded-lg shadow-lg w-3/4 overflow-auto h-3/4" >
        <Tittle level="h2" size="text-3xl" color="text-custom-red" align="text-center" className="p-5">Select a Sample</Tittle>
        {samples.map(sample => (
          <div key={sample.id} className="mb-6">
            <Tittle level="h3" size="text-3xl" color="text-custom-red" align="text-center" className="p-5">Sample: {sample.nombre}</Tittle>
            <Text size="text-sm" color="text-white-700" align="" weight="font-light">id: {sample.id}</Text>
            <Text size="text-sm" color="text-white-700" align="" weight="font-light">Descripcion: {sample.descripcion}</Text>
            <Text size="text-sm" color="text-white-700" align="" weight="font-light">Valor: {sample.valor}</Text>
            <table className="min-w-full border-collapse mt-4">
              <thead>
                <tr>
                  <th className="border px-4 py-2">ID</th>
                  <th className="border px-4 py-2">Name</th>
                  <th className="border px-4 py-2">Country</th>
                  <th className="border px-4 py-2">Region</th>
                  <th className="border px-4 py-2">Producer Farm</th>
                  <th className="border px-4 py-2">Species</th>
                  <th className="border px-4 py-2">Variety</th>
                  <th className="border px-4 py-2">Taste</th>
                  <th className="border px-4 py-2">Aroma</th>
                  <th className="border px-4 py-2">Acidity</th>
                  <th className="border px-4 py-2">Body</th>
                  <th className="border px-4 py-2">Balance</th>
                  <th className="border px-4 py-2">Process</th>
                  <th className="border px-4 py-2">Initial price</th>
                  <th className="border px-4 py-2">Image</th>
                  <th className="border px-4 py-2">Altitude</th>
                  <th className="border px-4 py-2">Humidity</th>
                  <th className="border px-4 py-2">Temperature</th>
                  <th className="border px-4 py-2">Lot</th>
                  <th className="border px-4 py-2">Coordinates</th>
                  <th className="border px-4 py-2">Create</th>
                  <th className="border px-4 py-2">Opdate</th>
                </tr>
              </thead>
              <tbody>
                {sample.productsId && sample.productsId.length > 0 ? (
                  sample.productsId.map(product => (
                    <tr key={product.id} className="border-b">
                      <td className="border px-4 py-2">{product.id}</td>
                      <td className="border px-4 py-2">{product.nombre}</td>
                      <td className="border px-4 py-2">{product.pais}</td>
                      <td className="border px-4 py-2">{product.region}</td>
                      <td className="border px-4 py-2">{product.fincaProductor}</td>
                      <td className="border px-4 py-2">{product.especie}</td>
                      <td className="border px-4 py-2">{product.variedad}</td>
                      <td className="border px-4 py-2">{product.sabor}</td>
                      <td className="border px-4 py-2">{product.aroma}</td>
                      <td className="border px-4 py-2">{product.acidez}</td>
                      <td className="border px-4 py-2">{product.cuerpo}</td>
                      <td className="border px-4 py-2">{product.balance}</td>
                      <td className="border px-4 py-2">{product.proceso}</td>
                      <td className="border px-4 py-2">{product.precioInicial}</td>
                      <td className="border px-4 py-2">
                        <img src={product.imagen} alt={product.nombre} className="w-20 h-20 object-cover" />
                      </td>
                      <td className="border px-4 py-2">{product.altitud}</td>
                      <td className="border px-4 py-2">{product.humedad}</td>
                      <td className="border px-4 py-2">{product.temperatura}</td>
                      <td className="border px-4 py-2">{product.lote}</td>
                      <td className="border px-4 py-2">{`${product.coordenadas.latitude}, ${product.coordenadas.longitude}`}</td>
                      <td className="border px-4 py-2">{new Date(product.createdAt).toLocaleString()}</td>
                      <td className="border px-4 py-2">{new Date(product.updatedAt).toLocaleString()}</td>
                    </tr>
                  ))
                ) : (
                  <tr>
                    <td colSpan="22" className="text-center py-4">No products available</td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        ))}
        <button className="bg-red-500 text-white px-4 py-2 rounded mt-4" onClick={onClose}>Close</button>
      </div>
    </div>
  );
};

export default ModalSampleSelect;
