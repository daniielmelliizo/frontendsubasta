import React from 'react';
import PropTypes from 'prop-types'; // Importar PropTypes
import CustomButton from '../Inviduals/CustomButton';
import Tittle from '../Inviduals/Tittle';
import Text from '../Inviduals/Text';

const Participacion = ({ isOpen, onClose }) => {
    return isOpen ? (
        <div className="fixed inset-0 bg-gray-50 bg-opacity-75 flex justify-center items-center z-50">
            <div className="bg-black p-5 rounded-lg w-1/4 relative">
                <CustomButton title='Close' onClick={onClose} className='bg-black absolute top-2 right-2 text-gray-500 hover:text-gray-500' />
                <div className='p-5'>
                <Tittle level="h1" size="text-3xl" color="text-white" align="text-center" className="p-5">
                Thank you for your participation!
                </Tittle>
                </div>
                <div className='p-5 justify-center'>
                <Text color="text-white" align="text-center">
                    Company name
                </Text>
                <Text color="text-white" align="text-center">
                    auction end time and date
                </Text>
                </div>
                <div className="flex justify-center items-center mt-4">
                    <CustomButton type="button" title="See our auctions" className="bg-[#D8354C] w-2/4" onClick={() => { /* Aquí puedes añadir la lógica para el botón */ }} />
                </div>
            </div>
        </div>
    ) : null;
};

Participacion.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
};

export default Participacion;
