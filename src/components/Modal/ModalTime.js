import React from 'react';
import PropTypes from 'prop-types'; // Importar PropTypes
import CustomButton from '../Inviduals/CustomButton';
import Tittle from '../Inviduals/Tittle';
import Text from '../Inviduals/Text';
import Image from '../Inviduals/Image';

const ModalTime = ({ isOpen, onClose }) => {
    return isOpen ? (
        <div>
            <div className="fixed inset-0 bg-custom-fondo-time bg-opacity-75 flex justify-center items-center">

                <div className="bg-black p-5 rounded-lg w-1/4 relative">
                    <CustomButton title='Close' onClick={onClose} className='bg-black absolute top-2 right-2 text-gray-500 hover:text-gray-500' />
                    <div className='p-5'>
                        <div className="flex flex-col md:flex-row items-center space-y-4 md:space-y-0 md:space-x-4">
                            <Image src='/assets/Images/logo.svg' alt="Logo" className='w-215 h-62' />
                        </div>
                        <Tittle level="h1" size="text-3xl" color="text-white" align="text-center" className="p-5">
                            Company name
                        </Tittle>
                    </div>
                    <div className='p-5 justify-center'>
                        <Text color="text-white" align="text-center">
                            auction end time and date
                        </Text>
                    </div>
                    <div className="flex justify-center items-center mt-4">
                        <CustomButton type="button" title="See our auctions" className="bg-[#D8354C] w-2/4" onClick={() => { /* Aquí puedes añadir la lógica para el botón */ }} />
                    </div>
                </div>
            </div>
        </div>
    ) : null;
};

ModalTime.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
};

export default ModalTime;
