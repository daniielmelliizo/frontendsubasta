// src/components/AgregarSubastaModal.js
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CustomButton from '../Inviduals/CustomButton';
import InputField from '../Inviduals/Input';
import Tittle from '../Inviduals/Tittle';
import Image from '../Inviduals/Image';
import ProductService from 'src/aplication/services/ProductService';
import ProductController from 'src/controllers/ProductController';
import CategoriaService from 'src/aplication/services/CategoryService';
import CategoriaController from 'src/controllers/CategoryController';
import SubastaService from 'src/aplication/services/SubastaService';
import SubastaController from 'src/controllers/SubastaController';

const productService = new ProductService();
const productController = new ProductController(productService);

const categoriaService = new CategoriaService();
const categoriaController = new CategoriaController(categoriaService);

const subastaService = new SubastaService();
const subastaController = new SubastaController(subastaService);

const EditarSubastaModal = ({ isOpen, onClose }) => {
  const [subastaData, setSubastaData] = useState({
    productId: "",
    categoriaId: "",
    estado: "Pendiente",
    fechaInicio: "",
    fechaFin: "",
    horaSubasta: ""
  });

  const [productos, setProductos] = useState([]);
  const [categorias, setCategorias] = useState([]);
  const [notification, setNotification] = useState(null);

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const products = await productController.getAllProducts();
        setProductos(products);
      } catch (error) {
        console.error("Error fetching products:", error);
      }
    };

    const fetchCategorias = async () => {
      try {
        const categorias = await categoriaController.getAllCategorias();
        setCategorias(categorias);
      } catch (error) {
        console.error("Error fetching categories:", error);
      }
    };

    fetchProducts();
    fetchCategorias();
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setSubastaData(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  

  const closeNotification = () => {
    setNotification(null);
  };

  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 bg-gray-50 bg-opacity-75 flex justify-center items-center z-50">
      <div className="bg-black flex justify-center items-center h-auto w-full max-w-md mx-auto rounded-lg">
        <div className="relative h-auto w-full">
          <div className="absolute inset-0 bg-custom-fondo-login bg-cover bg-center rounded-lg"></div>
          <div className="absolute inset-0 bg-black opacity-50 rounded-lg"></div>
          <div className="relative p-4 flex flex-col items-start justify-center h-full space-y-4">
            <div className="absolute top-2 left-2 ml-2 p-2">
              <Image src='/assets/Images/logo.svg' width={'100%'} height={'auto'} alt='Logo'/>
            </div>
            <div className="flex flex-col items-start justify-center h-full space-y-4 p-4 w-full">
              <Tittle
                level="h1"
                size="text-3xl"
                color="text-custom-red"
                className="flex justify-center p-4 text-center"
              >
                Edit Auctions
              </Tittle>
              <form className="w-full space-y-4">
                <select
                  name="productId"
                  value={subastaData.productId}
                  onChange={handleChange}
                  className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
                >
                  <option value="">Select a product</option>
                  {productos.map(product => (
                    <option key={product.id} value={product.id} className="text-black">
                      {product.nombre}
                    </option>
                  ))}
                </select>
                <select
                  name="categoriaId"
                  value={subastaData.categoriaId}
                  onChange={handleChange}
                  className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
                >
                  <option value="">Selecciona una categoría</option>
                  {categorias.map(categoria => (
                    <option key={categoria.id} value={categoria.id} className="text-black">
                      {categoria.nombre}
                    </option>
                  ))}
                </select>
                <InputField
                  type="text"
                  placeholder="Status"
                  name="estado"
                  value={subastaData.estado}
                  onChange={handleChange}
                  className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                />
                <InputField
                  type="date"
                  placeholder="Start date"
                  name="fechaInicio"
                  value={subastaData.fechaInicio}
                  onChange={handleChange}
                  className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                />
                <InputField
                  type="date"
                  placeholder="End date"
                  name="fechaFin"
                  value={subastaData.fechaFin}
                  onChange={handleChange}
                  className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                />
                <InputField
                  type="time"
                  placeholder="Auction Time"
                  name="horaSubasta"
                  value={subastaData.horaSubasta}
                  onChange={handleChange}
                  className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                />
              </form>
              <div className="flex justify-end items-center space-x-4 mt-4">
                <CustomButton
                  title="Save"
                  onClick={handleSave}
                  className="bg-[#D8354C] text-white px-4 py-2 rounded"
                />
                <CustomButton
                  title="Close"
                  onClick={onClose}
                  className="bg-gray-500 text-white px-4 py-2 rounded"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      {notification && (
        <div className={`fixed bottom-4 right-4 p-4 rounded ${notification.type === 'success' ? 'bg-green-500' : 'bg-red-500'}`}>
          <p className="text-white">{notification.message}</p>
          <button onClick={closeNotification} className="text-white ml-4">Close</button>
        </div>
      )}
    </div>
  );
};

EditarSubastaModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default EditarSubastaModal;
