import React, { useState } from "react";
import PropTypes from "prop-types";
import CustomButton from "../Inviduals/CustomButton";
import Input from "../Inviduals/Input";
import Tittle from "../Inviduals/Tittle";
import AuctionService from "src/aplication/services/AuctionService";
import AuctionController from "src/controllers/AuctionController";

const auctionService = new AuctionService();
const auctionController = new AuctionController(auctionService);

const ModalRegisterAuction = ({ isOpen, onClose, selectedProducts, idUser, sample }) => {
  const initialFormData = {
    estado: "Inactivo",
    muestraId: sample.id, // Assuming selectedProducts has an id property
    productId: selectedProducts.id, // Assuming selectedProducts has an id property
    userId: idUser, // Use idUser directly from props
    fechaInicio: "",
    horaSubasta: "",
  };

  const [formData, setFormData] = useState(initialFormData);
  const [loading, setLoading] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleRegisterAuction = async (event) => {
    event.preventDefault();
    try {
      setLoading(true);
      const auctionData = {
        productId: formData.productId, // Use productId from formData
        muestraId: formData.muestraId, // Use muestraId from formData
        userId: formData.userId, 
        ...formData,
      };
      const auctionResponse = await auctionController.handleCreateAuction(auctionData);
      setLoading(false);
        alert(auctionResponse.message);
        setFormData(initialFormData); // Clear the form after registration
        onClose();
      
    } catch (error) {
      setLoading(false);
      alert(`Error: ${error.message || "Error al registrar la subasta."}`);
    }
  };

  return isOpen ? (
    <div className="fixed inset-0 bg-gray-50 bg-opacity-75 flex justify-center items-center z-50">
      <div className="bg-black p-5 rounded-lg h-4/5 w-3/4 relative flex flex-col justify-center items-center overflow-y-auto">
        <CustomButton
          title="Close"
          onClick={onClose}
          className="absolute top-2 right-2 text-gray-500 hover:text-gray-600"
        />
        <Tittle
          level="h1"
          size="text-3xl"
          color="text-custom-red"
          align="text-center"
          className="mb-4"
        >
          Auction Registration
        </Tittle>
        <form onSubmit={handleRegisterAuction} className="w-full">
          <div className="grid grid-cols-2 gap-4">
            {[
              { name: "bid", placeholder: "Initial price" },
              { name: "fechaInicio", placeholder: "Start date", type: "date" },
              { name: "fechaFin", placeholder: "End date", type: "date" },
              { name: "horaSubasta", placeholder: "Auction time", type: "time" },
              { name: "increment", placeholder: "Increment" },
            ].map((field) => (
              <Input
                key={field.name}
                title={field.name.charAt(0).toUpperCase() + field.name.slice(1)}
                name={field.name}
                value={formData[field.name]}
                onChange={handleChange}
                placeholder={field.placeholder}
                type={field.type || "text"}
                className="w-full text-black"
              />
            ))}
          </div>
          <CustomButton
            type="submit"
            title={loading ? "Cargando..." : "Registrar"}
            disabled={loading}
            className="mt-4 w-full"
          />
        </form>
      </div>
    </div>
  ) : null;
};

ModalRegisterAuction.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  selectedProducts: PropTypes.object.isRequired, // Ensure selectedProducts is an object
  idUser: PropTypes.string.isRequired, // Ensure idUser is a string
};

export default ModalRegisterAuction;
