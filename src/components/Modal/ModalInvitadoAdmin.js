import React, { useState } from "react";
import PropTypes from "prop-types";
import CustomButton from "../Inviduals/CustomButton";
import Input from "../Inviduals/Input";
import Tittle from "../Inviduals/Tittle";
import Image from "../Inviduals/Image";
import Text from "../Inviduals/Text";
import UserInvitedService from "src/aplication/services/userInvitedService";
import UserInvitedController from "src/controllers/UserInvitedController";

const userInvitedService = new UserInvitedService();
const userInvitedController = new UserInvitedController(userInvitedService);

const InvitadoRegisterModal = ({ isOpen, onClose }) => {
  const [formData, setFormData] = useState({
    email: "",
    password: "",
    nombre: "",
    apellido: "",
    code: "",
  });

  const [loading, setLoading] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const validateEmail = (email) => {
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regex.test(email);
  };

  const validateRequiredFields = () => {
    const requiredFields = ["nombre", "email", "password", "code"];
    for (let field of requiredFields) {
      if (!formData[field]) {
        return false;
      }
    }
    return true;
  };

  const handleRegistro = async (event) => {
    event.preventDefault();

    if (!validateRequiredFields()) {
      alert("Missing fields to be filled in");
      return;
    }

    if (!validateEmail(formData.email)) {
      alert("The email is not valid.");
      return;
    }

    try {
      setLoading(true);
      const response = await userInvitedController.handleCreateUserInvited(
        formData
      );
      setLoading(false);
      alert(response.message);
    } catch (error) {
      setLoading(false);
    }
  };

  return isOpen ? (
    <>
      <div className="fixed inset-0 bg-gray-50 bg-opacity-75 flex justify-center items-center z-50">
        <div className="bg-black hidden md:flex justify-center h-screen-50 w-screen-50 rounded-lg">
          <div className="bg-black p-5 rounded-lg h-screen-50 w-96 relative flex flex-col justify-center items-center">
            <CustomButton
              title="Close"
              onClick={onClose}
              className="bg-black absolute top-2 left-2 text-gray-500 hover:text-gray-500"
            />
            <Tittle
              level="h1"
              size="text-3x1"
              color="text-custom-red"
              align="text-center"
              className="p-5"
            >
              Register as Guest
            </Tittle>
            <form className="w-full px-5 " onSubmit={handleRegistro}>
              <div className="mb-4 text-black">
                <Input
                  type="email"
                  placeholder="E-mail"
                  value={formData.email}
                  name="email"
                  onChange={handleChange}
                  className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                />
              </div>
              <div className="mb-4 text-black">
                <Input
                  type="password"
                  placeholder="Password"
                  value={formData.password}
                  name="password"
                  onChange={handleChange}
                  className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                />
              </div>
              <div className="mb-4 text-black">
                <Input
                  type="text"
                  placeholder="Name"
                  value={formData.nombre}
                  name="nombre"
                  onChange={handleChange}
                  className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                />
              </div>
              <div className="mb-4 text-black">
                <Input
                  type="text"
                  placeholder="Last name"
                  value={formData.apellido}
                  name="apellido"
                  onChange={handleChange}
                  className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                />
              </div>
              <div className="mb-4 text-black">
                <Input
                  type="text"
                  placeholder="Code"
                  value={formData.code}
                  name="code"
                  onChange={handleChange}
                  className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                />
              </div>
              <div className="col-span-3 flex justify-center items-center">
                <CustomButton
                  type="submit"
                  title="Register"
                  className="bg-[#D8354C] w-full"
                  disabled={loading}
                />
              </div>
            </form>
          </div>
          <div className="relative h-screen-50 w-96">
            <div className="absolute inset-0 bg-custom-fondo-login bg-cover bg-center rounded-lg"></div>
            <div className="absolute inset-0 bg-black opacity-50 rounded-lg"></div>
            <div className="relative p-4 flex flex-col items-start justify-center h-full space-y-4">
              <div className="absolute top-2 left-2">
                <Image src="../assets/Images/logo.svg" width={'100%'} height={'auto'} alt="Logo" />
              </div>
              <div className="flex flex-col items-start justify-center h-full space-y-4">
                <Tittle
                  level="h1"
                  size="text-3xl"
                  color="text-custom-red"
                  align="text-left"
                >
                  Hello, friend!
                </Tittle>
                <Text
                  size="text-sm"
                  color="text-white-700"
                  align="text-left"
                  weight="font-light"
                >
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam
                </Text>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  ) : null;
};

InvitadoRegisterModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default InvitadoRegisterModal;
