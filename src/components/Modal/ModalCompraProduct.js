import React from 'react';
import PropTypes from 'prop-types';
import Tittle from '../Inviduals/Tittle';
import CustomButton from '../Inviduals/CustomButton';

const ModalCompraMuestra = ({ isOpen, onClose, onConfirm, product }) => {
  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50">
      <div className="bg-[#131821] bg-opacity-80 fixed inset-0"></div>
      <div className="bg-white rounded-lg shadow-lg z-50 w-3/4 md:w-1/2 lg:w-1/3 p-6">
        <Tittle color="text-black" size="text-xl">
         Purchase Confirmation
        </Tittle>
        <div className="mt-4">
          <p className="text-gray-700">
            Are you sure you want to purchase the product <span className="font-bold">{product.nombre}</span>?
          </p>
        </div>
        <div className="mt-6 flex justify-end space-x-4">
          <CustomButton
            title="Cancel"
            onClick={onClose}
            className="bg-[#D8354C] hover:bg-[#C72B3F] text-white"
          />
          <CustomButton
            title="Confirm"
            onClick={onConfirm}
            className="bg-[#1293A1] hover:bg-[#0E7988] text-white"
          />
        </div>
      </div>
    </div>
  );
};

ModalCompraMuestra.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  product: PropTypes.object
};

export default ModalCompraMuestra;
