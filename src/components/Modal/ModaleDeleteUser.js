import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import CustomButton from '../Inviduals/CustomButton';
import Tittle from '../Inviduals/Tittle';
import UserService from 'src/aplication/services/userService';
import UserController from 'src/controllers/userController';
import Icons from '../Inviduals/Icons';
import Text from '../Inviduals/Text';

const userService = new UserService();
const userController = new UserController(userService);

const ConfirmDeleteUserModal = ({ isOpen, onClose, userId, fetchUsers }) => {
  useEffect(() => {}, [userId]);

  const handleDelete = async () => {
    try {
      const response = await userController.handleDeleteUser(userId.id);
      if (response) {
        alert(response.message);
        fetchUsers();
        onClose();
      } else {
        alert('Error al eliminar el usuario.');
      }
    } catch (error) {
      console.error('Error deleting user:', error);
      alert('Error al eliminar el usuario.');
    }
  };

  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 flex items-center justify-center bg-[#090B11] bg-opacity-75 z-50">
      <div className="flex flex-col bg-white p-6 rounded-lg shadow-lg w-screen-10 sm:max-w-sm md:max-w-lg lg:max-w-xl xl:max-w-2xl items-center justify-center">
        <Icons src="/assets/icons/report.svg" divSize='60%' />
        <Tittle color="text-black" size="text-xl" className="text-center mb-4">
        Confirmation of Elimination

        </Tittle>
        <Text className="text-gray-700 items-center text-sm sm:text-base md:text-lg">
          Are you sure you want to delete this user {userId.nombre} {userId.apellido}?
        </Text>
        <div className="flex justify-end space-x-4 mt-6">
          <CustomButton
            title="Cancel"
            onClick={onClose}
            className="px-4 py-2 text-sm sm:text-base md:text-lg bg-[#2F3542]  rounded-md"
          />
          <CustomButton
            title="Delete"
            onClick={handleDelete}
            className="px-4 py-2 text-sm sm:text-base md:text-lg bg-[#D03449] hover:bg-[#D03449] text-white rounded-md"
          />
        </div>
      </div>
    </div>
  );
};

ConfirmDeleteUserModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  userId: PropTypes.object.isRequired,
  fetchUsers: PropTypes.func.isRequired,
};

export default ConfirmDeleteUserModal;
