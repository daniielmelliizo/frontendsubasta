import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CustomButton from '../Inviduals/CustomButton';
import Tittle from '../Inviduals/Tittle';
import Text from '../Inviduals/Text';
import RegisterAdminModal from './ModalRegistroAdmin';
import InvitadoRegisterModal from './ModalInvitadoAdmin';

const OptionsModal = ({ isOpen, onClose }) => {
  const [isRegisterAdminModalOpen, setRegisterAdminModalOpen] = useState(false);
  const [isInvitadoRegisterModalOpen, setInvitadoRegisterModalOpen] = useState(false);

  return isOpen ? (
    <div className="fixed inset-0 bg-gray-50 bg-opacity-75 flex justify-center items-center z-50">
      <div className="bg-black p-5 rounded-lg h-96 w-96 relative flex flex-col justify-center items-center">
        <button onClick={onClose} className="bg-black absolute top-2 right-2 text-gray-500 hover:text-gray-500">
          Close
        </button>
        <Tittle level="h1" size="text-3xl" color="text-custom-red" align="text-center" className="mb-5">
          Register
        </Tittle>
        <Text size="text-sm" color="text-white-700" align="text-center" weight="font-light" className="mb-8">
          Select an option to register:
        </Text>
        <div className="flex space-x-4">
          <CustomButton title="Client" onClick={() => setRegisterAdminModalOpen(true)} className="bg-gray-500" />
          <CustomButton title="Guest" onClick={() => setInvitadoRegisterModalOpen(true)} className="bg-gray-500" />
        </div>
        <RegisterAdminModal isOpen={isRegisterAdminModalOpen} onClose={() => setRegisterAdminModalOpen(false)} />
        <InvitadoRegisterModal isOpen={isInvitadoRegisterModalOpen} onClose={() => setInvitadoRegisterModalOpen(false)} />
      </div>
    </div>
  ) : null;
};

OptionsModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default OptionsModal;
