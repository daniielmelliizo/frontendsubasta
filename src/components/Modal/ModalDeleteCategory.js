import React from 'react';

const ModalConfirmDelete = ({ isOpen, onClose, onConfirm, category }) => {
  if (!isOpen) {
    return null;
  }

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50">
      <div className="bg-black p-4 rounded-lg shadow-lg w-2/4 overflow-auto h-auto">
        <h2 className="text-xl mb-4">Confirm Delete</h2>
        <p>Are you sure to delete the following category <strong>{category.nombre}</strong>?</p>
        <div className="flex justify-end mt-4">
          <button
            className="bg-gray-500 text-white px-4 py-2 rounded mr-2"
            onClick={onClose}
          >
            Cancel
          </button>
          <button
            className="bg-red-500 text-white px-4 py-2 rounded"
            onClick={onConfirm}
          >
            Delete
          </button>
        </div>
      </div>
    </div>
  );
};

export default ModalConfirmDelete;
