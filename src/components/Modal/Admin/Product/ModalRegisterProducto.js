import { FaArrowLeft, FaArrowRight, FaTimes } from "react-icons/fa";
import React, { useEffect, useState } from "react";

import CategoriaController from "src/controllers/CategoryController";
import CategoriaService from "src/aplication/services/CategoryService";
import CompanyController from "src/controllers/CompanyController";
import CompanyService from "src/aplication/services/CompanyService";
import CustomButton from "../../../Inviduals/CustomButton";
import ProductController from "src/controllers/ProductController";
import ProductService from "src/aplication/services/ProductService";
import PropTypes from "prop-types";
import Tittle from "../../../Inviduals/Tittle";
import ModalAlertsConfir from "../../ModalAlertsConfir";
import ModalAlertsError from "../../ModalAlertsError";
import ProductRow from "src/components/Product/ProductRow";

const productService = new ProductService();
const productController = new ProductController(productService);
const companyService = new CompanyService();
const companyController = new CompanyController(companyService);
const categoriaService = new CategoriaService();
const categoriaController = new CategoriaController(categoriaService);

const RegisterProductModal = ({
  isOpen,
  onClose,
  selectedMuestraId,
  handleGetSampleAllId,
  selectedProduct,
}) => {
  const [muestraStatus, setMuestraStatus] = useState([]);
  const [categorias, setCategorias] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState({});
  const [showAlert, setShowAlert] = useState(false);
  const [currentScreen, setCurrentScreen] = useState(0);
  const [previewImages, setPreviewImages] = useState([]);
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  const [tempFormData, setTempFormData] = useState({
    nombre: "",
    precioInicial: "",
    pais: "",
    departamento: "",
    ciudad: "",
    fincaProductor: "",
    score: "",
    descriptores: "",
    variedad: "",
    peso: "",
    unidadPeso: "",
    process: "",
    increment: "",
    private: Boolean,
    lote: "",
    categoriaId: "",
    imagenes: [],
  });

  useEffect(() => {
    setMuestraStatus(selectedMuestraId);

    if (selectedProduct) {
      setTempFormData({
        nombre: selectedProduct.nombre || "",
        precioInicial: selectedProduct.precioInicial || "",
        pais: selectedProduct.pais || "",
        departamento: selectedProduct.departamento || "",
        ciudad: selectedProduct.ciudad || "",
        fincaProductor: selectedProduct.fincaProductor || "",
        score: selectedProduct.score || "",
        descriptores: selectedProduct.descriptores || "",
        variedad: selectedProduct.variedad || "",
        peso: selectedProduct.peso || "",
        unidadPeso: selectedProduct.unidadPeso || "",
        process: selectedProduct.process || "",
        increment: selectedProduct.increment || "",
        private: Boolean(selectedProduct.private),
        lote: selectedProduct.lote || "",
        categoriaId: selectedProduct.categoriaId || "",
        imagenes: selectedProduct.imagen || [],
      });
      const imageUrls = (selectedProduct.imagen || []).map((img) => img); // Asume que ya son URLs

      setPreviewImages(imageUrls);
    }
  }, [selectedMuestraId, selectedProduct]);

  useEffect(() => {
    const fetchCategorias = async () => {
      try {
        const categorias = await categoriaController.getAllCategorias();
        setCategorias(categorias);
      } catch (error) {
        console.error("Error fetching categories:", error);
      }
    };
    fetchCategorias();
  }, []);

  useEffect(() => {
    setMuestraStatus(selectedMuestraId);
  }, [selectedMuestraId]);

  const handleChange = (e) => {
    const { name, value, type, files } = e.target;

    if (type === "file") {
      // Convert files to an array and add them to formData
      const selectedFiles = Array.from(files);

      // Update tempFormData by adding the new files
      setTempFormData((prevFormData) => ({
        ...prevFormData,
        imagenes: [...(prevFormData.imagenes || []), ...selectedFiles], // Ensure it always remains an array
      }));

      // Update the preview images
      const imageUrls = selectedFiles.map((file) => URL.createObjectURL(file));
      setPreviewImages((prevPreviews) => [...prevPreviews, ...imageUrls]);
    } else {
      setTempFormData({
        ...tempFormData,
        [name]: name === "private" ? value === "true" : value,
      });
    }

    if (error[name]) {
      setError({ ...error, [name]: false });
    }
  };

  const handleRemoveImage = (index) => {
    // Eliminar la imagen del estado de imágenes seleccionadas (tempFormData.imagenes)
    setTempFormData((prevFormData) => ({
      ...prevFormData,
      imagenes: prevFormData.imagenes.filter((_, i) => i !== index),
    }));

    // Eliminar la previsualización correspondiente de previewImages
    setPreviewImages((prevPreviews) =>
      prevPreviews.filter((_, i) => i !== index)
    );
  };

  const validateCurrentScreen = () => {
    const requiredFields = [
      ["nombre", "precioInicial", "pais"],
      ["departamento", "ciudad", "fincaProductor", "score"],
      ["descriptores", "variedad", "peso", "unidadPeso", "process"],
      ["increment", "private", "imagen"],
    ];

    const currentFields = requiredFields[currentScreen];
    const newErrors = {};

    currentFields.forEach((field) => {
      if (!tempFormData[field]) {
        newErrors[field] = true;
      }
    });

    setError(newErrors);

    return Object.keys(newErrors).length === 0;
  };

  const handleNext = (e) => {
    e.preventDefault();
    if (validateCurrentScreen()) {
      setCurrentScreen(currentScreen + 1);
    }
  };

  const handlePrevious = (e) => {
    e.preventDefault();
    if (currentScreen > 0) {
      setCurrentScreen(currentScreen - 1);
    }
  };

  const resetForm = () => {
    setTempFormData({
      nombre: "",
      precioInicial: "",
      pais: "",
      departamento: "",
      ciudad: "",
      fincaProductor: "",
      score: "",
      descriptores: "",
      variedad: "",
      peso: "",
      unidadPeso: "",
      process: "",
      increment: "",
      private: Boolean,
      lote: "",
      categoriaId: "",
      imagenes: [],
    });
    setPreviewImages([]);
    setCurrentScreen(0); // Reset to first screen
    onClose();
  };

  const handleRegistro = async (event) => {
    event.preventDefault();
    try {
      setLoading(true);

      const scoreValue = parseInt(tempFormData.score, 10);

      // Validar score aquí
      if (scoreValue < 1 || scoreValue > 100) {
        handleErrorConfiModal("the scorecard can only be from 1 to 100");
        return; // No continuar con el envío si hay un error
      }

      // Create a FormData object to hold all form data and files
      const formData = new FormData();

      // Append all the text fields from tempFormData to formData
      Object.keys(tempFormData).forEach((key) => {
        if (key !== "imagenes") {
          formData.append(key, tempFormData[key]);
        }
      });

      // Append the image files to formData
      if (tempFormData.imagenes && tempFormData.imagenes.length > 0) {
        tempFormData.imagenes.forEach((file) => {
          formData.append("imagenes", file); // 'imagenes' is the field name for files
        });
      }

      // Send the form data to create the product
      const productResponse = await productController.handleCreateProduct(
        formData
      );

      setLoading(false);
      if (productResponse.status === 400) {
        handleErrorConfiModal(productResponse.data.message);
      } else {
        handleOpenConfiModal(productResponse.message);
      }

      // Prepare muestraToUpdate
      const muestraToUpdate = new FormData();
      const productsIdArray = muestraStatus.productsId || []; // Ensure it's an array

      // Push the new product ID as an object with the structure { product: string }
      productsIdArray.push({
        product: productResponse.savedCafe.id, // Assuming this is the correct path to the saved product ID
      });

      // Append each product in productsIdArray to FormData as separate fields
      productsIdArray.forEach((item, index) => {
        muestraToUpdate.append(`productsId[${index}][product]`, item.product);
      });

      // Append other muestraStatus properties to FormData
      Object.keys(muestraStatus).forEach((key) => {
        if (key !== "productsId") {
          // Avoid appending productsId again as it's handled above
          muestraToUpdate.append(key, muestraStatus[key]);
        }
      });

      // Update the company or handle the case where no muestra is selected
      if (muestraStatus.id) {
        await companyController.handleUpdateCompany(
          muestraStatus.id,
          muestraToUpdate
        );
      } else {
        alert("A valid sample has not been selected.");
      }

      // Reset form after successful registration
    } catch (error) {
      setLoading(false);
    }
  };

  const handleUpdate = async (event) => {
    event.preventDefault();
    try {
      const scoreValue = parseInt(tempFormData.score, 10);

      // Validar score aquí
      if (scoreValue < 1 || scoreValue > 100) {
        alert("the scorecard can only be from 1 to 100");
        return; // No continuar con el envío si hay un error
      }
      const formData = new FormData();

      // Append all the text fields from tempFormData to formData
      Object.keys(tempFormData).forEach((key) => {
        if (key !== "imagenes") {
          formData.append(key, tempFormData[key]);
        }
      });

      // Append the image files to formData
      if (tempFormData.imagenes && tempFormData.imagenes.length > 0) {
        tempFormData.imagenes.forEach((image) => {
          if (typeof image === "string") {
            // Si es una URL (cadena)
            formData.append("imagenesUrls", image); // Agregar URL de imagen existente
          }
        });

        // Agregar archivos de imagen nuevos a formData
        tempFormData.imagenes.forEach((file) => {
          if (file instanceof File) {
            // Verificar si es un objeto File
            formData.append("imagenes", file); // Agregar nuevo archivo de imagen
          }
        });
      }

      setLoading(true);
      const productResponse = await productController.handleUpdateProduct(
        selectedProduct.id,
        formData
      );
      setLoading(false);
      if (productResponse.status === 400) {
        handleErrorConfiModal(productResponse.data.message);
      } else {
        handleOpenConfiModal(productResponse.message);
      }
    } catch (error) {
      setLoading(false);
      alert(`Error: ${error.message || "Error updating the product."}`);
    }
  };

  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  const handleCloseConfiModal = async () => {
    await handleGetSampleAllId();
    resetForm();
    setConfirModalOpen(false);
    onClose();
  };

  return isOpen ? (
    <div className="fixed inset-0 bg-[#090B11] bg-opacity-75 flex justify-center items-center z-50">
      <div className="bg-[#2F3542] p-5 rounded-lg h-auto w-2/4 relative flex overflow-y-auto">
        <CustomButton
          onClick={onClose}
          className="absolute top-2 right-2 text-gray-500 hover:text-gray-600"
        >
          <FaTimes className="text-white text-xl" />
        </CustomButton>

        <div className="w-full flex flex-col justify-center items-center p-4">
          <Tittle
            level="h1"
            size="text-3xl"
            color="text-custom-red"
            align="text-center"
            className="mt-5 mb-10"
          >
            Registration Form
          </Tittle>
          <ProductRow
            selectedProduct={selectedProduct}
            tempFormData={tempFormData}
            handleChange={handleChange}
            error={error}
            currentScreen={currentScreen}
            categorias={categorias}
            previewImages={previewImages}
            handleUpdate={handleUpdate}
            handleRegistro={handleRegistro}
            handlePrevious={handlePrevious}
            handleNext={handleNext}
            handleRemoveImage={handleRemoveImage}
          />
          {showAlert && (
            <div className="absolute top-0 left-0 w-full h-full bg-gray-900 bg-opacity-75 flex justify-center items-center">
              <div className="bg-white p-6 rounded-lg">
                <p className="text-red-500 mb-4">
                  Error registering the product. Please try again.
                </p>
                <button
                  onClick={() => setShowAlert(false)}
                  className="bg-[#D03449] text-white px-4 py-2 rounded"
                >
                  Cancel
                </button>
              </div>
            </div>
          )}
        </div>
      </div>
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={handleCloseConfiModal}
        customMessage={confirMessage}
      />
    </div>
  ) : null;
};

RegisterProductModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  selectedMuestraId: PropTypes.object,
  handleGetSampleAllId: PropTypes.func.isRequired,
};

export default RegisterProductModal;
