import React, { useState } from 'react';
import CategoriaService from 'src/aplication/services/CategoryService';
import CategoriaController from 'src/controllers/CategoryController';
import ModalAlertsConfir from '../../ModalAlertsConfir';
import ModalAlertsError from '../../ModalAlertsError';
import Tittle from 'src/components/Inviduals/Tittle';
import Text from 'src/components/Inviduals/Text';
import CustomButton from 'src/components/Inviduals/CustomButton';

const categoriaService = new CategoriaService();
const categoriaController = new CategoriaController(categoriaService);

const ModalRegisterCategory = ({ isOpen, onClose }) => {
  const [nombre, setNombre] = useState('');
  const [descripcion, setDescripcion] = useState('');
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setConfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  const handleSaveCategory = async () => {
    if (!validateFields()) return;

    try {
      const categoryData = { nombre, descripcion };
      await categoriaController.handleCreateCategory(categoryData);
      handleOpenConfirModal('Category successfully created');
    } catch (error) {
      handleOpenErrorModal('There was an error when creating the category.');
    }
  };

  const validateFields = () => {
    if (!nombre || !descripcion) {
      handleOpenErrorModal('All fields are required.');
      return false;
    }
    return true;
  };

  const handleOpenConfirModal = (message) => {
    setConfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleOpenErrorModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  const handleClose = () => {
    setConfirModalOpen(false);
    onClose();
  };

  if (!isOpen) {
    return null;
  }

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50 bg-[#090B11] bg-opacity-50">
      <div className="bg-[#2F3542] p-4 rounded-lg shadow-lg w-2/4 overflow-auto h-auto">
        <Tittle level="h2" size="text-xl" color="text-white" align="text-center" className="mb-4">
          Register New Category
        </Tittle>
        <div className="mb-4">
          <Text className="block mb-2" color="text-white" size="text-lg">Name:</Text>
          <input
            type="text"
            value={nombre}
            onChange={(e) => setNombre(e.target.value)}
            className="w-full p-2 border border-gray-300 rounded text-black"
          />
        </div>
        <div className="mb-4">
          <Text className="block mb-2" color="text-white" size="text-lg">Description:</Text>
          <textarea
            value={descripcion}
            onChange={(e) => setDescripcion(e.target.value)}
            className="w-full p-2 border border-gray-300 rounded text-black h-24"
          />
        </div>
        <div className="flex justify-center space-x-4 mt-4">
          <CustomButton
            title="Close"
            onClick={onClose}
            className="bg-[#DFE4EA] text-[#2F3542] px-4 py-2 rounded"
          />
          <CustomButton
            title="Save"
            onClick={handleSaveCategory}
            className="bg-[#019267] text-white px-4 py-2 rounded"
          />
        </div>
      </div>
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={handleClose}
        customMessage={confirMessage}
      />
    </div>
  );
};

export default ModalRegisterCategory;
