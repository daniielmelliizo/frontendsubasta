import React from 'react';
import PropTypes from 'prop-types';
import Tittle from '../../../Inviduals/Tittle';
import CustomButton from '../../../Inviduals/CustomButton';

const ModalEliminarAuction = ({ isOpen, onClose, onConfirm, auction }) => {
  if (!isOpen) return null;
  const auctionNombre = auction.muestra || "Nombre no disponible";
  const firstCategory = auction.categorias ? Object.values(auction.categorias)[0] : [];
  const firstSubasta = firstCategory[0] || {};

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50">
      <div className="bg-[#131821] bg-opacity-80 fixed inset-0"></div>
      <div className="bg-white rounded-lg shadow-lg z-50 w-3/4 md:w-1/2 lg:w-1/3 p-6">
        <Tittle color="text-black" size="text-xl">
          Confirmation of Elimination
        </Tittle>
        <div className="mt-4">
          <p className="text-gray-700">
            Are you sure you want to remove the auction <span className="font-bold">{auctionNombre}</span>?
          </p>
        </div>
        <div className="mt-6 flex justify-end space-x-4">
          <CustomButton
            title="Cancel"
            onClick={onClose}
            className="bg-[#D8354C] hover:bg-[#C72B3F] text-white"
          />
          <CustomButton
            title="Confirm"
            onClick={() => onConfirm(firstSubasta.muestraId)}
            className="bg-[#1293A1] hover:bg-[#0E7988] text-white"
          />
        </div>
      </div>
    </div>
  );
};

export default ModalEliminarAuction;
