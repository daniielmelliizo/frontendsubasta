import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import CustomButton from "../../../Inviduals/CustomButton";
import Input from "../../../Inviduals/Input";
import Tittle from "../../../Inviduals/Tittle";
import Text from "src/components/Inviduals/Text";
import Image from "../../../Inviduals/Image";
import SampleService from "src/aplication/services/SampleService";
import SampleController from "src/controllers/SampleController";
import SubastaService from "src/aplication/services/SubastaService";
import SubastaController from "src/controllers/SubastaController";
import ModalAlertsConfir from "../../ModalAlertsConfir";
import ModalAlertsError from "../../ModalAlertsError";

const sampleService = new SampleService();
const sampleController = new SampleController(sampleService);

const subastaService = new SubastaService();
const subastaController = new SubastaController(subastaService);

const AgregarSubastaModal = ({
  isOpen,
  onClose,
  auction,
  fetchAuctionDetails,
}) => {


  const isUpdating = !!auction;

  const [subastaData, setSubastaData] = useState({
    muestraId: "",
    fechaInicio: "",
    horaSubasta: "",
    horaFin: "", // Initialize as an empty string
    imagenes: [],
  });

  const [auctionId, setIdAuction] = useState([]);
  const [muestra, setMuestras] = useState([]);
  const [error, setError] = useState({});
  const [previewImages, setPreviewImages] = useState([]);
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  useEffect(() => {
    const fetchMuestras = async () => {
      try {
        const response = await sampleController.handleGetSampleAdmin();
        const filteredMuestras = response.filter(
          (muestra) => muestra.private === true
        );
        setMuestras(filteredMuestras);
      } catch (error) {
        console.error("Error fetching muestras:", error);
      }
    };

    fetchMuestras();
  }, []);

  useEffect(() => {
    if (auction) {

      const firstCategory = auction.categorias
        ? Object.values(auction.categorias)[0]
        : null;
      const firstSubasta = firstCategory ? firstCategory[0] : null;
      const imageUrls = (firstSubasta.sample.imagen || []).map((img) => img); // Asume que ya son URLs
      setPreviewImages(imageUrls);

      setIdAuction(firstSubasta);

      if (firstSubasta) {
        setSubastaData({
          ...firstSubasta,
          fechaInicio: firstSubasta.fechaInicio
            ? formatDate(firstSubasta.fechaInicio)
            : "",
          horaSubasta: firstSubasta.horaSubasta
            ? formatTime(firstSubasta.horaSubasta)
            : "",
          imagenes: firstSubasta.sample.imagen || []
        });
      }
    }
  }, [auction]);

  const handleChange = (e) => {
    const { name, value, files, type } = e.target;

    if (type === "file") {
      const selectedFiles = Array.from(files);

      setSubastaData((prevFormData) => ({
        ...prevFormData,
        imagenes: [...(prevFormData.imagenes || []), ...selectedFiles], // Añadir archivos seleccionados
      }));

      const imageUrls = selectedFiles.map((file) => URL.createObjectURL(file));
      setPreviewImages((prevPreviews) => [...prevPreviews, ...imageUrls]);
    } else {
      setSubastaData({
        ...subastaData,
        [name]: value,
      });
    }
    setError((prev) => ({ ...prev, [name]: "" }));
  };

  const handleRemoveImage = (index) => {
    setSubastaData((prevFormData) => ({
      ...prevFormData,
      imagenes: (prevFormData.imagenes || []).filter((_, i) => i !== index),
    }));

    setPreviewImages((prevPreviews) => prevPreviews.filter((_, i) => i !== index));
  };


  const formatDate = (dateString) => {
    const date = new Date(dateString);
    date.setDate(date.getDate() + 1);
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear().toString().slice(-2);
    return `${day}/${month}/${year}`;
  };

  const formatTime = (timeString) => {
    const date = new Date(`1970-01-01T${timeString}:00Z`);
    let hours = date.getUTCHours();
    const minutes = date.getMinutes().toString().padStart(2, "0");
    const ampm = hours >= 12 ? "pm" : "am";
    hours = hours % 12;
    hours = hours ? hours : 12;
    return `${hours}:${minutes} ${ampm}`;
  };

  const validateForm = () => {
    const newErrors = {};
    if (!subastaData.muestraId) newErrors.muestraId = "This field is required";
    if (!subastaData.fechaInicio) newErrors.fechaInicio = "This field is required";
    if (!subastaData.horaSubasta) newErrors.horaSubasta = "This field is required";
    if (!subastaData.horaFin) newErrors.horaFin = "This field is required";
    setError(newErrors);
    return Object.keys(newErrors).length === 0;
  };

  const handleSave = async (event) => {
    event.preventDefault();

    if (!validateForm()) return;

    try {
      // Create a new FormData instance to hold the form data
      const formData = new FormData();

      // Append all the text fields from subastaData (except images) to formData
      Object.keys(subastaData).forEach((key) => {
        if (key !== "imagenes") {
          formData.append(key, subastaData[key]);
        }
      });

      // Append the image files to formData, if any exist
      if (subastaData.imagenes && subastaData.imagenes.length > 0) {
        subastaData.imagenes.forEach((file) => {
          formData.append("imagenes", file); // 'imagenes' is the form field name for files
        });
      }

      // Format the date and time fields before submission
      formData.set("fechaInicio", formatDate(subastaData.fechaInicio));
      formData.set("horaSubasta", formatTime(subastaData.horaSubasta));

      // Send the formatted data using subastaController
      const response = await subastaController.handleCreateSubasta(formData);

      if (response.status === 400) {
        handleErrorConfiModal(response.data.message);
      } else {
        handleOpenConfiModal(response.message);
      }

    } catch (error) {
      error
    }
  };


  const handleUpdate = async (event) => {
    event.preventDefault();

    if (!validateForm()) return;

    try {
      const formData = new FormData();

      // Append all the text fields from subastaData (except images) to formData
      Object.keys(subastaData).forEach((key) => {
        if (key !== "imagenes") {
          formData.append(key, subastaData[key]);
        }
      });

      if (subastaData.imagenes && subastaData.imagenes.length > 0) {
        subastaData.imagenes.forEach((image) => {
          if (typeof image === 'string') { // Si es una URL (cadena)
            formData.append("imagenesUrls", image); // Agregar URL de imagen existente
          }
        });

        // Agregar archivos de imagen nuevos a formData
        subastaData.imagenes.forEach((file) => {
          if (file instanceof File) { // Verificar si es un objeto File
            formData.append("imagenes", file); // Agregar nuevo archivo de imagen
          }
        });
      }

      // Format the date and time fields before submission
      formData.set("fechaInicio", formatDate(subastaData.fechaInicio));
      formData.set("horaSubasta", formatTime(subastaData.horaSubasta));

      const response = await subastaController.handleUpdateSubasta(
        auctionId.id,
        formData
      );
      if (response.status === 400) {

        handleErrorConfiModal(response.data.message);
      } else {
        handleOpenConfiModal(response.message);
      }
    } catch (error) {
    }
  };

  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  const handleClose = () => {
    onClose();
    fetchAuctionDetails();
    setConfirModalOpen(false);
  };

  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 bg-[#090B11] bg-opacity-50 flex justify-center items-center z-50">
      <div className="bg-black flex justify-center items-center h-auto w-2/4 mx-auto rounded-lg">
        <div className="relative h-auto w-full">
          <div className="absolute inset-0 bg-custom-fondo-subasta bg-cover bg-center p-4 rounded-lg"></div>
          <div className="absolute inset-0 bg-black opacity-50 rounded-lg"></div>
          <div className="relative p-4 flex flex-col items-center justify-center h-full space-y-4">
            <div className="flex flex-col items-start justify-center h-full space-y-4 p-4 w-4/5 bg-black bg-opacity-50 rounded-lg">
              <Tittle
                level="h1"
                size="text-3xl"
                color="text-white"
                className="flex justify-center p-4 text-center"
              >
                {isUpdating ? "Update Auction" : "Add Auction"}
              </Tittle>
              <form 
                onSubmit={ isUpdating ? handleUpdate : handleSave}
                className="w-full space-y-4">
                <div className="flex flex-row w-full space-x-4">
                  <div className="w-1/2">
                    <Text className="mb-2" color="text-white">Select Sample</Text>
                    <select
                      name="muestraId"
                      value={subastaData.muestraId}
                      onChange={handleChange}
                      disabled={isUpdating}
                      error={error.muestraId}
                      
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
                    >
                      <option value="">Select Sample</option>
                      {muestra.map((muestras) => (
                        <option
                          key={muestras.id}
                          value={muestras.id}
                          className="text-black"
                        >
                          {muestras.nombre}
                        </option>
                      ))}
                    </select>
                    {error.muestraId && <p className="error-message text-[#D03449]">{error.muestraId}</p>}
                  </div>

                  <div className="w-1/2">
                    <Text className="mb-2" color="text-white">Start Date</Text>
                    <Input
                      name="fechaInicio"
                      value={subastaData.fechaInicio}
                      onChange={handleChange}
                      placeholder="Start Date"
                      type="date"
                      title=""
                      error={error.fechaInicio}
                      
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                    />
                  </div>
                </div>
                <div className="flex flex-row w-full space-x-4">
                  <div className="w-1/2">
                    <Text className="mb-2" color="text-white">Auction Time</Text>
                    <Input
                      name="horaSubasta"
                      value={subastaData.horaSubasta}
                      onChange={handleChange}
                      placeholder=" Auction Time"
                      title=""
                      type="time"
                      error={error.horaSubasta}
                      
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                    />
                  </div>
                  <div className="w-1/2">
                    <Text className="mb-2" color="text-white">Duration of the Auction</Text>
                    <select
                      name="horaFin"
                      value={subastaData.horaFin}
                      onChange={handleChange}
                      error={error.horaFin}
                      
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
                    >
                      <option value="" disabled>
                        ¿Duration of the Auction?
                      </option>
                      <option value="24">24 Hours</option>
                      <option value="48">48 Hours</option>
                    </select>
                    {error.horaFin && <p className="error-message text-[#D03449]">{error.horaFin}</p>}
                  </div>
                </div>
                <Input
                  title="Images"
                  name="imagenes"
                  accept="image/*"
                  onChange={handleChange}
                  type="file"
                  multiple
                />
                <div className="flex flex-wrap gap-2 mt-2">
                  {previewImages.map((image, index) => (
                    <div key={index} className="relative">
                      <img
                        src={image}
                        alt={`Vista previa ${index + 1}`}
                        className="w-32 h-32 object-cover rounded-lg"
                      />
                      <button
                        type="button"
                        className="absolute top-0 right-0 bg-red-500 text-white rounded-full p-1"
                        onClick={() => handleRemoveImage(index)}
                      >
                        X
                      </button>
                    </div>
                  ))}
                </div>
                <div className="flex justify-center items-center space-x-4 mt-4">
                  <CustomButton
                    title="Close"
                    onClick={onClose}
                    className="border border-[#D8354C] bg-black text-[#D8354C] px-4 py-2 rounded"
                  />
                  
                  <button
                    type="submit"
                    className="flex items-center space-x-2 px-4 py-2 rounded bg-[#D03449] text-white hover:[#D03449]"
                  >
                    <span>{isUpdating ? "Update" : "Save"}</span>
                  </button>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={handleClose}
        customMessage={confirMessage}
      />
    </div>
  );
};

AgregarSubastaModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  auction: PropTypes.object,
  fetchAuctionDetails: PropTypes.func.isRequired,
};

export default AgregarSubastaModal;
