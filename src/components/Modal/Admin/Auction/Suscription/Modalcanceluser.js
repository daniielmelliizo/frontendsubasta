import PropTypes from 'prop-types';
import CustomButton from '../../../../Inviduals/CustomButton';
import Tittle from '../../../../Inviduals/Tittle';
import Text from '../../../../Inviduals/Text';
import { FaTimes } from 'react-icons/fa';

const InvalidUserModal = ({ onClose }) => {
  return (
    <div className="fixed inset-0 flex items-center justify-center bg-[#090B11] bg-opacity-75 z-50">
      <div className="flex flex-col bg-white p-6 rounded-lg shadow-lg w-screen-10 sm:max-w-sm md:max-w-lg lg:max-w-xl xl:max-w-2xl items-center justify-center">
        <FaTimes className=" text-[#D8354C] text-2xl"/>
        <Tittle color="text-black" size="text-xl" className="text-center mb-4">
        Unlinked User
        </Tittle>
        <Text className="text-gray-700 items-center text-sm sm:text-base md:text-lg">
        Your user was unlinked.
        </Text>
        <div className="flex justify-end space-x-4 mt-6">
          <CustomButton
            title="Confirm"
            onClick={onClose}
            className="px-4 py-2 text-sm sm:text-base md:text-lg bg-[#2F3542] text-white rounded-md"
          />
        </div>
      </div>
    </div>
  );
};

InvalidUserModal.propTypes = {
  onClose: PropTypes.func.isRequired, // Función para cerrar el modal
};

export default InvalidUserModal;
