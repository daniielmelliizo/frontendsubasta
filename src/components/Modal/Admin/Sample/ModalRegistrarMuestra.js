import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import CustomButton from "../../../Inviduals/CustomButton";
import Input from "../../../Inviduals/Input";
import Title from "../../../Inviduals/Tittle";
import SampleService from "src/aplication/services/SampleService";
import SampleController from "src/controllers/SampleController";
import ProductService from "src/aplication/services/ProductService";
import ProductController from "src/controllers/ProductController";
import CategoriaService from "src/aplication/services/CategoryService";
import CategoriaController from "src/controllers/CategoryController";
import ModalAlertsConfir from "../../ModalAlertsConfir";
import ModalAlertsError from "../../ModalAlertsError";
import { FaTimes } from "react-icons/fa";

// Inicializar los servicios y controladores
const sampleService = new SampleService();
const sampleController = new SampleController(sampleService);
const productService = new ProductService();
const productController = new ProductController(productService);
const categoriaService = new CategoriaService();
const categoriaController = new CategoriaController(categoriaService);

const RegisterMuestraModal = ({
  isOpen,
  onClose,
  onSave,
  idUser,
  handleGetSample,
  sampleToEdit,
}) => {
  const initialMuestraData = {
    nombre: "",
    descripcion: "",
    private: "",
    valor: "",
    productsId: [],
  };

  const [muestraData, setMuestraData] = useState(initialMuestraData);
  const [productos, setProductos] = useState([]);
  const [categorias, setCategorias] = useState([]);
  const [filteredProductos, setFilteredProductos] = useState([]);
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [weights, setWeights] = useState({});
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  useEffect(() => {
    setMuestraData((prevData) => ({ ...prevData, userId: idUser }));

    const fetchProducts = async () => {
      try {
        const products = await productController.HandlegetAllProductsSample();

        setProductos(products);

        if (sampleToEdit) {
          // Si hay una muestra para editar, inicializa el estado con ella
          setMuestraData(sampleToEdit);
          setSelectedProducts(
            sampleToEdit.productsId.map((item) => item.product)
          );
          setWeights(
            sampleToEdit.productsId.reduce((acc, item) => {
              acc[item.product] = item.peso;
              return acc;
            }, {})
          );
          const filtered = products.filter(
            (product) => product.private === sampleToEdit.private
          );
          setFilteredProductos(filtered);
        } else {
          // Si no hay muestra para editar, limpia los productos seleccionados
          setSelectedProducts([]);
          setWeights({});
          setFilteredProductos(products);
        }
      } catch (error) {
        console.error("Error fetching products:", error);
      }
    };

    fetchProducts();
  }, [idUser, sampleToEdit]);

  useEffect(() => {
    const fetchCategorias = async () => {
      try {
        const categorias = await categoriaController.getAllCategorias();
        setCategorias(categorias);
      } catch (error) {
        console.error("Error fetching categories:", error);
      }
    };

    fetchCategorias();
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;

    // Actualiza el valor del estado para el campo correspondiente
    const updatedValue = name === "private" ? value === "true" : value;
    setMuestraData({
      ...muestraData,
      [name]: updatedValue,
    });

    // Manejo de selección de "private" para filtrar productos
    if (name === "private") {
      const filtered = productos.filter(
        (product) => product.private === updatedValue
      );
      setFilteredProductos(filtered);

      // Filtrar los productos seleccionados según el valor de "private"
      const newSelectedProducts = selectedProducts.filter(
        (productId) =>
          productos.find((product) => product.id === productId).private ===
          updatedValue
      );
      setSelectedProducts(newSelectedProducts);

      const newWeights = {};
      newSelectedProducts.forEach((productId) => {
        newWeights[productId] = weights[productId];
      });
      setWeights(newWeights);

      setMuestraData((prevData) => ({
        ...prevData,
        productsId: prevData.productsId.filter(
          (item) =>
            productos.find((product) => product.id === item.product).private ===
            updatedValue
        ),
      }));
    }

    // Manejo de selección de categoría para filtrar productos
    if (name === "categoriaId") {
      const filtered = productos.filter(
        (product) => product.categoriaId === value
      );
      setFilteredProductos(filtered);
    }
  };

  const handleProductSelect = (e) => {
    const { value } = e.target;
    if (value && !selectedProducts.includes(value)) {
      setSelectedProducts([...selectedProducts, value]);
      setWeights({ ...weights, [value]: "" });
      setMuestraData((prevData) => ({
        ...prevData,
        productsId: [...prevData.productsId, { product: value, peso: "" }],
      }));
      setError(null);
    } else {
      setError("El producto ya está seleccionado.");
    }
  };

  const handleWeightChange = (e, productId) => {
    const { value } = e.target;
    setWeights({ ...weights, [productId]: value });
    setMuestraData((prevData) => ({
      ...prevData,
      productsId: prevData.productsId.map((item) =>
        item.product === productId ? { ...item, peso: value } : item
      ),
    }));
  };

  const handleRemoveProduct = (productId) => {
    setSelectedProducts(selectedProducts.filter((id) => id !== productId));
    setWeights((prevWeights) => {
      const newWeights = { ...prevWeights };
      delete newWeights[productId];
      return newWeights;
    });
    setMuestraData((prevData) => ({
      ...prevData,
      productsId: prevData.productsId.filter(
        (item) => item.product !== productId
      ),
    }));
  };

  const validateFields = () => {
    const { nombre, descripcion, productsId } = muestraData;
    if (!nombre || !descripcion) {
      setError("Todos los campos son obligatorios.");
      return false;
    }

    // Verificar si hay productos seleccionados
    if (!productsId.length) {
      setError("Debe seleccionar al menos un producto.");
      return false;
    }

    for (let item of productsId) {
      if (!item.peso) {
        setError("Todos los productos deben tener un peso especificado.");
        return false;
      }
    }
    return true;
  };

  const handleSave = async (e) => {
    e.preventDefault();
    if (!validateFields()) {
      return;
    }

    setLoading(true);
    setError(null);

    try {
      const response = sampleToEdit
        ? await sampleController.handleUpdateSample(muestraData)
        : await sampleController.handleCreateSample(muestraData);

      onSave(response);
      await handleGetSample();
      if (response.status === 400) {
        handleErrorConfiModal(response.data.message);
      } else {
        handleOpenConfiModal(response.message);
      }
    } catch (error) {
      setLoading(false);
    }
  };

  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  const handleClose = () => {
    setConfirModalOpen(false);
    setMuestraData(initialMuestraData);
    setSelectedProducts([]);
    setWeights({});
    setError(null);
    onClose();
  };

  return isOpen ? (
    <div className="fixed inset-0 bg-[#090B11] bg-opacity-75 flex justify-center items-center z-50">
      <div className="bg-[#2F3542] p-5 rounded-lg h-auto w-full max-w-lg relative flex flex-col justify-center items-center">
        <button
          onClick={handleClose}
          className="absolute top-2 right-2 text-gray-500 hover:text-gray-700"
        >
          <FaTimes className="text-white"/>
        </button>
        <Title
          level="h1"
          size="text-3xl"
          color="text-white"
          align="text-center"
          className="mb-5"
        >
          {sampleToEdit ? "Update Sample" : "Register New Sample"}
        </Title>
        <form className="w-full" onSubmit={handleSave}>
          <div className="mb-4">
            <Input
              title="Name Sample"
              name="nombre"
              placeholder="Name Sample"
              value={muestraData.nombre}
              onChange={handleChange}
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="mb-4">
            <Input
              title="Description"
              name="descripcion"
              placeholder="Description"
              value={muestraData.descripcion}
              onChange={handleChange}
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="mb-4">
            <Input
              title="Price USD"
              name="valor"
              placeholder="Price USD"
              value={muestraData.valor}
              type="number"
              onChange={handleChange}
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="mb-4">
            <select
              name="categoriaId"
              onChange={handleChange}
              className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
            >
              <option value="">Select a category</option>
              {categorias.map((categoria) => (
                <option
                  key={categoria.id}
                  value={categoria.id}
                  className="text-black"
                >
                  {categoria.nombre}
                </option>
              ))}
            </select>
          </div>
          <div className="mb-4">
            <select
              name="private"
              value={muestraData.private}
              onChange={handleChange}
              className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
            >
              <option value="" disabled>
                Select type
              </option>
              <option value="false">Direct Sales</option>
              <option value="true">Auctions</option>
            </select>
          </div>
          <div className="mb-4">
            <select
              name="productId"
              value=""
              onChange={handleProductSelect}
              className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
            >
              <option value="">Select a product</option>
              {filteredProductos.map((product) => (
                <option
                  key={product.id}
                  value={product.id}
                  className="text-black"
                >
                  {product.nombre}
                </option>
              ))}
            </select>
          </div>
          <div className="mb-4">
            {selectedProducts.map((productId) => {
              const product = productos.find((p) => p.id === productId);
              return (
                <div key={productId} className="flex items-center mb-2">
                  <span className=" text-white px-2 py-1 rounded mr-2">
                    {product.nombre}
                  </span>
                  <Input
                    title="Weight (kg)"
                    name="name"
                    value={weights[productId] || ""}
                    onChange={(e) => handleWeightChange(e, productId)}
                    placeholder="Weight"
                    type="number"
                    className="w-20 px-2 py-1 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black mr-2"
                  />
                  <button
                    type="button"
                    onClick={() => handleRemoveProduct(productId)}
                    className="text-red-500"
                  >
                    X
                  </button>
                </div>
              );
            })}
          </div>
          {error && <p className="text-red-500 mb-4">{error}</p>}
          <div className="flex justify-center items-center">
            <CustomButton
              type="submit"
              title={loading ? "Loading..." : "Save"}
              className="bg-[#D8354C] text-lg w-96"
            />
          </div>
        </form>
      </div>
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={handleClose}
        customMessage={confirMessage}
      />
    </div>
  ) : null;
};

RegisterMuestraModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  idUser: PropTypes.string.isRequired,
  handleGetSample: PropTypes.func.isRequired,
  sampleToEdit: PropTypes.object,
};

export default RegisterMuestraModal;
