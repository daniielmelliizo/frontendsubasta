import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import CustomButton from "../../../Inviduals/CustomButton";
import Input from "../../../Inviduals/Input";
import Tittle from "../../../Inviduals/Tittle";
import SampleService from "src/aplication/services/SampleService";
import SampleController from "src/controllers/SampleController";
import ProductService from "src/aplication/services/ProductService";
import ProductController from "src/controllers/ProductController";
import CategoriaService from "src/aplication/services/CategoryService";
import CategoriaController from "src/controllers/CategoryController";
import ModalAlertsConfir from "../../ModalAlertsConfir";
import ModalAlertsError from "../../ModalAlertsError";
import { FaTimes } from "react-icons/fa";

// Inicializar los servicios y controladores
const sampleService = new SampleService();
const sampleController = new SampleController(sampleService);
const productService = new ProductService();
const productController = new ProductController(productService);
const categoriaService = new CategoriaService();
const categoriaController = new CategoriaController(categoriaService);

const EditarMuestraModal = ({ isOpen, onClose, muestra, handleGetSample }) => {
  const initialMuestraData = {
    nombre: "",
    descripcion: "",
    valor: "",
    private: false,
    productsId: [],
  };

  const [muestraData, setMuestraData] = useState(initialMuestraData);
  const [categorias, setCategorias] = useState([]);
  const [productos, setProductos] = useState([]);
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [filteredProductos, setFilteredProductos] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [weights, setWeights] = useState({});
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  useEffect(() => {
    if (!isOpen || !muestra) return; // Si el modal no está abierto o no hay muestra, no ejecutar nada

    setMuestraData({
      nombre: muestra.nombre || "",
      descripcion: muestra.descripcion || "",
      valor: muestra.valor || "",
      private: muestra.private || false,
      productsId: muestra.productsId || [],
    });
    setSelectedProducts(muestra.productsId.map((p) => p.product));
    setWeights(
      muestra.productsId.reduce((acc, item) => {
        acc[item.product] = item.peso;
        return acc;
      }, {})
    );
  }, [isOpen, muestra]);

  useEffect(() => {
    if (!isOpen) return; // Validar que el modal esté abierto antes de ejecutar

    const fetchProducts = async () => {
      try {
        const products = await productController.getAllProducts();
        const productsSelect =
          await productController.HandlegetAllProductsSample();

        // Filtra productos de `products` que coincidan con los productos de `productsSelect`
        const filtered = products.filter((product) =>
          productsSelect.some((p) => p.id === product.id)
        );

        setProductos(products);
        setFilteredProductos(filtered);
      } catch (error) {
        console.error("Error fetching products:", error);
      }
    };

    const fetchCategorias = async () => {
      try {
        const categorias = await categoriaController.getAllCategorias();
        setCategorias(categorias);
      } catch (error) {
        console.error("Error fetching categories:", error);
      }
    };

    fetchCategorias();
    fetchProducts();
  }, [isOpen, muestra?.private]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    const newValue = name === "private" ? value === "true" : value;

    setMuestraData({
      ...muestraData,
      [name]: newValue,
    });

    if (name === "private") {
      const isPrivate = value === "true";
      const filtered = productos.filter(
        (product) => product.private === isPrivate
      );
      setFilteredProductos(filtered);

      const newSelectedProducts = selectedProducts.filter((productId) => {
        const product = productos.find((p) => p.id === productId);
        return product && product.private === isPrivate;
      });
      setSelectedProducts(newSelectedProducts);
      setMuestraData((prevData) => ({
        ...prevData,
        productsId: prevData.productsId.filter((item) =>
          newSelectedProducts.includes(item.product)
        ),
      }));
    }

    // Manejo de selección de categoría para filtrar productos
    if (name === "categoriaId") {
      const filtered = productos.filter(
        (product) => product.categoriaId === value
      );
      setFilteredProductos(filtered);
    }
  };

  const handleProductSelect = (e) => {
    const { value } = e.target;
    if (value && !selectedProducts.includes(value)) {
      setSelectedProducts([...selectedProducts, value]);
      setMuestraData((prevData) => ({
        ...prevData,
        productsId: [...prevData.productsId, { product: value, peso: "" }],
      }));
      setError(null);
    } else {
      setError("The product is already selected.");
    }
  };

  const handleWeightChange = (e, productId) => {
    const { value } = e.target;
    setWeights({ ...weights, [productId]: value });
    setMuestraData((prevData) => ({
      ...prevData,
      productsId: prevData.productsId.map((item) =>
        item.product === productId ? { ...item, peso: value } : item
      ),
    }));
  };

  const handleRemoveProduct = (productId) => {
    setSelectedProducts(selectedProducts.filter((id) => id !== productId));
    setWeights((prevWeights) => {
      const newWeights = { ...prevWeights };
      delete newWeights[productId];
      return newWeights;
    });
    setMuestraData((prevData) => ({
      ...prevData,
      productsId: prevData.productsId.filter(
        (item) => item.product !== productId
      ),
    }));
  };

  const validateFields = () => {
    const { nombre, descripcion, productsId } = muestraData;
    if (!nombre || !descripcion) {
      setError("All fields are required.");
      return false;
    }
    for (let item of productsId) {
      if (!item.peso) {
        setError("All products must have a specified weight.");
        return false;
      }
    }
    return true;
  };

  const handleSave = async (e) => {
    e.preventDefault();
    if (!validateFields()) {
      return;
    }

    setLoading(true);
    setError(null);

    try {
      const response = await sampleController.handleUpdateSample(
        muestra.id,
        muestraData
      );
      await handleGetSample();
      if (response.status === 400) {
        handleErrorConfiModal(response.data.message);
      } else {
        handleOpenConfiModal(response.message);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  const handleClose = () => {
    setConfirModalOpen(false);
    setMuestraData(initialMuestraData);
    setSelectedProducts([]);
    setError(null);
    onClose();
  };

  return isOpen ? (
    <div className="fixed inset-0 bg-[#090B11] bg-opacity-75 flex justify-center items-center z-50">
      <div className="bg-[#2F3542] p-5 rounded-lg h-auto w-full max-w-lg relative flex flex-col ">
        <div className="overflow-y-auto h-full">
          <button
            onClick={handleClose}
            className="absolute top-2 right-2 text-gray-500 hover:text-gray-700"
          >
            <FaTimes className="text-white"/>
          </button>
          <Tittle
            level="h1"
            size="text-3xl"
            color="text-white"
            align="text-center"
            className="mt-5 mb-5"
          >
            Edit Sample
          </Tittle>
          <form className="w-full" onSubmit={handleSave}>
            <div className="mb-4">
              <Input
                title="Sample name"
                name="nombre"
                value={muestraData.nombre}
                onChange={handleChange}
                placeholder="Name"
                className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
              />
            </div>
            <div className="mb-4">
              <Input
                title="Description"
                name="descripcion"
                value={muestraData.descripcion}
                onChange={handleChange}
                placeholder="Description"
                className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
              />
            </div>
            <div className="mb-4">
              <Input
                title="Price USD"
                name="valor"
                placeholder="Precio USD"
                value={muestraData.valor}
                type="number"
                onChange={handleChange}
                className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
              />
            </div>
            <div className="mb-4">
              <select
                name="categoriaId"
                onChange={handleChange}
                className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
              >
                <option value="">Select a category</option>
                {categorias.map((categoria) => (
                  <option key={categoria.id} value={categoria.id}>
                    {categoria.nombre}
                  </option>
                ))}
              </select>
            </div>
            <div className="mb-4">
              <select
                name="private"
                value={muestraData.private}
                onChange={handleChange}
                className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
              >
                <option value="false">Public</option>
                <option value="true">Private</option>
              </select>
            </div>
            <div className="mb-4">
              <select
                name="products"
                onChange={handleProductSelect}
                className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
              >
                <option value="">Select a product</option>
                {filteredProductos.map((producto) => (
                  <option key={producto.id} value={producto.id}>
                    {producto.nombre}
                  </option>
                ))}
              </select>
              </div>
              <div className="mb-4">
            {selectedProducts.map((productId) => {
              const product = productos.find((p) => p.id === productId);
              return (
                <div key={productId} className="flex items-center mb-2">
                  <span className=" text-white px-2 py-1 rounded mr-2">
                    {product?.nombre}
                  </span>
                  <Input
                    title="Weight (kg)"
                    name="name"
                    value={weights[productId] || ""}
                    onChange={(e) => handleWeightChange(e, productId)}
                    placeholder="Weight"
                    type="number"
                    className="w-20 px-2 py-1 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black mr-2"
                  />
                  <button
                    type="button"
                    onClick={() => handleRemoveProduct(productId)}
                    className="text-white"
                  >
                    X
                  </button>
                </div>
              );
            })}
          </div>
            {error && (
              <div className="text-red-500 mb-4">
                <p>{error}</p>
              </div>
            )}
            <div className="flex justify-center items-center">
              <CustomButton
                type="submit"
                title={loading ? "Loading..." : "Save"}
                className="bg-[#D8354C] text-lg w-96"
              />
            </div>
          </form>
        </div>
      </div>
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={handleClose}
        customMessage={confirMessage}
      />
    </div>
  ) : null;
};

EditarMuestraModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  muestra: PropTypes.object,
  handleGetSample: PropTypes.func.isRequired,
};

export default EditarMuestraModal;
