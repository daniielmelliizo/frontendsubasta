import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import CustomButton from '../../../Inviduals/CustomButton';
import Tittle from '../../../Inviduals/Tittle';
import Icons from '../../../Inviduals/Icons';
import Text from '../../../Inviduals/Text';
import ModalAlertsConfir from '../../ModalAlertsConfir';

const ConfirmDeleteModal = ({ 
  isOpen, 
  onClose, 
  item, 
  itemType, 
  fetchItems, 
  onDelete, 
  itemDisplayName, // Nombre o descripción del item a mostrar
  customMessage // Mensaje personalizado opcional
}) => {
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  useEffect(() => {}, [item]);

  const handleDelete = async () => {
    try {
      const response = await onDelete(item.id);
      if (response && response.message) {
        handleOpenConfiModal(response.message);
        fetchItems();
      } 
    } catch (error) {
    }
  };
  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };
  const handleClose = () => {
    setConfirModalOpen(false);
    onClose();
  };
  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 flex items-center justify-center bg-[#090B11] bg-opacity-75 z-50">
      <div className="flex flex-col bg-white p-6 rounded-lg shadow-lg w-screen-10 sm:max-w-sm md:max-w-lg lg:max-w-xl xl:max-w-2xl items-center justify-center">
        <Icons src="/assets/icons/report.svg" divSize='60%' />
        <Tittle color="text-black" size="text-xl" className="text-center mb-4">
          Confirmation of Elimination
        </Tittle>
        <Text className="text-gray-700 items-center text-sm sm:text-base md:text-lg">
          {customMessage || `¿Estás seguro de que deseas eliminar este ${itemType} ${itemDisplayName || ''}?`}
        </Text>
        <div className="flex justify-end space-x-4 mt-6">
          <CustomButton
            title="Cancel"
            onClick={onClose}
            className="px-4 py-2 text-sm sm:text-base md:text-lg bg-[#2F3542] rounded-md"
          />
          <CustomButton
            title="Delete"
            onClick={handleDelete}
            className="px-4 py-2 text-sm sm:text-base md:text-lg bg-[#D03449] hover:bg-[#D03449] text-white rounded-md"
          />
        </div>
      </div>
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={handleClose}
        customMessage={confirMessage}
      />
    </div>
  );
};

ConfirmDeleteModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  item: PropTypes.object,
  itemType: PropTypes.string.isRequired,
  fetchItems: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  itemDisplayName: PropTypes.string,
  customMessage: PropTypes.string
};

export default ConfirmDeleteModal;
