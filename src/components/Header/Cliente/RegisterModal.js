import React, { useState } from "react";
import PropTypes from "prop-types";
import CustomButton from "../../Inviduals/CustomButton";
import Input from "../../Inviduals/Input";
import Title from "../../Inviduals/Tittle";
import Image from "../../Inviduals/Image";
import Text from "../../Inviduals/Text";
import CodigoCorreoModal from "../../Modal/Cliente/Login/ModalCodigoEmail";
import UserService from "src/aplication/services/userService";
import UserController from "src/controllers/userController";
import { FaTimes, FaEye, FaEyeSlash } from "react-icons/fa";
import ModalAlertsConfir from "../../Modal/ModalAlertsConfir";
import ModalAlertsError from "../../Modal/ModalAlertsError";

const userService = new UserService();
const userController = new UserController(userService);

const RegisterModal = ({ isOpen, onClose, onCloseLogin }) => {
  const [formData, setFormData] = useState({
    email: "",
    password: "",
    nombre: "",
    apellido: "",
    cargo: "",
  });

  const [loading, setLoading] = useState(false);
  const [isCodigoCorreoOpen, setCodigoCorreoOpen] = useState(false);
  const [registeredEmail, setRegisteredEmail] = useState("");
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);
  const [isPasswordVisible, setPasswordVisible] = useState(false); // Estado para la visibilidad de la contraseña


  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({ ...prevData, [name]: value }));
  };
  

  const validateEmail = (email) => {
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regex.test(email);
  };

  const validateRequiredFields = () => {
    const requiredFields = ["nombre", "email", "password", "apellido", "cargo"];
    for (let field of requiredFields) {
      if (!formData[field]) {
        return false;
      }
    }
    return true;
  };

  const validatePassword = (password) => {
    // Expresión regular para al menos 8 caracteres, una letra mayúscula y un carácter especial
    const regex = /^(?=.*[A-Z])(?=.*[!@#$%^&*(),.?":{}|<>]).{8,}$/;
    return regex.test(password);
  };

  const handleRegistro = async (event) => {
    event.preventDefault();

    if (!validateRequiredFields()) {
      handleErrorConfiModal("Missing fields to be filled in");
      return;
    }

    if (!validateEmail(formData.email)) {
      handleErrorConfiModal("The e-mail is not valid.");
      return;
    }
    if (!validatePassword(formData.password)) {
      handleErrorConfiModal(
        "The password must be at least 8 characters long, include a capital letter and a special character."
      );
      return;
    }

    try {
      setLoading(true);
      const response = await userController.handleCreateUser(formData);
      setLoading(false);
      if (response.status === 409 || response.status === 400 ) {
        handleErrorConfiModal(response.data.message);
        return;
      }
      if (response) {
        const validationResponse = await userController.handleValidation({
          email: formData.email,
        });
        setRegisteredEmail(formData.email);
        setCodigoCorreoOpen(true);
      } 
    } catch (error) {
      setLoading(false);
    }
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  const handleClose = () => {
    setFormData({
      email: "",
      password: "",
      nombre: "",
      apellido: "",
      cargo: "",
    });
    onClose();
  };

  return isOpen ? (
    <>
      <div className="fixed inset-0 bg-gray-900 bg-opacity-75 flex justify-center items-center z-50">
        <div className="bg-black flex flex-col md:flex-row justify-center h-auto md:h-[60%] md:w-[50%] rounded-lg p-5 relative">
          <div className="bg-custom-fondo-login bg-cover bg-center rounded-lg items-center justify-center p-5 margin-auto overflow-hidden">
            <div className="absolute top-4 right-4 text-white">
              <FaTimes
                onClick={handleClose}
                className="text-2xl cursor-pointer text-[#D8354C] rounded-full bg-white"
              />
            </div>

            {/* Left Section: Image and Text */}
            <div className="flex felx-row bg-black bg-opacity-65 rounded-lg h-full items-center justify-center">
              <div className="hidden md:flex flex-col items-center justify-center bg-no-repeat bg-cover bg-center w-full md:w-2/5 p-6 ">
                <div className="w-full flex items-center justify-center p-6">
                  <Image
                    src="../assets/Images/logo.svg"
                    width={"70%"}
                    height={"auto"}
                    alt="Logo"
                  />
                </div>
                <Title
                  level="h1"
                  size="text-3xl"
                  color="text-white"
                  align="text-center"
                >
                  Hello, friend!
                </Title>
                <div className="hidden md:flex flex-col items-center justify-center bg-no-repeat bg-cover bg-center w-full  p-6 ">
                  <Text size="text-sm" color="text-white" align="text-left">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam.
                  </Text>
                </div>
              </div>

              {/* Right Section: Registration Form */}
              <div className="p-5 w-full md:w-1/2 rounded-lg shadow-md">
                <Title
                  level="h1"
                  size="text-3xl"
                  color="text-white"
                  align="text-center"
                  className="space-y-4"
                >
                  Register
                </Title>

                <form className="space-y-4" onSubmit={handleRegistro}>
                  <Input
                    name="nombre"
                    value={formData.nombre}
                    onChange={handleChange}
                    placeholder="Name"
                    type="text"
                    className="text-black items-center"
                  />

                  <Input
                    name="apellido"
                    value={formData.apellido}
                    onChange={handleChange}
                    placeholder="Last name"
                    type="text"
                    className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                  />

                  <Input
                    name="email"
                    value={formData.email}
                    onChange={handleChange}
                    placeholder="E-mail"
                    type="email"
                    className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                  />
                <div className="mb-4 relative text-black">
                    <Input
                      name="password"
                      value={formData.password}
                      onChange={handleChange}
                      placeholder="Password"
                      type={isPasswordVisible ? "text" : "password"}
                      className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                    />
                    <div
                      className="absolute right-3 top-1/2 transform -translate-y-1/2 cursor-pointer"
                      onClick={() => setPasswordVisible(!isPasswordVisible)}
                    >
                      {isPasswordVisible ? <FaEyeSlash /> : <FaEye />}
                    </div>
                  </div>

                  <select
                    title="Cargo"
                    name="cargo"
                    value={formData.cargo}
                    onChange={handleChange}
                    className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                  >
                    <option value="" disabled>
                    Select a position
                    </option>
                    <option value="Ninguna">None</option>
                    <option value="Importador">Importer</option>
                    <option value="exportador">Exporter</option>
                  </select>

                  <CustomButton
                    type="submit"
                    title="Register"
                    className="bg-[#D8354C] w-full text-white py-2 rounded-md"
                    disabled={loading}
                  />
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

      {isCodigoCorreoOpen && (
        <CodigoCorreoModal
          email={registeredEmail}
          isOpen={isCodigoCorreoOpen}
          onClose={() => setCodigoCorreoOpen(false)}
          onCloseRegister={onClose}
          onCloseLogin={onCloseLogin}

        />
      )}
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={() => setConfirModalOpen(false)}
        customMessage={confirMessage}
      />
    </>
  ) : null;
};

RegisterModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onCloseLogin: PropTypes.func,
};

export default RegisterModal;
