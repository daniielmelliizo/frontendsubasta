import React, { useState } from "react";
import PropTypes from "prop-types";
import CustomButton from "../../Inviduals/CustomButton";
import Input from "../../Inviduals/Input";
import Tittle from "../../Inviduals/Tittle";
import Image from "../../Inviduals/Image";
import Text from "../../Inviduals/Text";
import CodigoCorreoModal from "../../Modal/Cliente/Login/ModalCodigoEmail";
import UserService from "src/aplication/services/userService";
import UserController from "src/controllers/userController";
import { FaTimes } from "react-icons/fa";
import ModalAlertsError from "../../Modal/ModalAlertsError";


const userService = new UserService();
const userController = new UserController(userService);

const ValidacionEmail = ({ isOpen, onClose, onCloseLogin }) => {
  const [formData, setFormData] = useState({
    email: "",
  });

  const [loading, setLoading] = useState(false);
  const [isCodigoCorreoOpen, setCodigoCorreoOpen] = useState(false);
  const [registeredEmail, setRegisteredEmail] = useState("");
  const [errorMessage, setErrorMessage] = useState(null);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const validateEmail = (email) => {
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regex.test(email);
  };

  const validateRequiredFields = () => {
    const requiredFields = [ "email"];
    for (let field of requiredFields) {
      if (!formData[field]) {
        return false;
      }
    }
    return true;
  };

  const handleRegistro = async (event) => {
    event.preventDefault();

    if (!validateRequiredFields()) {
      handleErrorConfiModal("Missing fields to be filled in");
      return;
    }

    if (!validateEmail(formData.email)) {
      handleErrorConfiModal("The e-mail is not valid.");
      return;
    }

    try {
      setLoading(true);
        const validationResponse = await userController.handleValidation({
          email: formData.email,
        });

        if (validationResponse.status === 409 || validationResponse.status === 400) {
          handleErrorConfiModal(validationResponse.data.message);
        } else {
          setRegisteredEmail(formData.email);
          setCodigoCorreoOpen(true);
        }
      
    } catch (error) {
      setLoading(false);
    }finally {
      setLoading(false); // Asegúrate de que loading siempre se restablezca a false
    }
  };
  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };
  const handleClose = ()=>{
    setFormData({
    email: "",
    });
    onClose();
  }

  return isOpen ? (
    <>
      <div className="fixed inset-0 bg-gray-900 bg-opacity-75 flex justify-center items-center z-50">
        <div className="bg-black flex flex-col md:flex-row justify-center h-auto md:h-[60%] md:w-[50%] rounded-lg p-5 relative">
        <div className="bg-custom-fondo-login bg-cover bg-center rounded-lg items-center justify-center p-5 margin-auto overflow-hidden">
          <div className="absolute top-4 right-4 text-white">
            <FaTimes
              onClick={handleClose}
              className="text-2xl cursor-pointer text-[#D8354C] rounded-full bg-white"
            />
          </div>

          {/* Left Section: Image and Text */}
          <div className="flex felx-row bg-black bg-opacity-65 rounded-lg h-full items-center justify-center">
          <div className="hidden md:flex flex-col items-center justify-center bg-no-repeat bg-cover bg-center w-full md:w-2/5 p-6 ">
            
            <div className="w-full flex items-center justify-center p-6">
            <Image src="../assets/Images/logo.svg" width={'70%'} height={'auto'} alt='Logo' />
            </div>
              <Tittle level="h1" size="text-3xl" color="text-white" align="text-center">
                Hello, friend!
              </Tittle>
              <div className="hidden md:flex flex-col items-center justify-center bg-no-repeat bg-cover bg-center w-full  p-6 ">
              <Text size="text-sm" color="text-white" align="text-left" >
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
              </Text>
              </div>
            
          </div>

          {/* Right Section: Registration Form */}
          <div className="p-5 w-full md:w-1/2 rounded-lg shadow-md">
            <Tittle
              level="h1"
              size="text-3xl"
              color="text-white"
              align="text-center"
              className="space-y-4"
            >
              Mail Validation
            </Tittle>

            <form className="space-y-4" onSubmit={handleRegistro}>
              <Input
                name="email"
                value={formData.email}
                onChange={handleChange}
                placeholder="E-mail"
                type="email"
                className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
              />

              <CustomButton
                type="submit"
                title="Validate"
                className="bg-[#D8354C] w-full text-white py-2 rounded-md"
                disabled={loading}
              />
            </form>
          </div>
          </div>
          </div>
        </div>
      </div>

      {isCodigoCorreoOpen && (
        <CodigoCorreoModal
          email={registeredEmail}
          isOpen={isCodigoCorreoOpen}
          onClose={() => setCodigoCorreoOpen(false)}
          onCloseRegister={onClose}
          onCloseLogin= {onCloseLogin}
        />
      )}
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
    </>
  ) : null;
};

ValidacionEmail.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ValidacionEmail;
