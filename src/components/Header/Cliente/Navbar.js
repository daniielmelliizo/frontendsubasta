import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Image from "../../Inviduals/Image";
import Tittle from "../../Inviduals/Tittle";
import LoginModal from "./LoginModal";
import CustomButton from "../../Inviduals/CustomButton";
import Link from 'next/link';
import { FaUser, FaSignOutAlt, FaShoppingCart, FaBars, FaTimes } from 'react-icons/fa';
import secureStorage from "../../../utils/secureStorage";

const Navbar = ({
  links = [
    { href: "/user/", label: "Home" },
    { href: "/user/Muestras", label: "Samples" },
    { href: "/user/ListaSubasta", label: "Auctions" },
    { href: "/user/About_Us", label: "About Us" },
    { href: "/user/Subastas", label: "Auctions Terminated" },
  ]
}) => {
  const [isModalOpen, setModalOpen] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [username, setUsername] = useState('');
  const [user, setUser] = useState('');
  const [rol, setRol] = useState('');
  const [isLoading, setIsLoading] = useState(true); // Añadir estado de carga
  const [updateKey, setUpdateKey] = useState(0); // Estado para forzar re-renderización

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const storedIsLoggedIn = secureStorage.getItem('isLoggedIn');
      const storedUser = secureStorage.getItem('user');

      let parsedUser = null;
      try {
        parsedUser = storedUser ? JSON.parse(storedUser) : null;
      } catch (error) {
        console.error("Invalid JSON in stored user data:", storedUser);
        secureStorage.removeItem('user');
      }

      if (storedIsLoggedIn === 'true' && parsedUser) {
        setIsLoggedIn(true);
        setUser(parsedUser.email);
        setUsername(parsedUser.id);
        setRol(parsedUser.rol);
      }
      setIsLoading(false); // Cuando se termina de cargar, desactivar el estado de carga
    }
  }, []);

  const handleLogout = () => {
    secureStorage.removeItem('isLoggedIn');
    secureStorage.removeItem('user');
    setIsLoggedIn(false);
    setUser('');
    setUsername('');
    setRol('');
    setUpdateKey((prevKey) => prevKey + 1); // Actualizar la clave
  };

  const handleLoginSuccess = (userData) => {
    setIsLoggedIn(true);
    setUser(userData.email);
    setUsername(userData.id);
    setRol(userData.rol);
    setModalOpen(false);
    setUpdateKey((prevKey) => prevKey + 1); // Forzar actualización al iniciar sesión
  };

  const handleCloseModal = () => {
    setModalOpen(false);
  };

  if (isLoading) {
    return <div>Loading...</div>; // Puedes mostrar un spinner o un mensaje de carga
  }

  return (
    <nav key={updateKey} className="container mx-auto px-4 py-3 flex items-center justify-between relative z-40">
      <div className="flex items-center">
        <Image src="/assets/Images/logo.svg" alt="Logo" width={"100%"} height={"auto"} />
      </div>

      {/* Botón de menú hamburguesa para dispositivos móviles */}
      <div className="md:hidden flex items-center">
        {!isMenuOpen && (
          <CustomButton
            onClick={() => setIsMenuOpen(true)}
            className="text-white text-2xl focus:outline-none bg-[#D8354C]"
          >
            <FaBars />
          </CustomButton>
        )}
      </div>

      {/* Menú desplegable en dispositivos móviles */}
      {isMenuOpen && (
        <div className="md:hidden fixed top-0 right-0 w-screen h-auto bg-[#D8354C] text-white z-20 flex flex-col p-4">
          <button onClick={() => setIsMenuOpen(false)} className="text-white self-end">
            <FaTimes />
          </button>

          {/* Map links */}
          {links.map((link) => (
            <Link key={link.label} href={link.href} className="text-white text-bold py-2">
              {link.label}
            </Link>
          ))}

          {/* Agregar la opción Administration si el usuario es admin */}
          {rol === 'Administrador' && (
            <Link href="/admin/UserAdmin" className="text-white text-bold py-2">
              Administration
            </Link>
          )}

          {isLoggedIn && (
            <div className="flex flex-col space-y-4 mt-4">
              <Link href="/user/CarstClient">
                <CustomButton className="bg-[#D8354C] text-lg flex items-center justify-center">
                  <FaShoppingCart />
                </CustomButton>
              </Link>
              <Link href="/user/Perfiluser">
                <div className="flex items-center space-x-2">
                  <FaUser className="text-white" />
                  <Tittle level="h1" size="text-lg" color="text-white" align="text-center">
                    {user}
                  </Tittle>
                </div>
              </Link>
              <Link href="/user/" className='text-white'>
                <CustomButton
                  title={<FaSignOutAlt className='text-white'/>}
                  onClick={handleLogout}
                  className="bg-[#D8354C] text-lg flex items-center justify-center"
                />
              </Link>
            </div>
          )}
          {!isLoggedIn && (
            <>
              <CustomButton
                title="Login"
                onClick={() => setModalOpen(true)}
                className="bg-[#D8354C] text-lg mt-4"
              />
              <LoginModal
                isOpen={isModalOpen}
                onClose={() => setModalOpen(false)}
                onLoginSuccess={handleLoginSuccess}
              />
            </>
          )}
        </div>
      )}

      {/* Links normales para dispositivos más grandes */}
      <div className="hidden md:flex space-x-4 flex-grow text-white justify-center">
        {links.map((link) => (
          <Link key={link.label} href={link.href} className="text-white">
            {link.label}
          </Link>
        ))}

        {/* Agregar la opción Administration si el usuario es admin */}
        {rol === 'Administrador' && (
          <Link href="/admin/UsuariosAdmin" className="text-white">
            Administration
          </Link>
        )}
      </div>

      {/* Sección de perfil y carrito */}
      {isLoggedIn && (
        <div className="hidden md:flex items-center space-x-4 ml-auto">
          <Link href="/user/CarstClient">
            <CustomButton className="bg-[#F1C40F] text-lg flex items-center justify-center">
              <FaShoppingCart className='text-black' />
            </CustomButton>
          </Link>
          <Link href="/user/Perfiluser">
            <div className="flex items-center space-x-2">
              <FaUser className="text-white" />
              <Tittle level="h1" size="text-lg" color="text-white" align="text-center">
                {user}
              </Tittle>
            </div>
          </Link>
          <Link href="/user/">
            <CustomButton
              title={<FaSignOutAlt />}
              onClick={handleLogout}
              className="bg-[#D8354C] text-lg flex items-center justify-center"
            />
          </Link>
        </div>
      )}
      {!isLoggedIn && (
        <CustomButton
          title="Login"
          onClick={() => setModalOpen(true)}
          className="bg-[#D8354C] text-lg"
        />
      )}
      <LoginModal
        isOpen={isModalOpen}
        onClose={handleCloseModal}
        onLoginSuccess={handleLoginSuccess}
      />
    </nav>
  );
};

Navbar.propTypes = {
  title: PropTypes.string,
  links: PropTypes.arrayOf(
    PropTypes.shape({
      href: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    })
  ),
};

export default Navbar;
