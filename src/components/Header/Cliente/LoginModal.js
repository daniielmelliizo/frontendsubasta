import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import CustomButton from "../../Inviduals/CustomButton";
import InputField from "../../Inviduals/Input";
import Image from "../../Inviduals/Image";
import Tittle from "../../Inviduals/Tittle";
import Text from "../../Inviduals/Text";
import RegisterModal from "./RegisterModal";
import ValidacionEmail from "./ValidacionEmail";
import CodigoValidacionModal from "../../Modal/Cliente/Login/ModalCodigoValidar";
import AuthenticationController from "src/controllers/authController";
import secureStorage from "../../../utils/secureStorage";
import { FaTimes, FaEye, FaEyeSlash } from "react-icons/fa";
import ModalAlertsError from "../../Modal/ModalAlertsError";

const LoginModal = ({ isOpen, onClose, onLoginSuccess }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isPasswordVisible, setPasswordVisible] = useState(false); // Estado para la visibilidad de la contraseña
  const [isRegisterModalOpen, setRegisterModalOpen] = useState(false);
  const [isCodigoModalOpen, setCodigoModalOpen] = useState(false);
  const [codigoVerificacionModalOpen, setCodigoVerificacionModalOpen] = useState(false);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const authController = new AuthenticationController();
  const [errorMessage, setErrorMessage] = useState(null);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  const validateFields = () => {
    if (!email || !password) {
      setError("All fields are required.");
      return false;
    }
    if (!validateEmail(email)) {
      setError("Invalid e-mail address.");
      return false;
    }
    return true;
  };

  const validateEmail = (email) => {
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regex.test(email);
  };

  const handleLogin = async () => {
    if (!validateFields()) {
      return;
    }

    setLoading(true);
    setError(null);

    try {
      const response = await authController.handleLogin(email, password);
      setLoading(false);
      if (response.status === 400) {
        handleErrorConfiModal(response.data.message);
      } else if (response.message === "OK") {
        secureStorage.setItem("isLoggedIn", "true");
        secureStorage.setItem(
          "user",
          JSON.stringify({
            email: response.email,
            id: response.id_usuarios,
            rol: response.rol,
          })
        );
        onLoginSuccess(response);
        onClose(); // Cierra el modal después de un inicio de sesión exitoso
      }
    } catch (error) {
      setLoading(false);
    }
  };

  useEffect(() => {
    if (isOpen) {
      setEmail("");
      setPassword("");
      setError(null);
    }
  }, [isOpen]);

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  return isOpen ? (
    <div className="fixed inset-0 bg-black bg-opacity-75 flex justify-center items-center overflow-hidden z-100">
      <div className="bg-black flex flex-col sm:flex-col md:flex-row h-full md:h-auto w-full md:w-auto md:max-w-4xl rounded-3xl justify-center items-center p-5">
        <div className="relative flex flex-col sm:flex-col md:flex-row h-full md:h-auto w-full md:w-auto md:max-w-4xl rounded-lg bg-custom-fondo-login bg-cover bg-center justify-center items-center">
          <div className="absolute top-2 right-2">
            <FaTimes
              onClick={onClose}
              className="text-[#D8354C] bg-white rounded-full text-2xl cursor-pointer"
            />
          </div>

          <div className="relative p-8 flex flex-col items-left justify-center overflow-hidden lg:h-screen-50 md:h-screen-50 sm:h-auto w-full md:w-2/3 space-y-4 sm:w-2/3 sm:h-screen-50">
            <Image
              src="/assets/Images/logo.svg"
              width={"50%"}
              height={"auto"}
              alt="Logo"
              className="mb-4"
            />

            <Tittle
              level="h1"
              size="text-3xl"
              color="text-white"
              align="text-center md:text-left"
            >
              Discover the best coffees in the world!
            </Tittle>
            <div className="w-2/3">
              <Text
                size="text-sm"
                color="text-white"
                align="text-center md:text-left"
                weight="font-light"
              >
                Log in, explore our catalog with the most exotic coffees on the market and  participate in our exclusive auctions.


              </Text>
            </div>
            <Text
              size="text-sm"
              color="text-white"
              align="text-center md:text-left"
              weight="font-bold">

              Be part of becoffee.co
            </Text>
            <div className="flex flex-col md:flex-row space-x-0 md:space-x-2 space-y-2 md:space-y-0">
              <CustomButton
                title="Sign up"
                onClick={() => setRegisterModalOpen(true)}
                className="bg-transparent border border-white text-white py-2 px-2 rounded-lg"
              />
              <CustomButton
                title="Invitado"
                onClick={() => setCodigoModalOpen(true)}
                className="bg-transparent border border-white text-white py-2 px-2 rounded-lg"
              />
              <CustomButton
                title="Validacion Correo"
                onClick={() => setCodigoVerificacionModalOpen(true)}
                className="bg-transparent border border-white text-white py-2 px-2 rounded-lg"
              />
            </div>
            <RegisterModal
              isOpen={isRegisterModalOpen}
              onClose={() => setRegisterModalOpen(false)}
              onCloseLogin={onClose}
            />
            <CodigoValidacionModal
              isOpen={isCodigoModalOpen}
              onClose={() => setCodigoModalOpen(false)}
              onCloseLogin={onClose}
            />
            <ValidacionEmail
              isOpen={codigoVerificacionModalOpen}
              onClose={() => setCodigoVerificacionModalOpen(false)}
              onCloseLogin={onClose}
            />
          </div>

          <div className="p-5 w-full md:w-2/3 flex flex-col justify-center overflow-hidden items-center relative md:h-auto sm:h-auto">
            <div className="bg-black bg-opacity-65 md:w-4/5 md:h-screen-40 sm:w-2/3 sm:h-screen-40 flex flex-col justify-center items-center rounded-lg">
              <Tittle
                level="h1"
                size="text-3xl"
                color="text-white"
                align="text-center"
                className="p-5"
              >
                Sign in
              </Tittle>
              <form
                className="w-full px-5"
                onSubmit={(e) => {
                  e.preventDefault();
                  handleLogin();
                }}
              >
                <div className="mb-4 text-black">
                  <InputField
                    value={email}
                    title="Email"
                    name="email"
                    onChange={(e) => setEmail(e.target.value)}
                    placeholder="Email"
                    type="text"
                    className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                  />
                </div>
                <div className="mb-4 relative text-black">
                  <InputField
                    title="Password"
                    name="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    placeholder="Password"
                    type={isPasswordVisible ? "text" : "password"} // Cambia el tipo basado en el estado
                    className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
                  />
                  <div
                    className="absolute right-3 top-1/2 transform -translate-y-1/2 cursor-pointer"
                    onClick={() => setPasswordVisible(!isPasswordVisible)}
                  >
                    {isPasswordVisible ? <FaEyeSlash /> : <FaEye />}
                  </div>
                </div>
                {error && <p className="text-red-500 mb-4">{error}</p>}
                <div className="flex justify-center items-center">
                  <CustomButton
                    title={loading ? "Loading..." : "Login"}
                    disabled={loading}
                    onClick={handleLogin}
                    className={`bg-[#D8354C] text-lg w-full ${loading ? "opacity-50 cursor-not-allowed" : ""
                      }`}
                  />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
    </div>
  ) : null;
};

LoginModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onLoginSuccess: PropTypes.func.isRequired,
};

export default LoginModal;
