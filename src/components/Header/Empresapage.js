'use client';
import React, { useState } from "react";
import PropTypes from 'prop-types';
import CustomButton from "../Inviduals/CustomButton";
import Tittle from "../Inviduals/Tittle";
import Text from "../Inviduals/Text";
import Navbar from './Cliente/Navbar';
import Image from '../Inviduals/Image';
import ProductCard from "../Body/Cliente/Sample/ProductCardCliente";

const Empresa = () => {
  const [productos] = useState([
    {
      nombre: "Café de Altura",
      imagen: "https://example.com/imagen.jpg",
      sabor: "Notas de chocolate y nuez",
      aroma: "Aroma intenso y dulce",
      precioInicial: "$10,000"
    },
    {
      nombre: "Café Premium",
      imagen: "https://example.com/imagen2.jpg",
      sabor: "Notas frutales y florales",
      aroma: "Aroma suave y afrutado",
      precioInicial: "$15,000"
    },
    // Agrega más productos según sea necesario
  ]);

  return (
    <div className="bg-black bg-cover bg-center h-full w-full flex flex-col justify-between">
      <Navbar />
      <div className="flex justify-start items-center justify-center h-full">
        <div className="flex flex-col justify-center align-center max-w-lg space-y-5">
          <Tittle level="h1" size="text-4xl" color="text-custom-red" align="text-center">
            Auction active today
          </Tittle>
          <Text color="text-white" size="text-xs" align="text-center">
            the auction of the day (22/06/2024 in Colombia)
          </Text>
          <CustomButton
            title="Start"
            className="bg-[#D8354C] w-full"
          />
        </div>
      </div>
      <div className="flex justify-start items-center justify-center h-full">
        <div className="flex flex-col justify-center align-center max-w-lg space-y-5">
          <Tittle level="h1" size="text-4xl" color="text-white" align="text-center">
            Company name
          </Tittle>
          <Text color="text-white" size="text-s" align="text-center">
            Who we are?
          </Text>
        </div>
      </div>
      <div className="flex flex-col md:flex-row w-full h-full">
        <div className="flex justify-center items-center w-screen-75 md:w-2/3 h-3/4">
          <div className="w-1/2">
            <Image src="/assets/Images/Coffe.svg" alt="Logo" className="w-full h-full" />
          </div>
        </div>
        <div className="flex flex-col justify-center items-start w-full md:w-1/2 mr-4">
          <Tittle level="h1" size="text-4xl" color="text-white" align="text-left">
            Editado
          </Tittle>
          <Text color="text-white" size="text-s" align="text-left">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
            nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit
            esse cillum dolore eu fugiat nulla pariatur.
          </Text>
          <CustomButton
            title="Look at the upcoming actions"
            className="text-lg w-3/4 mt-4"
          />
        </div>
      </div>

      <div className="bg-white p-5 flex justify-center items-center h-40">
        <div className="flex flex-col md:flex-row items-center space-y-2 md:space-y-0 md:space-x-4">
          <Image src="/assets/icons/Asobagri_Logo 1.svg" alt="Logo" className='w-full h-full' />
        </div>
      </div>

      <div className="flex flex-wrap justify-center items-center mt-8">
        {productos.map((producto, index) => (
          <ProductCard key={index} product={producto} />
        ))}
      </div>
    </div>
  );
};

Empresa.propTypes = {
  title: PropTypes.string,
  links: PropTypes.arrayOf(PropTypes.shape({
    href: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
  })),
};

export default Empresa;
