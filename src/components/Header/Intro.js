'use client';
import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CustomButton from "../Inviduals/CustomButton";
import Tittle from "../Inviduals/Tittle";  // Asegúrate de que el nombre del archivo es correcto
import Text from "../Inviduals/Text";
import Navbar from './Cliente/Navbar';
import LoginModal from "./Cliente/LoginModal";  // Importa el modal de login
import secureStorage from '../../utils/secureStorage';  // Importa secureStorage
import Link from 'next/link';

const Intro = () => {
  const [isModalLoginOpen, setModalLoginOpen] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    // Verifica si el usuario ha iniciado sesión almacenando 'isLoggedIn' en secureStorage
    const storedIsLoggedIn = secureStorage.getItem('isLoggedIn');
    setIsLoggedIn(storedIsLoggedIn === 'true');
  }, []);

  const handleButtonClick = () => {
      if (!isLoggedIn) {
        setModalLoginOpen(true);
      }
  };

  return (
    <div className='bg-black p-5'>
      <div className="bg-custom-fondo bg-cover bg-center h-auto lg:h-screen-75 w-full flex flex-col justify-between overflow-hidden rounded-md">
        <Navbar />
        <div className="flex justify-start items-center ml-5 md:ml-10 h-auto lg:h-full p-5 overflow-hidden"> 
          <div className="flex flex-col items-start max-w-lg mx-5 md:mx-20 lg:mx-40 space-y-5 overflow-hidden">
            <Tittle 
              level="h1" 
              size="text-3xl md:text-4xl"  
              color="text-white" 
              align="text-left"
            >
              We connect you with the best and most renowned producers of exotic coffees in the world
            </Tittle>
            <Text 
              color="text-white" 
              size="text-base md:text-lg"  
              align="text-justify"
            >
              Under the auction modality, you will be able to access the most exclusive micro-lots of coffee; 
              which are produced for competition by the most recognized and awarded coffee growers in Colombia.
            </Text>
            {isLoggedIn ? (
              <Link href="/user/ListaSubasta" className="text-white text-bold py-2 w-full">
                <CustomButton 
                  title="Look at the upcoming auctions"
                  className="w-full sm:w-4/5 md:w-4/5 lg:w-full bg-[#D8354C] px-8 mb-8"
                />
              </Link>
            ) : (
              <CustomButton 
                title="Look at the upcoming auctions"
                onClick={handleButtonClick}
                className="w-full sm:w-4/5 md:w-4/5 lg:w-4/5 bg-[#D8354C] mb-8"
              />
            )}
          </div>
        </div>
      </div>

      <LoginModal
        isOpen={isModalLoginOpen}
        onClose={() => setModalLoginOpen(false)}
        onLoginSuccess={() => {
          setIsLoggedIn(true);
          setModalLoginOpen(false);
          secureStorage.setItem('isLoggedIn', 'true');  // Guarda el estado de inicio de sesión en secureStorage  // Redirige después del login
        }}
      />
    </div>
  );
};

Intro.propTypes = {
  title: PropTypes.string,
  links: PropTypes.arrayOf(PropTypes.shape({
    href: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
  })),
};

export default Intro;
