// components/Header/AdminLogin.js
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CustomButton from '../../Inviduals/CustomButton';
import InputField from '../../Inviduals/Input';
import Image from '../../Inviduals/Image';
import Tittle from '../../Inviduals/Tittle';
import Text from '../../Inviduals/Text';
import AuthenticationController from 'src/controllers/authController';
import secureStorage from "../../../utils/secureStorage";
import {  FaEye, FaEyeSlash } from "react-icons/fa";
import ModalAlertsError from "../../Modal/ModalAlertsError";


import { useRouter } from 'next/router';

const AdminLogin = ({ onLoginSuccess }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const authController = new AuthenticationController();
  const router = useRouter();
  const [showPassword, setShowPassword] = useState(false); // Estado para mostrar/ocultar contraseña
  const [errorMessage, setErrorMessage] = useState(null);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  const validateFields = () => {
    if (!email || !password) {
      setError('All fields are required.');
      return false;
    }
    if (!validateEmail(email)) {
      setError('Invalid e-mail address.');
      return false;
    }
    return true;
  };

  const validateEmail = (email) => {
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regex.test(email);
  };

  const handleLogin = async () => {
    if (!validateFields()) {
      return;
    }

    setLoading(true);
    setError(null);

    try {
      const response = await authController.handleLogin(email, password);
      setLoading(false);
      if (response.status === 400) {
        handleErrorConfiModal(response.data.message);
      }else if (response.message === 'OK') {
        if (response.rol !== 'Administrador') {
          setError('Invalid account. Only administrators can log in.');
          return;
        }
        secureStorage.setItem('isLoggedIn', 'true');
        secureStorage.setItem('user', JSON.stringify({ email: response.email, id: response.id_usuarios, rol: response.rol }));
        onLoginSuccess(response.email, response.id_usuarios, response.rol);
        router.push('/admin/UsuariosAdmin');
      }
    } catch (error) {
      setLoading(false);
      setError('Login error.');
    }
  };
  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  return (
    <div className="relative min-h-screen flex flex-col md:flex-row">
      <div className="absolute inset-0 bg-gradient-to-r  h-full w-full md:w-1/2 z-10"></div>
      <div className="absolute inset-0 bg-custom-fondo-login  bg-cover bg-center h-full w-full md:w-1/2 z-0"></div>

      {/* Sección de imagen con contenido en pantallas grandes */}
      <div className="relative hidden md:flex w-full md:w-1/2  bg-cover bg-center z-0 flex flex-col items-center justify-center p-5 md:p-10">
        <div className="absolute top-1 left-1">
          <Image src='/assets/Images/logo.svg' alt="Logo" />
        </div>
        <div className="flex flex-col  space-y-4 text-white ">
          <Tittle level="h1" size="text-3xl md:text-[49px]" color="text-white" align="text-left">
            Hello, friend!
          </Tittle>
          <div className="lg:w-3/4 sm:w-full">
            <Text size="text-base md:text-[16px]" color="text-white" align="text-left" weight="font-light">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
              et dolore magna aliqua. Ut enim ad minim veniam
            </Text>
          </div>
        </div>
      </div>

      {/* Formulario - Fondo negro en pantallas grandes, fondo imagen en pantallas pequeñas */}
      <div className="relative bg-black md:bg-black bg-opacity-5 p-5 md:p-8 rounded-lg w-full md:w-1/2 flex flex-col justify-center items-center z-20 min-h-screen md:min-h-auto">
        <div className="absolute top-2 left-2 md:hidden">
          <Image src='/assets/Images/logo.svg' alt='Logo' />
        </div>

        <Tittle level="h1" size="text-3xl" color="text-white" align="text-center" className="p-5">
          Sign in
        </Tittle>

        <form className="w-full px-5" onSubmit={(e) => { e.preventDefault(); handleLogin(); }}>
          <div className="text-black">
            <InputField
              type="text"
              placeholder="Email"
              value={email}
              title="Email"
              name="email"
              onChange={(e) => setEmail(e.target.value)}
              className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="mb-4 text-black relative">
            <InputField
              type={showPassword ? "text" : "password"} // Cambia el tipo según el estado
              placeholder="Password"
              value={password}
              title="Password"
              name="password"
              onChange={(e) => setPassword(e.target.value)}
              className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
            <CustomButton
              type="button"
              onClick={() => setShowPassword(!showPassword)} // Cambia el estado al hacer clic
              className="absolute right-3 top-8"
            >
              {showPassword ? <FaEye className="text-gray-500" /> : <FaEyeSlash className="text-gray-500" />}
            </CustomButton>
          </div>

          {error && <p className="text-red-500 mb-4">{error}</p>}
          <div className="flex justify-center items-center">
            <CustomButton
              title={loading ? "Loading..." : "Login"}
              onClick={handleLogin}
              className="bg-[#D8354C] text-lg w-full"
            />
          </div>
        </form>
      </div>
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
    </div>
  );
};

AdminLogin.propTypes = {
  onLoginSuccess: PropTypes.func.isRequired,
};

export default AdminLogin;
