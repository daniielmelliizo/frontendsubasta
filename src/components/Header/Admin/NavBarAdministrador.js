import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Image from "../../Inviduals/Image";
import Title from "../../Inviduals/Tittle";
import LoginModal from "../Cliente/LoginModal";
import CustomButton from "../../Inviduals/CustomButton";
import Link from 'next/link';
import { FaUser, FaSignOutAlt, FaShoppingCart, FaBars, FaTimes } from 'react-icons/fa';
import { useRouter } from 'next/router';
import secureStorage from '../../../utils/secureStorage';


const Navbaradmin = ({
  links = [
    { href: "/admin/UsuariosAdmin", label: "Usuarios" },
    { href: "/admin/ProductosAdmin", label: "Productos" },
    { href: "/admin/MuestraAdmin", label: "Muestras" },
    { href: "/admin/CategoriaAdmin", label: "Categorias" },
    { href: "/admin/SubastasAdmin", label: "Subasta" },
    { href: "/admin/ReporteAdmin", label: "Reportes" },
  ]
}) => {
  const [isModalOpen, setModalOpen] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [username, setUsername] = useState('');
  const [user, setUser] = useState('');
  const [menuOpen, setMenuOpen] = useState(false);
  const router = useRouter();

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const storedIsLoggedIn = secureStorage.getItem('isLoggedIn');
      const storedUser = secureStorage.getItem('user');
    
      const parsedUser = storedUser ? JSON.parse(storedUser) : null;

      if (
        storedIsLoggedIn === 'true' &&
        parsedUser.email &&
        parsedUser.id &&
        parsedUser.rol === 'Administrador'
      ) {
        setIsLoggedIn(true);
        setUser(parsedUser.email);
        setUsername(parsedUser.id);
      }
    }
  }, []);

  const handleLogout = () => {
    secureStorage.removeItem('isLoggedIn');
    secureStorage.removeItem('user');
    setIsLoggedIn(false);
    setUser('');
    setUsername('');
    router.push('/admin/LoginAdmin');
  };

  const handleLoginSuccess = (user, username, rol) => {
    if (rol === 'Administrador') {
      setIsLoggedIn(true);
      setUser(user);
      setUsername(username);
      secureStorage.setItem('isLoggedIn', 'true');      
      secureStorage.setItem('user', JSON.stringify({email:user, id:username, rol:rol  }));
      
    } else {
      setIsLoggedIn(false);
      setUser('');
      setUsername('');
    }
  };

  const handleCartClick = () => {
    router.push('/admin/HistoryShopAdmin');
  };

  return (
    <nav className="container mx-auto px-4 py-3 flex items-center justify-between relative z-40">
      <div className="flex items-center">
        <Image src="/assets/Images/logo.svg" alt="Logo" width={"100%"} height={"auto"} />
      </div>

      {/* Botón de menú hamburguesa para dispositivos móviles */}
      <div className="md:hidden flex items-center">
        <CustomButton
          onClick={() => setMenuOpen(!menuOpen)}
          className="text-white text-2xl focus:outline-none bg-[#D8354C]"
        >
          {menuOpen ? <FaTimes /> : <FaBars />}
        </CustomButton>
      </div>

      {/* Menú desplegable en dispositivos móviles */}
      {menuOpen && (
        <div className="md:hidden fixed top-0 right-0 w-auto max-w-xs h-auto bg-[#D8354C] text-white z-20 flex flex-col p-4 space-y-4">
          <button onClick={() => setMenuOpen(false)} className="text-white self-end text-2xl">
            <FaTimes />
          </button>

          <div className="flex flex-col space-y-2">
            {links.map((link) => (
              <Link key={link.label} href={link.href} className="text-white text-lg py-2">
                {link.label}
              </Link>
            ))}

            {isLoggedIn ? (
              <div className="flex flex-col space-y-4 mt-4">
                <Link href="/admin/HistoryShopAdmin">
                  <CustomButton className="bg-[#D8354C] text-lg flex items-center justify-center">
                    <FaShoppingCart />
                  </CustomButton>
                </Link>
                  <div className="flex items-center space-x-2">
                    <FaUser className="text-white" />
                    <Title level="h1" size="text-lg" color="text-white" align="text-center">
                      {user}
                    </Title>
                  </div>
                <CustomButton
                  title={<FaSignOutAlt />}
                  onClick={handleLogout}
                  className="bg-[#D8354C] text-lg flex items-center justify-center"
                />
              </div>
            ) : (
              <CustomButton
                title="Login"
                onClick={() => setModalOpen(true)}
                className="bg-[#D8354C] text-lg mt-4"
              />
            )}
            <LoginModal
              isOpen={isModalOpen}
              onClose={() => setModalOpen(false)}
              onLoginSuccess={handleLoginSuccess}
            />
          </div>
        </div>
      )}

      {/* Links normales para dispositivos más grandes */}
      <div className="hidden md:flex space-x-4 flex-grow text-white justify-center">
        {links.map((link) => (
          <Link key={link.label} href={link.href} className="text-white">
            {link.label}
          </Link>
        ))}
      </div>

      <div className="hidden md:flex items-center space-x-4">
        {isLoggedIn ? (
          <>
            <Link href="/admin/HistoryShopAdmin">
              <CustomButton className="bg-[#F1C40F] text-black text-lg flex items-center justify-center">
                <FaShoppingCart className='text-black' />
              </CustomButton>
            </Link>
              <div className="flex items-center space-x-2">
                <FaUser className="text-white" />
                <Title level="h1" size="text-lg" color="text-white" align="text-center">
                  {user}
                </Title>
              </div>
            <CustomButton
              title={<FaSignOutAlt />}
              onClick={handleLogout}
              className="bg-[#D8354C] text-lg flex items-center justify-center"
            />
          </>
        ) : (
          <CustomButton
            title="Login"
            onClick={() => setModalOpen(true)}
            className="bg-[#D8354C] text-lg"
          />
        )}
        <LoginModal
          isOpen={isModalOpen}
          onClose={() => setModalOpen(false)}
          onLoginSuccess={handleLoginSuccess}
        />
      </div>
    </nav>
  );
};

Navbaradmin.propTypes = {
  title: PropTypes.string,
  links: PropTypes.arrayOf(
    PropTypes.shape({
      href: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    })
  ),
};

export default Navbaradmin;
