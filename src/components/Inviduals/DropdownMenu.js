import React, { useState } from 'react';
import { FaEllipsisV } from 'react-icons/fa';
import Link from 'next/link';

const DropdownMenu = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div className="relative inline-block text-left">
      <button
        onClick={toggleMenu}
        className="flex items-center justify-center p-2 text-lg text-white bg-[#D8354C] rounded-full hover:bg-[#a7283b] focus:outline-none"
      >
        <FaEllipsisV />
      </button>
      {isOpen && (
        <div className="absolute right-0 mt-2 w-48 bg-white rounded-md shadow-lg">
          <Link href="/user/Perfiluser" className="block px-4 py-2 text-black hover:bg-gray-200">
            Profie
          </Link>
          <Link href="/user/PasswordEdit" className="block px-4 py-2 text-black hover:bg-gray-200">
            Password Edit
          </Link>
          <Link href="/user/Activas" className="block px-4 py-2 text-black hover:bg-gray-200">
            Activas
          </Link>
          <Link href="/user/HistoryAuction" className="block px-4 py-2 text-black hover:bg-gray-200">
            History auctions
          </Link>
        </div>
      )}
    </div>
  );
};

export default DropdownMenu;
