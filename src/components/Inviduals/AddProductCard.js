import React, { useState } from "react";
import { FaPlus } from "react-icons/fa";

function AddProductCard({ onClick }) {
  const [isHovered, setIsHovered] = useState(false);

  return (
    <div
      className={`p-4 rounded-lg cursor-pointer flex flex-col items-center justify-center w-80 h-96 m-4 transition-colors duration-300 ${
        isHovered ? 'bg-[#D03449]' : 'bg-white'
      }`}
      onClick={onClick}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <FaPlus
        size={100}
        className={`rounded-full transition-colors duration-300 ${
          isHovered ? 'text-[#D03449] bg-black' : 'text-white bg-[#D9D9D9]'
        }`}
      />
      <span className={`mt-4 text-lg transition-colors duration-300 ${isHovered ? 'text-white' : 'text-black'}`}>
        Add Product
      </span>
    </div>
  );
}

export default AddProductCard;
