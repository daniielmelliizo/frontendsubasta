import React from 'react';
import PropTypes from 'prop-types';
import { FaChevronDown, FaChevronUp } from 'react-icons/fa';
import CustomButton from './CustomButton';
CustomButton

const ExpandableButton = ({ isExpanded, onClick, empresaName }) => {
  return (
    <button
      className={`flex items-center justify-between rounded w-full text-left p-4 transition-colors duration-300 ${
        isExpanded ? 'bg-white text-black' : 'bg-[#2F3542] text-white'
      } hover:bg-white hover:text-black`}
      onClick={onClick}
    >
      <div>{empresaName}</div>
      <div className="flex items-center">
        {isExpanded ? (
          <FaChevronUp size={20} />
        ) : (
          <FaChevronDown size={20} />
        )}
      </div>
    </button>
  );
};

ExpandableButton.propTypes = {
  isExpanded: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  empresaName: PropTypes.string.isRequired,
};

export default ExpandableButton;
