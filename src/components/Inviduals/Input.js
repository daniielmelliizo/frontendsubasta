import React from 'react';
import PropTypes from 'prop-types';

const Input = ({
  title,
  name,
  value = '',
  onChange,
  placeholder = '',
  type = 'text',
  error = false,
  required = false
}) => {
  // Convert value to the appropriate type
  let safeValue = '';
  if (type === 'number') {
    safeValue = typeof value === 'number' ? value : Number(value) || '';
  } else if (type === 'date' || type === 'time') {
    safeValue = value || '';
  } else {
    safeValue = value || '';
  }

  return (
    <div className={`form-group ${error ? 'has-error' : ''}`}>
      <label htmlFor={name}>{title}</label>
      <input
        id={name}
        name={name}
        value={safeValue}
        onChange={onChange}
        placeholder={placeholder}
        type={type}
        required={required}
        className={`w-full text-black mb-2 p-2 border ${error ? 'border-[#D8354C]' : 'border-black'} rounded-md focus:outline-none focus:border-indigo-500`}
      />
      {error && <span className="error-message text-[#D03449]">Este campo es obligatorio</span>}
    </div>
  );
};

Input.propTypes = {
  title: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  error: PropTypes.bool,
  required: PropTypes.bool
};

export default Input;
