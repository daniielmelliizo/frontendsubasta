import React from 'react';
import PropTypes from 'prop-types';
import Image from 'next/image';

function Icons({ src, divSize }) {
    const size = parseInt(divSize); // Convert divSize to a number for proper width/height

    return (
        
            <Image
                src={src}
                alt="Icon"
                width={size} // Set the width directly
                height={size} // Set the height directly
                objectFit="contain" // Ensure it maintains aspect ratio
            />
    );
}

Icons.propTypes = {
    src: PropTypes.string.isRequired,
    divSize: PropTypes.string,
};

export default Icons;
