import React from 'react';

const Table = () => {
  return (
    <div className="bg-blue-900 text-white">
      <table className="w-full">
        <thead>
          <tr>
            <th className="px-4 py-2">Lot</th>
            <th className="px-4 py-2">Variety</th>
            <th className="px-4 py-2">Process</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className="px-4 py-2">1</td>
            <td className="px-4 py-2">VarietyA</td>
            <td className="px-4 py-2">Process A</td>
          </tr>
          <tr>
            <td className="px-4 py-2">2</td>
            <td className="px-4 py-2">Variety B</td>
            <td className="px-4 py-2">Process B</td>
          </tr>
          <tr>
            <td className="px-4 py-2">3</td>
            <td className="px-4 py-2">Variety C</td>
            <td className="px-4 py-2">Process C</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default Table;
