// src/components/ChatBox.js

import React from 'react';
import Image from './Image';

const ChatBox = ({ auctionData }) => {
  return (
    <div className="bg-white p-4 rounded-lg overflow-y-auto h-3/4 max-h-screen">
      <h3 className="text-lg font-bold mb-4">Bid History</h3>
      <ul className="space-y-3">
        {auctionData.map((item, index) => (
          <li key={index} className="flex items-center space-x-3">
            <Image src={item.user.profileImage} alt={item.user.name} className="w-10 h-10 rounded-full flex-shrink-0" />
            <span className="text-black font-semibold flex-grow"> {item.user.code}</span>
            <span className="text-green-600 flex-grow">${item.value}</span>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ChatBox;
