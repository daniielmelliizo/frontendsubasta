import React from 'react';
import PropTypes from 'prop-types';
import { FaChevronDown, FaChevronUp } from 'react-icons/fa';
import Tittle from './Tittle';
import Text from './Text';

const ExpandableButtonMuestra = ({
  isExpanded,
  onClick,
  empresaName,
  descripcion,
  valor,
  codeInvitation,
  privateData,
}) => {
  // Determine background and text colors based on the expanded state
  const backgroundColor = isExpanded ? 'bg-white' : 'bg-[#2F3542]';
  const textColor = isExpanded ? 'text-black' : 'text-white';

  return (
    <button
      className={`group flex items-center justify-between rounded w-full text-left p-2 ${backgroundColor} transition-colors hover:bg-white`}
      onClick={onClick}
    >
      <div className="flex flex-col w-full">
        <Tittle
          color={`${
            isExpanded ? 'text-black' : 'text-white group-hover:text-black'
          }`}
        >
          {empresaName || 'Name not available'}
        </Tittle>
        <Text
        size="text-xl"
          color={`${
            isExpanded ? 'text-black' : 'text-white group-hover:text-black'
          }`}
        >
          {descripcion || 'Description not available'}
        </Text>
        <Text
        size="text-xl"
          color={`${
            isExpanded ? 'text-black' : 'text-white group-hover:text-black'
          }`}
        >
          {valor || 'Value not available'}
        </Text>
      </div>
      {isExpanded ? (
        <FaChevronUp className={`${textColor} group-hover:text-black`} />
      ) : (
        <FaChevronDown className={`${textColor} group-hover:text-black`} />
      )}
    </button>
  );
};

ExpandableButtonMuestra.propTypes = {
  isExpanded: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  empresaName: PropTypes.string,
  descripcion: PropTypes.string,
  valor: PropTypes.string,
  codeInvitation: PropTypes.string,
  privateData: PropTypes.bool,
};

export default ExpandableButtonMuestra;
