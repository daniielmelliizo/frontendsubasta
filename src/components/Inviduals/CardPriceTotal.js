import React from "react";
import { FaDollarSign } from "react-icons/fa";
import Text from "./Text";

const TotalAuctionValueCard = ({ totalValue }) => {
  return (
    <div className="flex flex-col items-center p-10 bg-white rounded-lg shadow-lg">
      <FaDollarSign className="text-[#D8354C] text-4xl mb-4" />
      <div className="text-center">
        <span className="text-4xl font-bold text-gray-700">
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
          }).format(totalValue)}
        </span>
        <Text className="text-sm">Total Auction Value</Text>
      </div>
    </div>
  );
};

export default TotalAuctionValueCard;
