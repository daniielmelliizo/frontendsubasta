import React from 'react';
import Link from 'next/link';
import DropdownMenu from './DropdownMenu'; // Asegúrate de que la ruta sea correcta

const ResponsiveMenu  = ({ bgColor = 'bg-white', textColor = 'text-black' }) => {
        return (
          <nav className={`w-full ${bgColor} ${textColor} p-4`}>
            <div className="hidden md:flex justify-center space-x-8">
              <Link
                href="/user/Perfiluser"
                className={`relative px-4 py-2 ${textColor} hover:text-red-500 after:content-[''] after:block after:w-0 after:h-[2px] after:bg-red-500 after:transition-all hover:after:w-full`}
              >
                Profile
              </Link>
              <Link
                href="/user/PasswordEdit"
                className={`relative px-4 py-2 ${textColor} hover:text-red-500 after:content-[''] after:block after:w-0 after:h-[2px] after:bg-red-500 after:transition-all hover:after:w-full`}
              >
                Password Edit
              </Link>
              <Link
                href="/user/Activas"
                className={`relative px-4 py-2 ${textColor} hover:text-red-500 after:content-[''] after:block after:w-0 after:h-[2px] after:bg-red-500 after:transition-all hover:after:w-full`}
              >
                Activas
              </Link>
              <Link
                href="/user/HistoryAuction"
                className={`relative px-4 py-2 ${textColor} hover:text-red-500 after:content-[''] after:block after:w-0 after:h-[2px] after:bg-red-500 after:transition-all hover:after:w-full`}
              >
                History Auctions
              </Link>
            </div>
      
            {/* Dropdown para pantallas pequeñas */}
            <div className="md:hidden flex justify-end p-4">
              <DropdownMenu />
            </div>
          </nav>
        );
      };
      
      export default ResponsiveMenu;
      

