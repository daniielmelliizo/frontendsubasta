// src/Inviduals/Graphic.js

import React from 'react';
import { Line } from 'react-chartjs-2';
import { Chart as ChartJS, CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend } from 'chart.js';

// Registrar los componentes necesarios de Chart.js
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

const Graphic = ({ auctionData }) => {
  // Preparar los datos para el gráfico
  const chartData = {
    labels: auctionData.map(item => item.user.code), // Etiquetas en el eje X con los códigos de usuario
    datasets: [
      {
        label: 'Auction Value',
        data: auctionData.map(item => item.value), // Valores en el eje Y
        fill: false,
        backgroundColor: 'rgba(75, 192, 192, 0.2)',
        borderColor: 'rgba(75, 192, 192, 1)',
      },
    ],
  };

  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
      title: {
        display: true,
        text: 'Auction Progress',
      },
    },
    scales: {
      x: {
        title: {
          display: true,
          text: 'User Code', // Etiqueta del eje X
        },
      },
      y: {
        title: {
          display: true,
          text: 'Value', // Etiqueta del eje Y
        },
      },
    },
  };

  return <Line data={chartData} options={options} />;
};

export default Graphic;
