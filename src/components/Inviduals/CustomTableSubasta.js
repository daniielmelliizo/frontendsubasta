// CustomTableSubasta.jsx
import React from "react";
import Text from "./Text";

const CustomTableSubasta = ({ data }) => {
  return (
    <div className="mb-4 bg-[#2F3542] rounded-md p-3">
      <h3 className="text-lg text-white mb-2 pl-4">{data.categoria}</h3>
      
      {/* Tabla para pantallas grandes */}
      <div className="hidden md:block">
        <table className="w-full table-auto mb-4">
          <thead className="text-gray-200">
            <tr>
              <th className="w-1/12 px-2 py-1 text-center">Lot</th>
              <th className="w-1/12 px-2 py-1 text-center">Product name</th>
              <th className="w-1/12 px-2 py-1 text-center">Process</th>
              <th className="w-1/12 px-2 py-1 text-center">Increment</th>
              <th className="w-1/12 px-2 py-1 text-center">Initial price</th>
              <th className="w-1/12 px-2 py-1 text-center">Final price</th>
              <th className="w-1/12 px-2 py-1 text-center">Status</th>
            </tr>
          </thead>
          <tbody>
            {data.auctions.map((auction) => (
              <tr key={auction.id}>
                <td className="px-2 py-1 text-center">{auction.product.lote}</td>
                <td className="px-2 py-1 text-center">{auction.product.nombre}</td>
                <td className="px-2 py-1 text-center">{auction.product.process}</td>
                <td className="px-2 py-1 text-center">{auction.product.increment}</td>
                <td className="px-2 py-1 text-center">{auction.product.precioInicial}</td>
                <td className="px-2 py-1 text-center">{auction.product.precioFinal}</td>
                <td className="px-2 py-1 text-center">{auction.estado}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      
      {/* Vista en formato de tarjetas para pantallas pequeñas */}
      <div className="block md:hidden">
        {data.auctions.map((auction) => (
          <div key={auction.id} className="bg-[#3B3F51] rounded-lg p-4 mb-3">
            <div className="mb-2">
              <Text className="text-white" bold>Lot:</Text> {auction.product.lote}
            </div>
            <div className="mb-2">
              <Text className="text-white" bold>Product name:</Text> {auction.product.nombre}
            </div>
            <div className="mb-2">
              <Text className="text-white" bold>Process:</Text> {auction.product.process}
            </div>
            <div className="mb-2">
              <Text className="text-white" bold>Increment:</Text> {auction.product.increment}
            </div>
            <div className="mb-2">
              <Text className="text-white" bold>Initial price:</Text> {auction.product.precioInicial}
            </div>
            <div className="mb-2">
              <Text className="text-white" bold>Final price:</Text> {auction.product.precioFinal}
            </div>
            <div className="mb-2">
              <Text className="text-white" bold>Status:</Text> {auction.estado}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default CustomTableSubasta;
