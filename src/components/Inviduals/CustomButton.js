import React from 'react';
import PropTypes from 'prop-types';
import Image from 'next/image'; // Import the Next.js Image component

const CustomButton = ({ title, src, alt, onClick = () => {}, className, children, ...props }) => {
    return (
        <button
            {...props}
            className={`flex items-center justify-center py-2 px-2 rounded ${className}`}
            onClick={onClick}
        >
            {src && (
                <div className="h-12 w-12 mr-2 relative">
                    <Image src={src} alt={alt || 'button icon'} layout="fill" objectFit="contain" />
                </div>
            )}
            {children ? children : title}
        </button>
    );
};

CustomButton.propTypes = {
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    src: PropTypes.string,
    alt: PropTypes.string,
    onClick: PropTypes.func,
    className: PropTypes.string,
    children: PropTypes.node,
};

export default CustomButton;
