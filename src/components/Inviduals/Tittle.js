// components/Title.js
import React from 'react';
import PropTypes from 'prop-types';

const Tittle = ({
    level = 'h1',
    size = 'text-2xl',
    color = 'text-black',
    align = 'text-left',
    children,
}) => {
    const Tag = level;

    return (
        <Tag className={`${size} ${color} ${align} font-bold uppercase`}>
            {children}
        </Tag>
    );
};

Tittle.propTypes = {
    level: PropTypes.string,
    size: PropTypes.string,
    color: PropTypes.string,
    align: PropTypes.string,
    
};

export default Tittle;
