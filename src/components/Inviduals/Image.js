import React from 'react';
import PropTypes from 'prop-types';

function Image({ src, width = '100%', height = 'auto' }) {
    return (
        <img
            className="object-contain"
            src={src}
            alt="Café"
            style={{ width, height, objectFit: 'contain' }} // Using the props directly in the style
        />
    );
}

Image.propTypes = {
    src: PropTypes.string.isRequired,
    width: PropTypes.string,
    height: PropTypes.string,
};

export default Image;
