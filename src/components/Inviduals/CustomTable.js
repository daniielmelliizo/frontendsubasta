import React from 'react';

const CustomTable = ({ headers, data, configureRow, configureActions }) => {
  return (
    <div className="w-full p-4">
      {/* Tabla solo para pantallas grandes */}
      <div className="hidden lg:block overflow-x-auto">
        <table
          className="w-full text-white rounded-lg"
          style={{ borderSpacing: '0 10px', borderCollapse: 'separate' }}
        >
          <thead className="bg-black text-white rounded-lg">
            <tr className="rounded-lg">
              {headers.map((header, index) => (
                <th
                  key={index}
                  className="px-4 py-3 text-left text-xs font-medium uppercase tracking-wider"
                  scope="col"
                >
                  {header}
                </th>
              ))}
              {configureActions && (
                <th
                  className="px-4 py-3 text-left text-xs font-medium uppercase tracking-wider"
                  scope="col"
                >
                  Actions
                </th>
              )}
            </tr>
          </thead>
          <tbody>
            {data.map((rowData, index) => {
              const row = configureRow(rowData); // Configuración específica para row
              return (
                <tr
                  key={index}
                  className="bg-transparent text-[#F2F2F2] rounded-lg mb-2"
                >
                  {Object.values(row).map((cell, idx) => (
                    <td
                      key={idx}
                      className={`px-4 py-4 ${
                        idx === 0 ? 'rounded-l-lg' : ''
                      } ${
                        idx === Object.values(row).length - 1
                          ? 'rounded-r-lg'
                          : ''
                      } bg-[#454B58]`}
                    >
                      {cell}
                    </td>
                  ))}
                  {configureActions && (
                    <td className="px-4 py-4 bg-black text-white flex items-center justify-center space-x-4">
                      {configureActions(rowData)}
                    </td>
                  )}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>

      {/* Tarjetas para pantallas pequeñas y medianas */}
      <div
        className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:hidden gap-4"
      >
        {data.map((rowData, index) => {
          const row = configureRow(rowData);
          return (
            <div
              key={index}
              className="bg-[#F2F2F2] shadow-md rounded-lg p-4 text-gray-800"
            >
              {Object.entries(row).map(([key, value], idx) => (
                <div key={idx} className="mb-2">
                  <span className="font-bold text-gray-900">
                    {headers[idx]}:{' '}
                  </span>
                  <span className="text-gray-700">{value}</span>
                </div>
              ))}
              {configureActions && (
                <div className="flex flex-col sm:flex-row justify-end items-center space-y-2 sm:space-y-0 sm:space-x-4 mt-4">
                  {configureActions(rowData)}
                </div>
              )}
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default CustomTable;
