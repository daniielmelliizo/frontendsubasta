import React from 'react';
import PropTypes from 'prop-types';

const Textdata = ({ children, color = 'black', size = '16px' }) => {
  return (
    <p style={{ color, fontSize: size }}>
      {children}
    </p>
  );
};

Textdata.propTypes = {
  children: PropTypes.node.isRequired,
  color: PropTypes.string,
  size: PropTypes.string,
};

export default Textdata;
