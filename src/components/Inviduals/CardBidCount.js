import React from "react";
import { FaMoneyBill } from "react-icons/fa"; 

const TotalBidsCard = ({ totalBids }) => {
  return (
    <div className="flex flex-col items-center p-10 bg-white rounded-lg shadow-lg ">
      <FaMoneyBill className="text-[#D8354C] text-4xl mb-4" /> 
      <div className="text-center">
        <p className="text-4xl font-bold text-gray-700">{totalBids}</p>
        <p className="text-sm text-gray-500">Total Bids</p>
      </div>
    </div>
  );
};

export default TotalBidsCard;
