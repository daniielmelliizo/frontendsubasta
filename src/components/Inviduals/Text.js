import React from 'react';
import PropTypes from 'prop-types';

const Text = ({
    size = 'text-sm', // Default size using Tailwind CSS class
    color = 'text-black', // Default color using Tailwind CSS class
    align = 'text-left', // Default alignment using Tailwind CSS class
    children,
}) => {
    return (
        <p className={`${size} ${color} ${align}`}>
            {children}
        </p>
    );
};

Text.propTypes = {
    size: PropTypes.string,
    color: PropTypes.string,
    align: PropTypes.string,
    children: PropTypes.node.isRequired,
};

export default Text;
