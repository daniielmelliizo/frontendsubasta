import React, { useState, useEffect } from 'react';
import { FaRegClock } from 'react-icons/fa'; // Logo de reloj de react-icons

const CountTimer = ({ startDate, endDate }) => {
  const [timeLeft, setTimeLeft] = useState(0);
  const [timerState, setTimerState] = useState("countdown"); // Puede ser "countdown" o "countup"

  const calculateTimeLeft = (startDate, endDate) => {
    const now = new Date();
    const start = new Date(startDate);
    const end = new Date(endDate);

    if (now < start) {
      // Si la subasta aún no ha comenzado
      setTimerState("countdown");
      return Math.max((start - now) / 1000, 0);
    } else if (now > end) {
      // Si la subasta ya ha terminado
      setTimerState("countup");
      return Math.max((now - end) / 1000, 0);
    } else {
      // Si la subasta está en curso
      setTimerState("countdown");
      return Math.max((end - now) / 1000, 0);
    }
  };

  useEffect(() => {
    const interval = setInterval(() => {
      setTimeLeft(prevTime => Math.max(prevTime - 1, 0));
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    const time = calculateTimeLeft(startDate, endDate);
    setTimeLeft(time);
  }, [startDate, endDate]);

  const formatTime = (time) => {
    const days = Math.floor(time / (3600 * 24));
    const hours = Math.floor((time % (3600 * 24)) / 3600);
    const minutes = Math.floor((time % 3600) / 60);
    const seconds = Math.floor(time % 60);
    return { days, hours, minutes, seconds };
  };

  const { days, hours, minutes, seconds } = formatTime(timeLeft);

  return (
    <div className="flex flex-col items-center p-6 bg-white rounded-lg shadow-lg">
      <FaRegClock className="text-[#D8354C] text-4xl mb-4" />
      <div className="flex justify-center space-x-4">
        {days > 0 && (
          <div className="text-center">
            <span className="text-4xl font-bold text-gray-700">{days}</span>
            <p className="text-sm text-gray-500">Days</p>
          </div>
        )}
        <div className="text-center">
          <span className="text-4xl font-bold text-gray-700">{String(hours).padStart(2, '0')}</span>
          <p className="text-sm text-gray-500">Hours</p>
        </div>
        <div className="text-center">
          <span className="text-4xl font-bold text-gray-700">{String(minutes).padStart(2, '0')}</span>
          <p className="text-sm text-gray-500">Minutes</p>
        </div>
        <div className="text-center">
          <span className="text-4xl font-bold text-gray-700">{String(seconds).padStart(2, '0')}</span>
          <p className="text-sm text-gray-500">Seconds</p>
        </div>
      </div>
      <p className="text-gray-600 mt-4">
        {timerState === "countdown" ? "Time to auction " : "TIME TO FINISH"}
      </p>
    </div>
  );
};

export default CountTimer;
