import React, { useState } from 'react';

const NumericInput = ({ className }) => {
    const [value, setValue] = useState(0);

    

    const handleChange = (event) => {
        const newValue = parseInt(event.target.value, 10);
        if (!isNaN(newValue)) {
            setValue(newValue);
        }
    };

    return (
        <div className={`flex items-center ${className}`}>
            
            <input
                type="number"
                className="w-full text-center"
                value={value}
                onChange={handleChange}
            />
            
        </div>
    );
};

export default NumericInput;
