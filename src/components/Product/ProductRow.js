import React from "react";
import Input from "../Inviduals/Input";
import { FaArrowLeft, FaArrowRight } from "react-icons/fa";

const ProductRow = ({
  selectedProduct,
  tempFormData,
  handleChange,
  error,
  currentScreen,
  categorias,
  previewImages,
  handleUpdate,
  handleRegistro,
  handlePrevious,
  handleNext,
  handleRemoveImage,
}) => {
  return (
    <form
      onSubmit={selectedProduct ? handleUpdate : handleRegistro}
      className="w-full space-y-4"
    >
      {currentScreen === 0 && (
        <>
          <Input
            title="Name"
            name="nombre"
            value={tempFormData.nombre}
            onChange={handleChange}
            placeholder="Name"
            error={error.nombre}
            required
          />
          <Input
            title="Lot"
            name="lote"
            value={tempFormData.lote}
            onChange={handleChange}
            placeholder="Lot"
            error={error.lote}
            required
          />
          <Input
            title="Country"
            name="pais"
            value={tempFormData.pais}
            onChange={handleChange}
            placeholder="Country"
            error={error.pais}
            required
          />
          <Input
            title="Initial price $USD"
            name="precioInicial"
            value={tempFormData.precioInicial}
            onChange={handleChange}
            placeholder="Initial price"
            type="number"
            error={error.precioInicial}
            required
          />
        </>
      )}
      {currentScreen === 1 && (
        <>
          <Input
            title="Departament"
            name="departamento"
            value={tempFormData.departamento}
            onChange={handleChange}
            placeholder="Departament"
            error={error.departamento}
            required
          />
          <Input
            title="City"
            name="ciudad"
            value={tempFormData.ciudad}
            onChange={handleChange}
            placeholder="City"
            error={error.ciudad}
            required
          />
          <Input
            title="Producer Farm"
            name="fincaProductor"
            value={tempFormData.fincaProductor}
            onChange={handleChange}
            placeholder="Producer Farm"
            error={error.fincaProductor}
            required
          />
          <Input
            title="Score SCA"
            name="score"
            value={tempFormData.score}
            onChange={handleChange}
            placeholder="Score SCA"
            type="number"
            error={error.score}
            required
          />
        </>
      )}
      {currentScreen === 2 && (
        <>
          <Input
            title="Descriptors"
            name="descriptores"
            value={tempFormData.descriptores}
            onChange={handleChange}
            placeholder="Descriptors"
            error={error.descriptores}
            required
          />
          <Input
            title="Variety"
            name="variedad"
            value={tempFormData.variedad}
            onChange={handleChange}
            placeholder="Variety"
            error={error.variedad}
            required
          />
          <Input
            title="Weight"
            name="peso"
            value={tempFormData.peso}
            onChange={handleChange}
            placeholder="Weight"
            type="number"
            error={error.peso}
            required
          />
          <select
            title="Unit Weight"
            name="unidadPeso"
            value={tempFormData.unidadPeso}
            onChange={handleChange}
            required
            className={`w-full text-black mb-2 p-2 border ${
              error.unidadPeso ? "border-red-500" : "border-gray-300"
            } rounded-md focus:outline-none focus:border-indigo-500`}
          >
            <option value="" disabled>
            Select Weight Unit
            </option>
            <option value="Kg">Kg</option>
            <option value="Lb">Lb</option>
          </select>
          <Input
            title="Process"
            name="process"
            value={tempFormData.process}
            onChange={handleChange}
            placeholder="Process"
            error={error.process}
            required
          />
        </>
      )}
      {currentScreen === 3 && (
        <>
          <Input
            title="Increment"
            name="increment"
            value={tempFormData.increment}
            onChange={handleChange}
            placeholder="Increment"
            error={error.increment}
            type="number"
            required
          />
          <select
            title="Private"
            name="private"
            value={tempFormData.private}
            onChange={handleChange}
            className="block w-full px-4 py-2 mt-2 text-black bg-white border rounded-md focus:border-blue-500 focus:outline-none focus:ring"
          >
            <option value="">¿Private?</option>
            <option value="true">Auction</option>
            <option value="false">Buy Direct</option>
          </select>
          <select
            name="categoriaId"
            value={tempFormData.categoriaId}
            onChange={handleChange}
            className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
          >
            <option value="">Selecciona una categoría</option>
            {categorias.map((categoria) => (
              <option key={categoria.id} value={categoria.id} className="text-black">
                {categoria.nombre}
              </option>
            ))}
          </select>
          <Input
            title="Images"
            name="imagenes"
            accept="image/*"
            onChange={handleChange}
            placeholder="Images"
            type="file"
            multiple
            error={error.imagen}
          />
          <div className="flex flex-wrap gap-2 mt-2">
            {previewImages.map((image, index) => (
              <div key={index} className="relative">
                <img src={image} alt={`Preview ${index + 1}`} className="w-32 h-32 object-cover rounded-lg" />
                <button
                  type="button"
                  className="absolute top-0 right-0 bg-red-500 text-white rounded-full p-1"
                  onClick={() => handleRemoveImage(index)}
                >
                  X
                </button>
              </div>
            ))}
          </div>
        </>
      )}
      <div className="flex justify-between mt-6">
        <button
          onClick={handlePrevious}
          disabled={currentScreen === 0}
          className={`flex items-center space-x-2 px-4 py-2 rounded ${
            currentScreen === 0 ? "bg-gray-300 cursor-not-allowed" : "bg-[#F1C40F] text-black hover:bg-[#F1C40F]"
          }`}
        >
          <FaArrowLeft />
          <span>Previous</span>
        </button>
        {currentScreen === 3 ? (
          <button
            type="submit"
            className="flex items-center space-x-2 px-4 py-2 rounded bg-[#D03449] text-white hover:[#D03449]"
          >
            <span>{selectedProduct ? "Update" : "Register"}</span>
          </button>
        ) : (
          <button
            onClick={handleNext}
            className="flex items-center space-x-2 px-4 py-2 rounded bg-[#D03449] text-white hover:bg-[#D03449]"
          >
            <span>Next</span>
            <FaArrowRight />
          </button>
        )}
      </div>
    </form>
  );
};

export default ProductRow;
