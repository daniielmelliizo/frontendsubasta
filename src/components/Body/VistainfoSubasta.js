import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Tittle from "../Inviduals/Tittle";
import Text from "../Inviduals/Text";
import SubastaController from "src/controllers/SubastaController";
import SubastaService from "src/aplication/services/SubastaService";
import CountTimer from "../Inviduals/CardReloj";
import TotalBidsCard from "../Inviduals/CardBidCount";
import TotalAuctionValueCard from "../Inviduals/CardPriceTotal";

const subastaService = new SubastaService();
const subastaController = new SubastaController(subastaService);

const VistaInfoSubasta = () => {
  const router = useRouter();
  const { auctions, username, auctionsFin } = router.query;
  let auction = null;
  let user = null;
  let auctionFin = null;

  try {
    auction = auctions ? JSON.parse(auctions) : null;
    user = username ? JSON.parse(username) : null;
    auctionFin = auctionsFin ? JSON.parse(auctionsFin) : null;
  } catch (error) {}

  const [auctionDetails, setAuctionDetails] = useState(null);
  const [error, setError] = useState(null);
  const [activeTable, setActiveTable] = useState("auctions");
  const [totalFinalPrice, setTotalFinalPrice] = useState(0);
  const [totalFinalPriceBid, setTotalFinalPriceBid] = useState(0);
  const [auctionDetailsFinali, setAuctionDetailsFinali] = useState(null);

  useEffect(() => {
    if (router.isReady && auction && user) {
      fetchAuctionDetails(auction.muestraId, user);
    }

    if (auctionsFin) {
      fetchAuctionDetailsFina(auctionFin);
    }
  }, [router.isReady, auction, user, auctionsFin]);

  const fetchAuctionDetails = async (muestraId, user, auctionFin) => {
    try {
      const response = await subastaController.handleGetAuctionByUserMuestra(
        muestraId,
        user
      );

      setAuctionDetails(response.data);
      setError(null);

      // Cálculo de la suma de los precios finales al obtener los detalles.
      let total = 0;
      Object.keys(response.data).forEach((muestraKey) => {
        Object.keys(response.data[muestraKey]).forEach((categoria) => {
          response.data[muestraKey][categoria].forEach((auctionItem) => {
            // Convertimos a número antes de sumar
            const finalPrice = parseFloat(auctionItem.product.precioFinal) || 0;
            total += finalPrice;
          });
        });
      });
      setTotalFinalPrice(total);
    } catch (err) {
      setError(`Error fetching auction details: ${err.message}`);
    }
  };

  const fetchAuctionDetailsFina = async (auctionFin) => {
    try {
      const firstKey = Object.keys(auctionFin)?.[0]; // Toma la primera clave
      const sampleId = auctionFin[firstKey]?.[0]?.sample?.id || null;
      const auction = auctionFin[firstKey]?.[0] || null;

      const response = await subastaController.handleGetAuctionByAuctionFini(
        sampleId
      );
      console.log("que me trae response", response);
      setAuctionDetailsFinali(auction);
      setAuctionDetails(response.data);
      setError(null);

      // Cálculo de la suma de los precios finales al obtener los detalles.
      let total = 0;
      let bidTotal = 0;

      Object.keys(response.data).forEach((muestraKey) => {
        Object.keys(response.data[muestraKey]).forEach((categoria) => {
          response.data[muestraKey][categoria].forEach((auctionItem) => {
            // Convertimos a número antes de sumar
            const finalPrice = parseFloat(auctionItem.product.precioFinal) || 0;
            const finalPriceBid = parseFloat(auctionItem.totalBidsCount) || 0;
            total += finalPrice;
            bidTotal += finalPriceBid;
          });
        });
      });

      setTotalFinalPrice(total);
      setTotalFinalPriceBid(bidTotal);
    } catch (err) {
      setError(`Error fetching auction details: ${err.message}`);
    }
  };
  if (!auctionDetails) {
    return <p>Cargando detalles de la subasta...</p>;
  }
  return (
    <div className="p-6 w-full lg:w-3/4 text-white shadow-md rounded-lg">
      {error && <p className="text-red-500 mb-4">{error}</p>}
      {Object.keys(auctionDetails).map((muestraKey, muestraIndex) => (
        <div key={muestraIndex} className="mb-8">
          <Tittle
            level="h1"
            size="text-3xl"
            color="text-white"
            align="text-center"
          >
            {muestraKey}
          </Tittle>

          <div
            className={`flex flex-row items-stretch space-x-4 mb-8 ${
              auction?.estado !== "Finalizada" &&
              auctionDetailsFinali?.estado !== "Finalizada"
                ? "justify-center"
                : ""
            }`}
          >
            <div className="w-1/3">
              <CountTimer
                startDate={
                  auction?.fechaInicio ?? auctionDetailsFinali?.fechaInicio
                }
                endDate={auction?.horaFin ?? auctionDetailsFinali?.horaFin}
              />
            </div>
            {(auction?.estado === "Finalizada" ||
              auctionDetailsFinali?.estado === "Finalizada") && (
              <>
                <div className="w-1/3">
                  <TotalBidsCard totalBids={totalFinalPriceBid} />
                </div>
                <div className="w-1/3">
                  <TotalAuctionValueCard totalValue={totalFinalPrice} />
                </div>
              </>
            )}
          </div>

          <div className="flex justify-between mb-4">
            <button
              className={`w-1/2 mr-2 px-4 py-2 bg-[#D8354C] text-white rounded ${
                activeTable === "auctions"
                  ? "bg-black text-[#D8354C] border border-[#D8354C]"
                  : ""
              }`}
              onClick={() => setActiveTable("auctions")}
            >
              Auctions
            </button>
            <button
              className={`w-1/2 ml-2 px-4 py-2 bg-[#D8354C] text-white rounded ${
                activeTable === "details"
                  ? "bg-black text-[#D8354C] border border-[#D8354C]"
                  : ""
              }`}
              onClick={() => setActiveTable("details")}
            >
              Details
            </button>
          </div>

          {activeTable === "auctions" ? (
            <div>
              <table
                className="hidden lg:table w-full mb-8  text-white rounded-lg shadow-lg"
                style={{ borderSpacing: "0 10px", borderCollapse: "separate" }}
              >
                <thead className="bg-black rounded-lg">
                  <tr>
                    <th className="px-4 py-3 text-left text-xs font-medium uppercase tracking-wider">
                      Name
                    </th>
                    <th className="px-2 py-3 text-left text-xs font-medium uppercase tracking-wider">
                      Category
                    </th>
                    <th className="px-4 py-3 text-left text-xs font-medium uppercase tracking-wider">
                      Score
                    </th>
                    <th className="px-2 py-3 text-left text-xs font-medium uppercase tracking-wider">
                      Initial price
                    </th>
                    <th className="px-2 py-3 text-left text-xs font-medium uppercase tracking-wider">
                      Final price
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {auctionDetails[muestraKey] &&
                    Object.keys(auctionDetails[muestraKey]).flatMap(
                      (categoria) =>
                        auctionDetails[muestraKey][categoria].map(
                          (auctionItem, auctionIndex) => (
                            <tr
                              key={auctionIndex}
                              className="bg-[#2F3542] text-[#F2F2F2]"
                            >
                              <td className="px-4 py-2 border-b">
                                {auctionItem.product.nombre || "N/A"}
                              </td>
                              <td className="px-4 py-2 border-b">
                                {categoria}
                              </td>
                              <td className="px-4 py-2 border-b">
                                {auctionItem.product.score || "N/A"}
                              </td>
                              <td className="px-4 py-2 border-b">
                                {auctionItem.product.precioInicial || "N/A"} USD
                              </td>
                              <td className="px-4 py-2 border-b">
                                {auctionItem.product.precioFinal || "N/A"} USD
                              </td>
                            </tr>
                          )
                        )
                    )}
                </tbody>
              </table>

              {/* Vista en cards para pantallas pequeñas */}
              <div className="lg:hidden grid grid-cols-1 gap-4">
                {auctionDetails[muestraKey] &&
                  Object.keys(auctionDetails[muestraKey]).flatMap((categoria) =>
                    auctionDetails[muestraKey][categoria].map(
                      (auctionItem, auctionIndex) => (
                        <div
                          key={auctionIndex}
                          className="bg-[#2F3542] p-4 rounded-lg shadow-lg text-white"
                        >
                          <p>
                            <strong>Name:</strong>{" "}
                            {auctionItem.product.nombre || "N/A"}
                          </p>
                          <p>
                            <strong>Category:</strong> {categoria}
                          </p>
                          <p>
                            <strong>Score:</strong>{" "}
                            {auctionItem.product.score || "N/A"}
                          </p>
                          <p>
                            <strong>Initial price:</strong>{" "}
                            {auctionItem.product.precioInicial || "N/A"} USD
                          </p>
                          <p>
                            <strong>Final price:</strong>{" "}
                            {auctionItem.product.precioFinal || "N/A"} USD
                          </p>
                        </div>
                      )
                    )
                  )}
              </div>
            </div>
          ) : (
            // Similar para la tabla de 'details'
            <div>
              <table
                className="hidden lg:table w-full mb-8  text-white rounded-lg shadow-lg"
                style={{ borderSpacing: "0 10px", borderCollapse: "separate" }}
              >
                <thead className="bg-black rounded-lg">
                  <tr>
                    <th className="px-4 py-3 text-left text-xs font-medium uppercase tracking-wider">
                      Name
                    </th>
                    <th className="px-2 py-3 text-left text-xs font-medium uppercase tracking-wider">
                      Variety
                    </th>
                    <th className="px-4 py-3 text-left text-xs font-medium uppercase tracking-wider">
                      Weight
                    </th>
                    <th className="px-2 py-3 text-left text-xs font-medium uppercase tracking-wider">
                      Process
                    </th>
                    <th className="px-2 py-3 text-left text-xs font-medium uppercase tracking-wider">
                      Producing Farm
                    </th>
                    <th className="px-2 py-3 text-left text-xs font-medium uppercase tracking-wider">
                      Final price
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {auctionDetails[muestraKey] &&
                    Object.keys(auctionDetails[muestraKey]).flatMap(
                      (categoria) =>
                        auctionDetails[muestraKey][categoria].map(
                          (auctionItem, auctionIndex) => (
                            <tr
                              key={auctionIndex}
                              className="bg-[#2F3542] text-[#F2F2F2]"
                            >
                              <td className="px-4 py-2 border-b">
                                {auctionItem.product.nombre || "N/A"}
                              </td>
                              <td className="px-4 py-2 border-b">
                                {auctionItem.product.variedad || "N/A"}
                              </td>
                              <td className="px-4 py-2 border-b">{`${
                                auctionItem.product.peso || "N/A"
                              } ${auctionItem.product.unidadPeso || ""}`}</td>
                              <td className="px-4 py-2 border-b">
                                {auctionItem.product.process || "N/A"}
                              </td>
                              <td className="px-4 py-2 border-b">
                                {auctionItem.product.fincaProductor || "N/A"}
                              </td>
                              <td className="px-4 py-2 border-b">
                                {auctionItem.product.precioFinal || "N/A"} USD
                              </td>
                            </tr>
                          )
                        )
                    )}
                </tbody>
              </table>

              {/* Cards para pantallas pequeñas */}
              <div className="lg:hidden grid grid-cols-1 gap-4">
                {auctionDetails[muestraKey] &&
                  Object.keys(auctionDetails[muestraKey]).flatMap((categoria) =>
                    auctionDetails[muestraKey][categoria].map(
                      (auctionItem, auctionIndex) => (
                        <div
                          key={auctionIndex}
                          className="bg-[#2F3542] p-4 rounded-lg shadow-lg text-white"
                        >
                          <p>
                            <strong>Name:</strong>{" "}
                            {auctionItem.product.nombre || "N/A"}
                          </p>
                          <p>
                            <strong>Variety:</strong>{" "}
                            {auctionItem.product.variedad || "N/A"}
                          </p>
                          <p>
                            <strong>Weight:</strong>{" "}
                            {`${auctionItem.product.peso || "N/A"} ${
                              auctionItem.product.unidadPeso || ""
                            }`}
                          </p>
                          <p>
                            <strong>Process:</strong>{" "}
                            {auctionItem.product.process || "N/A"}
                          </p>
                          <p>
                            <strong>Producing Farm:</strong>{" "}
                            {auctionItem.product.fincaProductor || "N/A"}
                          </p>
                          <p>
                            <strong>Final price:</strong>{" "}
                            {auctionItem.product.precioFinal || "N/A"} USD
                          </p>
                        </div>
                      )
                    )
                  )}
              </div>
            </div>
          )}
        </div>
      ))}
    </div>
  );
};

export default VistaInfoSubasta;
