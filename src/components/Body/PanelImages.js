import Icons from '../Inviduals/Icons';
import Text from '../Inviduals/Text';
import Tittle from '../Inviduals/Tittle';
import Image from '../Inviduals/Image';

function Images() {
    return (
        <div className="relative bg-black mx-auto h-auto lg:h-[680px] flex items-center justify-center ">
            {/* Imágenes de fondo para pantallas grandes y medianas */}
            <div className="absolute inset-0 hidden md:flex items-center justify-center">
                <div className="absolute w-full h-full z-0 overflow-hidden flex items-center justify-center">
                    <Image
                        src="/assets/Images/fondo_curva.svg"
                        width="100%"
                        height="auto"
                        alt=""
                    />
                </div>
                <div className="absolute left-0 top-0 bottom-0 flex items-center justify-center">
                    <Image
                        src="/assets/Images/granos_izquierda.svg"
                        width="90%"
                        height="50%"
                        alt="Granos a la izquierda"
                    />
                </div>
                <div className="absolute right-0 top-0 bottom-0 flex items-center justify-center">
                    <Image
                        src="/assets/Images/grano_derecha.svg"
                        width="90%"
                        height="50%"
                        alt="Granos a la derecha"
                    />
                </div>
            </div>

            {/* Contenido responsive con breakpoints personalizados */}
            <div className="relative z-10 items-center w-11/12 md:w-2/3 mx-auto overflow-hidden z-10">

                {/* Configuración para pantallas menores a 650px */}
                <div className="block sm:hidden grid grid-cols-1 gap-8 p-5">
                    {/* Card 1 */}
                    <div className="flex flex-row justify-center sm:justify-start items-center bg-[#D8354C] p-5 rounded-md" style={{ height: 'auto' }}>
                        <div className="flex justify-center items-center mb-5 rounded-full overflow-hidden bg-yellow-400 w-24 h-24 sm:w-32 sm:h-32">
                            <Icons src="/assets/icons/Exclusivity.svg" divSize='60%' />
                        </div>
                        <div className="flex flex-col items-center justify-center p-5 flex-grow">
                            <Tittle level="h1" size="text-xl" color="text-white" align="text-center" className="font-bold">
                                Exclusivity
                            </Tittle>
                            <Text size="text-sm" color="text-white" align="text-center" weight="font-light">
                                Access to unique, exotic and high-quality coffee.
                            </Text>
                        </div>
                    </div>

                    {/* Card 2 */}
                    <div className="flex flex-row justify-center items-center bg-white p-5 rounded-md" style={{ height: 'auto' }}>
                        <div className="flex justify-center items-center mb-5 rounded-full overflow-hidden bg-yellow-400 w-24 h-24 sm:w-32 sm:h-32">
                            <Icons src="/assets/icons/Transparency.svg" divSize="60%" />
                        </div>
                        <div className="flex flex-col items-center justify-center p-5 flex-grow">
                            <Tittle level="h1" size="text-xl" color="text-[#D8354C]" align="text-center" className="font-bold">
                                Transparency and traceability
                            </Tittle>
                            <Text size="text-sm" color="text-[#D8354C]" align="text-center" weight="font-light">
                                BeCoffee auctions provide transparency, confidence, and authenticity in product traceability.
                            </Text>
                        </div>
                    </div>

                    {/* Card 3 */}
                    <div className="flex flex-row justify-center items-center bg-[#D8354C] p-5 rounded-md" style={{ height: 'auto' }}>
                        <div className="flex justify-center items-center mb-5 rounded-full overflow-hidden bg-yellow-400 w-24 h-24 sm:w-32 sm:h-32">
                            <Icons src="/assets/icons/Trust.svg" divSize="60%" />
                        </div>
                        <div className="flex flex-col items-center justify-center p-5 flex-grow">
                            <Tittle level="h1" size="text-xl" color="text-white" align="text-center" className="font-bold">
                                Trust partnerships
                            </Tittle>
                            <Text size="text-sm" color="text-white" align="text-center" weight="font-light">
                                You can confidently bid on unique and high-quality coffee lots, supporting producers.
                            </Text>
                        </div>
                    </div>
                </div>

                {/* Configuración para pantallas mayores a 650px hasta 770px */}
                <div className="hidden sm:block md:hidden  p-5">
                    {/* Card 1 */}
                    <div className="flex flex-col sm:flex-row justify-center sm:justify-start items-center bg-[#D8354C] p-5 rounded-md mb-8" style={{ height: 'auto' }}>
                        <div className="flex justify-center items-center mb-5 rounded-full overflow-hidden bg-yellow-400 w-24 h-24 sm:w-32 sm:h-32">
                            <Icons src="/assets/icons/Exclusivity.svg" divSize="60%" />
                        </div>
                        <div className="flex flex-col items-center sm:items-start justify-center p-5 flex-grow">
                            <Tittle level="h1" size="text-xl md:text-25px" color="text-white" align="text-left" className="font-bold">
                                Exclusivity
                            </Tittle>
                            <Text size="text-sm" color="text-white" align="text-left" weight="font-light">
                                Access to unique, exotic and high-quality coffee.
                            </Text>
                        </div>
                    </div>

                    {/* Card 2 */}
                    <div className="flex flex-col sm:flex-row justify-center sm:justify-start items-center bg-white p-5 rounded-md mb-8" style={{ height: 'auto' }}>
                        <div className="flex justify-center items-center mb-5 rounded-full overflow-hidden bg-yellow-400 w-24 h-24 sm:w-32 sm:h-32">
                            <Icons src="/assets/icons/Transparency.svg" divSize="60%" />
                        </div>
                        <div className="flex flex-col items-center sm:items-start justify-center p-5 flex-grow">
                            <Tittle level="h1" size="text-xl md:text-25px" color="text-[#D8354C]" align="text-left" className="font-bold">
                                Transparency and traceability
                            </Tittle>
                            <Text size="text-sm" color="text-[#D8354C]" align="text-left" weight="font-light">
                                BeCoffee auctions provide transparency, confidence, and authenticity in product traceability.
                            </Text>
                        </div>
                    </div>

                    {/* Card 3 */}
                    <div className="flex flex-col sm:flex-row justify-center sm:justify-start items-center bg-[#D8354C] p-5 rounded-md mb-8" style={{ height: 'auto' }}>
                        <div className="flex justify-center items-center mb-5 rounded-full overflow-hidden bg-yellow-400 w-24 h-24 sm:w-32 sm:h-32">
                            <Icons src="/assets/icons/Trust.svg" divSize="60%" />
                        </div>
                        <div className="flex flex-col items-center sm:items-start justify-center p-5 flex-grow">
                            <Tittle level="h1" size="text-xl md:text-25px" color="text-white" align="text-left" className="font-bold">
                                Trust partnerships
                            </Tittle>
                            <Text size="text-sm" color="text-white" align="text-left" weight="font-light">
                                You can confidently bid on unique and high-quality coffee lots, supporting producers.
                            </Text>
                        </div>
                    </div>
                </div>

                {/* Configuración para pantallas mayores a 770px hasta 1000px */}
                <div className="hidden md:block lg:hidden p-5">



                    {/* Card 1 */}
                    <div className="flex flex-row justify-between items-center bg-[#D8354C] p-5 rounded-md mb-8 w-full">
                        <div className="flex justify-center items-center mb-5 rounded-full overflow-hidden bg-yellow-400 w-24 h-24 sm:w-32 sm:h-32">
                            <Icons src="/assets/icons/Exclusivity.svg" divSize="100%" />
                        </div>
                        <div className="flex flex-col items-center sm:items-start justify-center p-5 flex-grow w-full">
                            <Tittle level="h1" size="text-xl md:text-25px" color="text-white" align="text-left" className="font-bold">
                                Exclusivity
                            </Tittle>
                            <Text size="text-sm" color="text-white" align="text-left" weight="font-light">
                                Access to unique, exotic and high-quality coffee.
                            </Text>
                        </div>
                    </div>

                    {/* Card 2 */}
                    <div className="flex flex-row justify-between items-center bg-white p-5 rounded-md mb-8 w-full">
                        <div className="flex justify-center items-center mb-5 rounded-full overflow-hidden bg-yellow-400 w-24 h-24 sm:w-32 sm:h-32">
                            <Icons src="/assets/icons/Transparency.svg" divSize="60%" />
                        </div>
                        <div className="flex flex-col items-center sm:items-start justify-center p-5 flex-grow w-full">
                            <Tittle level="h1" size="text-xl md:text-25px" color="text-[#D8354C]" align="text-left" className="font-bold">
                                Transparency and traceability
                            </Tittle>
                            <Text size="text-sm" color="text-[#D8354C]" align="text-left" weight="font-light">
                                BeCoffee auctions provide transparency, confidence, and authenticity in product traceability.
                            </Text>
                        </div>
                    </div>

                    {/* Card 3 */}
                    <div className="flex flex-row justify-between items-center bg-[#D8354C] p-5 rounded-md w-full">
                        <div className="flex justify-center items-center mb-5 rounded-full overflow-hidden bg-yellow-400 w-24 h-24 sm:w-32 sm:h-32">
                            <Icons src="/assets/icons/Trust.svg" divSize="60%" />
                        </div>
                        <div className="flex flex-col items-center sm:items-start justify-center p-5 flex-grow w-full">
                            <Tittle level="h1" size="text-xl md:text-25px" color="text-white" align="text-left" className="font-bold">
                                Trust partnerships
                            </Tittle>
                            <Text size="text-sm" color="text-white" align="text-left" weight="font-light">
                                You can confidently bid on unique and high-quality coffee lots, supporting producers.
                            </Text>
                        </div>
                    </div>
                </div>

                {/* Configuración para pantallas mayores a 1000px */}
                <div className="hidden lg:grid grid-cols-3 gap-12 ">
                    {/* Card 1 */}
                    <div className="flex flex-col justify-center items-center bg-[#D8354C] p-5 rounded-md" style={{ height: '424px' }}>
                        <div className="flex justify-center items-center mb-5 rounded-full overflow-hidden bg-yellow-400 w-24 h-24 sm:w-32 sm:h-32">
                            <Icons src="/assets/icons/Exclusivity.svg" divSize="90%" />
                        </div>
                        <div className="flex flex-col items-center justify-center p-5 flex-grow">
                            <Tittle level="h1" size="text-xl" color="text-white" align="text-center" className="font-bold">
                                Exclusivity
                            </Tittle>
                            <Text size="text-sm" color="text-white" align="text-center" weight="font-light">
                                Access to unique, exotic and high-quality coffee.
                            </Text>
                        </div>
                    </div>

                    {/* Card 2 */}
                    <div className="flex flex-col justify-center items-center bg-white p-5 rounded-md" style={{ height: '424px' }}>
                        <div className="flex justify-center items-center mb-5 rounded-full overflow-hidden bg-yellow-400 w-24 h-24 sm:w-32 sm:h-32">
                            <Icons src="/assets/icons/Transparency.svg" divSize="90%" />
                        </div>
                        <div className="flex flex-col items-center justify-center p-5 flex-grow">
                            <Tittle level="h1" size="text-xl" color="text-[#D8354C]" align="text-center" className="font-bold">
                                Transparency and traceability
                            </Tittle>
                            <Text size="text-sm" color="text-[#D8354C]" align="text-center" weight="font-light">
                                BeCoffee auctions provide transparency, confidence, and authenticity in product traceability.
                            </Text>
                        </div>
                    </div>

                    <div className="flex flex-col justify-center items-center bg-[#D8354C] p-5 rounded-md" style={{ height: '424px' }}>
                        <div className="flex justify-center items-center mb-5 rounded-full overflow-hidden bg-yellow-400 w-24 h-24 sm:w-32 sm:h-32">
                            <Icons src="/assets/icons/Trust.svg" divSize="80%" />
                        </div>
                        <div className="flex flex-col items-center justify-center p-5 flex-grow">
                            <Tittle level="h1" size="text-xl md:text-25px" color="text-white" align="text-center">
                                Trust partnerships
                            </Tittle>
                            <Text size="text-sm" color="text-white" align="text-center" weight="font-light">
                                You can confidently bid on unique and high-quality coffee lots, supporting producers.
                            </Text>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Images;
