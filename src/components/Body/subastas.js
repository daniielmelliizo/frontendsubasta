import Image from "../Inviduals/Image";

function Marca() {
  return (
    <div className="bg-[#D8354C] p-5 flex justify-center items-center h-40">
      <div className="flex flex-col md:flex-row items-center space-y-4 md:space-y-0 md:space-x-4">
        <Image src="/assets/Images/logo.svg" alt="Logo" className='w-215 h-62' />
      </div>
    </div>
  );
}

export default Marca;
