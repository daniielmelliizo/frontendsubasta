import React, { useState, useEffect, useRef } from "react";
import SubastaController from "src/controllers/SubastaController";
import SubastaService from "src/aplication/services/SubastaService";
import CustomButton from "../Inviduals/CustomButton";
import LoginModal from "../Header/Cliente/LoginModal";
import Text from "../Inviduals/Text";
import Tittle from "../Inviduals/Tittle";
import Image from "../Inviduals/Image";
import secureStorage from '../../utils/secureStorage'; // Use secureStorage instead of localStorage

const subastaService = new SubastaService();
const subastaController = new SubastaController(subastaService);

const Info = () => {
  const [auctions, setAuctions] = useState([]);
  const [error, setError] = useState(null);
  const [isModalLoginOpen, setModalLoginOpen] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [selectedAuction, setSelectedAuction] = useState(null);
  const fetchIntervalRef = useRef(null);

  useEffect(() => {
    // Check if the user is logged in using secureStorage
    const storedIsLoggedIn = secureStorage.getItem('isLoggedIn');
    setIsLoggedIn(storedIsLoggedIn === 'true');

    fetchAuctionDetails();
    fetchIntervalRef.current = setInterval(fetchAuctionDetails, 10000);

    return () => clearInterval(fetchIntervalRef.current);
  }, []);

  const fetchAuctionDetails = async () => {
    try {
      const response = await subastaController.handleGetSubastas();
      const data = response.data;

      if (data) {
        const auctionsData = {};

        Object.entries(data).forEach(([sampleName, categories]) => {
          Object.values(categories).forEach((auctionList) => {
            auctionList.forEach((auction) => {
              const sampleId = auction.muestraId;

              if (!auctionsData[sampleId]) {
                auctionsData[sampleId] = {
                  id: auction.id,
                  nombre: auction.sample.nombre,
                  descripcion: auction.sample.descripcion,
                  fechaInicio: auction.fechaInicio,
                  horaFin: auction.horaFin,
                  imagen: auction.sample?.imagen[0] || []
                };
              }
            });
          });
        });
        console.log("que me trae la data", data);
        
        setAuctions(Object.values(auctionsData));
        setError(null);
      } else {
        setAuctions([]);
      }
    } catch (err) {
      setError(`Error al obtener los detalles de la subasta: ${err.message}`);
    }
  };

  const handleSubscriptionClick = (auction) => {
    setSelectedAuction(auction);

    // Check login status using secureStorage
    const storedIsLoggedIn = secureStorage.getItem('isLoggedIn');

    if (storedIsLoggedIn === 'true') {
      // If logged in, redirect to the auction page
    } else {
      // If not logged in, show the login modal
      setModalLoginOpen(true);
    }
  };

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div className="flex flex-col bg-black text-white w-full mx-auto p-4">
      <Tittle level="h1" size="text-xl" color="text-white" align="text-center" className="font-bold">
        Auctions
      </Tittle>
      <div className="flex flex-wrap justify-center gap-4">
        {auctions.slice(0, 3).map((auction, index) => (
          <div key={index} className="flex flex-col item-left justify-left bg-white text-black rounded-lg shadow-lg p-6 w-80">
            <Tittle level="h1" size="text-xl" color="text-black" align="text-left" className="font-bold">
              {auction.nombre}
            </Tittle>
            <Image src={auction.imagen} />
            <div className="flex flex-row">
              <Text size="text-sm" color="text-[#D8354C]" align="text-left" weight="font-light">
                Start date: {new Date(auction.fechaInicio).toLocaleString()}
              </Text>
              <Text size="text-sm" color="text-[#D8354C]" align="text-left" weight="font-light">
                End date: {new Date(auction.horaFin).toLocaleString()}
              </Text>
            </div>
            <Text size="text-sm" color="text-black" align="text-left" weight="font-light">
              {auction.descripcion}
            </Text>

            <CustomButton
              onClick={() => handleSubscriptionClick(auction)}
              className="mt-4 bg-[#D8354C] text-white px-4 py-2 rounded"
            >
              Auctions
            </CustomButton>
          </div>
        ))}
      </div>

      <LoginModal
        isOpen={isModalLoginOpen}
        onLoginSuccess={() => {
          setModalLoginOpen(false);
          setIsLoggedIn(true); // Update login state on successful login
          secureStorage.setItem('isLoggedIn', 'true'); // Save login state in secureStorage
        }}
        onClose={() => setModalLoginOpen(false)}
      />
    </div>
  );
};

export default Info;
