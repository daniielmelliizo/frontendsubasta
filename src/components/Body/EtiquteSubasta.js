import React from 'react';
import Icons from '../Inviduals/Icons';
import Textdata from '../Inviduals/Textdata';
import CustomButton from '../Inviduals/CustomButton';

const EtiquetaSubasta = () => {
  return (
    <div className="bg-white w-full flex items-center justify-between p-4">
      <Icons src='../assets/Icons/Icon awesome-star.svg' className="h-12 w-12" />
      <Textdata>Lot Name</Textdata>
      <Textdata>Variety</Textdata>
      <Textdata>Toast</Textdata>
      <Textdata>100</Textdata>
      <Textdata>Increment</Textdata>
      <CustomButton>Puja</CustomButton>
      <CustomButton className="text-white font-bold py-2 px-4 rounded ${className}">Subasta</CustomButton>
    </div>
  );
};

export default EtiquetaSubasta;
