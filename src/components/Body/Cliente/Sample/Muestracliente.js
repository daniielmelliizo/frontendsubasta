import React, { useState, useEffect } from "react";
import { FaShoppingCart, FaChevronDown, FaChevronUp } from "react-icons/fa";
import Tittle from "../../../Inviduals/Tittle";
import Text from "../../../Inviduals/Text";
import ProductCard from "./ProductCardCliente";
import InputBuscador from "../../../Inviduals/InputBuscador";
import ModalCompraMuestra from "../../../Modal/Cliente/Sample/ModalCompraMuestra";
import SampleController from "src/controllers/SampleController";
import SampleService from "src/aplication/services/SampleService";
import CarstController from "src/controllers/CarstController";
import CarstService from "src/aplication/services/CarstService";
import LoginModal from "../../../Header/Cliente/LoginModal"; // Importa el LoginModal
import secureStorage from "../../../../utils/secureStorage";
import ModalAlertsConfir from "../../../Modal/ModalAlertsConfir";
import ModalAlertsError from "../../../Modal/ModalAlertsError";

const sampleService = new SampleService();
const sampleController = new SampleController(sampleService);

const carstService = new CarstService();
const carstController = new CarstController(carstService);

function MuestraCliente() {
  const [muestras, setMuestras] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [idUser, setIdUser] = useState("");
  const [expandedMuestra, setExpandedMuestra] = useState(null);
  const [isConfirmModalOpen, setIsConfirmModalOpen] = useState(false);
  const [selectedMuestra, setSelectedMuestra] = useState(null);
  const [isLoginModalOpen, setLoginModalOpen] = useState(false); // Estado para el LoginModal
  const [filter, setFilter] = useState("all");
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  useEffect(() => {
    const handleGetSampleAllId = async () => {
      try {
        const user = secureStorage.getItem("user");
        const parsedUser = user ? JSON.parse(user) : null;
        if (parsedUser && parsedUser.rol && parsedUser.id) {
          setIdUser(parsedUser.id);
        }
        const response = await sampleController.handleGetSample();
        const muestrasConDatos = response.map((muestra) => ({
          id: muestra.id,
          nombre: muestra.nombre,
          codeInvitation: muestra.codeInvitation,
          peso: muestra.peso,
          valor: muestra.valor,
          productos: muestra.productsId,
          isPurchased: false,
          private: muestra.private,
        }));
        setMuestras(muestrasConDatos || []);
      } catch (error) {
        console.error("Error al obtener las muestras:", error.message);
      }
    };

    handleGetSampleAllId();
  }, []);

  const handleCartClick = (muestra) => {
    if (!idUser) {
      setLoginModalOpen(true); // Abrir el LoginModal si el usuario no está registrado
      return;
    }
    setSelectedMuestra(muestra);
    setIsConfirmModalOpen(true);
  };

  const handleConfirmPurchase = async () => {
    if (!selectedMuestra) {
      console.error("No hay muestra seleccionada para la compra.");
      return;
    }

    try {
      const formData = {
        userId: idUser,
        muestraId: selectedMuestra.id,
      };
      // Aquí se haría la llamada al controlador para realizar la compra
      const response = await carstController.handleCreateCarst(formData);
      if (response.status === 400) {
        handleErrorConfiModal(response.data.message);
      } else {
        handleOpenConfiModal(`${response.message} la muestra: ${selectedMuestra.nombre}`);
      }
      setIsConfirmModalOpen(false);
    } catch (error) {
      
    }
  };

  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  const handleToggleExpand = (index) => {
    setExpandedMuestra((prevIndex) => (prevIndex === index ? null : index));
  };

  const handleSearch = (query) => {
    setSearchTerm(query);
  };

  const handleLoginSuccess = (email, username, rol) => {
    setIdUser(username);
    setLoginModalOpen(false);
  };

  const filteredMuestras = muestras.filter((muestra) =>
    muestra.nombre.toLowerCase().includes(searchTerm.toLowerCase())
  );

  const displayedMuestras = filteredMuestras.filter((muestra) => {
    if (filter === "all") return true;
    if (filter === "public") return muestra.private !== true;
    if (filter === "private") return muestra.private === true;
    return true;
  });

  return (
    <div className="flex flex-col w-full px-4 md:px-8 lg:px-16">
      <div className="flex flex-col items-center justify-center mx-auto w-full lg:w-3/4">
        <div className="w-full mb-4 md:mb-0 flex items-center space-x-4">
          <InputBuscador
            onSearch={handleSearch}
            placeholder="Search samples..."
          />
        </div>
        <div className="flex justify-between items-center w-full">
          {/* Título alineado a la izquierda */}
          <Tittle color="text-white" className="text-xl">
            Samples
          </Tittle>

          {/* Botones alineados a la derecha */}
          <div className="flex space-x-6">
            <button
              className={`relative text-xs md:text-sm lg:text-base ${
                filter === "all"
                  ? "text-white border-b-4 border-[#D8354C]"
                  : "text-white"
              }`}
              onClick={() => setFilter("all")}
            >
              All
            </button>

            <button
              className={`relative text-xs md:text-sm lg:text-base ${
                filter === "public"
                  ? "text-white border-b-4 border-[#D8354C]"
                  : "text-white"
              }`}
              onClick={() => setFilter("public")}
            >
              Direct sales
            </button>

            <button
              className={`relative text-xs md:text-sm lg:text-base ${
                filter === "private"
                  ? "text-white border-b-4 border-[#D8354C]"
                  : "text-white"
              }`}
              onClick={() => setFilter("private")}
            >
              Auctions
            </button>
          </div>
        </div>
      </div>
      <div className="flex flex-wrap items-start justify-center md:justify-start  w-full lg:w-3/4 mx-auto mt-4">
        {displayedMuestras.map((muestra, index) => (
          <div key={index} className="w-full lg:w-full mb-6">
            <div className="flex flex-col md:flex-row justify-between items-center mb-4">
              <div
                className={`flex items-center justify-between rounded w-full text-left p-4 ${
                  muestra.isPurchased
                    ? " bg-[#F1F2F6] text-black"
                    : " bg-[#2F3542] hover:bg-[#F1F2F6] hover:text-black"
                }`}
                onClick={() => handleToggleExpand(index)}
              >
                <div>
                  <Tittle color="inherit">{muestra.nombre}</Tittle>
                  <Text color="inherit">Weight: {muestra.peso} Kg</Text>
                  <Text color="inherit">Value: ${muestra.valor}</Text>
                </div>
                <div className="flex items-center">
                  {/* Separa el botón de carrito del resto del botón */}
                  <button
                    className={`rounded p-2 ${
                      muestra.isPurchased
                        ? "bg-gray-400 cursor-not-allowed"
                        : "text-black bg-[#F1C40F] hover:bg-[#D8354C]"
                    }`}
                    onClick={(e) => {
                      e.stopPropagation();
                      if (!muestra.isPurchased) {
                        handleCartClick(muestra);
                      }
                    }}
                    title={muestra.isPurchased ? "Purchased" : ""}
                  >
                    <FaShoppingCart size={20} />
                  </button>
                  {expandedMuestra === index ? (
                    <FaChevronUp size={20} className="ml-2 text-black" />
                  ) : (
                    <FaChevronDown size={20} className="ml-2 text-white" />
                  )}
                </div>
              </div>
            </div>
            {expandedMuestra === index && (
              <div className="flex flex-wrap items-center justify-center space-y-4 mt-4">
                {muestra.productos.length > 0 ? (
                  muestra.productos.map((producto, productIndex) => (
                    <ProductCard
                      key={productIndex}
                      product={producto}
                      isSelected={false}
                      isPurchased={muestra.isPurchased}
                      onCartClick={() => handleCartClick(muestra)}
                    />
                  ))
                ) : (
                  <p>There are no products associated with this sample.</p>
                )}
              </div>
            )}
          </div>
        ))}
      </div>
      {isConfirmModalOpen && (
        <ModalCompraMuestra
          isOpen={isConfirmModalOpen}
          onClose={() => setIsConfirmModalOpen(false)}
          onConfirm={handleConfirmPurchase}
          muestra={selectedMuestra}
        />
      )}
      {isLoginModalOpen && (
        <LoginModal
          isOpen={isLoginModalOpen}
          onClose={() => setLoginModalOpen(false)}
          onLoginSuccess={handleLoginSuccess}
        />
      )}
      {isConfirModalOpen && (
        <ModalAlertsConfir
          isOpen={isConfirModalOpen}
          onClose={() => setConfirModalOpen(false)}
          customMessage={confirMessage}
        />
      )}
      {isErrorModalOpen && (
        <ModalAlertsError
          isOpen={isErrorModalOpen}
          onClose={() => setErrorModalOpen(false)}
          customMessage={errorMessage}
        />
      )}
    </div>
  );
}

export default MuestraCliente;
