import React from 'react';
import PropTypes from 'prop-types';
import Imagen from '../../../Inviduals/Image';
import Tittle from '../../../Inviduals/Tittle';
import Text from '../../../Inviduals/Text';
import { useRouter } from 'next/router';
import { FaStar } from 'react-icons/fa';

const ProductCard = ({ product }) => {
  const router = useRouter();

  const handleButtonClick = (e) => {
    e.stopPropagation(); // Evita que el click se propague a otros elementos
    router.push({
      pathname: '/VistaProduct',
      query: {
        product: JSON.stringify(product)
      }
    });
  };

  const productImage = product.imagen && product.imagen.length > 0 ? product.imagen[0] : 'default-image-url';

  return (
    <div className="bg-white shadow-md rounded-lg overflow-hidden m-4 w-80 relative cursor-pointer" onClick={handleButtonClick}>
      <Imagen
        src={productImage} 
        alt={product.nombre} 
        className="w-full h-40 object-cover" 
        width="100%"  // Provide width explicitly
        height="160px" // Provide height explicitly
      />
      <div className="bg-gray rounded-lg overflow-hidden relative flex flex-col md:flex-row">
        <div className="p-4 w-2/4 flex-grow">
          <Tittle level="h2" size="text-xl" color="text-black" align="text-left">
            {product.nombre}
          </Tittle>
          <Text size="text-sm" color="text-gray-700" align="text-left">
            Country: {product.pais}
          </Text>
          <Text size="text-sm" color="text-gray-700" align="text-left">
            Farm: {product.fincaProductor}
          </Text>
          <Text size="text-sm" color="text-gray-700" align="text-left">
            Producer: {product.productor}
          </Text>
          <Text size="text-sm" color="text-gray-700" align="text-left">
            Variety: {product.variedad}
          </Text>
          <Text size="text-sm" color="text-gray-700" align="text-left">
            Weight: {product.cantidad} {product.unidadPeso}
          </Text>
        </div>
        <div className="p-4 flex flex-col items-center justify-center">
          <div className="text-yellow-400 w-2/4 text-2xl flex items-center">
            <FaStar />
            <Text color="text-black text-20 ml-2">SCA: {product.score}</Text>
          </div>
          <Text color="text-black text-20 mt-4">Initial price: {product.precioInicial} USD</Text>
        </div>
      </div>
    </div>
  );
};

ProductCard.propTypes = {
  product: PropTypes.object.isRequired,
};

export default ProductCard;
