import React, { useState, useEffect } from "react";
import CustomButton from "../../../Inviduals/CustomButton";
import { FaTrash } from "react-icons/fa";
import InputBuscador from "../../../Inviduals/InputBuscador";
import ModalDeletCarst from "../../../Modal/Cliente/Carst/ModalDeletCarst";
import CarstController from "src/controllers/CarstController";
import CarstService from "src/aplication/services/CarstService";
import HistoryShopController from "src/controllers/HistoryShopController";
import HistoryShopService from "src/aplication/services/HistoryShopService";
import secureStorage from "../../../../utils/secureStorage";
import ModalAlertsConfir from "../../../Modal/ModalAlertsConfir";
import ModalAlertsError from "../../../Modal/ModalAlertsError";
import Text from "src/components/Inviduals/Text";
import Tittle from "src/components/Inviduals/Tittle";
import RecommendedProducts from "./RecomendedProducts";
import ProgressTimeline from "./ProgressTimeLine";
import PaymentMethodForm from "./PaymentMethodForm";
import ShippingDetailsForm from "./ShippingDetailsForm";
import { stringify } from "postcss";

const cartsService = new CarstService();
const carstController = new CarstController(cartsService);

const historyShopService = new HistoryShopService();
const historyShopController = new HistoryShopController(historyShopService);

const CarstClient = () => {
  const [clientes, setClientes] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [isDeleteModalOpen, setDeleteModalOpen] = useState(false);
  const [selectedCarst, setSelectedCarst] = useState(null);
  const [idUser, setIdUser] = useState("");
  const [isTableView, setIsTableView] = useState(true);
  const [selectedRows, setSelectedRows] = useState([]);
  const [totalValue, setTotalValue] = useState(0);
  const [pagosEstados, setPagosEstados] = useState([]);
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setConfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);
  const [isPaymentComplete, setIsPaymentComplete] = useState(false);
  const [cartState, setCartState] = useState({});
  const [updatedIDs, setUpdatedIDs] = useState([]); // Para almacenar los IDs actualizados
  const [dataInfo, setDataInfo] = useState("");

  useEffect(() => {
    fetchSuscripciones();
    fetchPagosEstados();
  }, []);

  useEffect(() => {
    setTotalValue(calculateTotalValue());
  }, [selectedRows]);

  const fetchSuscripciones = async () => {
    try {
      const user = secureStorage.getItem("user");
      const parsedUser = user ? JSON.parse(user) : null;
      setIdUser(parsedUser.id);

      const response = await carstController.handleGetCarstUser(parsedUser.id);
      setClientes(Array.isArray(response.carts) ? response.carts : []);
    } catch (error) {
      console.error("Error fetching subscriptions:", error);
    }
  };

  const fetchPagosEstados = async () => {
    try {
      const user = secureStorage.getItem("user");
      const parsedUser = user ? JSON.parse(user) : null;
      const response = await historyShopController.handleGetHistoryShopUser(
        parsedUser.id
      );
      setPagosEstados(response);
    } catch (error) {
      console.error("Error fetching payment statuses:", error);
    }
  };

  const handleSearch = (query) => setSearchTerm(query);

  const handleDelete = (product) => {
    setSelectedCarst(product);
    setDeleteModalOpen(true);
  };

  const handleConfirmDelete = async (suscripcionId) => {
    try {
      const response = await carstController.handleDeletCarst(suscripcionId);
      setConfirMessage(response.message);
      setConfirModalOpen(true);
      fetchSuscripciones();
    } catch (error) {
      console.error("Error deleting subscription:", error);
    } finally {
      setDeleteModalOpen(false);
    }
  };

  const calculateTotalValue = () =>
    selectedRows.reduce((acc, row) => {
      let totalPrice = acc;
      if (row.product) {
        totalPrice += parseFloat(row.product.precioInicial || 0);
      }
      if (row.sample) {
        totalPrice += parseFloat(row.sample.valor || 0);
      }
      return totalPrice;
    }, 0);

  const handlePay = async () => {
    try {
      let total = 0;
      let cartsIds = [];
      // Verificar si `updatedIDs` tiene datos
      if (updatedIDs && updatedIDs.ids && updatedIDs.ids.length > 0) {
        // Extraer y calcular el total basándose en los `updatedIDs`
        cartsIds = updatedIDs.ids.map((id) => {
          const item = filteredClientes.find((cliente) => cliente.id === id); // Buscar el item correspondiente en `filteredClientes`

          if (item) {
            const itemValue = Number(
              item.sample ? item.sample.valor : item.product.precioInicial
            );
            total += itemValue; // Sumar al total
          }
          return id;
        });
      } else {
        // Si `updatedIDs` está vacío, usar lógica con `filteredClientes`
        cartsIds = filteredClientes.flatMap((item) => {
          // Calcular el valor total para cada ítem
          const itemValue = item.sample
            ? item.sample.valor
            : item.product.precioInicial || 0; // Manejar valores nulos o undefined

          const itemTotal = itemValue * item.quantity; // Calcular el total basado en cantidad
          total += itemTotal; // Sumar al total general

          // Ajustar los IDs:
          // - Usar los IDs disponibles en `item.ids`.
          // - Completar los IDs hasta alcanzar la cantidad necesaria (`item.quantity`).
          const idsToRepeat = Array.from(
            { length: item.quantity - item.ids.length },
            () => item.ids[0] // Repetir el primer ID si faltan más.
          );

          return [...item.ids, ...idsToRepeat];
        });
      }

      // Construir el objeto de datos
      const data = {
        ...dataInfo,
        userId: idUser,
        cartsIds: cartsIds,
        valor: total,
      };

      // Descomentar las líneas para realizar la solicitud al controlador
      const result = await historyShopController.handleCreateHistoryShop(data);
      setIsPaymentComplete(false);
      if (result.status === 400) {
        setErrorMessage(result.data.message);
        setErrorModalOpen(true);
      } else {
        setConfirMessage(result.message);
        setConfirModalOpen(true);
        fetchSuscripciones();
      }
    } catch (error) {
      console.error("Error during payment:", error);
    }
  };

  const handleContinueShopping = () => {
    console.log("Seguir comprando");
  };

  const handleShippingSubmit = (data) => {
    const { nombre, direccion, ciudad, estado, codigoPostal } = data;

    if (!nombre || !direccion || !ciudad || !estado || !codigoPostal) {
      setErrorMessage("Please complete all fields before proceeding.");
      setErrorModalOpen(true);
      return; // Detener el proceso
    }

    if (data) {
      setIsPaymentComplete(true); // Muestra el formulario de envío
      setDataInfo(data);
    }
  };

  const handleClose = () => {
    setConfirModalOpen(false);
    setDeleteModalOpen(false);
    setErrorModalOpen(false);
  };

  const groupedClientes = clientes.reduce((acc, cliente) => {
    const key = cliente.sample?.id || cliente.product?.id; // Identifica por muestraId o productId
    if (!key) return acc;

    if (!acc[key]) {
      acc[key] = {
        ...cliente,
        ids: [cliente.id], // Inicializa un array con el id del cart
        quantity: cliente.quantity || 1,
      };
    } else {
      acc[key].quantity += cliente.quantity || 1;
      acc[key].ids.push(cliente.id); // Agrega el id del cart al array
    }

    return acc;
  }, {});

  const filteredClientes = Object.values(groupedClientes).filter(
    (cliente) =>
      (cliente.product &&
        cliente.product.nombre
          .toLowerCase()
          .includes(searchTerm.toLowerCase())) ||
      (cliente.sample &&
        cliente.sample.nombre.toLowerCase().includes(searchTerm.toLowerCase()))
  );

  // Manejar aumento de cantidad
  const handleIncrease = (user) => {
    const id = user.sample?.id || user.product?.id;

    if (!id) return; // Verifica que el ID exista

    setCartState((prevState) => {
      const currentQuantity = prevState[id]?.quantity || user.quantity || 1;
      const updatedQuantity = currentQuantity + 1;

      // Actualizar el estado del carrito
      const updatedState = {
        ...prevState,
        [id]: {
          ...user,
          quantity: updatedQuantity,
        },
      };

      // Generar los IDs
      const ids = Object.values(updatedState).flatMap((item) => {
        // Verifica si hay IDs disponibles
        const baseIds = item.ids || [];
        const idsToRepeat = Array(item.quantity - baseIds.length)
          .fill(baseIds[0]) // Repite el primer ID si faltan más
          .slice(0, Math.max(0, item.quantity)); // Limita la cantidad total

        return [...baseIds, ...idsToRepeat];
      });

      // Actualizar los IDs
      setUpdatedIDs({ ids });

      return updatedState; // Devuelve el nuevo estado
    });
  };

  const handleDecrease = (user) => {
    const id = user.sample?.id || user.product?.id;

    if (!id) return; // Verifica que el ID exista

    setCartState((prevState) => {
      const currentQuantity = prevState[id]?.quantity || user.quantity || 1;
      const updatedQuantity = Math.max(currentQuantity - 1, 1); // No permite disminuir más allá de 1

      // Actualizar el estado del carrito
      const updatedState = {
        ...prevState,
        [id]: {
          ...user,
          quantity: updatedQuantity,
        },
      };

      // Generar los IDs
      const ids = Object.values(updatedState).flatMap((item) => {
        const baseIds = item.ids || [];
        const idsToRepeat = Array(item.quantity - baseIds.length)
          .fill(baseIds[0]) // Repite el primer ID si faltan más
          .slice(0, Math.max(0, item.quantity)); // Limita la cantidad total

        return [...baseIds, ...idsToRepeat];
      });

      // Actualizar los IDs
      setUpdatedIDs({ ids });

      return updatedState; // Devuelve el nuevo estado
    });
  };

  // Cálculo de currentQuantity para la tabla
  const getCurrentQuantity = (id, user) => {
    return cartState[id]?.quantity || user.quantity || 1; // Usa la cantidad inicial desde la data o el estado
  };

  const renderPagosEstadosTable = () => {
    if (!pagosEstados || pagosEstados.length === 0) {
      return <div className="text-center text-white">No paid products.</div>;
    }

    return (
      <div className="w-full h-screen bg-gray-900 p-6">
        <div className="max-w-screen-xl mx-auto px-4">
          <h2 className="text-2xl font-bold text-white mb-6 text-center">
            Status of Products Purchased
          </h2>
          {pagosEstados.map((estado) => (
            <div
              key={estado.id}
              className="mb-8 bg-gray-800 p-6 rounded-lg shadow-lg"
            >
              {/* Timeline en la parte superior */}
              <div className="mb-6">
                <ProgressTimeline estado= {estado.status} />
              </div>
              <div className="flex-1 items-center justify-center text-white mt-4 lg:mt-0 grid grid-cols-1 sm:grid-cols-2 gap-4">
                <div>
                  <p className="text-sm">
                    <span className="font-semibold">Valor:</span> {estado.valor}
                  </p>
                  <p className="text-sm">
                    <span className="font-semibold">direccion:</span>{" "}
                    {estado.direccion}
                  </p>
                  <p className="text-sm">
                    <span className="font-semibold">ciudad:</span>{" "}
                    {estado.ciudad}
                  </p>
                  <p className="text-sm">
                    <span className="font-semibold">Nombre quien recibe:</span>{" "}
                    {estado.nombre}
                  </p>
                </div>
              </div>

              {/* Información de los detalles (Muestra o Producto) */}
              {estado.carts.length > 0 && (
                <div>
                  {estado.carts.map((cartItem, index) => (
                    <div
                      key={index}
                      className="mb-6 bg-gray-600 p-6 rounded-lg shadow"
                    >
                      <h3 className="text-lg font-semibold text-white mb-4">
                        Purchase made on:{" "}
                        <span className="text-[#D8354C]">
                          {new Date(estado.createdAt).toLocaleDateString()}
                        </span>
                      </h3>

                      {cartItem.muestra ? (
                        <div>
                          <h4 className="text-md font-medium text-white mb-3">
                            Muestra Details
                          </h4>
                          <div className="flex flex-col lg:flex-row lg:space-x-6">
                            {/* Imagen del producto */}
                            <div className="flex-shrink-0">
                              <img
                                src={cartItem.muestra.imagen[0]}
                                alt={cartItem.muestra.nombre}
                                className="w-40 h-40 object-cover rounded-lg border-2 shadow-md"
                              />
                            </div>

                            {/* Información del producto */}
                            <div className="flex-1 items-center justify-center text-white mt-4 lg:mt-0 grid grid-cols-1 sm:grid-cols-2 gap-4">
                              <div>
                                <p className="text-sm">
                                  <span className="font-semibold">Name:</span>{" "}
                                  {cartItem.muestra.nombre}
                                </p>
                                <p className="text-sm">
                                  <span className="font-semibold">
                                    Description:
                                  </span>{" "}
                                  {cartItem.muestra.descripcion}
                                </p>
                                <p className="text-sm">
                                  <span className="font-semibold">Weight:</span>{" "}
                                  {cartItem.muestra.peso} Kg
                                </p>
                                <p className="text-sm">
                                  <span className="font-semibold">
                                    Quantity:
                                  </span>{" "}
                                  {cartItem.quantity}
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      ) : cartItem.product ? (
                        <div>
                          <h4 className="text-md font-medium text-white mb-3">
                            Product Details
                          </h4>
                          <div className="flex flex-col lg:flex-row lg:space-x-6">
                            {/* Imagen del producto */}
                            <div className="flex-shrink-0">
                              <img
                                src={cartItem.product.imagen[0]}
                                alt={cartItem.product.nombre}
                                className="w-40 h-40 object-cover rounded-lg border-2 shadow-md"
                              />
                            </div>

                            {/* Información del producto */}
                            <div className="flex-1 items-center justify-center text-white mt-4 lg:mt-0 grid grid-cols-1 sm:grid-cols-2 gap-4">
                              <div>
                                <p className="text-sm">
                                  <span className="font-semibold">Name:</span>{" "}
                                  {cartItem.product.nombre}
                                </p>
                                <p className="text-sm">
                                  <span className="font-semibold">Farm:</span>{" "}
                                  {cartItem.product.fincaProductor}
                                </p>
                                <p className="text-sm">
                                  <span className="font-semibold">
                                    Variety:
                                  </span>{" "}
                                  {cartItem.product.variedad}
                                </p>
                                <p className="text-sm">
                                  <span className="font-semibold">Lot:</span>{" "}
                                  {cartItem.product.lote}
                                </p>
                              </div>
                              <div>
                                <p className="text-sm">
                                  <span className="font-semibold">Weight:</span>{" "}
                                  {cartItem.product.peso}{" "}
                                  {cartItem.product.unidadPeso}
                                </p>
                                <p className="text-sm">
                                  <span className="font-semibold">
                                    Quantity:
                                  </span>{" "}
                                  {cartItem.quantity}
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      ) : null}
                    </div>
                  ))}
                </div>
              )}
            </div>
          ))}
        </div>
      </div>
    );
  };

  return (
    <div className="flex flex-col h-full">
      {/* Barra superior con el buscador y opciones */}
      <div className="flex justify-between items-center p-4 bg-gray-800 shadow-md">
        <InputBuscador
          onSearch={handleSearch}
          placeholder="Search shopping ..."
        />
        <div className="flex space-x-4">
          <CustomButton
            onClick={() => setIsTableView(true)}
            className={`text-white border-b-4 ${
              isTableView ? "border-[#D8354C]" : "border-transparent"
            }`}
          >
            <Text color="text-white">Products</Text>
          </CustomButton>
          <CustomButton
            onClick={() => setIsTableView(false)}
            className={`text-white border-b-4 ${
              !isTableView ? "border-[#D8354C]" : "border-transparent"
            }`}
          >
            <Text color="text-white">Details</Text>
          </CustomButton>
        </div>
      </div>

      {/* Contenido principal */}
      <div className="flex-1 overflow-y-auto bg-gray-900">
        {filteredClientes.length === 0 && isTableView ? (
          <div className="flex items-center justify-center h-full text-white">
            There are no products to buy.
          </div>
        ) : (
          <>
            {isTableView ? (
              <div className="flex flex-col h-full bg-gray-900">
                {/* Header */}
                <div className="flex justify-between items-center p-4 bg-gray-800 shadow-md">
                  <h1 className="text-2xl font-bold text-white">
                    Shopping Cart
                  </h1>
                </div>

                {/* Main Content */}
                <div className="flex flex-1 p-6 space-x-6">
                  {/* Tabla de productos */}
                  <div className="flex-1 bg-black rounded-lg shadow-lg p-6">
                    <h2 className="text-xl font-semibold mb-4 text-white">
                      Purchase Summary
                    </h2>
                    {filteredClientes.length > 0 ? (
                      <table className="table-auto w-full text-white border-collapse border border-gray-700 rounded-lg shadow-md">
                        <thead className="bg-gray-800">
                          <tr>
                            <th className="p-4 text-left text-sm font-medium uppercase">
                              Image
                            </th>
                            <th className="p-4 text-left text-sm font-medium uppercase">
                              Product
                            </th>
                            <th className="p-4 text-left text-sm font-medium uppercase">
                              Price
                            </th>
                            <th className="p-4 text-left text-sm font-medium uppercase">
                              Quantity
                            </th>
                            <th className="p-4 text-left text-sm font-medium uppercase">
                              Total
                            </th>
                            <th className="p-4 text-left text-sm font-medium uppercase">
                              Action
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {filteredClientes.map((user) => {
                            const id = user.sample?.id || user.product?.id; // Calcula el ID dinámicamente
                            const currentQuantity = getCurrentQuantity(
                              id,
                              user
                            ); // Obtiene la cantidad actual

                            return (
                              <tr
                                key={id}
                                className="border-t border-gray-700 hover:bg-gray-700 transition"
                              >
                                <td className="p-4 text-center">
                                  {user.sample?.imagen?.[0] ? (
                                    <img
                                      src={user.sample.imagen[0]}
                                      alt={
                                        user.product?.nombre ||
                                        user.sample?.nombre
                                      }
                                      className="w-12 h-12 object-cover rounded-md shadow-md"
                                    />
                                  ) : user.product?.imagen?.[0] ? (
                                    <img
                                      src={user.product.imagen[0]}
                                      alt={
                                        user.product?.nombre ||
                                        user.sample?.nombre
                                      }
                                      className="w-12 h-12 object-cover rounded-md shadow-md"
                                    />
                                  ) : (
                                    <div className="w-12 h-12 bg-gray-700 rounded-md flex items-center justify-center text-gray-400">
                                      Sin Imagen
                                    </div>
                                  )}
                                </td>
                                <td className="p-4">
                                  <span className="font-medium">
                                    {user.product?.nombre ||
                                      user.sample?.nombre}
                                  </span>
                                </td>
                                <td className="p-4 text-green-400">
                                  {user.product
                                    ? `$${parseFloat(
                                        user.product.precioInicial
                                      ).toFixed(2)} USD`
                                    : `$${parseFloat(user.sample.valor).toFixed(
                                        2
                                      )} USD`}
                                </td>
                                <td className="p-4 flex items-center space-x-2">
                                  <button
                                    onClick={() => handleDecrease(user)}
                                    className="text-lg font-bold text-red-500"
                                  >
                                    -
                                  </button>
                                  <span>{currentQuantity}</span>
                                  <button
                                    onClick={() => handleIncrease(user)}
                                    className="text-lg font-bold text-red-500"
                                  >
                                    +
                                  </button>
                                </td>
                                <td className="p-4">
                                  {user.product
                                    ? `$${(
                                        currentQuantity *
                                        parseFloat(user.product.precioInicial)
                                      ).toFixed(2)} USD`
                                    : `$${(
                                        currentQuantity *
                                        parseFloat(user.sample.valor)
                                      ).toFixed(2)} USD`}
                                </td>
                                <td className="p-4">
                                  <button
                                    onClick={() => handleDelete(user)}
                                    className="bg-red-600 hover:bg-red-500 text-white font-bold py-2 px-4 rounded shadow"
                                  >
                                    <FaTrash />
                                  </button>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                    ) : (
                      <div className="text-center mt-4 text-white">
                        There are no products in your cart.
                      </div>
                    )}

                    {/* Total */}
                    {totalValue > 0 && (
                      <div className="flex justify-end mt-4">
                        <span className="text-xl font-bold text-white">
                          Total:{" "}
                          <span className="text-green-400">
                            ${totalValue.toFixed(2)}
                          </span>
                        </span>
                      </div>
                    )}
                  </div>

                  {!isPaymentComplete ? (
                    <ShippingDetailsForm onSendData={handleShippingSubmit} />
                  ) : (
                    <PaymentMethodForm
                      onPay={handlePay}
                      onContinueShopping={handleContinueShopping}
                    />
                  )}
                </div>
                <RecommendedProducts
                  handleFetchSuscripciones={fetchSuscripciones}
                  idUser={idUser}
                />
              </div>
            ) : (
              renderPagosEstadosTable()
            )}
          </>
        )}
      </div>

      {/* Modales */}
      {isDeleteModalOpen && (
        <ModalDeletCarst
          isOpen={isDeleteModalOpen}
          onRequestClose={() => setDeleteModalOpen(false)}
          onClose={handleClose}
          onConfirm={handleConfirmDelete}
          selectedCarst={selectedCarst}
        />
      )}
      {isErrorModalOpen && (
        <ModalAlertsError
          isOpen={isErrorModalOpen}
          onClose={() => setErrorModalOpen(false)}
          customMessage={errorMessage}
        />
      )}
      {isConfirModalOpen && (
        <ModalAlertsConfir
          isOpen={isConfirModalOpen}
          onClose={handleClose}
          customMessage={confirMessage}
        />
      )}
    </div>
  );
};

export default CarstClient;
