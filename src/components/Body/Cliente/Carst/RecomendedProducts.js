import React, { useState, useEffect } from "react";
import CustomButton from "../../../Inviduals/CustomButton";
import HistoryShopController from "src/controllers/HistoryShopController";
import HistoryShopService from "src/aplication/services/HistoryShopService";
import ModalCarts from "./ModalCarts";
import CarstController from "../../../../controllers/CarstController";
import CarstService from "../../../../aplication/services/CarstService";
import PropTypes from 'prop-types';
import ModalAlertsConfir from "src/components/Modal/ModalAlertsConfir";
import ModalAlertsError from "src/components/Modal/ModalAlertsError";

const historyShopService = new HistoryShopService();
const historyShopController = new HistoryShopController(historyShopService);

const carstService = new CarstService();
const carstController = new CarstController(carstService);

const RecommendedProducts = ({handleFetchSuscripciones, idUser}) => {
  const [items, setItems] = useState([]); // Almacena todos los datos
  const [filteredItems, setFilteredItems] = useState([]); // Datos filtrados para mostrar
  const [filter, setFilter] = useState("all"); // Estado para el filtro actual
  const [highestCount, setHighestCount] = useState(0); // Mayor número de compras
  const [isConfirmModalOpen, setIsConfirmModalOpen] = useState(false);
  const [selectedMuestra, setSelectedMuestra] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  useEffect(() => {
    fetchSuscripciones();
  }, []);

  const fetchSuscripciones = async () => {
    try {
      const response = await historyShopController.handleGetMaxShop();
      const { data } = response;

      // Procesamos la información en un formato uniforme
      const combinedItems = data.map((item) => {
        if (item.sample) {
          return {
            type: "sample",
            id: item.sample.id,
            details: {
              nombre: item.sample.nombre,
              valor: item.sample.valor,
              imagen: item.sample.imagen,
            },
            count: item.count,
          };
        } else if (item.product) {
          return {
            type: "product",
            id: item.product.id,
            details: {
              nombre: item.product.nombre,
              precioInicial: item.product.precioInicial,
              imagen: item.product.imagen,
            },
            count: item.count,
          };
        }
        return null; // Caso de seguridad
      });

      // Filtramos valores nulos y calculamos el mayor número de compras
      const filteredItems = combinedItems.filter((item) => item !== null);
      const maxCount = Math.max(...filteredItems.map((item) => item.count));

      // Actualizamos los estados
      setItems(filteredItems);
      setFilteredItems(filteredItems);
      setHighestCount(maxCount);
    } catch (error) {
      console.error("Error fetching subscriptions:", error);
    }
  };

  const handleFilterChange = (type) => {
    setFilter(type);
    if (type === "all") {
      setFilteredItems(items);
    } else {
      setFilteredItems(items.filter((item) => item.type === type));
    }
  };

  const handleConfirmPurchase = async () => {
    if (!selectedMuestra) {
      console.error("No sample or product selected for purchase.");
      return;
    }
  
    try {
      // Determina el campo a enviar basado en el tipo
      const formData = {
        userId: idUser,
        ...(selectedMuestra.type === "sample" 
          ? { muestraId: selectedMuestra.id } 
          : { productId: selectedMuestra.id }),
      };
  
      // Llamada al controlador para realizar la compra
      const response = await carstController.handleCreateCarst(formData);
      if (response.status === 400) {
        handleErrorConfiModal(response.data.message);
      } else {
        const itemName =
          selectedMuestra.type === "sample"
            ? selectedMuestra.details.nombre
            : selectedMuestra.details.nombre;
        handleOpenConfiModal(`Added to cart: ${itemName}`);
      }
      handleFetchSuscripciones();
      setIsConfirmModalOpen(false);
    } catch (error) {
      console.error("Error al confirmar la compra:", error);
    }
  };
  
  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  const handleCartClick = (item) => {
    setSelectedMuestra(item);
    setIsConfirmModalOpen(true);
  };

  return (
    <div className="mt-6 bg-gray-900 rounded-lg shadow-lg p-6">
      <h2 className="text-2xl font-bold text-white">Recommended Products</h2>

      <div className="flex justify-end space-x-4 mb-4">
        <CustomButton
          className={`py-2 px-4 rounded-md ${
            filter === "all" ? "bg-red-600 text-white" : "bg-gray-700 text-white"
          }`}
          onClick={() => handleFilterChange("all")}
        >
          All
        </CustomButton>
        <CustomButton
          className={`py-2 px-4 rounded-md ${
            filter === "product"
              ? "bg-red-600 text-white"
              : "bg-gray-700 text-white"
          }`}
          onClick={() => handleFilterChange("product")}
        >
          Products
        </CustomButton>
        <CustomButton
          className={`py-2 px-4 rounded-md ${
            filter === "sample"
              ? "bg-red-600 text-white"
              : "bg-gray-700 text-white"
          }`}
          onClick={() => handleFilterChange("sample")}
        >
          Samples
        </CustomButton>
      </div>

      <div className="grid grid-cols-2 md:grid-cols-4 gap-4">
        {filteredItems.map((item) => (
          <div
            key={item.id}
            className="bg-gray-800 p-4 rounded-md shadow-md hover:bg-gray-700 transition"
          >
            <img
              src={item.details.imagen?.[0] || "https://via.placeholder.com/150"}
              alt={item.details.nombre}
              className="w-full h-32 object-contain rounded-md mb-4 bg-gray-700"
            />
            <h3 className="text-sm font-medium text-white mb-2">
              {item.details.nombre}
            </h3>
            <p className="text-green-400 font-semibold mb-2">
              {item.type === "product"
                ? `$${item.details.precioInicial} USD`
                : `$${item.details.valor} USD`}
            </p>
            <p className="text-gray-400 text-sm mb-4">
              Total Purchases: <span className="text-white">{item.count}</span>
            </p>
            <button
              className="w-full py-2 px-4 bg-red-600 text-white rounded-md hover:bg-red-500 transition"
              onClick={() =>
                handleCartClick(item)
              }
            >
              Add to Cart
            </button>
          </div>
        ))}
      </div>
      {isConfirmModalOpen && (
        <ModalCarts
          isOpen={isConfirmModalOpen}
          onClose={() => setIsConfirmModalOpen(false)}
          onConfirm={handleConfirmPurchase}
          item={selectedMuestra}
        />
      )}
      {isConfirModalOpen && (
        <ModalAlertsConfir
          isOpen={isConfirModalOpen}
          onClose={() => setConfirModalOpen(false)}
          customMessage={confirMessage}
        />
      )}
      {isErrorModalOpen && (
        <ModalAlertsError
          isOpen={isErrorModalOpen}
          onClose={() => setErrorModalOpen(false)}
          customMessage={errorMessage}
        />
      )}
    </div>
  );
};

RecommendedProducts.propTypes = {
  handleFetchSuscripciones: PropTypes.func.isRequired,
};
export default RecommendedProducts;
