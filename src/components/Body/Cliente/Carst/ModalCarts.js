import React from 'react';
import PropTypes from 'prop-types';
import Tittle from '../../../Inviduals/Tittle';
import CustomButton from '../../../Inviduals/CustomButton';

const ModalCarts = ({ isOpen, onClose, onConfirm, item }) => {
  if (!isOpen) return null;

  // Determinar si el item es un sample o un product
  const isSample = item?.type === 'sample';
  const isProduct = item?.type === 'product';

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50">
      {/* Fondo oscuro */}
      <div className="bg-[#131821] bg-opacity-80 fixed inset-0"></div>

      {/* Contenedor del modal */}
      <div className="bg-white rounded-lg shadow-lg z-50 w-3/4 md:w-1/2 lg:w-1/3 p-6">
        <Tittle color="text-black" size="text-xl">
          Purchase Confirmation
        </Tittle>

        <div className="mt-4">
          {isSample && (
            <p className="text-gray-700">
              Are you sure you want to purchase the sample{' '}
              <span className="font-bold">{item.details.nombre}</span>?
            </p>
          )}
          {isProduct && (
            <p className="text-gray-700">
              Are you sure you want to purchase the product{' '}
              <span className="font-bold">{item.details.nombre}</span>?
            </p>
          )}
          {!isSample && !isProduct && (
            <p className="text-gray-700">No item selected.</p>
          )}
        </div>

        {/* Botones de acción */}
        <div className="mt-6 flex justify-end space-x-4">
          <CustomButton
            title="Cancel"
            onClick={onClose}
            className="bg-[#D8354C] hover:bg-[#C72B3F] text-white"
          />
          <CustomButton
            title="Confirm"
            onClick={onConfirm}
            className="bg-[#1293A1] hover:bg-[#0E7988] text-white"
            disabled={!isSample && !isProduct} // Desactivar si no hay item válido
          />
        </div>
      </div>
    </div>
  );
};

ModalCarts.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  item: PropTypes.shape({
    type: PropTypes.oneOf(['sample', 'product']).isRequired,
    details: PropTypes.shape({
      nombre: PropTypes.string.isRequired,
    }),
  }),
};

export default ModalCarts;
