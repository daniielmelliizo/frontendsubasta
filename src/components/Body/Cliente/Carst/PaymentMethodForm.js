import React, { useState } from "react";

const PaymentMethodForm = ({ onPay, onContinueShopping }) => {
  const [paymentMode, setPaymentMode] = useState(null); // Estado para el modo de pago seleccionado
  const [cashOption, setCashOption] = useState(""); // Opción seleccionada para pago en efectivo
  const [invoiceNumber, setInvoiceNumber] = useState(""); // Número de factura

  // Opciones de pago en efectivo
  const cashOptions = ["Efecty", "Juguemos", "Servientrega"];

  // Generar un número de factura aleatorio
  const generateInvoiceNumber = () => {
    const randomNumber = Math.floor(100000 + Math.random() * 900000); // Número de 6 dígitos
    setInvoiceNumber(`FAC-${randomNumber}`);
  };

  const handleCashPay = () => {
    if (cashOption) {
      generateInvoiceNumber();
      onPay();
    } else {
      alert("Por favor selecciona una opción de pago en efectivo.");
    }
  };

  // Formulario de selección de pago en efectivo
  const CashPaymentForm = () => (
    <div>
      <h4 className="text-lg font-semibold text-white mb-4">
        Select a cash payment option:
      </h4>
      <div className="grid grid-cols-2 gap-4">
        {cashOptions.map((option) => (
          <button
            key={option}
            onClick={() => setCashOption(option)}
            className={`py-3 px-4 rounded-md shadow text-white font-bold ${cashOption === option
                ? "bg-red-600"
                : "bg-gray-700 hover:bg-gray-600"
              }`}
          >
            {option}
          </button>
        ))}
      </div>
      {cashOption && (
        <p className="mt-4 text-green-400">
          You have selected: <span className="font-bold">{cashOption}</span>
        </p>
      )}
    </div>
  );

  // Formulario de pago con tarjeta
  const CardPaymentForm = () => (
    <>
      <div>
        <label className="block text-sm font-medium text-white">
          Card Number
        </label>
        <input
          type="number"
          placeholder="XXXX XXXX XXXX XXXX"
          className="w-full mt-1 p-3 rounded-md border border-gray-700 bg-gray-800 text-white"
        />
      </div>
      <div className="grid grid-cols-2 gap-4">
        <div>
          <label className="block text-sm font-medium text-white">
            Expiration Date
          </label>
          <input
            type="text"
            placeholder="MM/YY"
            className="w-full mt-1 p-3 rounded-md border border-gray-700 bg-gray-800 text-white"
          />
        </div>
        <div>
          <label className="block text-sm font-medium text-white">CVV</label>
          <input
            type="number"
            placeholder="XXX"
            className="w-full mt-1 p-3 rounded-md border border-gray-700 bg-gray-800 text-white"
          />
        </div>
      </div>
      <div>
        <label className="block text-sm font-medium text-white">
          Name of Holder
        </label>
        <input
          type="text"
          placeholder="John Doe"
          className="w-full mt-1 p-3 rounded-md border border-gray-700 bg-gray-800 text-white"
        />
      </div>
    </>
  );

  return (
    <div className="w-full lg:w-1/3 bg-black rounded-lg shadow-lg p-6">
      <h2 className="text-xl font-semibold mb-4 text-white">Method of Payment</h2>

      {/* Selección del tipo de pago */}
      {!paymentMode && (
        <div className="flex space-x-4">
          <button
            type="button"
            onClick={() => setPaymentMode("card")}
            className="flex-1 py-3 px-4 rounded-md shadow bg-red-600 text-white hover:bg-red-500"
          >
            Card Payment
          </button>
          <button
            type="button"
            onClick={() => setPaymentMode("cash")}
            className="flex-1 py-3 px-4 rounded-md shadow bg-gray-700 text-white hover:bg-gray-600"
          >
            Cash Payment
          </button>
        </div>
      )}

      {/* Formularios según el tipo de pago */}
      {paymentMode && (
        <form className="space-y-4 mt-4">
          {paymentMode === "card" && <CardPaymentForm />}
          {paymentMode === "cash" && <CashPaymentForm />}

          {/* Botones */}
          <div className="flex space-x-4 mt-4">
            <button
              type="button"
              onClick={() => setPaymentMode(null)} // Regresar a la selección
              className="flex-1 py-3 px-4 rounded-md shadow bg-gray-700 text-white hover:bg-gray-600"
            >
              Change Payment Method
            </button>
            <button
              type="button"
              onClick={paymentMode === "card" ? onPay : handleCashPay}
              className="flex-1 py-3 px-4 rounded-md shadow bg-red-600 text-white hover:bg-red-500"
            >
              {paymentMode === "card" ? "Pagar" : "Generar Factura"}
            </button>
          </div>
        </form>
      )}

      {/* Mostrar número de factura si existe */}
      {invoiceNumber && (
        <div className="mt-6 bg-gray-800 p-4 rounded-lg shadow text-center">
          <h4 className="text-lg font-bold text-white">Invoice Number</h4>
          <p className="text-green-400 text-xl font-bold">{invoiceNumber}</p>
        </div>
      )}

      {/* Botón de seguir comprando */}
      {!paymentMode && (
        <div className="mt-4">
          <button
            type="button"
            onClick={onContinueShopping}
            className="w-full py-3 px-4 rounded-md shadow bg-gray-700 text-white hover:bg-gray-600"
          >
            Continue Shopping
          </button>
        </div>
      )}
    </div>
  );
};

export default PaymentMethodForm;
