import React, { useState } from "react";

const ShippingDetailsForm = ({ onSendData }) => {
  const [formData, setFormData] = useState({
    nombre: "",
    direccion: "",
    ciudad: "",
    estado: "",
    codigoPostal: "",
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSendData = () => {
    if (onSendData) onSendData(formData);
  };

  return (
    <div className="w-full lg:w-1/3 bg-black rounded-lg shadow-lg p-6">
      <h2 className="text-xl font-semibold mb-4 text-white">Shipping Details</h2>
      <div className="space-y-4">
        <div>
          <label className="block text-sm font-medium text-white">Name</label>
          <input
            type="text"
            name="nombre"
            value={formData.nombre}
            onChange={handleChange}
            placeholder="John Doe"
            className="w-full mt-1 p-3 rounded-md border border-gray-700 bg-gray-800 text-white"
          />
        </div>
        <div>
          <label className="block text-sm font-medium text-white">Address</label>
          <input
            type="text"
            name="direccion"
            value={formData.direccion}
            onChange={handleChange}
            placeholder="123 Main Street"
            className="w-full mt-1 p-3 rounded-md border border-gray-700 bg-gray-800 text-white"
          />
        </div>
        <div>
          <label className="block text-sm font-medium text-white">City</label>
          <input
            type="text"
            name="ciudad"
            value={formData.ciudad}
            onChange={handleChange}
            placeholder="Ciudad"
            className="w-full mt-1 p-3 rounded-md border border-gray-700 bg-gray-800 text-white"
          />
        </div>
        <div className="grid grid-cols-2 gap-4">
          <div>
            <label className="block text-sm font-medium text-white">Status</label>
            <input
              type="text"
              name="estado"
              value={formData.estado}
              onChange={handleChange}
              placeholder="Estado"
              className="w-full mt-1 p-3 rounded-md border border-gray-700 bg-gray-800 text-white"
            />
          </div>
          <div>
            <label className="block text-sm font-medium text-white">Postal code</label>
            <input
              type="number"
              name="codigoPostal"
              value={formData.codigoPostal}
              onChange={handleChange}
              placeholder="00000"
              className="w-full mt-1 p-3 rounded-md border border-gray-700 bg-gray-800 text-white"
            />
          </div>
        </div>
        <button
          type="button"
          onClick={handleSendData}
          className="w-full py-3 px-4 rounded-md shadow bg-red-600 text-white hover:bg-red-500"
        >
          Confirm Shipment
        </button>
      </div>
    </div>
  );
};

export default ShippingDetailsForm;
