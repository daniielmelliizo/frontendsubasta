import { FaCheckCircle, FaTruck, FaWarehouse, FaBoxOpen } from "react-icons/fa";

const ProgressTimeline = ({ estado }) => {
  const steps = [
    { label: "Payment Validation", icon: <FaCheckCircle /> },
    { label: "Leaving Central", icon: <FaTruck /> },
    { label: "In Distribution", icon: <FaWarehouse /> },
    { label: "Delivered", icon: <FaBoxOpen /> },
  ];

  // Determinar el paso actual basado en el estado
  const currentStep = estado ? 1 : 0; // Si estado es true, el paso será "Leaving Central", si es false será "Payment Validation"

  return (
    <div className="flex items-center justify-between w-full">
      {steps.map((step, index) => (
        <div key={index} className="flex items-center">
          {/* Punto con ícono */}
          <div className="flex flex-col items-center">
            <div
              className={`h-12 w-12 flex items-center justify-center rounded-full border-2 ${
                index <= currentStep ? "border-[#D8354C]" : "border-gray-500"
              }`}
            >
              <div
                className={`text-2xl ${index <= currentStep ? "text-[#D8354C]" : "text-gray-500"}`}
              >
                {step.icon}
              </div>
            </div>

            {/* Texto debajo del ícono */}
            <div className="flex justify-between items-center w-full">
              <span
                className={`mt-2 text-sm ${index <= currentStep ? "text-[#D8354C] font-bold" : "text-gray-500"
                  } text-center`}
              >
                {step.label}
              </span>
            </div>
          </div>

          {/* Línea entre puntos (excepto el último) */}
          {index < steps.length - 1 && (
            <div
              className={`h-0.5 w-12 lg:w-16 ${index < currentStep ? "bg-[#D8354C]" : "bg-gray-500"}`}
              style={{
                position: "relative",
                top: "-24px", // Alinea la línea con el centro de la bolita
                marginLeft: "12px", // Espaciado entre bolita y línea
                marginRight: "12px", // Espaciado entre línea y siguiente bolita
              }}
            ></div>
          )}
        </div>
      ))}
    </div>
  );
};

export default ProgressTimeline;
