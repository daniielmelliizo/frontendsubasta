import React from "react";
import Tittle from "src/components/Inviduals/Tittle";
import Text from "src/components/Inviduals/Text";
import Image from "../../../Inviduals/Image";
import Subasta from "src/components/Body/subastas";


const AboutUs = () => {
  const teamImages = [
    { name: "John Doe", src: "https://plus.unsplash.com/premium_photo-1693258698597-1b2b1bf943cc?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxjb2xsZWN0aW9uLXBhZ2V8NXx4RG8yNGlPa0ptVXx8ZW58MHx8fHx8" },
    { name: "Jane Smith", src: "https://plus.unsplash.com/premium_photo-1690587673708-d6ba8a1579a5?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxjb2xsZWN0aW9uLXBhZ2V8MTJ8eERvMjRpT2tKbVV8fGVufDB8fHx8fA%3D%3D" },
    { name: "Carlos Rivera", src: "https://plus.unsplash.com/premium_photo-1690407617542-2f210cf20d7e?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxjb2xsZWN0aW9uLXBhZ2V8MTh8eERvMjRpT2tKbVV8fGVufDB8fHx8fA%3D%3D" },
    // Agrega más miembros del equipo aquí
  ];

  return (
    <div className="bg-[#090B11] text-white min-h-screen p-8 items-center justify-center">
      {/* Imagen de Encabezado */}
      <div className="flex relative w-full h-64 overflow-hidden mb-8">
      <Image src="/assets/Images/logo.svg" alt="Logo" className='w-1/3 h-auto' />
      </div>

      {/* Título principal */}
      <div className="text-center mb-12">
        <Tittle level="h1" size="text-5xl font-bold" color="text-[#D8354C]">
          About Us
        </Tittle>
      </div>

      {/* Descripción */}
      <div className="max-w-4xl mx-auto mb-12">
        <Text color="text-white text-lg">
          We are a group of coffee enthusiasts, technology lovers, and advocates
          for generating opportunities for small coffee-farming families,
          allowing them to earn a decent income from the work they have done
          for generations.
        </Text>
      </div>

      {/* Imágenes del equipo */}
      <div className="flex flex-wrap justify-center gap-4 mb-12">
        {teamImages.map((member, index) => (
          <div key={index} className="w-1/4 bg-[#2D2D2D] rounded-lg shadow-md text-center">
            <Image
              src={member.src}
              alt={`Team member ${member.name}`}
              className="w-1/3 h-20 object-cover rounded-lg"
            />
            <div className="">
              <Text color="text-white text-lg font-semibold">{member.name}</Text>
            </div>
          </div>
        ))}
      </div>

      {/* Historia y tecnologías */}
      <div className="max-w-4xl mx-auto text-center">
        <Text color="text-white text-lg mb-4">
          Since 2016, Becoffee has been developing technologies that facilitate
          small producers&apos; access to the global coffee market, creating the
          possibility for them to earn better income from their harvests.
        </Text>
        <Text color="text-white text-lg mb-4">
          We interact with small producers (799) through user-friendly
          technologies that allow them to characterize and geo-reference their
          crops, thereby ensuring the traceability of the coffee.
        </Text>

        {/* Imagen de la plataforma */}
        <figure className="my-8">
          <Image src="/assets/Images/About_Us_map.svg" alt="Logo" className=' bg-cover bg-center' />
          <figcaption className="text-gray-400 mt-2 text-sm">
            Figure 1. Image shown by our crop characterization and geo-referencing platform.
          </figcaption>
        </figure>

        <Text color="text-white text-lg">
          Through our geo-referencing and crop characterization technology, we
          identify agro-climatic conditions, altitude, exotic coffee varieties,
          and producers who carry out differentiated processes. By combining all
          these criteria, we have the opportunity to offer unique coffees.
        </Text>
      </div>

      {/* CTA */}
      <div className="text-center mt-12">
        <Tittle color="text-white text-2xl font-bold mb-4">
          Be part of becoffee.co
        </Tittle>
      </div>
      <div>
      <Subasta />
      </div>
    </div>
  );
};

export default AboutUs;
