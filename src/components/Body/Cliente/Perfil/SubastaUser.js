import React, { useState, useEffect } from "react";
import SuscripcionController from "src/controllers/SuscripcionController";
import SuscripcionService from "src/aplication/services/SuscripcionService";
import ResponsiveMenu from "src/components/Inviduals/MainProfile";
import Tittle from "src/components/Inviduals/Tittle";
import Text from "../../../Inviduals/Text";
import secureStorage from "../../../../utils/secureStorage";
import CustomTable from "../../../Inviduals/CustomTable";

const suscripcionService = new SuscripcionService();
const suscripcionController = new SuscripcionController(suscripcionService);

const SubastaUsers = () => {
  const [username, setUsername] = useState("");
  const [validatedSuscriptions, setValidatedSuscriptions] = useState([]);
  const [pendingSuscriptions, setPendingSuscriptions] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetchSuscripcionDetails();
  }, []);

  const fetchSuscripcionDetails = async () => {
    try {
      const storedUsername = secureStorage.getItem("user");
      const parsedUser = storedUsername ? JSON.parse(storedUsername) : null;
      setUsername(parsedUser.id);
      const data = await suscripcionController.handleGetsuscripcionList(
        parsedUser.id
      );

      if (data && data.suscripciones) {
        const activeSuscriptions = data.suscripciones.filter(
          (suscripcion) => suscripcion.status
        );
        const inactiveSuscriptions = data.suscripciones.filter(
          (suscripcion) => !suscripcion.status
        );

        setValidatedSuscriptions(activeSuscriptions);
        setPendingSuscriptions(inactiveSuscriptions);
        setError(null);
      } else {
        setValidatedSuscriptions([]);
        setPendingSuscriptions([]);
      }
    } catch (err) {
      setError(`Error fetching subscription details: ${err.message}`);
    }
  };

  const headers = ["Product name", "Stataus"];

  const configureRow = (suscripcion) => ({
    "Prodcut name": (
      <Text color="text-white-700" className="text-sm">
        {suscripcion.auction?.product?.nombre || "N/A"}
      </Text>
    ),
    Estado: (
      <Text color="text-white-700" className="text-sm">
        {suscripcion.auction?.estado || "N/A"}
      </Text>
    ),
  });

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div className="flex flex-col items-start justify-start bg-[#090B11] text-white w-5/6 p-4 h-screen">
      <div className="flex justify-center w-full">
      <Tittle color="text-white" className="text-xl font-bold mb-4">Subastas Usuario</Tittle>
      </div>
      <ResponsiveMenu bgColor="bg-[#090B11]" textColor="text-white"/>
      <div className="flex-grow w-full  overflow-auto">
        <div className="mb-8">
          <Tittle color="text-white" className="text-xl font-bold mb-4">Validated Subscriptions</Tittle>
          <CustomTable
            headers={headers}
            data={validatedSuscriptions}
            configureRow={configureRow}
          />
        </div>
        <div className="mb-8">
          <Tittle color="text-white" className="text-xl font-bold mb-4">Pending Subscriptions</Tittle>
          <CustomTable
            headers={headers}
            data={pendingSuscriptions}
            configureRow={configureRow}
          />
        </div>
      </div>
    </div>
  );
};

export default SubastaUsers;
