import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import CustomButton from "../../../Inviduals/CustomButton";
import Input from "../../../Inviduals/Input";
import Title from "../../../Inviduals/Tittle";
import ResponsiveMenu from "src/components/Inviduals/MainProfile";
import UserService from "src/aplication/services/userService";
import UserController from "src/controllers/userController";
import secureStorage from "../../../../utils/secureStorage";
import ModalAlertsConfir from "../../../Modal/ModalAlertsConfir";
import ModalAlertsError from "../../../Modal/ModalAlertsError";

const userService = new UserService();
const userController = new UserController(userService);

const EditPassword = ({ onClose }) => {
  const [formData, setFormData] = useState({
    currentPassword: "",
    newPassword: "",
    confirmNewPassword: "",
  });

  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [userId, setUserId] = useState(null); // Estado para almacenar el userId
  const [isCurrentPasswordValid, setIsCurrentPasswordValid] = useState(false); // Estado para controlar si la contraseña actual es válida
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  useEffect(() => {
    const storedUsername = secureStorage.getItem("user");
    const parsedUser = storedUsername ? JSON.parse(storedUsername) : null;
    if (parsedUser.id) {
      setUserId(parsedUser.id);
    }
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const validateCurrentPassword = async (e) => {
    e.preventDefault();

    if (!formData.currentPassword) {
      setErrorMessage("Please enter your current password.");
      return;
    }

    try {
      setLoading(true);
      const user = await userController.handleGetUsersPassword(userId, formData.currentPassword);
      if (user && user.message === "Password is correct") {
        // Verificación simple, puede ajustarse según lógica de autenticación real
        setIsCurrentPasswordValid(true);
        handleOpenConfiModal("Correct Password");
      } else {
        handleErrorConfiModal("Incorrect current password");
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  const handlePasswordChange = async (e) => {
    e.preventDefault();

    const { newPassword, confirmNewPassword } = formData;

    if (!newPassword || !confirmNewPassword) {
      setErrorMessage("Please fill in all fields.");
      return;
    }

    if (newPassword !== confirmNewPassword) {
      setErrorMessage("Passwords do not match.");
      return;
    }
    try {
      setLoading(true);
      const data = {
        userId: userId,
        newPassword: newPassword
      };
      
      const response = await userController.handleEditPasswordUser(data);

      handleOpenConfiModal(response.message);
      setIsCurrentPasswordValid(false);
      setFormData({
        currentPassword: "",
        newPassword: "",
        confirmNewPassword: "",
      });
    } catch (error) {
      
    } finally {
      setLoading(false);
    }
  };

  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };
  

  return (
    <div className=" flex w-full h-screen items-start justify-center">
    <div className="flex flex-col items-center bg-[#090B11] w-5/6 justify-center p-4">
      <Title
        level="h1"
        size="text-3x1"
        color="text-white"
        align="text-center"
        className="p-5"
      >
        Change Password
      </Title>
      <ResponsiveMenu bgColor="bg-[#090B11]" textColor="text-white"/>
      {errorMessage && <div className="mb-4 text-red-500">{errorMessage}</div>}

      {!isCurrentPasswordValid ? (
        <form className="w-full px-5" onSubmit={validateCurrentPassword}>
          <div className="mb-4">
            <Input
              title="Current Password"
              name="currentPassword"
              value={formData.currentPassword}
              onChange={handleChange}
              placeholder="Current Password"
              type="password"
              className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <CustomButton
            title="Validate"
            type="submit"
            className="bg-[#D8354C] w-full"
            disabled={loading}
          />
        </form>
      ) : (
        <form className="w-full px-5" onSubmit={handlePasswordChange}>
          <div className="mb-4">
            <Input
              title="New Password"
              name="newPassword"
              value={formData.newPassword}
              onChange={handleChange}
              placeholder="New Password"
              type="password"
              className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="mb-4">
            <Input
              title="Confirm New Password"
              name="confirmNewPassword"
              value={formData.confirmNewPassword}
              onChange={handleChange}
              placeholder="Confirm New Password"
              type="password"
              className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <CustomButton
            title="Change Password"
            type="submit"
            className="bg-[#D8354C] w-full"
            disabled={loading}
          />
        </form>
      )}
      {isErrorModalOpen && (
        <ModalAlertsError
          isOpen={isErrorModalOpen}
          onClose={() => setErrorModalOpen(false)}
          customMessage={errorMessage}
        />
      )}
      {isConfirModalOpen && (
        <ModalAlertsConfir
          isOpen={isConfirModalOpen}
          onClose={() => setConfirModalOpen(false)}
          customMessage={confirMessage}
        />
      )}
    </div>
    </div>
  );
};

EditPassword.propTypes = {
  onClose: PropTypes.func,
};

export default EditPassword;
