import React, { useEffect, useState } from "react";
import UserService from "src/aplication/services/userService";
import UserController from "src/controllers/userController";
import CustomButton from "../../../Inviduals/CustomButton";
import Input from "src/components/Inviduals/Input";
import ResponsiveMenu from "src/components/Inviduals/MainProfile";
import ModalAlertsConfir from "src/components/Modal/ModalAlertsConfir";
import ModalAlertsError from "src/components/Modal/ModalAlertsError";
import secureStorage from "src/utils/secureStorage";
import { FaEdit } from "react-icons/fa";
import Text from "src/components/Inviduals/Text";

const userService = new UserService();
const userController = new UserController(userService);

const Perfil = () => {
  const [user, setUser] = useState(null);
  const [formState, setFormState] = useState({});
  const [loading, setLoading] = useState(false);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);
  const [confirMessage, setConfirMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    const fetchUser = async () => {
      const storedUsername = secureStorage.getItem("user");
      const parsedUser = storedUsername ? JSON.parse(storedUsername) : null;

      if (parsedUser?.id) {
        try {
          const userData = await userController.handleGetUsersId(parsedUser.id);
          setUser(userData);
          setFormState(userData);
        } catch (error) {
          console.error("Error fetching user data:", error);
        }
      }
    };

    fetchUser();
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleSubmit = async () => {
    setLoading(true);
    try {
      const updatedUser = {
        nombre: formState.nombre,
        apellido: formState.apellido,
        email: formState.email,
        phone: formState.phone,
        pais: formState.pais,
        typeIdentificacion: formState.typeIdentificacion,
        identificacion: formState.identificacion,
        cargo: formState.cargo,
        direccion: formState.direccion,
        codigoPostal: formState.codigoPostal,
      };

      const response = await userController.handleUpdateUsers(user.id, updatedUser);

      if (response.status === 400) {
        handleErrorConfiModal(response.data.message);
        return null;
      }

      setUser(formState);
      handleOpenConfiModal("User information updated successfully!");
    } catch (error) {
      handleErrorConfiModal("An error occurred while updating user information.");
    } finally {
      setLoading(false);
    }
  };

  const handleOpenConfiModal = (message) => {
    setConfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  if (!user) {
    return <div>Loading profile...</div>;
  }

  return (
    <div className="bg-black p-6 rounded-lg shadow-md w-full flex items-center justify-center">
      <div className="flex flex-col bg-[#090B11] p-4 rounded-lg items-center justify-center shadow-lg w-5/6">
        <h1 className="text-2xl font-bold mb-4 text-white">User Profile</h1>
        <ResponsiveMenu bgColor="bg-[#090B11]" textColor="text-white"/>
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 p-8 rounded-md w-full">
          <div className="text-white">
            <Input
              title="Name"
              name="nombre"
              value={formState.nombre}
              onChange={handleChange}
              placeholder="Name"
              className="w-full text-black px-4 py-2 border border-black rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="text-white">
            <Input
              title="Last name"
              name="apellido"
              value={formState.apellido}
              onChange={handleChange}
              placeholder="Last name"
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="text-white">
            <Input
              title="E-mail"
              name="email"
              value={formState.email}
              onChange={handleChange}
              disabled
              placeholder="E-mail"
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="text-white">
            <Input
              title="Phone"
              name="phone"
              value={formState.phone}
              onChange={handleChange}
              placeholder="Phone"
              type="number"
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="text-white">
            <Input
              title="Country"
              name="pais"
              value={formState.pais}
              onChange={handleChange}
              placeholder="Country"
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="flex flex-col ">
            <Text color="text-white" size="text-3x1">Type of identification</Text>
            <select
              name="type Identificacion"
              value={formState.typeIdentificacion}
              onChange={handleChange}
              className="w-full px-4 py-2 border border-black rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
            >
              <option value="">Type of identification</option>
              <option value="Cedula Ciudadana">Citizen Identification Card</option>
              <option value="Cedula Extranjera">Foreign Cedula</option>
              <option value="Otro">Otro</option>
            </select>
          </div>
          <div className=" text-white">
            <Input
              title="Identificación"
              name="identificacion"
              value={formState.identificacion}
              onChange={handleChange}
              placeholder="Identificación"
              type="number"
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className=" text-white">
            <Input
              title="Status"
              name="estatus"
              value={formState.status}
              onChange={handleChange}
              placeholder="Status"
              disabled
              type="number"
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="flex flex-col ">
            <Text color="text-white" size="text-3x1">Cargo</Text>
            <select
              name="cargo"
              value={formState.cargo}
              onChange={handleChange}
              className="w-full px-4 py-2 border border-black rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C] text-black"
            >
              <option value="">Select the position</option>
              <option value="Importador">Importer</option>
              <option value="Tostador">Roaster</option>
            </select>
          </div>
          <div className="text-white">
            <Input
              title="Address"
              name="direccion"
              value={formState.direccion}
              placeholder="Address"
              onChange={handleChange}
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="text-white">
            <Input
              title="Zip Code"
              name="codigoPostal"
              value={formState.codigoPostal}
              onChange={handleChange}
              placeholder="Zip Code"
              type="number"
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
          <div className="text-white">
            <Input
              title="Role"
              name="rol"
              value={formState.rol}
              onChange={handleChange}
              placeholder="Rol"
              disabled
              type="number"
              className="w-full text-black px-4 py-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-[#D8354C]"
            />
          </div>
        </div>
        <div className="flex w-full justify-center mt-4">
          <CustomButton
            title={<div className="flex items-center space-x-2"><FaEdit /><Text color="text-wite" className="font-bold">Editar</Text></div>}
            onClick={handleSubmit}
            disabled={loading}
            className="bg-[#D8354C] text-lg w-1/3"
          />
        </div>
      </div>
      {isErrorModalOpen && (
        <ModalAlertsError
          isOpen={isErrorModalOpen}
          onClose={() => setErrorModalOpen(false)}
          customMessage={errorMessage}
        />
      )}
      {isConfirModalOpen && (
        <ModalAlertsConfir
          isOpen={isConfirModalOpen}
          onClose={() => setConfirModalOpen(false)}
          customMessage={confirMessage}
        />
      )}
    </div>
  );
};

export default Perfil;