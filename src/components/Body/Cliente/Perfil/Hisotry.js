import React, { useState, useEffect, useRef } from "react";
import SubastaController from "src/controllers/SubastaController";
import SubastaService from "src/aplication/services/SubastaService";
import AuctionTable from "src/components/Modal/Cliente/perfil/TableClient/AuctionTable";
import secureStorage from "src/utils/secureStorage";
import ResponsiveMenu from "src/components/Inviduals/MainProfile";
import Tittle from "src/components/Inviduals/Tittle";


const subastaService = new SubastaService();
const subastaController = new SubastaController(subastaService);

const Hisotry = () => {
  const [auctionDetails, setAuctionDetails] = useState({});
  const [error, setError] = useState(null);
  useEffect(() => {
    fetchAuctionDetails();
  }, []);

  

  const fetchAuctionDetails = async () => {
    try {
      const storedUsername = secureStorage.getItem("user");
      const parsedUser = storedUsername ? JSON.parse(storedUsername) : null;
      let data;

      if (!data) {
        const response = await subastaController.handleGetSubastasFinalizadasclient(parsedUser.id);
        data = response.data;
      }

      if (data) {
        const filteredData = Object.fromEntries(
          Object.entries(data).filter(
            ([key, value]) => Object.values(value).flat().length > 0
          )
        );
        setAuctionDetails(filteredData);
        setError(null);
      } else {
        setAuctionDetails({});
      }
    } catch (err) {
      console.log('Error fetching auction details: ${ err.message }', err);
    }
  };



  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div className="flex flex-col bg-[#090B11] text-white w-5/6 mx-auto p-4 min-h-screen">
      <div className="flex flex-col items-center justify-center mb-4">
        <Tittle color="text-white" className="text-2xl font-bold mb-4 text-white">Participation in Auctions</Tittle>
        <ResponsiveMenu bgColor="bg-[#090B11]" textColor="text-white"/>
      </div>
      {Object.keys(auctionDetails).length > 0 ? (
        <AuctionTable
          auctionDetails={auctionDetails}
        />
      ) : (
        <div className="text-center text-gray-100 mt-8">
           There are no completed auctions
        </div>
      )}
    </div>
  );
};

export default Hisotry;
