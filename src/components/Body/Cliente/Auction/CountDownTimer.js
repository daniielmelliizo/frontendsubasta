import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Tittle from '../../../Inviduals/Tittle';
import Text from "../../../Inviduals/Text";
import CustomButton from "../../../Inviduals/CustomButton";
import ModalSubscription from "../../../Modal/Cliente/Auction/ModalSubscription";
import ModalPreconfigPuja from "../../../Modal/Cliente/Auction/ConfigBid/ModalPreconfigPuja";
import LoginModal from "../../../Header/Cliente/LoginModal";
import Link from 'next/link';

const calculateTimeLeft = (startTime) => {
    const targetDateTime = new Date(startTime);
    const now = new Date();
    const difference = targetDateTime - now;

    let timeLeft = {};

    if (difference > 0) {
        timeLeft = {
            days: Math.floor(difference / (1000 * 60 * 60 * 24)),
            hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
            minutes: Math.floor((difference / 1000 / 60) % 60),
            seconds: Math.floor((difference / 1000) % 60)
        };
    } else {
        timeLeft = {
            days: 0,
            hours: 0,
            minutes: 0,
            seconds: 0
        };
    }

    return timeLeft;
};

const CountdownTimer = ({ NombreMuestra, startTime, auctions, username, fetchAuctionDetails, onTimerEnd }) => {
    
    const [isModalLoginOpen, setModalLoginOpen] = useState(false);
    const [timeLeft, setTimeLeft] = useState(calculateTimeLeft(startTime));
    const [isModalSubscriptionOpen, setModalSubscriptionOpen] = useState(false);
    const [isModalPreconfigOpen, setModalPreconfig] = useState(false);
    const [selectedAuction, setSelectedAuction] = useState(null);
    const [selectAuctionConfig, setSelectAuctionConfig] = useState(null);

    useEffect(() => {
        const timer = setInterval(() => {
            const newTimeLeft = calculateTimeLeft(startTime);
            setTimeLeft(newTimeLeft);

            if (newTimeLeft.days === 0 && newTimeLeft.hours === 0 && newTimeLeft.minutes === 0 && newTimeLeft.seconds === 0) {
                clearInterval(timer);
                onTimerEnd(); // Llamar al callback cuando el temporizador llegue a cero
            }
        }, 1000);

        return () => clearInterval(timer);
    }, [startTime, onTimerEnd]);

    const handleSubscriptionClick = (auction) => {
        if (username) {
            setSelectedAuction(auction);
            setModalSubscriptionOpen(true);
        } else {
            setModalLoginOpen(true);
        }
    };

    const handlePreconfigClick = (auction) => {
        setSelectAuctionConfig(auction);
        setModalPreconfig(true);
    };

    return (
        <div className="bg-custom-fondo-time bg-cover bg-center h-screen-50 flex items-center rounded-lg justify-center relative">
            <div className="absolute inset-0 bg-black bg-opacity-50"></div> {/* Overlay */}
            <div className="relative z-10 flex flex-col items-center space-y-6 p-4 rounded-lg w-full max-w-md md:max-w-lg bg-black bg-opacity-75">
                <Link
                    href={{
                        pathname: '/VistaSubasta',
                        query: {
                            auctions: JSON.stringify(auctions),
                            username: JSON.stringify(username)
                        }
                    }}
                >
                    <Tittle level="h1" size="text-3xl md:text-4xl" color="text-white" align="text-center">
                        {NombreMuestra}
                    </Tittle>
                </Link>

                <div className="grid grid-cols-4 gap-2 md:gap-4">
                    {['days', 'hours', 'minutes', 'seconds'].map((unit) => (
                        <div key={unit} className="flex flex-col items-center">
                            <span className="text-xs md:text-sm text-white mb-1">
                                {unit.charAt(0).toUpperCase() + unit.slice(1)}
                            </span>
                            <div className="flex space-x-1">
                                {String(timeLeft[unit]).padStart(2, '0').split('').map((digit, digitIndex) => (
                                    <div
                                        key={digitIndex}
                                        className="bg-black text-white border-2 border-red-500 p-2 rounded-md text-center w-10 h-14 md:w-12 md:h-16 flex items-center justify-center"
                                    >
                                        <span className="text-lg md:text-2xl font-bold">{digit}</span>
                                    </div>
                                ))}
                            </div>
                        </div>
                    ))}
                </div>

                {Array.isArray(auctions.suscriptions) && auctions.suscriptions.length === 0 ? (
                    <CustomButton onClick={() => handleSubscriptionClick(auctions)} className="bg-[#D8354C] text-white py-2 px-6 rounded-full text-sm md:text-base">
                        Request Access
                    </CustomButton>
                ) : (
                    Array.isArray(auctions.suscriptions) && auctions.suscriptions[0]?.status === false ? (
                        <Text color="text-white" className="text-sm font-bold">
                            Access Pending
                        </Text>
                    ) : (
                        <Text color="text-white" className="text-sm font-bold">
                            Bid Now
                        </Text>
                    )
                )}
                {Array.isArray(auctions.suscriptions) && auctions.suscriptions.length !== 0 ? (
                    Array.isArray(auctions.bidUsers) && auctions.bidUsers.length !== 0 ? (
                        <CustomButton onClick={() => handlePreconfigClick(auctions)} className="text-sm rounded bg-red-500 text-white font-bold">
                            Update your Bid
                        </CustomButton>
                    ) : (
                        <CustomButton onClick={() => handlePreconfigClick(auctions)} className="text-sm rounded bg-red-500 text-white font-bold">
                            Configure tu BID
                        </CustomButton>
                    )
                ) : null}
            </div>

            {selectedAuction && (
                <ModalSubscription
                    isOpen={isModalSubscriptionOpen}
                    auction={selectedAuction}
                    onClose={() => setModalSubscriptionOpen(false)}
                    username={username}
                    fetchAuctionDetails={fetchAuctionDetails}
                />
            )}
            {selectAuctionConfig && (
                <ModalPreconfigPuja
                    isOpen={isModalPreconfigOpen}
                    auction={selectAuctionConfig}
                    onClose={() => setModalPreconfig(false)}
                    username={username}
                    fetchAuctionDetails={fetchAuctionDetails}
                />
            )}
            <LoginModal
                isOpen={isModalLoginOpen}
                onLoginSuccess={() => {
                    setModalLoginOpen(false);
                    handleSubscriptionClick(selectedAuction); // Re-attempt the action after login
                }}
                onClose={() => setModalLoginOpen(false)}
            />
        </div>
    );
};

CountdownTimer.propTypes = {
    NombreMuestra: PropTypes.string.isRequired,
    startTime: PropTypes.string.isRequired,
    auctions: PropTypes.object.isRequired,
    username: PropTypes.string.isRequired,
    fetchAuctionDetails: PropTypes.func.isRequired,
    onTimerEnd: PropTypes.func.isRequired,
};

export default CountdownTimer;
