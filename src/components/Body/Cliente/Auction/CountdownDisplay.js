import React, { useEffect, useState } from "react";
import { FaClock } from "react-icons/fa";

const CountdownDisplay = ({ endTime }) => {
    const [timeLeft, setTimeLeft] = useState(calculateTimeLeft(endTime));

    useEffect(() => {
        const timer = setInterval(() => {
            setTimeLeft(calculateTimeLeft(endTime));
        }, 1000);

        return () => clearInterval(timer);
    }, [endTime]);

    function calculateTimeLeft(endTime) {
        const end = new Date(endTime).getTime();
        const now = Date.now();
        const difference = end - now;

        if (difference <= 0) {
            return { days: 0, hours: 0, minutes: 0, seconds: 0 };
        }

        return {
            days: Math.floor(difference / (1000 * 60 * 60 * 24)),
            hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
            minutes: Math.floor((difference / 1000 / 60) % 60),
            seconds: Math.floor((difference / 1000) % 60),
        };
    }

    return (
        <div className="flex items-center bg-white text-black p-2 rounded-md shadow-md space-x-2">
            <FaClock className="text-red-500 text-lg" />
            <div className="flex items-center space-x-1">
                <div className="text-center">
                    <div className="text-lg font-semibold">{timeLeft.days}</div>
                    <div className="text-xs">Days</div>
                </div>
                <div className="text-center">
                    <div className="text-lg font-semibold">{timeLeft.hours}</div>
                    <div className="text-xs">Hours</div>
                </div>
                <div className="text-center">
                    <div className="text-lg font-semibold">{timeLeft.minutes}</div>
                    <div className="text-xs">Minutes</div>
                </div>
                <div className="text-center">
                    <div className="text-lg font-semibold">{timeLeft.seconds}</div>
                    <div className="text-xs">Seconds</div>
                </div>
            </div>
        </div>
    );
};

export default CountdownDisplay;
