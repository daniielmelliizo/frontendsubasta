import React, { useState, useEffect, useRef } from "react";
import { FaList, FaMicrosoft } from "react-icons/fa";
import SubastaController from "src/controllers/SubastaController";
import SubastaService from "src/aplication/services/SubastaService";
import AuctionTable from "../../../Table/AuctionTable";
import AuctionCardView from "../../../Table/AuctionCardView";
import BidManual from "../../../Modal/Cliente/Auction/BibManual/ModalPujaManual";
import CustomButton from "../../../Inviduals/CustomButton";
import ModalSubscription from "../../../Modal/Cliente/Auction/ModalSubscription";
import LoginModal from "../../../Header/Cliente/LoginModal";
import ModalPreconfigPuja from "../../../Modal/Cliente/Auction/ConfigBid/ModalPreconfigPujaIndividi";
import secureStorage from "src/utils/secureStorage";
import { useSocket } from "src/infraestructura/Socketcontext";


const subastaService = new SubastaService();
const subastaController = new SubastaController(subastaService);

const TableSubasta = () => {
  const socket = useSocket();
  const [username, setUsername] = useState("");
  const [auctionDetails, setAuctionDetails] = useState({});
  const [error, setError] = useState(null);
  const [isBidManualOpen, setIsBidManualOpen] = useState(false);
  const [maxBidValue, setMaxBidValue] = useState(0);
  const [isTableView, setIsTableView] = useState(true);
  const [isModalSubscriptionOpen, setModalSubscriptionOpen] = useState(false);
  const [isModalLoginOpen, setModalLoginOpen] = useState(false);
  const [selectedAuction, setSelectedAuction] = useState(null);
  const [selectAuctionConfig, setSelectAuctionConfig] = useState(null);
  const [isModalPreconfigOpen, setModalPreconfig] = useState(false);
  const [updatedRows, setUpdatedRows] = useState({});
  const [IdAuction, setIdAuction] = useState("");

  useEffect(() => {
    fetchAuctionDetails();
    if (socket) {
        console.log("Registrando eventos del socket...");

        socket.on("newBid", (data) => {
            console.log("Evento newBid recibido:", data);
            highlightRow(data.auctionId);
            fetchAuctionDetails();
        });

        socket.on("bidCreated", (data) => {
            console.log("Evento bidCreated recibido:", data);
            highlightRow(data.auctionId);
            fetchAuctionDetails();
        });

        return () => {
            socket.off("newBid");
            socket.off("bidCreated");
        };
    }
}, [socket]);

  const highlightRow = (auctionId) => {
    setUpdatedRows((prevState) => ({
      ...prevState,
      [auctionId]: true,
    }));

    setTimeout(() => {
      setUpdatedRows((prevState) => ({
        ...prevState,
        [auctionId]: false,
      }));
    }, 2000);
  };


  const fetchAuctionDetails = async () => {
    try {
      const storedUsername = secureStorage.getItem("user");
      const parsedUser = storedUsername ? JSON.parse(storedUsername) : null;

      let data;

      // Check if user data exists in secureStorage
      if (parsedUser && parsedUser.rol && parsedUser.id) {
        setUsername(parsedUser.id);

        // If user is "Cliente" or "Invitado", fetch their specific auctions
        if (parsedUser.rol === "Cliente" || parsedUser.rol === "Invitado") {
          const clientResponse =
            await subastaController.handleGetSubastasClient(parsedUser.id);
          data = clientResponse.data;
        }
      }
      if (!data) {
        const response = await subastaController.handleGetSubastas();
        data = response.data;
      }

      if (data) {
        const filteredData = Object.fromEntries(
          Object.entries(data).filter(
            ([key, value]) => Object.values(value).flat().length > 0
          )
        );
        setAuctionDetails(filteredData);
        setError(null);
      } else {
        setAuctionDetails({});
      }
    } catch (err) {
      console.log("Error fetching auction details: ${ err.message }");
    }
  };

  const handleDropdownChange = async (value, auction) => {
    const matchingSubscription = auction.suscriptions.find(
      (sub) => sub.auctionId === auction.id
    );

    if (matchingSubscription) {
      const matchingBidUser = auction.bidUsers.find(
        (bidUser) => bidUser.suscribedId === matchingSubscription.id
      );

      if (matchingBidUser) {
        let valorPor;
        if (value === "x1") {
          valorPor = 1;
        } else if (value === "x2") {
          valorPor = 2;
        } else if (value === "x3") {
          valorPor = 3;
        }

        if (valorPor !== undefined) {
          const data = {
            valorPor: valorPor.toString(),
          };
          await subastaController.handleUpdateConfigBid(
            matchingBidUser.id,
            data
          );
          fetchAuctionDetails();
        }
      }

      if (value === "Manual") {
        setIsBidManualOpen(true);
        setIdAuction(matchingSubscription.auctionId);
        setMaxBidValue(matchingBidUser.id);
      }
    }
  };

  const handleSubscriptionClick = (auction) => {
    if (username) {
      setSelectedAuction(auction);
      setModalSubscriptionOpen(true);
    } else {
      setModalLoginOpen(true);
    }
  };

  const handlePreconfigClick = (auction) => {
    setSelectAuctionConfig(auction);
    setModalPreconfig(true);
  };

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div className="flex flex-col bg-black text-white w-5/6 mx-auto p-4 ">
      <div className="flex justify-end mb-4">
        <CustomButton
          onClick={() => setIsTableView(!isTableView)}
          className="text-lg w-1/10"
        >
          {isTableView ? <FaMicrosoft /> : <FaList />}
        </CustomButton>
      </div>
      {Object.keys(auctionDetails).length > 0 ? (
        isTableView ? (
          <AuctionTable
            auctionDetails={auctionDetails}
            updatedRows={updatedRows}
            fetchAuctionDetails={fetchAuctionDetails}
            handleDropdownChange={handleDropdownChange}
            handlePreconfigClick={handlePreconfigClick}
          />
        ) : (
          <AuctionCardView
            auctionDetails={auctionDetails}
            handlePreconfigClick={handlePreconfigClick}
          />
        )
      ) : (
        <div className="text-center text-gray-100 mt-8">
          No auctions available
        </div>
      )}
      <BidManual
        isOpen={isBidManualOpen}
        maxBid={maxBidValue}
        IdAuction = {IdAuction}
        onClose={() => setIsBidManualOpen(false)}
        fetchAuctionDetails={fetchAuctionDetails}
      />
      {selectedAuction && (
        <ModalSubscription
          isOpen={isModalSubscriptionOpen}
          auction={selectedAuction}
          onClose={() => setModalSubscriptionOpen(false)}
          username={username}
          fetchAuctionDetails={fetchAuctionDetails}
        />
      )}
      <LoginModal
        isOpen={isModalLoginOpen}
        onLoginSuccess={() => {
          setModalLoginOpen(false);
          fetchAuctionDetails();
          handleSubscriptionClick(selectedAuction);
        }}
        onClose={() => setModalLoginOpen(false)}
      />
      {selectAuctionConfig && (
        <ModalPreconfigPuja
          isOpen={isModalPreconfigOpen}
          auction={selectAuctionConfig}
          onClose={() => setModalPreconfig(false)}
          username={username}
          fetchAuctionDetails={fetchAuctionDetails}
        />
      )}
    </div>
  );
};

export default TableSubasta;
