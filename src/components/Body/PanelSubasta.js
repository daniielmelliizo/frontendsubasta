import React, { useEffect, useState } from 'react';
import Image from '../Inviduals/Image';
import Text from '../Inviduals/Text';
import Title from '../Inviduals/Tittle';
import CustomButton from '../Inviduals/CustomButton';
import Participacion from '../Modal/ModalParticipación';
import ModalTime from '../Modal/ModalTime';
import LoginModal from '../Header/Cliente/LoginModal'; // Importar el LoginModal
import secureStorage from '../../utils/secureStorage';

function PanelSubasta() {
    const [isParticipacionModalOpen, setParticipacionModalOpen] = useState(false);
    const [isModalTimeOpen, setModalTimeOpen] = useState(false);
    const [isLoginModalOpen, setLoginModalOpen] = useState(false);
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    useEffect(() => {
        const storedIsLoggedIn = secureStorage.getItem('isLoggedIn');
        if (storedIsLoggedIn === 'true') {
            setIsLoggedIn(true);
        }
    }, []);

    const handleLoginSuccess = () => {
        setIsLoggedIn(true);
        setLoginModalOpen(false);
    };

    const handleButtonClick = () => {
        if (isLoggedIn) {
        } else {
            setLoginModalOpen(true);
        }
    };

    return (
        <div className="bg-black p-5">
            <Title align="text-center" color="text-white" padding="p-4">
                Choose your subscription
            </Title>
            <div className="gap-5 flex justify-center items-center h-full">
                <div className="bg-[#131821] flex flex-col justify-center items-start p-5 rounded-lg w-80 shadow-lg h-screen-50">
                    <Image src="/assets/Images/grano.svg" alt="Grano de café" />
                    <Text color="text-white">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </Text>
                    <CustomButton
                        title="Invited"
                        className="bg-[#1293A1] text-lg w-60 mt-4 ml-5"
                        onClick={handleButtonClick} // Usar handleButtonClick
                    />
                </div>
                <div className="bg-[#131821] flex flex-col justify-center items-start p-5 rounded-lg w-80 shadow-lg h-screen-50">
                    <Image src="/assets/Images/grano.svg" alt="Grano de café" />
                    <Text color="text-white">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </Text>
                    <CustomButton
                        title="Invited"
                        className="bg-[#1293A1] text-lg w-60 mt-4 ml-5"
                        onClick={() => setModalTimeOpen(true)}
                    />
                </div>
                <ModalTime isOpen={isModalTimeOpen} onClose={() => setModalTimeOpen(false)} />
                <div className="bg-[#131821] flex flex-col justify-center items-start p-5 rounded-lg w-80 shadow-lg h-screen-50">
                    <Image src="/assets/Images/grano.svg" alt="Grano de café" />
                    <Text color="text-white">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </Text>
                    <CustomButton
                        title="Invited"
                        className="bg-[#1293A1] text-lg w-60 mt-4 ml-5"
                        onClick={() => setParticipacionModalOpen(true)}
                    />
                </div>
                <Participacion isOpen={isParticipacionModalOpen} onClose={() => setParticipacionModalOpen(false)} />
            </div>
            <LoginModal isOpen={isLoginModalOpen} onClose={() => setLoginModalOpen(false)} onLoginSuccess={handleLoginSuccess} />
        </div>
    );
}

export default PanelSubasta;
