import React, { useState } from "react";
import CustomButton from "src/components/Inviduals/CustomButton";
import Tittle from "src/components/Inviduals/Tittle";
import Text from "src/components/Inviduals/Text";
import { useRouter } from "next/router";
import { FaArrowLeft, FaArrowRight, FaStar } from "react-icons/fa";
import Image from "../Inviduals/Image";

const VistaProductos = () => {
  const router = useRouter();
  const { query } = router;
  const selectedProduct = query.product ? JSON.parse(query.product) : {};

  const images = selectedProduct.imagen || [];
  const companyImages = selectedProduct.compania?.imagen || [];
  const [currentIndex, setCurrentIndex] = useState(0);

  const nextSlide = () => {
    setCurrentIndex((prevIndex) => (prevIndex + 1) % images.length);
  };

  const prevSlide = () => {
    setCurrentIndex(
      (prevIndex) => (prevIndex - 1 + images.length) % images.length
    );
  };

  if (!selectedProduct || !images.length) {
    return <div>Loading...</div>;
  }

  return (
    <div className="bg-black min-h-screen flex flex-col justify-between">

      <div className="relative w-full h-screen-60 overflow-hidden">
        {images.map((src, index) => (
          <Image
            key={index}
            src={src}
            alt={`Coffee image ${index + 1}`}
            className={`absolute inset-0 w-full h-full object-cover transition-opacity duration-1000 ${index === currentIndex ? "opacity-100" : "opacity-0"
              }`}
          />
        ))}
      </div>
      <div className="bg-black flex-grow flex flex-col justify-center items-center">
        <div className="flex-grow flex m-8 flex-col justify-center items-center">
          <Tittle
            level="h1"
            size="text-25"
            color="text-white"
            align="text-center"
          >
            {selectedProduct.nombre}
          </Tittle>

          <Text color="text-white text-20">{selectedProduct.descriptores}</Text>
          <div className="bg-gray-800 p-4 m-4 w-2/4 rounded-lg flex flex-col md:flex-row justify-center items-center">
            <div className="w-full md:w-1/2 p-4">
              <Text color="text-white text-20">
                Category: {selectedProduct.categoria.nombre}
              </Text>
              <Text color="text-white text-20">
                Coubntry: {selectedProduct.pais || null}
              </Text>
              <Text color="text-white text-20">
                Departament: {selectedProduct.departamento || null}
              </Text>
              <Text color="text-white text-20">
                City: {selectedProduct.ciudad || null}
              </Text>
              <Text color="text-white text-20">
                Farm: {selectedProduct.fincaProductor || null}
              </Text>
              <Text color="text-white text-20">
                Variety: {selectedProduct.variedad || null}
              </Text>
              <Text color="text-white text-20">
                Descriptors: {selectedProduct.descriptores || null}
              </Text>
              <Text color="text-white text-20">
                Weight: {selectedProduct.peso || null} {selectedProduct.unidadPeso || null}
              </Text>
            </div>
            <div className="w-full md:w-1/2 flex flex-col items-center">
              <div className="relative w-full h-screen-30 overflow-hidden">
                {images.map((src, index) => (
                  <Image
                    key={index}
                    src={src}
                    alt={`Coffee image ${index + 1}`}
                    className={`absolute inset-0 w-full h-full object-cover transition-opacity duration-1000 ${index === currentIndex ? "opacity-100" : "opacity-0"
                      }`}
                  />
                ))}
              </div>
              <div className="text-yellow-400 text-2xl flex items-center">
                <FaStar />
                <Text color="text-white text-20 ml-2">
                  Score: {selectedProduct.score}
                </Text>
              </div>
              <Text color="text-white text-20 mt-4">
                Initial price: {selectedProduct.precioInicial} USD
              </Text>
            </div>
          </div>
          <Tittle color="text-white text-30 font-bold">
            {/* {selectedEmpresa.nombre || null} */}
          </Tittle>
          <Tittle color="text-white text-25 font-bold">
            Producer: {selectedProduct.fincaProductor} -{" "}
            {selectedProduct.ciudad || null}
          </Tittle>
          <div className="relative w-full h-60 overflow-hidden">
            <div className="absolute inset-y-0 left-0 flex items-center">
              <button
                onClick={prevSlide}
                className="text-white bg-gray-800 bg-opacity-50 hover:bg-opacity-75 p-2 rounded-full"
              >
                <FaArrowLeft />
              </button>
            </div>
            <div className="absolute inset-y-0 right-0 flex items-center">
              <button
                onClick={nextSlide}
                className="text-white bg-gray-800 bg-opacity-50 hover:bg-opacity-75 p-2 rounded-full"
              >
                <FaArrowRight />
              </button>
            </div>
            <div className="flex">
              {companyImages.map((src, index) => (
                <Image
                  key={index}
                  src={src}
                  alt={`Coffee image ${index + 1}`}
                  className={`w-1/3 h-full object-cover transition-opacity duration-1000 ${index === currentIndex ? "opacity-100" : "opacity-0"
                    }`}
                />
              ))}
            </div>
          </div>
          <Tittle color="text-white text-25 font-bold">
            Process: {selectedProduct.process}
          </Tittle>
        </div>
        <CustomButton
          title="Back"
          onClick={() => router.back()}
          className="bg-[#D8354C] text-lg mt-4"
        />
      </div>
    </div>
  );
};

export default VistaProductos;
