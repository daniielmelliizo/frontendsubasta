import Image from "../Inviduals/Image";
import CustomButton from "../Inviduals/CustomButton";
import Tittle from "../Inviduals/Tittle";
import Text from "../Inviduals/Text";

function Textorelleno() {
    return (
        <div className="bg-black items-center h-screen overflow-hidden flex justify-center">
            <div className="flex flex-col md:flex-row  w-full ">
                <div className="flex justify-center items-center w-full md:w-1/2  h-full ">
                    <Image src="/assets/Images/Coffe.svg" alt="Logo" className="w-full h-full" />
                </div>
                <div className="flex flex-col justify-center items-start w-full md:w-1/2 ml-5 ">
                    <Tittle level="h1" size="text-4xl" color="text-white" align="text-left">
                        The guarantee we offer you</Tittle>
                    <Text color="text-white">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                         nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit 
                         esse cillum dolore eu fugiat nulla pariatur.
                    </Text>
                    <CustomButton
                        title="Look at the upcoming actions"
                        className="text-lg w-3/4 mt-4"
                    />
                </div>
            </div>
        </div>
    );
}

export default Textorelleno;
