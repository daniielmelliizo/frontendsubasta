import React, { useState, useEffect } from 'react';
import CustomButton from '../../../Inviduals/CustomButton';
import HistoryShopController from "src/controllers/HistoryShopController";
import HistoryShopService from "src/aplication/services/HistoryShopService";
import { FaCheck, FaTimes } from "react-icons/fa";
import InputBuscador from '../../../Inviduals/InputBuscador';
import OptionsModal from '../../../Modal/ModalOptionRegister';
import ModalUpdateShop from '../../../Modal/Cliente/HistoryShop/ModalUpdateShop';
import ModalAlertsConfir from "../../../Modal/ModalAlertsConfir";
import ModalAlertsError from "../../../Modal/ModalAlertsError";
import CustomTable from '../../../Inviduals/CustomTable';

const historyShopService = new HistoryShopService();
const historyShopController = new HistoryShopController(historyShopService);

const HistoryShopAdmin = () => {
  const [clientes, setClientes] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [isOptionModalOpen, setOptionModalOpen] = useState(false);
  const [isConfirmationModalOpen, setConfirmationModalOpen] = useState(false);
  const [selectedUser, setSelectedUser] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setConfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  const headers = ["User","Address", "City", "Postal Code", "Product or Sample", "Final Price (USD)", "State"];

  const handleGet = async () => {
    try {
      const response = await historyShopController.handleGetHistoryShopAdmin();
      setClientes(response);
    } catch (error) {
      console.error("Error fetching suscripciones:", error);
    }
  };

  useEffect(() => {
    handleGet();
  }, []);

  const handleSearch = (query) => {
    setSearchTerm(query);
  };

  const openConfirmationModal = (user) => {
    setSelectedUser(user);
    setConfirmationModalOpen(true);
  };

  const handleConfirmUpdate = async () => {
    try {
      const updatedUser = { status: true };
      const response = await historyShopController.handleUpdateShop(selectedUser.id, updatedUser);
      if (response.status === 400) {
        handleErrorConfiModal(response.data.message);
      } else {
        handleOpenConfiModal(response.message);
      }
      handleGet();
    } catch (error) {
    } finally {
      setConfirmationModalOpen(false);
    }
  };

  const handleOpenConfiModal = (message) => {
    setConfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  const handleClose = () => {
    setConfirModalOpen(false);
  };

  const handleReject = async (user) => {
    try {
      const updatedUser = { status: false };
      const response = await historyShopController.handleUpdateShop(user.id, updatedUser);
      if (response.status === 400) {
        handleErrorConfiModal(response.data.message);
      } else {
        handleOpenConfiModal(`Compra de ${user.user.nombre} ${user.user.apellido} ha sido negada.`);
      }
      handleGet();
    } catch (error) {
      console.error("Error rechazando la compra:", error);
    }
  };

  const filteredClientes = clientes.filter(cliente =>
    cliente.user.nombre.toLowerCase().includes(searchTerm.toLowerCase()) ||
    cliente.user.apellido.toLowerCase().includes(searchTerm.toLowerCase()) ||
    cliente.user.email.toLowerCase().includes(searchTerm.toLowerCase())
  );

  const configureRow = (user) => ({
    usuario: `${user.user.nombre} ${user.user.apellido}`,
    direccion: user.direccion || "Not available",
    ciudad: user.ciudad || "Not available",
    codigoPostal: user.codigoPostal || "Not available",
    productoMuestra: user.carts.map((cartItem, index) => (
      <div key={index}>
        {cartItem.product && (
          <div>
            <strong>Product:</strong> {cartItem.product.nombre}
          </div>
        )}
        {cartItem.muestra && (
          <div>
            <strong>Sample:</strong> {cartItem.muestra.nombre}
          </div>
        )}
      </div>
    )),
    precio: `${user.valor} USD`,
    estado: user.status ? "Enviado" : "En Proceso"
  });

  const configureActions = (user) => (
    <div className="flex space-x-2">
      <CustomButton
        className="bg-[#019267]" 
        title={<FaCheck />}
        onClick={() => openConfirmationModal(user)}
      />
      <CustomButton
        className="bg-[#D8354C]" 
        title={<FaTimes />}
        onClick={() => handleReject(user)}
      />
    </div>
  );

  return (
    <div className="flex flex-col space-y-8">
      <div className="flex justify-between items-center">
        <InputBuscador onSearch={handleSearch} />
        <OptionsModal isOpen={isOptionModalOpen} onClose={() => setOptionModalOpen(false)} />
      </div>
      <CustomTable
        headers={headers}
        data={filteredClientes}
        configureRow={configureRow}
        configureActions={configureActions}
      />
      <ModalUpdateShop
        isOpen={isConfirmationModalOpen}
        onClose={() => setConfirmationModalOpen(false)}
        onConfirm={handleConfirmUpdate}
        message={`¿Estás seguro de que deseas validar la compra de este usuario ${selectedUser?.user.nombre} ${selectedUser?.user.apellido}?`}
      />
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={handleClose}
        customMessage={confirMessage}
      />
    </div>
  );
};

export default HistoryShopAdmin;
