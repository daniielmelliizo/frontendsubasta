import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Image from '../../../Inviduals/Image';
import Tittle from '../../../Inviduals/Tittle';
import Text from '../../../Inviduals/Text';
import { useRouter } from 'next/router';
import { FaEdit, FaTrash } from 'react-icons/fa';
import RegisterProductModal from '../../../Modal/Admin/Product/ModalRegisterProducto';
import ProductService from 'src/aplication/services/ProductService';
import CompanyService from 'src/aplication/services/CompanyService';
import ProductController from 'src/controllers/ProductController';
import CompanyController from 'src/controllers/CompanyController';
import ConfirmDeleteModal from '../../../Modal/Admin/users/ConfirmDeleteModal'; // Importa el nuevo modal
import CustomButton from '../../../Inviduals/CustomButton';
import ModalAlertsConfir from '../../../Modal/ModalAlertsConfir';
import ModalAlertsError from '../../../Modal/ModalAlertsError';

const productService = new ProductService();
const companyService = new CompanyService();
const productController = new ProductController(productService);
const companyController = new CompanyController(companyService);

const ProductCardAdmin = ({ product, empresa, handleGetSampleAllId }) => {
  const router = useRouter();
  const [isModalOpen, setModalOpen] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [isDeleteModalOpen, setDeleteModalOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  const handleButtonClick = (e) => {
    e.stopPropagation();
    router.push({
      pathname: '/VistaProduct',
      query: {
        product: JSON.stringify(product),
        empresa: JSON.stringify(empresa),
      },
    });
  };

  const handleEditClick = (e) => {
    e.stopPropagation();
    setSelectedProduct(product);
    setModalOpen(true);
  };

  const handleDeleteClick = (e) => {
    e.stopPropagation();
    setSelectedProduct(product); // Establece el producto seleccionado
    setDeleteModalOpen(true); // Abre el modal de confirmación de eliminación
  };

  const handleConfirmDelete = async (productId) => {
    try {
      const response = await productController.handleDeletProduct(productId);
      if (response) {
        await companyController.removeProductFromCompany(empresa, productId);
      }
      await handleGetSampleAllId();
      if(response.status === 400){
        handleErrorConfiModal(response.data.message);
      }else {
        handleOpenConfiModal(response.message);
      }

      setDeleteModalOpen(false);
    } catch (error) {
      alert(`Error al eliminar el producto: ${error.message || 'Error desconocido'}`);
    }
  };

  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };
  const handleCloseConfiModal = () => {
    setConfirModalOpen(false);
    setDeleteModalOpen(false);
    onClose();
  };

  const productImage = product.imagen && product.imagen.length > 0 ? product.imagen[0] : 'default-image-url';

  return (
    <>
      <div
        className="bg-white shadow-md rounded-lg overflow-hidden m-4 w-80 h-96 relative cursor-pointer flex flex-col"
        onClick={handleButtonClick}
      >
        <div className="h-40 w-full flex items-center justify-center">
          <Image src={productImage} alt={product.nombre} className="w-full h-full object-contain mx-auto" />
        </div>
        <div className="bg-white p-4 flex-grow flex flex-col justify-between">
          <div>
            <Tittle level="h2" size="text-xl" color="text-black" align="text-left">
              {product.nombre}
            </Tittle>
            <Text size="text-sm" color="text-black" align="text-left">
              Country: {product.pais}
            </Text>
            <Text size="text-sm" color="text-black" align="text-left">
              Farm: {product.fincaProductor}
            </Text>
            <Text size="text-sm" color="text-black" align="text-left">
              Variety: {product.variedad}
            </Text>
            <Text size="text-sm" color="text-black" align="text-left">
              Weight: {product.peso} {product.unidadPeso}
            </Text>
            <Text size="text-sm" color="text-black" align="text-left">
              SCA: {product.score}
            </Text>
            <Text size="text-sm" color="text-black" align="text-left">
             Initial price: {product.precioInicial}
            </Text>
          </div>
          <div className="flex justify-end mt-4">
            <CustomButton
              className="rounded p-2 mr-2 text-white bg-[#F1C40F] "
              onClick={handleEditClick}
              title="Edit Product"
            >
              <FaEdit size={20} className='text-black'/>
            </CustomButton>
            <CustomButton
              className="rounded p-2 text-white bg-[#D8354C] "
              onClick={handleDeleteClick}
              title="Delete Product"
            >
              <FaTrash size={20} />
            </CustomButton>
          </div>
        </div>
      </div>
      {isModalOpen && (
        <RegisterProductModal
          isOpen={isModalOpen}
          onClose={() => setModalOpen(false)}
          selectedProduct={selectedProduct}
          handleGetSampleAllId={handleGetSampleAllId}
        />
      )}
      {isDeleteModalOpen && (
        <ConfirmDeleteModal
          isOpen={isDeleteModalOpen}
          onDelete={handleConfirmDelete}
          item={selectedProduct}
          itemType="producto"
          fetchItems={handleGetSampleAllId}
          itemDisplayName={product.nombre} // Pasar el nombre del producto al modal
        />
      )}
      <ModalAlertsError isOpen={isErrorModalOpen} onClose={() => setErrorModalOpen(false)} customMessage={errorMessage} />
      <ModalAlertsConfir isOpen={isConfirModalOpen} onClose={handleCloseConfiModal} customMessage={confirMessage} />
    </>
  );
};

ProductCardAdmin.propTypes = {
  product: PropTypes.object.isRequired,
  empresa: PropTypes.object.isRequired,
  handleGetSampleAllId: PropTypes.func.isRequired,
};

export default ProductCardAdmin;
