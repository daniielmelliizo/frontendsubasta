import React, { useState, useEffect } from "react";
import Tittle from "../../../Inviduals/Tittle";
import ProductCardAdmin from "./ProductCardAdmin";
import InputBuscador from "../../../Inviduals/InputBuscador";
import CompanyService from "src/aplication/services/CompanyService";
import CompanyController from "src/controllers/CompanyController";
import ExpandableButton from "../../../Inviduals/Desplegable";
import NewDropdownModal from "../Company/ModalRegisterEmpresa";
import RegisterProductModal from "../../../Modal/Admin/Product/ModalRegisterProducto";
import ConfirmDeleteModal from "../../../Modal/Admin/users/ConfirmDeleteModal";
import { FaPlus, FaEdit, FaTrash } from "react-icons/fa";
import AddProductCard from "../../../Inviduals/AddProductCard";
import secureStorage from '../../../../utils/secureStorage';
import CustomButton from "../../../Inviduals/CustomButton";
import ModalAlertsError from "../../../Modal/ModalAlertsError";
import ModalAlertsConfir from "../../../Modal/ModalAlertsConfir";


const companyService = new CompanyService();
const companyController = new CompanyController(companyService);

function ProductsAdmin() {
  const [empresas, setEmpresas] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [expandedCompany, setExpandedCompany] = useState(null);
  const [selectedEmpresa, setSelectedEmpresa] = useState(null);
  const [isDropdownModalOpen, setIsDropdownModalOpen] = useState(false);
  const [isNewProductModalOpen, setIsNewProductModalOpen] = useState(false);
  const [selectedEmpresaForProduct, setSelectedEmpresaForProduct] = useState(null);
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  const handleGetCompanies = async () => {
    try {
      const storedUsername = secureStorage.getItem("user");
      const parsedUser = storedUsername ? JSON.parse(storedUsername) : null;
      const response = await companyController.handleGetCompany();
      setEmpresas(response || []);
    } catch (error) {
      setErrorMessage(error.message || "Ocurrió un error inesperado.");
    }
  };

  useEffect(() => {
    handleGetCompanies();
  }, []);

  const handleCartClick = (empresa) => {
    setSelectedEmpresa(empresa);
    setIsConfirmModalOpen(true);
  };

  const handleToggleExpand = (index) => {
    setExpandedCompany((prevIndex) => (prevIndex === index ? null : index));
  };

  const handleSearch = (query) => {
    setSearchTerm(query);
  };

  const handleOpenDropdownModal = () => {
    setIsDropdownModalOpen(true);
  };

  const handleCloseDropdownModal = () => {
    setIsDropdownModalOpen(false);
  };

  const handleSaveNewEmpresa = async (empresaData) => {
    try {
      const response = await companyController.handleCreateCompany(empresaData);
      await companyController.handleGetCompany();
      handleGetCompanies();
      setIsDropdownModalOpen(false);
      if (response.status === 400) {
        handleErrorConfiModal(response.data.message);
      } else {
        handleOpenConfiModal(response.message);
      }
    } catch (error) {
      // console.log("que me trae el error", error);
      // handleErrorConfiModal(error);
    }
  };

  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  const handleOpenNewProductModal = (empresa) => {
    setSelectedEmpresaForProduct(empresa);
    setIsNewProductModalOpen(true);
  };

  const handleCloseNewProductModal = () => {
    setIsNewProductModalOpen(false);
  };

  const handleEditEmpresa = (empresa) => {
    setSelectedEmpresa(empresa);
    setIsEditModalOpen(true);
  };

  const handleDeleteEmpresa = (empresa) => {
    setSelectedEmpresa(empresa);
    setIsDeleteModalOpen(true);
  };


  const handleConfirmDeleteEmpresa = async (id) => {
    try {
      const response = await companyController.handleDeletCompany(id);
      await handleGetCompanies(); // Refrescar la lista de empresas después de eliminar
      handleOpenConfiModal(response.message);
      setIsDeleteModalOpen(false);
    } catch (error) {
      console.error("Error al eliminar la empresa:", error);
    }
  };




  const filteredEmpresas = empresas.length > 0
    ? empresas.filter((empresa) => empresa.nombre.toLowerCase().includes(searchTerm.toLowerCase()))
    : [];

  return (
    <div className="flex flex-col w-full">
      <div className="flex flex-wrap items-start space-y-4 md:space-y-0 md:space-x-4 w-3/4 mx-auto mt-4">
        <div className="w-full mb-4 md:mb-0 flex flex-col md:flex-row items-center space-y-4 md:space-y-0 md:space-x-4">
          <InputBuscador onSearch={handleSearch} placeholder="Search company..." />
          <CustomButton
            className="w-5/6 md:w-1/3 flex flex-row text-white bg-[#D03449] p-2 rounded-lg items-center justify-center"
            onClick={handleOpenDropdownModal}
          >
            <FaPlus size={24} />
            Add company
          </CustomButton>
        </div>
      </div>
      <div className="flex flex-wrap items-start space-y-4 md:space-y-4 w-3/4 mx-auto mt-4">
        {filteredEmpresas.map((empresa, index) => (
          <div key={empresa.id} className="w-full mb-6">
            <div className="flex flex-row items-center justify-center space-x-4">
              <ExpandableButton
                isExpanded={expandedCompany === index}
                onClick={() => handleToggleExpand(index)}
                onCartClick={() => handleCartClick(empresa)}
                empresaName={empresa.nombre}
                isPurchased={empresa.isPurchased || false}
              />
              <div>
                <CustomButton
                  className="text-white bg-[#F1C40F] p-2 rounded-full hover:bg-yellow-600"
                  onClick={() => handleEditEmpresa(empresa)}
                >
                  <FaEdit size={20} className="text-black" />
                </CustomButton>
              </div>
              <div className="flex space-x-4 ">
                {empresa.productsId.length === 0 && (
                  <CustomButton
                    className="text-white bg-[#D8354C] p-2 rounded-full hover:bg-[#D8354C]"
                    onClick={() => handleDeleteEmpresa(empresa)}
                  >
                    <FaTrash size={20} />
                  </CustomButton>
                )}
              </div>
            </div>

            {expandedCompany === index && (
              <div className="flex flex-wrap items-start space-y-4 mt-4">
                <AddProductCard onClick={() => handleOpenNewProductModal(empresa)} />
                {empresa.productsId.length > 0 ? (
                  empresa.productsId.map((producto) => (
                    <ProductCardAdmin
                      key={producto.id}
                      isPurchased={empresa.isPurchased || false}
                      product={producto}
                      empresa={empresa}
                      handleGetSampleAllId={handleGetCompanies}
                      onCartClick={() => handleCartClick(empresa)}
                      isInSample={empresa.isInSample || false}
                    />
                  ))
                ) : (
                  <div key={`no-products-${empresa.id}`} className="w-full text-center">
                    <Tittle color="text-gray-500">
                      No associated products
                    </Tittle>
                  </div>
                )}
              </div>
            )}
          </div>
        ))}
      </div>
      <NewDropdownModal
        isOpen={isDropdownModalOpen}
        onClose={handleCloseDropdownModal}
        onSave={handleSaveNewEmpresa}
        handleGetCompanies={handleGetCompanies}
      />
      <RegisterProductModal
        isOpen={isNewProductModalOpen}
        onClose={handleCloseNewProductModal}
        selectedMuestraId={selectedEmpresaForProduct}
        handleGetSampleAllId={handleGetCompanies}
      />
      {isEditModalOpen && (
        <NewDropdownModal
          isOpen={isEditModalOpen}
          onClose={() => setIsEditModalOpen(false)}
          initialData={selectedEmpresa}
          // onSave={handleUpdateEmpresa}
          handleGetCompanies={handleGetCompanies}

        />
      )}
      <ConfirmDeleteModal
        isOpen={isDeleteModalOpen}
        onClose={() => setIsDeleteModalOpen(false)}
        item={selectedEmpresa}
        itemType="empresa"
        fetchItems={handleGetCompanies}
        onDelete={handleConfirmDeleteEmpresa}
        itemDisplayName={selectedEmpresa?.nombre}
        customMessage={`¿Estás seguro de que deseas eliminar la empresa ${selectedEmpresa?.nombre}?`}
      />
      <ModalAlertsError isOpen={isErrorModalOpen} onClose={() => setErrorModalOpen(false)} customMessage={errorMessage} />
      <ModalAlertsConfir isOpen={isConfirModalOpen} onClose={() => setConfirModalOpen(false)} customMessage={confirMessage} />
    </div>
  );
}

export default ProductsAdmin;
