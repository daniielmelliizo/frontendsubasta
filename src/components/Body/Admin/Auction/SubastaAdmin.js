import React, { useState, useEffect } from "react";
import { useRouter } from 'next/router';
import SubastaController from "src/controllers/SubastaController";
import SubastaService from "src/aplication/services/SubastaService";
import CustomButton from "../../../Inviduals/CustomButton";
import { FaPlus, FaEdit, FaTrash } from "react-icons/fa";
import InputBuscador from "../../../Inviduals/InputBuscador";
import AgregarSubastaModal from "../../../Modal/Admin/Auction/ModalPruebaSubasta";
import ModalEliminarAuction from "../../../Modal/Admin/Auction/ModalEliminarAuction";
import CustomTableSubasta from "../../../Inviduals/CustomTableSubasta";
import Tittle from "../../../Inviduals/Tittle";
import ModalAlertsConfir from "../../../Modal/ModalAlertsConfir";
import ModalAlertsError from "../../../Modal/ModalAlertsError";

const subastaService = new SubastaService();
const subastaController = new SubastaController(subastaService);

const SubastaAdministrador = () => {
  const [subasta, setSubasta] = useState({});
  const [loading, setLoading] = useState(true);
  const [isAgregarModalOpen, setAgregarModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [selectedAuction, setSelectedAuction] = useState(null);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  const router = useRouter();

  const fetchAuctionDetails = async () => {
    setLoading(true);
    try {
      const response = await subastaController.handleGetSubastas();
      setSubasta(response.data);
    } catch (err) {
      console.error("Error in obtaining auction details:", err);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchAuctionDetails();
  }, []);

  const handleOpenEditModal = (data) => {
    setSelectedAuction(data);
    setEditModalOpen(true);
  };

  const handleSearch = (query) => {
    setSearchTerm(query);
  };

  const handleSuscribed = (data) => {
    const firstAuctions = Object.keys(data.categorias).map(categoria => data.categorias[categoria][0]);
    router.push({
      pathname: '/admin/SuscripcionAdmin',
      query: { auction: JSON.stringify(firstAuctions), muestraName: data.muestra }
    });
  };

  const handleDeleteAuction = (data) => {
    setSelectedAuction(data);
    setIsDeleteModalOpen(true);
  };

  const handleConfirmDeleteAuction = async (id) => {
    try {
      const response = await subastaController.handleDeletAuction(id);
      if (response.status === 400) {
        handleErrorConfiModal(response.data.message);
      } else {
        handleOpenConfiModal(response.message);
      }
    } catch (error) {
      console.error("Error deleting the auction:", error.message);
    }
  };

  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  const handleClose = () => {
    setConfirModalOpen(false);
    setIsDeleteModalOpen(false);
    fetchAuctionDetails();
  };

  const filteredSubasta = {};
  for (const muestra in subasta) {
    filteredSubasta[muestra] = {};
    for (const categoria in subasta[muestra]) {
      filteredSubasta[muestra][categoria] = subasta[muestra][categoria].filter(auction =>
        auction.product.nombre.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
  }

  if (loading) {
    return <div>Uploading your auction details ...</div>;
  }

  return (
    <div className="flex flex-col h-screen text-white w-5/6 mx-auto p-4">
      <div className="flex flex-col md:flex-row items-center justify-between mb-4 space-y-2 md:space-y-0">
        <InputBuscador onSearch={handleSearch} placeholder="Search Auctions..."/>
        <CustomButton
          onClick={() => setAgregarModalOpen(true)}
          className="w-full md:w-1/4 flex flex-row text-white bg-[#D03449] p-2 rounded-lg items-center justify-center"
        >
          <FaPlus />
          Add Auction
        </CustomButton>
        <AgregarSubastaModal
          isOpen={isAgregarModalOpen}
          onClose={() => setAgregarModalOpen(false)}
          fetchAuctionDetails={fetchAuctionDetails}
        />
      </div>

      <div className="flex-grow overflow-auto">
        {Object.keys(filteredSubasta).map(muestra => (
          <div key={muestra} className="mb-6 rounded-lg p-4 shadow-md">
            <div className="flex flex-col sm:flex-row sm:items-center sm:justify-between mb-2 space-y-2 sm:space-y-0">
              <Tittle color="text-white">{muestra}</Tittle>
              <div className="flex flex-col space-y-2 sm:flex-col md:flex-row md:space-x-2 lg:space-x-2 md:space-y-0 lg:space-y-0 justify-start">
                <CustomButton
                  onClick={() => handleSuscribed({ muestra, categorias: filteredSubasta[muestra] })}
                  className="bg-white text-[#D8354C] text-lg font-bold rounded-md px-4"
                >
                  Subscription
                </CustomButton>

                <CustomButton
                  onClick={() => handleOpenEditModal({ muestra, categorias: filteredSubasta[muestra] })}
                  className="bg-[#F1C40F] text-white rounded-md px-3 "
                >
                  <FaEdit size={20} className="text-black " />
                </CustomButton>

                <CustomButton
                  onClick={() => handleDeleteAuction({ muestra, categorias: filteredSubasta[muestra] })}
                  className="bg-[#D8354C] text-white rounded-md px-3 "
                >
                  <FaTrash size={20} className="text-white " />
                </CustomButton>
              </div>

            </div>

            {Object.keys(filteredSubasta[muestra]).map(categoria => (
              <CustomTableSubasta
                key={categoria}
                data={{ categoria, auctions: filteredSubasta[muestra][categoria] }}
              />
            ))}
          </div>
        ))}
      </div>

      <AgregarSubastaModal
        isOpen={editModalOpen}
        onClose={() => setEditModalOpen(false)}
        auction={selectedAuction}
        fetchAuctionDetails={fetchAuctionDetails}
      />
      <ModalEliminarAuction
        isOpen={isDeleteModalOpen}
        onConfirm={() => handleConfirmDeleteAuction(selectedAuction.categorias[Object.keys(selectedAuction.categorias)[0]][0].muestraId)}
        auction={selectedAuction}
      />
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={handleClose}
        customMessage={confirMessage}
      />
    </div>
  );
};

export default SubastaAdministrador;
