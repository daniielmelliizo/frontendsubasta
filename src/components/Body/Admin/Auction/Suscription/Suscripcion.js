import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { FaTrash, FaCheck, FaTimes } from "react-icons/fa";
import PropTypes from "prop-types";
import Tittle from "../../../../Inviduals/Tittle";
import InputBuscador from "../../../../Inviduals/InputBuscador";
import OptionsModal from "../../../../Modal/ModalOptionRegister";
import ConfirmDeleteModal from "../../../../Modal/Admin/users/ConfirmDeleteModal"; // Importa el nuevo modal
import CustomTable from "../../../../Inviduals/CustomTable";
import SuscripcionService from "src/aplication/services/SuscripcionService";
import SuscripcionController from "src/controllers/SuscripcionController";
import CustomButton from "../../../../Inviduals/CustomButton";
import Text from "../../../../Inviduals/Text";
import CheckUserModal from "../../../../Modal/Admin/Auction/Suscription/Modalcheckuser";
import InvalidUserModal from "../../../../Modal/Admin/Auction/Suscription/Modalcanceluser";
import ModalAlertsConfir from "../../../../Modal/ModalAlertsConfir";

const suscripcionService = new SuscripcionService();
const suscripcionController = new SuscripcionController(suscripcionService);

const Suscripcion = () => {
  const [clientes, setClientes] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [isOptionModalOpen, setOptionModalOpen] = useState(false);
  const [isDeleteModalOpen, setDeleteModalOpen] = useState(false);
  const [selectedSuscripcion, setSelectedSuscripcion] = useState(null);
  const [nombre, setNombre] = useState("");
  const [isCheckModalOpen, setCheckModalOpen] = useState(false); // Estado para el modal de suscripción validada
  const [isInvalidModalOpen, setInvalidModalOpen] = useState(false);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const router = useRouter();

  useEffect(() => {
    const { auction, muestraName } = router.query;
    setNombre(muestraName);

    if (auction) {
      const auctionData = JSON.parse(auction);
      const muestraId = auctionData[0].muestraId;
      fetchSuscripciones(muestraId);
    }
  }, [router.query]);

  const fetchSuscripciones = async (muestraId) => {
    try {
      const suscripciones = await suscripcionController.handleGetsuscripcion(
        muestraId
      );
      const uniqueSuscripciones = suscripciones.filter(
        (suscripcion, index, self) =>
          index === self.findIndex((s) => s.userId.id === suscripcion.userId.id)
      );
      setClientes(uniqueSuscripciones);
    } catch (error) {
      console.error("Error fetching suscripciones:", error);
      setClientes([]);
    }
  };

  const handleSearch = (query) => setSearchTerm(query);

  const handleDelete = (suscripcion) => {
    setSelectedSuscripcion(suscripcion);
    setDeleteModalOpen(true);
  };

  const handleConfirmDelete = async (suscripcionId) => {
    try {
      await suscripcionController.handleDeletsuscripcion(
        suscripcionId.userId.id,
        suscripcionId.muestraId
      );
      fetchSuscripciones(suscripcionId.muestraId); // Asegúrate de recargar después de eliminar
      handleOpenConfiModal("Suscripción eliminada exitosamente");
    } catch (error) {
    } finally {
      setDeleteModalOpen(false);
    }
  };

  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleUpdateStatus = async (user, newStatus) => {
    try {
      const updatedUser = {
        ...user,
        status: newStatus,
        muestraId: user.muestraId,
      };
      await suscripcionController.handleUpdatesuscripcion(
        user.userId.id,
        updatedUser
      );
      setClientes(clientes.map((u) => (u.id === user.id ? updatedUser : u)));
    } catch (error) {
      console.error("Error updating status:", error);
      alert("Error al actualizar la suscripción.");
    }
  };

  const filteredClientes = clientes.filter(
    (cliente) =>
      cliente.userId.nombre.toLowerCase().includes(searchTerm.toLowerCase()) ||
      cliente.userId.apellido
        .toLowerCase()
        .includes(searchTerm.toLowerCase()) ||
      cliente.userId.email.toLowerCase().includes(searchTerm.toLowerCase())
  );

  const headers = ["Nombre", "Correo", "Status"];
  const configureRow = (user) => ({
    Nombre: `${user.userId.nombre} ${user.userId.apellido}`,
    Correo: user.userId.email,
    Status: user.status ? "Bid Now" : "Access Pending",
  });

  const configureActions = (user) => (
    <div className="flex space-x-2">
      <CustomButton
        className={user.status ? "bg-green-500" : "bg-yellow-500"}
        title={<FaCheck className="text-black" />}
        onClick={() => {
          setCheckModalOpen(true);
          handleUpdateStatus(user, true);
        }}
      />
      <CustomButton
        className={user.status ? "bg-yellow-500" : "bg-green-500 "}
        title={<FaTimes className="text-black" />}
        onClick={() => {
          setInvalidModalOpen(true);
          handleUpdateStatus(user, false);
        }}
      />
      <CustomButton
        className="bg-[#D8354C]"
        title={<FaTrash />}
        onClick={() => handleDelete(user)}
      />
    </div>
  );

  return (
    <div className="bg-black flex flex-col w-full h-full">
      <div className="bg-black rounded-lg shadow-lg w-full h-full">
        <Tittle color="text-white" size="text-xl">
          Subscriptions for auction {nombre}
        </Tittle>

        <div className="flex justify-between items-center">
          <InputBuscador onSearch={handleSearch} />
          <OptionsModal
            isOpen={isOptionModalOpen}
            onClose={() => setOptionModalOpen(false)}
          />
        </div>

        <div className="mt-4">
          {clientes.length === 0 ? (
            <Text className="text-center mt-4 text-gray-500">
              This auction has no subscriptions yet.
            </Text>
          ) : (
            <CustomTable
              headers={headers}
              data={filteredClientes}
              configureRow={configureRow}
              configureActions={configureActions}
            />
          )}
        </div>

        <ConfirmDeleteModal
          isOpen={isDeleteModalOpen}
          onClose={() => setDeleteModalOpen(false)}
          item={selectedSuscripcion} // Pasamos la suscripción seleccionada
          itemType="suscripción"
          fetchItems={() => fetchSuscripciones(selectedSuscripcion.muestraId)} // Recarga las suscripciones
          onDelete={() => handleConfirmDelete(selectedSuscripcion)} // Usa selectedSuscripcion directamente
          itemDisplayName={`${selectedSuscripcion?.userId?.nombre} ${selectedSuscripcion?.userId?.apellido}`} // Nombre del suscriptor
        />

        {isCheckModalOpen && (
          <CheckUserModal onClose={() => setCheckModalOpen(false)} />
        )}

        {isInvalidModalOpen && (
          <InvalidUserModal onClose={() => setInvalidModalOpen(false)} />
        )}
        {isConfirModalOpen && (
          <ModalAlertsConfir
            isOpen={isConfirModalOpen}
            onClose={() => setConfirModalOpen(false)}
            customMessage={confirMessage}
          />
        )}
      </div>
    </div>
  );
};

Suscripcion.propTypes = {
  auction: PropTypes.object,
};

export default Suscripcion;
