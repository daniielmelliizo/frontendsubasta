import React, { useState, useEffect } from 'react';
import CustomButton from '../../../Inviduals/CustomButton';
import UserService from 'src/aplication/services/userService';
import UserController from 'src/controllers/userController';
import { FaTrash, FaCheck, FaTimes } from "react-icons/fa";
import InputBuscador from '../../../Inviduals/InputBuscador';
import ConfirmDeleteModal from '../../../Modal/Admin/users/ConfirmDeleteModal';
import CheckUserModal from '../../../Modal/Admin/Auction/Suscription/Modalcheckuser';
import InvalidUserModal from '../../../Modal/Admin/Auction/Suscription/Modalcanceluser';
import CustomTable from '../../../Inviduals/CustomTable';

const userService = new UserService();
const userController = new UserController(userService);

const UserTable = () => {
  const [clientes, setClientes] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [isModalCheckUserOpen, setIsModalCheckUserOpen] = useState(false);
  const [isInvalidModalOpen, setIsInvalidModalOpen] = useState(false);
  const [isDeleteModalOpen, setDeleteModalOpen] = useState(false);
  const [selectedUser, setSelectedUser] = useState(null);  // Estado para almacenar el usuario seleccionado

  const fetchUsers = async () => {
    try {
      const users = await userController.handleGetUsers();
 
      const nonAdminUsers = users.filter(user => user.rol !== "Administrador");
      
      setClientes(nonAdminUsers);
    } catch (error) {
      console.error("Error fetching users:", error);
    }
  };
  

  useEffect(() => {
    fetchUsers();
  }, []);

  const handleSearch = (query) => {
    setSearchTerm(query);
  };

  // Función que se activa al intentar eliminar un usuario
  const handleDelete = (user) => {
    setSelectedUser(user);  // Almacena el usuario seleccionado en el estado
    setDeleteModalOpen(true);  // Abre el modal de eliminación
  };

  const handleUpdateStatus = async (user, newStatus) => {
    try {
      const data = { status: newStatus }; // Actualizamos el estado
      
      await userController.handleUpdateUsers(user.id, data);
      fetchUsers();
    } catch (error) {
      console.error("Error updating user status:", error);
    }
  };

  const filteredClientes = clientes.filter(cliente =>
    cliente.nombre.toLowerCase().includes(searchTerm.toLowerCase()) ||
    cliente.apellido.toLowerCase().includes(searchTerm.toLowerCase()) ||
    cliente.email.toLowerCase().includes(searchTerm.toLowerCase())
  );

  const headers = [
    'Name', 'Last name', 'Email', 'Role', 'Phone', 'Type of identity', 'Cargo', 'Country', 'Address', 'Status'
  ];

  const data = filteredClientes.map(user => ({
    id: user.id,
    status: user.status,
    nombre: user.nombre,
    apellido: user.apellido,
    email: user.email,
    rol: user.rol,
    phone: user.phone,
    typeIdentificacion: user.typeIdentificacion,
    cargo: user.cargo,
    pais: user.pais,
    direccion: user.direccion,
  }));

  const configureRow = (rowData) => ({
    nombre: rowData.nombre,
    apellido: rowData.apellido,
    email: rowData.email,
    rol: rowData.rol,
    phone: rowData.phone,
    typeIdentificacion: rowData.typeIdentificacion,
    cargo: rowData.cargo,
    pais: rowData.pais,
    direccion: rowData.direccion,
    status: rowData.status ? 'Activo' : 'Inactivo',
  });

  const configureActions = (actionData) => (
    <div className="flex space-x-2">
      <CustomButton
        className={`rounded-lg ${actionData.status ? "bg-[#019267] hover:bg-[#218838]" : "bg-[#F1C40F] hover:bg-[#E6B800]"}`}
        title={<FaCheck className='text-black' />}
        onClick={() => {
          setIsModalCheckUserOpen(true);
          handleUpdateStatus(actionData, true); // Cambiar status a true
        }}
      />
      <CustomButton
        className={`rounded-lg ${!actionData.status ? "bg-[#019267] hover:bg-[#218838]" : "bg-[#F1C40F] hover:bg-[#E6B800]"}`}
        title={<FaTimes className='text-black' />}
        onClick={() => {
          setIsInvalidModalOpen(true);
          handleUpdateStatus(actionData, false); // Cambiar status a false
        }}
      />
      <CustomButton className="bg-[#D8354C] rounded-lg hover:bg-[#D8354C]" onClick={() => handleDelete(actionData)}>
        <FaTrash className="text-white" />
      </CustomButton>
    </div>
  );

  return (
    <div className="flex flex-col space-y-8">
      <div className="flex items-center justify-between mb-4">
        <InputBuscador onSearch={handleSearch} placeholder="Search Users..."/>
      </div>

      <CustomTable
        headers={headers}
        data={data}
        configureRow={configureRow}
        configureActions={configureActions}
      />

      {isDeleteModalOpen && selectedUser && (
        <ConfirmDeleteModal
          isOpen={isDeleteModalOpen}
          onClose={() => setDeleteModalOpen(false)}
          item={selectedUser}  // Pass the selected user to the modal
          itemType="usuario"  // Set the item type to "usuario"
          fetchItems={fetchUsers}  // Fetch users after deletion
          onDelete={async (userId) => {
            try {
              const response = await userController.handleDeleteUser(userId);
              if (response) {
                return { message: `Usuario eliminado con éxito.` };
              } else {
                return null;
              }
            } catch (error) {
              console.error("Error deleting user:", error);
              return null;
            }
          }}
          itemDisplayName={`${selectedUser?.nombre} ${selectedUser?.apellido}`}  // Display user’s name
          customMessage={`¿Estás seguro de que deseas eliminar el usuario ${selectedUser?.nombre} ${selectedUser?.apellido}?`}
        />
      )}

      {isModalCheckUserOpen && (
        <CheckUserModal onClose={() => setIsModalCheckUserOpen(false)} />
      )}
      {isInvalidModalOpen && (
        <InvalidUserModal onClose={() => setIsInvalidModalOpen(false)} />
      )}
    </div>
  );
};

export default UserTable;
