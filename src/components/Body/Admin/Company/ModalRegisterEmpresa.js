import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import UserService from "src/aplication/services/userService";
import UserController from "src/controllers/userController";
import Input from "../../../Inviduals/Input";
import CompanyService from "src/aplication/services/CompanyService";
import CompanyController from "src/controllers/CompanyController";
import ModalAlertsConfir from "../../../Modal/ModalAlertsConfir";
import ModalAlertsError from "../../../Modal/ModalAlertsError";

const userService = new UserService();
const userController = new UserController(userService);
const companyService = new CompanyService();
const companyController = new CompanyController(companyService);

const NewDropdownModal = ({ isOpen, onClose, onSave, initialData, handleGetCompanies }) => {
  const [formData, setFormData] = useState({
    userId: "",
    nit: "",
    nombre: "",
    webSite: "",
    imagenes: [],
  });
  const [users, setUsers] = useState([]);
  const [previewImages, setPreviewImages] = useState([]);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  // Actualizar el formulario si hay datos iniciales (edición)
  useEffect(() => {
    if (initialData) {
      setFormData({
        userId: initialData.userId || "",
        nit: initialData.nit || "",
        nombre: initialData.nombre || "" ,
        webSite: initialData.webSite || "",
        imagenes: initialData.imagen || [],
      });
      const imageUrls = (initialData.imagen || []).map((img) => img); // Asume que ya son URLs

      setPreviewImages(imageUrls);
    }
  }, [initialData]);

  // Cargar los usuarios solo cuando el modal esté abierto
  useEffect(() => {
    const handleGetUsers = async () => {
      try {
        const response = await userController.handleGetUsersCargo();
        setUsers(response);
      } catch (error) {
        console.error("Error al obtener los usuarios:", error.message);
      }
    };

    if (isOpen) {
      handleGetUsers();
    }
  }, [isOpen]);

  // Manejo de cambios en los campos de entrada
  const handleInputChange = (e) => {
    const { id, value, files, type } = e.target;

    if (type === "file") {
      const selectedFiles = Array.from(files);

      setFormData((prevFormData) => ({
        ...prevFormData,
        imagenes: [...(prevFormData.imagenes || []), ...selectedFiles], // Añadir archivos seleccionados
      }));

      const imageUrls = selectedFiles.map((file) => URL.createObjectURL(file));
      setPreviewImages((prevPreviews) => [...prevPreviews, ...imageUrls]);
    } else {
      setFormData({
        ...formData,
        [id]: value,
      });
    }
  };

  const handleRemoveImage = (index) => {
    setFormData((prevFormData) => {
      // Ensure imagenes is an array and check its existence
      const updatedImages = (prevFormData.imagenes || []).filter(
        (_, i) => i !== index
      );
      return {
        ...prevFormData,
        imagenes: updatedImages,
      };
    });

    // Remove the corresponding preview image
    setPreviewImages((prevPreviews) =>
      prevPreviews.filter((_, i) => i !== index)
    );
  };

  // Manejo de la selección de usuario desde el dropdown
  const handleUserSelect = (e) => {
    const selectedUser = users.find((user) => user.id === e.target.value);
    if (selectedUser) {
      setFormData({
        ...formData,
        userId: selectedUser.id,
      });
    }
  };

  // Guardar los datos y limpiar el formulario
  const handleSave = () => {
    const formDataSave = new FormData();

    // Append all the text fields from tempFormData to formDataSave
    Object.keys(formData).forEach((key) => {
      if (key !== "imagenes") {
        formDataSave.append(key, formData[key]);
      }
    });

    // Append the image files to formDataSave
    if (formData.imagenes && formData.imagenes.length > 0) {
      formData.imagenes.forEach((file) => {
        formDataSave.append("imagenes", file); // 'imagenes' is the field name for files
      });
    }
    onSave(formDataSave);
    setFormData({
      userId: "",
      nit: "",
      nombre: "",
      webSite: "",
      imagenes: [],
    });
  };

  const handleUpdateEmpresa = async () => {
    
    try {
      const formDataSave = new FormData();

    // Append all the text fields from tempFormData to formDataSave
    Object.keys(formData).forEach((key) => {
      if (key !== "imagenes") {
        formDataSave.append(key, formData[key]);
      }
    });
    if (formData.imagenes && formData.imagenes.length > 0) {
      formData.imagenes.forEach((image) => {
        if (typeof image === 'string') { // Si es una URL (cadena)
          formDataSave.append("imagenesUrls", image); // Agregar URL de imagen existente
        }
      });

      // Agregar archivos de imagen nuevos a formDataSave
      formData.imagenes.forEach((file) => {
        if (file instanceof File) { // Verificar si es un objeto File
          formDataSave.append("imagenes", file); // Agregar nuevo archivo de imagen
        }
      });
    }
      const response = await companyController.handleUpdateCompany(initialData.id, formDataSave);
      handleGetCompanies();
      if(response.status === 400){
        handleErrorConfiModal(response.data.message);
     }else {
       handleOpenConfiModal(response.message);
     }
    } catch (error) {
      console.error("Error updating the company:", error);
    }
  };

  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };
  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };
  const handleCloseConfirModal = () => {
    setConfirModalOpen(false);
    setErrorModalOpen(false);
    onClose();
    // Add any additional logic needed after confirmation, if required
  };

  // Si el modal no está abierto, no mostrar nada
  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 flex bg-#090B11  items-center justify-center z-50 bg-black bg-opacity-50">
      <div className="bg-[#2F3542] p-6 rounded-lg shadow-lg w-full max-w-md md:max-w-lg lg:max-w-xl">
        <h2 className="text-xl font-bold mb-4 text-white">Company registration</h2>

        {/* Selección de usuario */}
        <div className="mb-4">
          <label
            className="block text-white text-sm font-bold mb-2"
            htmlFor="nombre_empresa"
          >
            Company user
          </label>
          <select
            id="nombre_empresa"
            value={formData.userId}
            onChange={handleUserSelect}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          >
            <option value="">Select a user...</option>
            {users.map((user) => (
              <option key={user.id} value={user.id}>
                {user.nombre} {user.apellido}
              </option>
            ))}
          </select>
        </div>

        {/* Campos de formulario adicionales */}
        <div className="mb-4">
          <label
            className="block text-white text-sm font-bold mb-2"
            htmlFor="nit"
          >
            NIT
          </label>
          <Input
            type="text"
            id="nit"
            name="nit"
            value={formData.nit}
            onChange={handleInputChange}
            placeholder="NIT"
          />
        </div>

        <div className="mb-4">
          <label
            className="block text-white text-sm font-bold mb-2"
            htmlFor="nombre"
          >
            Company name
          </label>
          <Input
            type="text"
            name="nombre"
            id="nombre"
            value={formData.nombre}
            onChange={handleInputChange}
            placeholder="Comapany name"
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          />
        </div>

        <div className="mb-4">
          <label
            className="block text-white text-sm font-bold mb-2"
            htmlFor="webSite"
          >
            Web Site
          </label>
          <Input
            type="text"
            id="webSite"
            name="webSite"
            value={formData.webSite}
            onChange={handleInputChange}
            placeholder="Web site"
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          />
        </div>

        <div className="mb-4">
          <label
            className="block text-white text-sm font-bold mb-2"
            htmlFor="imagenes"
          >
            Company logo
          </label>
          <Input
            title="Images"
            name="imagenes"
            accept="image/*"
            onChange={handleInputChange}
            type="file"
            multiple
          />
          <div className="flex flex-wrap gap-2 mt-2">
            {previewImages.map((image, index) => (
              <div key={index} className="relative">
                <img
                  src={image}
                  alt={`Vista previa ${index + 1}`}
                  className="w-32 h-32 object-cover rounded-lg"
                />
                <button
                  type="button"
                  className="absolute top-0 right-0 bg-[#D03449] text-white rounded-full p-1"
                  onClick={() => handleRemoveImage(index)}
                >
                  X
                </button>
              </div>
            ))}
          </div>
        </div>

        {/* Botones de acción */}
        <div className="flex justify-end">
          <button
            className="bg-[#D03449] text-white py-2 px-4 rounded mr-2 hover:bg-red-600"
            onClick={onClose}
          >
            Cancel
          </button>
          {initialData ? (
            // Show "Actualizar" button if initialData exists
            <button
              className="bg-[#019267] text-white py-2 px-4 rounded "
              onClick={handleUpdateEmpresa}
            >
              Update
            </button>
          ) : (
            // Show "Guardar" button if there's no initialData
            <button
              className="bg-[#019267] text-white py-2 px-4 rounded hover:bg-green-600"
              onClick={handleSave}
            >
              Save
            </button>
          )}
        </div>
      </div>
      <ModalAlertsError isOpen={isErrorModalOpen} onClose={handleCloseConfirModal} customMessage={errorMessage} />
      <ModalAlertsConfir isOpen={isConfirModalOpen} onClose={handleCloseConfirModal} customMessage={confirMessage} />
    </div>
  );
};

NewDropdownModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  // onSave: PropTypes.func.isRequired,
  initialData: PropTypes.object,
  handleGetCompanies: PropTypes.func.isRequired,

};

export default NewDropdownModal;
