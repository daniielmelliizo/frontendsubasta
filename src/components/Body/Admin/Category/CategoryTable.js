import React, { useEffect, useState } from 'react';
import CategoriaService from 'src/aplication/services/CategoryService';
import CategoriaController from 'src/controllers/CategoryController';
import ModalRegisterCategory from '../../../Modal/Admin/Category/ModalCategoryRegister';
import ModalEditCategory from '../../../Modal/Admin/Category/ModalEditCategory';
import ConfirmDeleteModal from '../../../Modal/Admin/users/ConfirmDeleteModal';  // Importamos el nuevo modal de confirmación
import InputBuscador from '../../../Inviduals/InputBuscador';
import CustomButton from '../../../Inviduals/CustomButton';
import { FaPlus, FaEdit, FaTrash } from 'react-icons/fa';
import CustomTable from '../../../Inviduals/CustomTable';
import ModalAlertsConfir from '../../../Modal/ModalAlertsConfir';
import ModalAlertsError from '../../../Modal/ModalAlertsError';

const categoriaService = new CategoriaService();
const categoriaController = new CategoriaController(categoriaService);

const CategoryTable = () => {
  const [categories, setCategories] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [isModalRegisterOpen, setIsModalRegisterOpen] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalConfirmOpen, setIsModalConfirmOpen] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  useEffect(() => {
    const fetchCategories = async () => {
      try {
        const categoryData = await categoriaController.getAllCategorias();
        setCategories(categoryData);
      } catch (error) {
        console.error('Error fetching categories:', error);
      }
    };
    fetchCategories();
  }, []);

  const handleSearch = (query) => {
    setSearchTerm(query);
  };

  const filteredCategories = categories.filter(category =>
    category.nombre.toLowerCase().includes(searchTerm.toLowerCase())
  );

  const handleAddCategory = () => {
    setIsModalRegisterOpen(true);
  };

  const handleEditCategory = (category) => {
    setSelectedCategory(category);
    setIsModalOpen(true);
  };

  const handleConfirmDelete = (category) => {
    setSelectedCategory(category);
    setIsModalConfirmOpen(true);
  };

  const handleDeleteCategory = async () => {
    try {
      await categoriaController.handleDeleteCategory(selectedCategory.id);
      setCategories(categories.filter(category => category.id !== selectedCategory.id));
      handleOpenConfiModal("Eliminacion exitosa");
    } catch (error) {
      console.error('Error deleting category:', error);
    }
    setIsModalConfirmOpen(false);
  };

  const handleModalClose = () => {
    setIsModalRegisterOpen(false);
    setIsModalOpen(false);
    setIsModalConfirmOpen(false);
    setSelectedCategory(null);
    categoriaController.getAllCategorias().then(setCategories); // Actualizar categorías
  };
  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };
  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  const handleClose = () => {
    setConfirModalOpen(false);
    setIsModalConfirmOpen(false);
    setIsModalConfirmOpen(false);
  };

  const headers = ['Nombre', 'Descripción'];

  const configureRow = (category) => ({
    nombre: category.nombre,
    descripcion: category.descripcion,
  });

  const configureActions = (category) => (
    <>
      <CustomButton
        onClick={() => handleEditCategory(category)}
        className="bg-[#FFCC00] text-black  rounded-full "
      >
        <FaEdit className='text-black'/>
      </CustomButton>
      <CustomButton
        onClick={() => handleConfirmDelete(category)}
        className="bg-[#D8354C] text-white rounded-full"
      >
        <FaTrash />
      </CustomButton>
    </>
  );

  return (
    <div className="p-4">
      <div className="w-full mb-4 md:mb-0 flex flex-col md:flex-row items-center space-y-4 md:space-y-0 md:space-x-4">
        <InputBuscador
          onSearch={handleSearch}
          placeholder="Search categories..."
        />
        <CustomButton
          onClick={handleAddCategory}
          className="w-5/6 md:w-1/3 flex flex-row text-white bg-[#D03449] p-2 rounded-lg items-center justify-center"
        >
          <FaPlus />
          Add Category
        </CustomButton>
      </div>

      <CustomTable
        headers={headers}
        data={filteredCategories}
        configureRow={configureRow}
        configureActions={configureActions}
      />

      <ModalRegisterCategory
        isOpen={isModalRegisterOpen}
        onClose={handleModalClose}
      />
      <ModalEditCategory
        isOpen={isModalOpen}
        onClose={handleModalClose}
        category={selectedCategory}
      />
      <ConfirmDeleteModal
        isOpen={isModalConfirmOpen}
        onClose={handleModalClose}
        item={selectedCategory}  // Pasamos la categoría seleccionada
        itemType="categoría"  // Tipo del ítem (categoría)
        itemDisplayName={selectedCategory?.nombre}  // Nombre de la categoría
        fetchItems={() => categoriaController.getAllCategorias().then(setCategories)} // Actualizar la lista tras eliminar
        onDelete={handleDeleteCategory}  // Función de eliminación
        customMessage={`¿Estás seguro de que deseas eliminar la categoría ${selectedCategory?.nombre}?`} // Mensaje personalizado (opcional)
      />
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={handleClose}
        customMessage={confirMessage}
      />
    </div>
  );
};

export default CategoryTable;
