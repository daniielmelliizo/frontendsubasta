
import React, { useState, useEffect } from "react";
import Tittle from "../../../Inviduals/Tittle";
import ProductCardSubastador from "./ProductCartMuestraAdmin";
import InputBuscador from "../../../Inviduals/InputBuscador";
import { FaPlus, FaTrash, FaEdit, FaUsers  } from "react-icons/fa";
import ExpandableButtonMuestra from "../../../Inviduals/Expandlebuttonmuestra";
import RegisterMuestraModal from "../../../Modal/Admin/Sample/ModalRegistrarMuestra";
import SampleService from "src/aplication/services/SampleService";
import SampleController from "src/controllers/SampleController";
import ConfirmDeleteModal from "../../../Modal/Admin/users/ConfirmDeleteModal";
import EditMuestraModal from "../../../Modal/Admin/Sample/ModalEditMuestra";
import secureStorage from '../../../../utils/secureStorage';
import CustomButton from "../../../Inviduals/CustomButton";
import ModalAlertsConfir from "../../../Modal/ModalAlertsConfir";
import ModalAlertsError from "../../../Modal/ModalAlertsError";

const sampleService = new SampleService();
const sampleController = new SampleController(sampleService);

function MuestraAdmin() {
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [expandedCompany, setExpandedCompany] = useState(null);
  const [isMuestraModalOpen, setIsMuestraModalOpen] = useState(false);
  const [muestras, setMuestras] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [idUser, setIdUser] = useState("");
  const [isConfirmDeleteModalOpen, setIsConfirmDeleteModalOpen] = useState(false);
  const [muestraToDeleteId, setMuestraToDeleteId] = useState(null);
  const [isEditMuestraModalOpen, setIsEditMuestraModalOpen] = useState(false);
  const [muestraToEdit, setMuestraToEdit] = useState(null);
  const [filter, setFilter] = useState("all");
  const [errorMessage, setErrorMessage] = useState(null);
  const [confirMessage, setconfirMessage] = useState(null);
  const [isConfirModalOpen, setConfirModalOpen] = useState(false);
  const [isErrorModalOpen, setErrorModalOpen] = useState(false);

  useEffect(() => {
    handleGetSample();
  }, []);

  const handleGetSample = async () => {
    try {
      const storedUsername = secureStorage.getItem("user");
      const parsedUser = storedUsername ? JSON.parse(storedUsername) : null;
      setIdUser(parsedUser.id);
      const response = await sampleController.handleGetSampleAdmin();
      setMuestras(response || []);
    } catch (error) {
      console.error("Error al obtener las muestras:", error.message);
      setMuestras([]);
    }
  };

  const handleProductSelect = (producto, muestraIndex) => {
    const newMuestras = [...muestras];
    newMuestras[muestraIndex].productsId.push(producto);
    setMuestras(newMuestras);
  };

  const handleToggleExpand = (index) => {
    setExpandedCompany(expandedCompany === index ? null : index);
  };

  const handleDelete = (id) => {
    setMuestraToDeleteId(id);
    setIsConfirmDeleteModalOpen(true);
  };

  const confirmDelete = async (id) => {
    try {
      const response = await sampleController.handleDeleteUser(id);
      if (response.status === 400) {
        handleErrorConfiModal(response.data.message);
      } else {
        handleOpenConfiModal(response.message);
      }
      setIsConfirmDeleteModalOpen(false);
      setMuestraToDeleteId(null);
      await handleGetSample();
    } catch (error) {
      setIsConfirmDeleteModalOpen(false);
      setMuestraToDeleteId(null);
    }
  };
  const handleCodicoInvit = async (id) => {
    try {
      const data = {
        sampleId: id
      }
      const response = await sampleController.handleCreateCodigoSample(data);
      console.log("que me trae esto", response);
      
        handleOpenConfiModal(`Codigo de Invitdao es  ${response.code} - ${response.randomPart} `);
      await handleGetSample();
    } catch (error) {
    }
  };

  const handleOpenConfiModal = (message) => {
    setconfirMessage(message);
    setConfirModalOpen(true);
  };

  const handleErrorConfiModal = (message) => {
    setErrorMessage(message);
    setErrorModalOpen(true);
  };

  const handleClose = () => {
    setConfirModalOpen(false);
    setIsConfirmDeleteModalOpen(false)
  };

  const addNewMuestra = (muestra) => {
    setMuestras([
      ...muestras,
      { ...muestra, productsId: muestra.productsId || [] },
    ]);
  };

  const openMuestraModal = () => {
    setIsMuestraModalOpen(true);
  };

  const closeMuestraModal = () => {
    setIsMuestraModalOpen(false);
  };

  const handleSearch = (query) => {
    setSearchTerm(query);
  };

  const handleEdit = (muestra) => {
    setMuestraToEdit(muestra);
    setIsEditMuestraModalOpen(true);
  };

  const filteredMuestras = Array.isArray(muestras)
    ? muestras.filter(
      (muestra) =>
        muestra.nombre &&
        muestra.nombre.toLowerCase().includes(searchTerm.toLowerCase())
    )
    : [];

  const displayedMuestras = filteredMuestras.filter((muestra) => {
    if (filter === "all") return true;
    if (filter === "public") return muestra.private !== true;
    if (filter === "private") return muestra.private === true;
    return true;
  });

  return (
    <div className="flex flex-col w-full">
      <div className="flex flex-wrap items-start space-y-4 md:space-y-0 md:space-x-4 w-3/4 mx-auto mt-4">
        <div className="w-full mb-4 md:mb-0 flex flex-col md:flex-row items-center space-y-4 md:space-y-0 md:space-x-4">
          <InputBuscador
            onSearch={handleSearch}
            placeholder="Search sample..."
          />
          <button
            className="w-full md:w-1/3 flex flex-row text-white bg-[#D03449] p-2 rounded-lg items-center justify-center"
            onClick={openMuestraModal}
          >
            <FaPlus size={20} className="mr-2" />
            Add New Sample
          </button>
        </div>
        <div className="flex justify-between items-center w-full">
          {/* Título alineado a la izquierda */}
          <Tittle color="text-white" className="text-xl">Samples</Tittle>

          {/* Botones alineados a la derecha */}
          <div className="flex space-x-6">
            <button
              className={`relative text-xs md:text-sm lg:text-base ${filter === "all" ? "text-white border-b-4 border-[#D8354C]" : "text-white"}`}
              onClick={() => setFilter("all")}
            >
              All
            </button>

            <button
              className={`relative text-xs md:text-sm lg:text-base ${filter === "public" ? "text-white border-b-4 border-[#D8354C]" : "text-white"}`}
              onClick={() => setFilter("public")}
            >
              Direct sales
            </button>

            <button
              className={`relative text-xs md:text-sm lg:text-base ${filter === "private" ? "text-white border-b-4 border-[#D8354C]" : "text-white"}`}
              onClick={() => setFilter("private")}
            >
              Auctions
            </button>
          </div>
        </div>
      </div>
      <div className="flex flex-wrap items-start space-y-4 md:space-y-0 w-3/4 mx-auto mt-4">
        {displayedMuestras.map((muestra, index) => (
          <div key={muestra.id} className="w-full mb-6">
            <div className="flex justify-between items-center mb-2 overflow-hidden flex-col md:flex-row">
              <ExpandableButtonMuestra
                isExpanded={expandedCompany === index}
                onClick={() => handleToggleExpand(index)}
                empresaName={muestra.nombre}
                descripcion={muestra.descripcion}
                valor={muestra.valor}
                codeInvitation={muestra.codeInvitation}
                privateData={muestra.private}
              />
              <div className="flex lg:flex-row items-center justify-center space-x-2  md:mt-0 ml-2 md:flex-col ">
                <CustomButton
                  className="bg-[#F1C40F] text-white rounded-full"
                  onClick={() => handleEdit(muestra)}
                >
                  <FaEdit size={20} className="text-black" />
                </CustomButton>
                <CustomButton
                  className="bg-[#D8354C]  rounded-full"
                  onClick={() => handleDelete(muestra.id)}
                >
                  <FaTrash size={20} />
                </CustomButton>
                {muestra.private ? (
                  <CustomButton
                    className="bg-[#F1C40F] text-black rounded-full"
                    onClick={() => handleCodicoInvit(muestra.id)}
                  >
                    <FaUsers  size={20} />
                  </CustomButton>
                ) : null}

              </div>
            </div>
            {expandedCompany === index && (
              <div className="flex flex-wrap items-start space-y-4 h-auto mt-4">
                {muestra.productsId.length > 0 ? (
                  muestra.productsId.map((producto, productIndex) => (
                    <ProductCardSubastador
                      key={producto.id}
                      product={producto}
                      isSelected={selectedProducts.includes(producto)}
                      onSelect={(product) =>
                        handleProductSelect(product, index)
                      }
                      idUser={idUser}
                      muestra={muestra}
                    />
                  ))
                ) : (
                  <div className="w-full text-center">
                    <Tittle color="text-gray-500">No associated products</Tittle>
                  </div>
                )}
              </div>
            )}
          </div>
        ))}
      </div>
      <RegisterMuestraModal
        isOpen={isMuestraModalOpen}
        onClose={closeMuestraModal}
        onSave={addNewMuestra}
        idUser={idUser}
        handleGetSample={handleGetSample}
      />
      <ConfirmDeleteModal
        isOpen={isConfirmDeleteModalOpen}
        item={{ id: muestraToDeleteId }} // Pasa el objeto de la muestra que deseas eliminar
        itemType="muestra" // Define el tipo de elemento a eliminar
        fetchItems={handleGetSample} // La función para actualizar la lista de muestras después de eliminar
        onDelete={confirmDelete} // La función para eliminar la muestra
        itemDisplayName={muestraToDeleteId && muestras.find(m => m.id === muestraToDeleteId)?.nombre} // Nombre o descripción de la muestra
        customMessage={`¿Estás seguro de que deseas eliminar la muestra ${muestraToDeleteId && muestras.find(m => m.id === muestraToDeleteId)?.nombre}?`} // Mensaje personalizado
      />

      <EditMuestraModal
        isOpen={isEditMuestraModalOpen}
        onClose={() => setIsEditMuestraModalOpen(false)}
        muestra={muestraToEdit}
        handleGetSample={handleGetSample}
      />
      <ModalAlertsError
        isOpen={isErrorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        customMessage={errorMessage}
      />
      <ModalAlertsConfir
        isOpen={isConfirModalOpen}
        onClose={handleClose}
        customMessage={confirMessage}
      />
    </div>
  );
}

export default MuestraAdmin;

