import CustomButton from "../Inviduals/CustomButton";
import Tittle from "../Inviduals/Tittle";
import Text from "../Inviduals/Text";
import Icons from "../Inviduals/Icons";
import { useState, useEffect } from 'react';
import LoginModal from "../Header/Cliente/LoginModal";
import secureStorage from '../../utils/secureStorage';

const Oferta = () => {

    const [isModalLoginOpen, setModalLoginOpen] = useState(false);
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    useEffect(() => {
        // Verifica si el usuario ha iniciado sesión almacenando 'isLoggedIn' en secureStorage
        const storedIsLoggedIn = secureStorage.getItem('isLoggedIn');
        setIsLoggedIn(storedIsLoggedIn === 'true');
    }, []);

    const handleButtonClick = () => {
        if (isLoggedIn) {
            // Si está logueado, redirige a la página de subastas
        } else {
            // Si no está logueado, abre el modal de inicio de sesión
            setModalLoginOpen(true);
        }
    };
    return (
        <div className="bg-black flex justify-center items-center  min-h-screen-75 h-auto overflow-hidden">
            <div className="flex flex-col md:flex-row items-center justify-center space-y-4 md:space-y-0 md:space-x-3 w-2/3 h-full max-w-6x1 mb-8 overflow-hidden">
                <div className=" w-3/4 h-full flex flex-col item-center justify-center p-5">
                    <Text color="text-white w-full text-justify">THE BEST EXOTIC COFFEE</Text>
                    <Tittle color="text-white" className="text-left">
                        We are excited to introduce a unique experience that is transforming the coffee industry
                    </Tittle>
                    <Text color="text-white " className="w-full mx-auto text-justify">
                        Discover a new way to enhance your harvests: join our coffee auction platform and maximize the
                        value of your product!
                    </Text>
                    <CustomButton
                        title="Look at the upcoming actions"
                        onClick={handleButtonClick}
                        className="bg-[#D8354C] text-lg w-full md:w-2/3 mt-4 overflow-hidden text-justify"
                    />
                </div>


                <div className="bg-[#2F3542] h-full border border-white item-center justify-center p-5 w-full md:w-1/2 overflow-hidden rounded-md">
                    <Icons src="/assets/icons/icon-key.svg" divSize="50%" />
                    <Tittle color="text-white" className="text-left">Transparency is key</Tittle>
                    <Text color="text-white" className="text-justify">We believe in building strong, reliable relationships between
                        producers and buyers, allowing mutual benefits. Our platform lets producers offer
                        their best coffee lots, giving buyers access to high-quality beans directly from the growers.</Text>

                </div>
                <div className="bg-[#2F3542] h-full  border border-white item-center justify-center p-5 w-full md:w-1/2 overflow-hidden rounded-md">
                    <Icons src="/assets/icons/buy.svg" divSize="50%" />
                    <Tittle color="text-white" className="text-left">But thats not all.</Tittle>
                    <Text color="text-white" className="text-justify">We are committed to discovering and developing new talent in the coffee
                        industry. Our platform allows emerging producers to be recognized for their quality, ensuring
                        buyers enjoy diverse, high-quality offerings while fostering innovation and growth.</Text>

                </div>



            </div>
            <LoginModal
                isOpen={isModalLoginOpen}
                onClose={() => setModalLoginOpen(false)}
                onLoginSuccess={() => {
                    setIsLoggedIn(true);
                    setModalLoginOpen(false);
                    localStorage.setItem('isLoggedIn', 'true');  // Guarda el estado de inicio de sesión  // Redirige después del login
                }}
            />
        </div>
    );
}

export default Oferta;
