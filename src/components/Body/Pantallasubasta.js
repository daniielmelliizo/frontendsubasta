import React, { useState, useEffect, useRef } from "react";
import { FaList, FaMicrosoft } from "react-icons/fa";
import SubastaController from "src/controllers/SubastaController";
import SubastaService from "src/aplication/services/SubastaService";
import AuctionTable from "../TableTerminate/AuctionTable";
import CustomButton from "../Inviduals/CustomButton";
import AuctionCardView from "../TableTerminate/AuctionCardView";
import secureStorage from '../../utils/secureStorage';
import { useSocket } from "src/infraestructura/Socketcontext";

const subastaService = new SubastaService();
const subastaController = new SubastaController(subastaService);

const TableSubasta = () => {
  const [auctionDetails, setAuctionDetails] = useState({});
  const [error, setError] = useState(null);
  const [isTableView, setIsTableView] = useState(true);
  const [updatedRows, setUpdatedRows] = useState({});
  const [userRol, setUserRol] = useState("");
  const socket = useSocket();

  useEffect(() => {
    fetchAuctionDetails();
    if (socket) {
        console.log("Registrando eventos del socket...");

        socket.on("newBid", (data) => {
            console.log("Evento newBid recibido:", data);
            highlightRow(data.auctionId);
            fetchAuctionDetails();
        });

        socket.on("bidCreated", (data) => {
            console.log("Evento bidCreated recibido:", data);
            highlightRow(data.auctionId);
            fetchAuctionDetails();
        });

        return () => {
            socket.off("newBid");
            socket.off("bidCreated");
        };
    }
}, [socket]);

  const highlightRow = (auctionId) => {
    setUpdatedRows((prevState) => ({
      ...prevState,
      [auctionId]: true,
    }));

    setTimeout(() => {
      setUpdatedRows((prevState) => ({
        ...prevState,
        [auctionId]: false,
      }));
    }, 3000);
  };

  const fetchAuctionDetails = async () => {
    try {
      let data;
      const storedUsername = secureStorage.getItem("user");
      const parsedUser = storedUsername ? JSON.parse(storedUsername) : null;

    // Verificar si parsedUser no es nulo antes de acceder a rol
    if (parsedUser && parsedUser.rol === "Administrador") {
      setUserRol(parsedUser.rol);
    }

      // Si no se ha obtenido data del admin, obtener subastas finalizadas generales
      if (!data) {
        const response = await subastaController.handleGetSubastasFinalizadasAdmin();
        data = response.data;
      }

      // Procesar datos obtenidos
      if (data) {
        const filteredData = Object.fromEntries(
          Object.entries(data).filter(
            ([key, value]) => Object.values(value).flat().length > 0
          )
        );
        
        setAuctionDetails(filteredData);
        setError(null);
      } else {
        setAuctionDetails({});
      }
    } catch (err) {
      console.error(`Error fetching auction details: ${err.message}`, err);
    }
  };

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div className="flex flex-col bg-black text-white w-5/6 mx-auto p-4 ">
      <div className="flex justify-end mb-4">
        <CustomButton
          onClick={() => setIsTableView(!isTableView)}
          className="text-lg w-1/10"
        >
          {isTableView ? <FaMicrosoft /> : <FaList />}
        </CustomButton>
      </div>
      {Object.keys(auctionDetails).length > 0 ? (
        isTableView ? (
          <AuctionTable
            auctionDetails={auctionDetails}
            updatedRows={updatedRows}
            userRol={userRol}
          />
        ) : (
          <AuctionCardView
            auctionDetails={auctionDetails}
            userRol={userRol}
          />

        )
      ) : (
        <div className="text-center text-gray-100 mt-8">
          There are no completed auctions
        </div>
      )}
    </div>
  );
};

export default TableSubasta;
