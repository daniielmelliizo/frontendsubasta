import React, { useState, useEffect } from "react";
import Tittle from "../Inviduals/Tittle";
import ProductCard from "./Cliente/Sample/ProductCardCliente";
import Navbar from "../Header/Cliente/Navbar";
import InputBuscador from "../Inviduals/InputBuscador";
import CompanyService from "src/aplication/services/CompanyService";
import CompanyController from "src/controllers/CompanyController";
import ModalCompraProduct from "../Modal/ModalCompraProduct";
import ExpandableButton from "../Inviduals/Desplegable";
import CarstController from "src/controllers/CarstController";
import CarstService from "src/aplication/services/CarstService";
import LoginModal from "../Header/Cliente/LoginModal"; // Importar el LoginModal
import secureStorage from '../../utils/secureStorage';

const companyService = new CompanyService();
const companyController = new CompanyController(companyService);

const carstService = new CarstService();
const carstController = new CarstController(carstService);

function Products() {
  const [empresas, setEmpresas] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [idUser, setIdUser] = useState("");
  const [expandedCompany, setExpandedCompany] = useState(null);
  const [isConfirmModalOpen, setIsConfirmModalOpen] = useState(false);
  const [selectedProducto, setSelectedProducto] = useState(null);
  const [selectedEmpresa, setSelectedEmpresa] = useState(null);
  const [isLoginModalOpen, setLoginModalOpen] = useState(false); // Estado para el LoginModal

  useEffect(() => {
    const handleGetSampleAllId = async () => {
      try {
        const storedUsername = secureStorage.getItem("user");
        const parsedUser = storedUsername ? JSON.parse(storedUsername) : null;
        setIdUser(parsedUser.id);
        const response = await companyController.handleGetCompanyClient();
        setEmpresas(response);
      } catch (error) {
        console.error("Error al obtener las empresas:", error.message);
      }
    };

    handleGetSampleAllId();
  }, []);

  const handleCartClick = (empresa, producto) => {
    if (!idUser) {
      setLoginModalOpen(true); // Abrir el LoginModal si el usuario no está registrado
      return;
    }
    setSelectedEmpresa(empresa);
    setSelectedProducto(producto);
    setIsConfirmModalOpen(true);
  };

  const handleLoginSuccess = (email, userId, rol) => {
    setIdUser(userId);
    setLoginModalOpen(false); // Cerrar el LoginModal al iniciar sesión correctamente
    // Aquí puedes manejar la lógica adicional después de iniciar sesión, si es necesario
  };

  const handleConfirmPurchase = async () => {
    try {
      const formData = {
        userId: idUser,
        productId: selectedProducto.id
      };
      const response = await carstController.handleCreateCarst(formData);
      alert(`Tu compra ${response.message} para el producto: ${selectedProducto.nombre} de la empresa: ${selectedEmpresa.nombre}`);

      setIsConfirmModalOpen(false);
    } catch (error) {
      console.error("Error al comprar el producto:", error.message);
      alert("Error al intentar comprar el producto");
    }
  };

  const handleToggleExpand = (index) => {
    setExpandedCompany((prevIndex) => (prevIndex === index ? null : index));
  };

  const handleSearch = (query) => {
    setSearchTerm(query);
  };

  const filteredEmpresas = empresas.filter((empresa) =>
    empresa.nombre.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <div className="flex flex-col w-full">
      <div className="bg-[#1293A1] w-full flex justify-between items-center p-4">
        <Navbar />
      </div>
      <div className="flex flex-wrap items-start space-y-4 md:space-y-0 md:space-x-4 w-3/4 mx-auto mt-4">
        <div className="w-full mb-4 md:mb-0 flex items-center space-x-4">
          <InputBuscador
            onSearch={handleSearch}
            placeholder="Search company..."
          />
        </div>
      </div>
      <div className="flex flex-wrap items-start space-y-4 md:space-y-4 w-3/4 mx-auto mt-4">
        {filteredEmpresas.map((empresa, index) => (
          <div key={empresa.id} className="w-full mb-6">
            <ExpandableButton
              isExpanded={expandedCompany === index}
              onClick={() => handleToggleExpand(index)}
              onCartClick={() => handleCartClick(empresa)}
              empresaName={empresa.nombre}
              isPurchased={empresa.isPurchased || false} // Añadir valor por defecto si esPurchased no está definido
            />
            {expandedCompany === index && (
              <div className="flex flex-wrap items-start space-y-4 mt-4">
                {empresa.productsId.length > 0 ? (
                  empresa.productsId.map((producto) => (
                    <ProductCard
                      key={producto.id}
                      isPurchased={empresa.isPurchased}
                      product={producto}
                      empresa={empresa}
                      onCartClick={() => handleCartClick(empresa, producto)}
                    />
                  ))
                ) : (
                  <div className="w-full text-center">
                    <Tittle color="text-gray-500">
                    No associated products
                    </Tittle>
                  </div>
                )}
              </div>
            )}
          </div>
        ))}
      </div>
      {selectedProducto && (
        <ModalCompraProduct
          isOpen={isConfirmModalOpen}
          onClose={() => setIsConfirmModalOpen(false)}
          onConfirm={handleConfirmPurchase}
          product={selectedProducto}
        />
      )}
      <LoginModal
        isOpen={isLoginModalOpen}
        onClose={() => setLoginModalOpen(false)}
        onLoginSuccess={handleLoginSuccess}
      />
    </div>
  );
}

export default Products;
