"use client";
import Navbar from "src/components/Header/Cliente/Navbar";
import VistaInfoSubasta from "src/components/Body/VistainfoSubasta";

export default function VistaSubasta() {

  return (
    <div className="bg-black min-h-screen flex flex-col">
      <Navbar />
      <div className="flex justify-center items-center h-full">
        <VistaInfoSubasta />
      </div>
    </div>
  );
}
