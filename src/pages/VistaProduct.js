
import VistaProductos from "src/components/Body/VistainfoProductos";

export default function VistaProducto() {

  return (
    <div className="bg-black min-h-screen flex flex-col">
      <div className="flex justify-center items-center h-full">
        <VistaProductos />
      </div>
    </div>
  );
}
