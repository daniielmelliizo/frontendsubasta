import React from "react";
import Navbar from "src/components/Header/Cliente/Navbar";
import PantallaSubasta from "src/components/Body/Pantallasubasta";


export default function Subastas() {
  return (
    <div className="bg-black min-h-screen flex flex-col">
      <Navbar />
      <div className="flex-grow flex justify-center items-center">
          <PantallaSubasta />
      </div>
    </div>
  );
}
