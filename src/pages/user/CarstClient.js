import React from "react";
import Navbar from "src/components/Header/Cliente/Navbar";
import CarstClient from "src/components/Body/Cliente/Carst/CarstClient";


export default function CartsClient() {
  return (
    <div className="bg-black min-h-screen container-fluid p-0">
      <Navbar />
      <div className="flex justify-center items-center">
          <CarstClient />
      </div>
    </div>
  );
}
