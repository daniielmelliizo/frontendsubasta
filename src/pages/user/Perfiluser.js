import React, { useEffect, useState } from "react";
import Navbar from "src/components/Header/Cliente/Navbar";
import Perfil from "src/components/Body/Cliente/Perfil/Perfil";
import secureStorage from '../../utils/secureStorage';

export default function Perfiluser() {
  const [rol, setRol] = useState("");

  useEffect(() => {
    const storedUsername = secureStorage.getItem("user");
      const parsedUser = storedUsername ? JSON.parse(storedUsername) : null;
    setRol(parsedUser.rol);
  }, []);

  return (
    <div className="bg-black min-h-screen flex flex-col mx-auto">
      <Navbar />
      <div className="flex justify-center items-center h-full">
        <Perfil />
      </div>
    </div>
  );
}
