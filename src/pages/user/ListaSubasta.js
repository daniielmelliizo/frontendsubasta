import TableSubasta from "../../components/Body/Cliente/Auction/TableSubastas";
import Navbar from "../../components/Header/Cliente/Navbar";
import { SocketProvider } from "src/infraestructura/Socketcontext";

export default function ListaSubasta() {
  return (

    <div className="bg-black min-h-screen flex flex-col">
      <Navbar />
      <div className="flex-grow flex justify-center items-center">
        <SocketProvider>
          <TableSubasta />
        </SocketProvider>
      </div>
    </div>

  );
}