import Navbar from "src/components/Header/Cliente/Navbar";
import MuestraCliente from "src/components/Body/Cliente/Sample/Muestracliente";

export default function ListaSubasta() {
 

  return (
    <div className="bg-black min-h-screen flex flex-col">
      <Navbar />
      <div className="flex justify-center items-center">
          <MuestraCliente />
      </div>
    </div>
  );
}
