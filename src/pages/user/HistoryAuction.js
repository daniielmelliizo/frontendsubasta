import React from "react";
import Navbar from "src/components/Header/Cliente/Navbar";
import Hisotry from "src/components/Body/Cliente/Perfil/Hisotry";


export default function Subastas() {
  return (
    <div className="bg-black min-h-screen flex flex-col">
      <Navbar />
      <div className="flex-grow flex justify-center items-center">
          <Hisotry />
      </div>
    </div>
  );
}
