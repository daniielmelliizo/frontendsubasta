import React from "react";
import Navbar from "src/components/Header/Cliente/Navbar";
import AboutUs from "src/components/Body/Cliente/AboutUs/Aboutus";


export default function Conocenos() {
  return (
    <div className="bg-black container-fluid p-0">
      <Navbar />
      <div className="flex fle-col justify-center items-center">
          <AboutUs />
      </div>
    </div>
  );
}
