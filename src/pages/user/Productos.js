import React, { useEffect, useState } from "react";
import Products from "src/components/Body/Products";
import secureStorage from '../../utils/secureStorage';

export default function ListaSubasta() {
  const [rol, setRol] = useState("");

  useEffect(() => {
    const storedUsername = secureStorage.getItem("user");
      const parsedUser = storedUsername ? JSON.parse(storedUsername) : null;
    setRol(parsedUser.rol);
    
  }, []);

  return (
    <div className="bg-black min-h-screen flex flex-col">
      <div className="flex justify-center items-center h-full">
          <Products />
      </div>
    </div>
  );
}
