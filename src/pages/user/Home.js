"use client";

import React, { useState, useEffect } from 'react';
import Intro from 'src/components/Header/Intro';
import Images from 'src/components/Body/PanelImages';
import Info from 'src/components/Body/Info';
import Oferta from 'src/components/Body/Oferta';
import Subasta from "src/components/Body/subastas";
import DesplegarSubasta from 'src/components/Modal/Cliente/Home/SubastaActualModal'; // Ensure the path is correct
import SubastaController from "src/controllers/SubastaController";
import SubastaService from "src/aplication/services/SubastaService";

const subastaService = new SubastaService();
const subastaController = new SubastaController(subastaService);

export default function Home() {
  const [isSubastaModalOpen, setSubastaModalOpen] = useState(false);
  const [auctionData, setAuctionData] = useState([]);

  useEffect(() => {
    const fetchAuctions = async () => {
      try {
        const response = await subastaController.handleGetSubastas();
        
        if (response && response.data) {
          // Filter and structure auction data as needed
          const data = response.data;
          const auctions = [];
          Object.entries(data).forEach(([sampleName, categories]) => {
            Object.values(categories).forEach((auctionList) => {
              auctionList.forEach((auction) => {
                auctions.push({
                  id: auction.id,
                  nombre: auction.sample.nombre,
                  descripcion: auction.sample.descripcion,
                  fechaInicio: auction.fechaInicio,
                  horaFin: auction.horaFin,
                });
              });
            });
          });

          if (auctions.length > 0) {
            setAuctionData(auctions);
            setSubastaModalOpen(true); // Open modal only if there is auction data
          }
        }
      } catch (error) {
        console.error("Failed to fetch auctions:", error);
      }
    };

    fetchAuctions();
  }, []);

  const handleCloseSubastaModal = () => {
    setSubastaModalOpen(false);
  };

  return (
    <div className="container-fluid">
      <Intro />
      <Images />
      <Info />
      <Oferta />
      <Subasta />
      <DesplegarSubasta isOpen={isSubastaModalOpen} onClose={handleCloseSubastaModal} auctions={auctionData} />
    </div>
  );
}
