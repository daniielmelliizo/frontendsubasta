import SubastaUsers from "src/components/Body/Cliente/Perfil/SubastaUser";
import Navbar from "../../components/Header/Cliente/Navbar";

export default function Activas() {
  return (
    <div className="bg-black min-h-screen flex flex-col">
      <Navbar />
      <div className="flex-grow flex justify-center items-center">
        <SubastaUsers />
      </div>
    </div>
  );
}