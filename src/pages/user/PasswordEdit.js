import EditPassword from "src/components/Body/Cliente/Perfil/EditPassword";
import Navbar from "../../components/Header/Cliente/Navbar";

export default function PasswordEdit() {
  return (
    <div className="bg-black w-full min-h-screen flex flex-col">
      <Navbar />
      <div className="flex-grow flex justify-center items-center">
        <EditPassword />
      </div>
    </div>
  );
}0.