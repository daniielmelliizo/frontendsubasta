import React from "react";
import Navbaradmin from "src/components/Header/Admin/NavBarAdministrador";
import HistoryShopAdmin from "src/components/Body/Admin/HistoryShop/HistoryShopAdmin";


export default function HistorysShopAdmin() {
  return (
    <div className="bg-black min-h-screen container-fluid p-0">
      <Navbaradmin />
      <div className="flex justify-center items-center">
          <HistoryShopAdmin />
      </div>
    </div>
  );
}
