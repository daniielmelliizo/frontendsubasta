import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Navbaradmin from 'src/components/Header/Admin/NavBarAdministrador';
import ProductsAdmin from 'src/components/Body/Admin/Product/ProductAdmin';
import secureStorage from '../../utils/secureStorage';

export default function ProductostAdmin() {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const router = useRouter();

  useEffect(() => {
    const user = secureStorage.getItem('user');
    try {
      const parsedUser = user ? JSON.parse(user) : null;

      if (parsedUser && parsedUser.rol === 'Administrador') {
        setIsAuthenticated(true);
      } else {
        setErrorMessage('Page not available. Only administrators can access.');
        // Opcional: Redirigir a una página de error
        // router.push('/error');
      }
    } catch (error) {
      console.error("Error parsing user from secureStorage: ", error);
      setErrorMessage('Page not available. Only administrators can access.');
    }
  }, []);

  if (!isAuthenticated) {
    return (
      <div className="bg-black min-h-screen flex flex-col justify-center items-center text-white">
        <h1 className="text-3xl font-bold">{errorMessage}</h1>
      </div>
    );
  }

  return (
    <div className="bg-black container-fluid p-0 min-h-screen flex flex-col overflow-hidden">
      <Navbaradmin />
      <div className="flex-grow flex justify-center">
        <ProductsAdmin />
      </div>
    </div>
  );
}
