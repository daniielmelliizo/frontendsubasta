
import Adminlogin from "src/components/Header/Admin/Adminlogin";

export default function LoginAdmin() {
  const handleLoginSuccess = (email, userId, role) => {
    // Maneja el inicio de sesión exitoso (por ejemplo, redirigir, actualizar el estado, etc.)
  };
  return (
    <div >
      <div >
          <Adminlogin onLoginSuccess={handleLoginSuccess}/>
      </div>
    </div>
  );
}
