import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Navbaradmin from "src/components/Header/Admin/NavBarAdministrador";
import CategoryTable from 'src/components/Body/Admin/Category/CategoryTable';
import secureStorage from '../../utils/secureStorage';


export default function UsuarioAdmin() {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const router = useRouter();

  useEffect(() => {
    const user = secureStorage.getItem('user');
    const parsedUser = user ? JSON.parse(user) : null;

    if (parsedUser && parsedUser.rol === 'Administrador') {
      setIsAuthenticated(true);
    } else {
      setErrorMessage('Página no disponible. Solo los administradores pueden acceder.');
      // Opcional: Redirigir a una página de error
      // router.push('/error');
    }
  }, []);

  if (!isAuthenticated) {
    return (
      <div className="bg-black min-h-screen flex flex-col justify-center items-center text-white">
        <h1 className="text-3xl font-bold">{errorMessage}</h1>
      </div>
    );
  }

  return (
    <div className="bg-black min-h-screen w-full flex flex-col overflow-hidden">
      <Navbaradmin />
      <div className="flex-grow flex justify-center">
        <CategoryTable />
      </div>
    </div>
  );
}
