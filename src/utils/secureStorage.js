import SecureStorage from "secure-web-storage";
import CryptoJS from "crypto-js";

// Clave secreta para encriptar y desencriptar datos.
const SECRET_KEY = process.env.NEXT_PUBLIC_SECRET_KEY || "ea4d14539552e3eb5c31aea14b9b0f71bd92ec37dc966640e0126813a976c89d";

// Verificar si estamos en el entorno del cliente
const isClient = typeof window !== "undefined";

let secureStorage;

if (isClient) {
  secureStorage = new SecureStorage(localStorage, {
    hash: function (key) {
      return CryptoJS.SHA256(key, SECRET_KEY).toString();
    },
    encrypt: function (data) {
      return CryptoJS.AES.encrypt(data, SECRET_KEY).toString();
    },
    decrypt: function (data) {
      return CryptoJS.AES.decrypt(data, SECRET_KEY).toString(CryptoJS.enc.Utf8);
    },
  });
} else {
  // En el servidor no creamos una instancia de SecureStorage
  secureStorage = null; // o podrías lanzar un error si es necesario
}

export default secureStorage;
