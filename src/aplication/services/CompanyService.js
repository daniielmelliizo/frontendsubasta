import { requests } from "../../adapters/ServiciosAdapter";

class CompanyService {
  async CreateCompany(data) {
    if (!(data instanceof FormData)) {
      return Promise.reject(
        new Error("La información de la compañia debe ser de tipo FormData.")
      );
    }

    // Establecer los encabezados necesarios
    const headers = { "Content-Type": "multipart/form-data" };

    // Hacer la petición para crear el producto y manejar la respuesta
    return requests("POST", "/companys/companyCreate", data, headers)
      .then((company) => {
        return company;
      })
      .catch((error) => {
        // Manejar el error
        if (
          error.response &&
          error.response.data &&
          error.response.data.message
        ) {
          return error.response
        } else {
          return alert(
            "Ocurrió un error inesperado en el servidor al crear el producto."
          );
        }
      });
  }

  async getCompanyProduct() {
    try {
      const company = await requests("GET", "/companys/company");
      return company;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      return alert(error.response.data.message);
    }
  }
  async updateCompany(id, data) {
    if (!(data instanceof FormData)) {
      return Promise.reject(
        new Error("La información de la compañia debe ser de tipo FormData.")
      );
    }

    // Establecer los encabezados necesarios
    const headers = { "Content-Type": "multipart/form-data" };

    // Hacer la petición para crear el producto y manejar la respuesta
    return requests("PUT", `/companys/companyUpdate/${id}`, data, headers)
      .then((company) => {
        return company;
      })
      .catch((error) => {
        // Manejar el error
        if (
          error.response &&
          error.response.data &&
          error.response.data.message
        ) {
          return error.response
        } else {
          return alert(
            "Ocurrió un error inesperado en el servidor al actualizar la compañia."
          );
        }
      });
  }

  async deleteCompany(id) {
    try {
      const result = await requests("DELETE", `/companys/companyDelet/${id}`);
      return result;
    } catch (error) {
      return error.response
    }
  }

  async getCompanyProductClient() {
    try {
      const company = await requests("GET", "/companys/companyClient");
      return company;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      return alert(error.response.data.message);
    }
  }
}
export default CompanyService;
