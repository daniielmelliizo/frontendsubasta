import { requests } from "../../adapters/ServiciosAdapter";

class CarstService {
  async CreateCarst(data) {
    try {
      const result = await requests(
        "POST",
        "/carts/cartsCreate",
        data
      );
      return result;
    } catch (error) {
      return error.response;
    }
  }

  async getCarstUser(id) {
    try {
      const result = await requests("GET", `/carts/cartsUser/${id}`);
      return result;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      return error.response;
    }
  }
  
  async updateCarstProduct(id, data) {
    try {
      const result = await requests("PUT", `/carts/cartsUpdate/${id}`, data);
      return result;
    } catch (error) {
      return error.response;
    }
  }

  async deleteCarst(id) {
    try {
      const result = await requests("DELETE", `/carts/cartsDelete/${id}`);
      return result;
    } catch (error) {
      return error.response;
    }
  }

  async getCompanyProductClient() {
    try {
      const company = await requests("GET", "/companys/companyClient");
      return company;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      return error.response;
    }
  }
}
export default CarstService;
