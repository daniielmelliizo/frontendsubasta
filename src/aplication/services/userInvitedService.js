import { requests } from "../../adapters/ServiciosAdapter";

class UserInvitedService {
  async createUser(data) {
     try {
      const user = await requests(
        "POST",
        "/users/registerquick",
        data
      );
      return user;
     } catch (error) {
      return alert(error.response.data.message);
     }
  }

};
export default UserInvitedService;
