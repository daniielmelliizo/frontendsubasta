import { requests } from "../../adapters/ServiciosAdapter";

class UserSubastaService {
  async createSubastador(data) {
    try {
      const user = await requests(
        "POST",
        "/users/registerAuction",
        data
      );
      return user;
    } catch (error) {
      return alert(error.response.data.message);
    }
  }

  async Validation(data) {
    try {
      const user = await requests(
        "POST",
        "/auth/send-verification-code",
        data
      );
      return user;
    } catch (error) {
      return alert(error.response.data.message);
    }
  }

  async ValidationComplet(data) {
    try {
      const user = await requests(
        "POST",
        "/auth/verify-email",
        data
      );
      return user;
    } catch (error) {
      return alert(error.response.data.message);
    }
  }

  async createCompany(data) {
    try {
      const user = await requests(
        "POST",
        "/companys/companyCreate",
        data
      );
      return user;
    } catch (error) {
      return alert(error.response.data.message);
    }
  }
}

export default UserSubastaService;
