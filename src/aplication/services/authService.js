import { requests } from "../../adapters/ServiciosAdapter"; 
class AuthenticationService {

  // Método para realizar el inicio de sesión
  async login(email, password) {
    try {
      const datosDeAutenticacion = {
        email: email,
        password: password,
      };
      const user = await requests('POST', '/auth/login', datosDeAutenticacion);

      // Aquí podrías realizar acciones adicionales después de una autenticación exitosa
      return user;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      return error.response;
    }
  }

  // Método para cerrar sesión (si es necesario)
  async logout() {
    // Implementa la lógica de cierre de sesión si es aplicable
  }
}

export default AuthenticationService;
