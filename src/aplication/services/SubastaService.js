import { requests } from "src/adapters/ServiciosAdapter";

class SubastaService {
  
  async getTodasSubastas() {
    try {
      const response = await requests("GET", "/auction/auctions");
      return response;
    } catch (error) {
      return error.response
    }
  }



  async getTodasSubastasClient(id) {
    try {
      const response = await requests("GET", `/auction/auctionsclient/${id}`);
      return response;
    } catch (error) {
      return error.response

    }
  }

  async createSubasta(data) {
    if (!(data instanceof FormData)) {
      return Promise.reject(new Error("La información del producto debe ser de tipo FormData."));
    }
    const headers = { "Content-Type": "multipart/form-data" };
    return requests("POST", "/auction/createAuction", data, headers)
    .then((product) => {
      // Retornar el producto creado si la petición es exitosa
      return product;
    })
    .catch((error) => {
        return error.response
      
      
    });
  }

  

  async updateSubasta(id, data) {
    if (!(data instanceof FormData)) {
      return Promise.reject(new Error("La información del producto debe ser de tipo FormData."));
    }
    const headers = { "Content-Type": "multipart/form-data" };
    return requests("PUT",`/auction/editAuction/${id}`, data, headers)
    .then((product) => {
      // Retornar el producto creado si la petición es exitosa
      return product;
    })
    .catch((error) => {

      // Manejar el error
      if (error.response && error.response.data && error.response.data.message) {
        return error.response
      } else {
        return alert("Ocurrió un error inesperado en el servidor al crear el producto.");
      }
    });
  }



  async deleteSubasta(id) {
    try {
      const result = await requests("DELETE", `/auction/deleteAuction/${id}`);
      return result;
    } catch (error) {
      return error.response
    }
  }

  async getConfigBid(id) {
    try {
      const response = await requests("GET", `/bidsUser/getAll/${id}`);
      return response;
    } catch (error) {
      return error.response
    }
  }

  async createConfigBid(data) {
    try {
      const response = await requests("POST", `bidsUser/create`, data);
      return response;
    } catch (error) {
      return error.response
    }
  }

  async updateConfigBid(id, data) {
    try {
      const response = await requests(
        "PUT",
        `/bidsUser/update/${id}`,
        data
      );
      return response;
    } catch (error) {
      return error.response
    }
  }

  async getAuctionByUserMuestra(muestraId, userId) {
    try {
      const response = await requests(
        "GET",
        `/auction/auctionIdUser?muestraId=${muestraId}&userId=${userId}`
      );
      return response;
    } catch (error) {
      return error.response
    }
  }

  async getAuctionByMuestraId(muestraId) {
    try {
      const response = await requests(
        "GET",
        `/auction/auctionId?muestraId=${muestraId}`
      );
      return response;
    } catch (error) {
      return error.response
    }
  }

  async getSubastasFinalizadasCliente(id) {
    try {
      const response = await requests("GET", `/auction/historyCliente/${id}`);
      return response;
    } catch (error) {
      return error.response
    }
  }

  async getSubastasFinalizadasAdmin() {
    try {
      const response = await requests("GET", "/auction/historySubastaAdmin");
      return response;
    } catch (error) {
      return error.response
    }
  }

  async getAuctionByAuctionFinali(muestraId) {
    try {
      const response = await requests(
        "GET",
        `/auction/auctionByFinali?muestraId=${muestraId}`
      );
      return response;
    } catch (error) {
      return error.response
    }
  }
}

export default SubastaService;
