import { requests } from "src/adapters/ServiciosAdapter";

class CategoriaService {
  async getAllCategorias() {
    try {
      const categorias = await requests("GET", "/categorias/categorias");
      return categorias;
    } catch (error) {
      return alert(error.response.data.message);
    }
  }

  async createCategory(data) {
    try {
      const category = await requests(
        "POST",
        "/categorias/categoriacreate",
        data
      );
      return category;
    } catch (error) {
      return alert(error.response.data.message);
    }
  }

  async updateCategory(id, data) {
    try {
      const category = await requests("PUT", `/categorias/categoriasPut/${id}`, data);
      return category;
    } catch (error) {
      return alert(error.response.data.message);
    }
  }

  async deleteCategory(id) {
    try {
      const category = await requests("DELETE", `/categorias/categoriasDelet/${id}`);
      return category;
    } catch (error) {
      return alert(error.response.data.message);
    }
  }
}
export default CategoriaService;