import { requests } from '../../adapters/ServiciosAdapter';

class InvitationService {
    async checkInvitation(data) {
        try {
            const response = await requests("POST",`/auction/checkInvitation`, data);
            return response;
        } catch (error) {
            return error.response
        }
    }
}

export default InvitationService;
