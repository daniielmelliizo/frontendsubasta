import { requests } from "../../adapters/ServiciosAdapter";

class suscripcionService {
  async CreateSuscripcion(data) {
    try {
      const suscripcion = await requests(
        "POST",
        "/suscribed/suscribedCreate",
        data
      );
      return suscripcion;
    } catch (error) {
      return error.response;
    }
  }

  async getSuscripcionAuction(id) {
    try {
      const suscripcion = await requests("GET", `/suscribed/suscribedAll/${id}`);
      return suscripcion;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      // return alert(error.response.data.message);
    }
  }
  async updateSuscripcion(id, data) {
    try {
      const suscripcion = await requests("PUT", `/suscribed/suscribedPut/${id}`, data);
      return suscripcion;
    } catch (error) {
      return error.response;
    }
  }

  async deletesuscripcion(id, muestraId) {
    try {
      const result = await requests("DELETE", `/suscribed/suscribedDelete?userId=${id}&muestraId=${muestraId}`);
      return result;
    } catch (error) {
      return error.response;
    }
  }

  async getSuscripcionList(id) {
    try {
      const suscripcionlist = await requests("GET", `/suscribed/userSuscribed/${id}`);
      return suscripcionlist;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      // return alert(error.response.data.message);
    }
  }
}
export default suscripcionService;
