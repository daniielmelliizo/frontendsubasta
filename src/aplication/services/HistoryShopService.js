import { requests } from "../../adapters/ServiciosAdapter";

class HistoryShopService {
  async CreateHistoryShop(data) {
    try {
      const result = await requests(
        "POST",
        "/historyShop/historyShopCreate",
        data
      );
      return result;
    } catch (error) {
            return error.response;
    }
  }

  async getHistoryShopUser(id) {
    try {
      const result = await requests("GET", `/historyShop/historyShop/${id}`);
      return result;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
            return error.response;
    }
  }

  async getHistoryShopAdmin() {
    try {
      const result = await requests("GET", `/historyShop/historyShopGet`);
      return result;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
            return error.response;
    }
  }

  async updateShopAdmin(id, data) {
    try {
      const result = await requests("PUT", `/historyShop/historyShopPut/${id}`, data);
      return result;
    } catch (error) {
            return error.response;
    }
  }

  async deleteCarst(id) {
    try {
      const result = await requests("DELETE", `/carts/cartsDelete/${id}`);
      return result;
    } catch (error) {
            return error.response;
    }
  }

  async getCompanyProductClient() {
    try {
      const company = await requests("GET", "/companys/companyClient");
      return company;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
            return error.response;
    }
  }

  async getHistoryShopMax() {
    try {
      const company = await requests("GET", "/historyShop/historyShopGetMax");
      return company;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
            return error.response;
    }
  }
}
export default HistoryShopService;
