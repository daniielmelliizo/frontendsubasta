import { requests } from "../../adapters/ServiciosAdapter";

class UserService {
  async createUser(data) {
    try {
      const user = await requests("POST", "/auth/register", data);
      return user;
    } catch (error) {
      return error.response;
    }
  }

  async getUsers() {
    try {
      const user = await requests("GET", "/users");
      return user;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      return error.response;
    }
  }

  async getUsersId(id) {
    try {
      const user = await requests("GET", `/users/userId/${id}`);
      return user;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
    }
  }

  async updateUser(id, data) {
    try {
      const users = await requests("PUT", `/users/edit/${id}`, data);
      return users;
    } catch (error) {
      return error.response;
    }
  }

  async deleteUser(id) {
    try {
      const result = await requests("DELETE", `/users/userDelete/${id}`);
      return result;
    } catch (error) {
      return error.response;
    }
  }

  async Validation(data) {
    try {
      const user = await requests("POST", "/auth/send-verification-code", data);
      return user;
    } catch (error) {
      return error.response;
    }
  }

  async ValidationComplet(data) {
    try {
      const user = await requests("POST", "/auth/verify-email", data);
      return user;
    } catch (error) {
      return error.response;
    }
  }

  async getUsersCargo() {
    try {
      const user = await requests("GET", "/users/usuarioCargo");
      return user;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      return error.response;
    }
  }

  async updatePassword(id, data) {
    try {
      const password = await requests("PUT", `/users/edit/${id}`, data);
      return password;
    } catch (error) {
      return error.response;
    }
  }
  async getUserId(id) {
    try {
      const user = await requests("GET", `/users/userId/${id}`);
      return user;
    } catch (error) {
      return error.response;
    }
  }

  async editPasswordUser(data) {
    try {
      const updatedUser = await requests("PUT", `/users/updatePassword`, data);
      return updatedUser;
    } catch (error) {
      return error.response;
    }
  }

  async getUsersIdPassword(id, data) {
    try {
      const user = await requests(
        "GET",
        `/users/cambioPassword?userId=${id}&currentPassword=${data}`
      );
      return user;
    } catch (error) {}
  }
}
export default UserService;
