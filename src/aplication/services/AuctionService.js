import { requests } from "../../adapters/ServiciosAdapter";

class AuctionService {
  async createAuction(data) {
     try {
      const auction = await requests(
        "POST",
        "/auction/createAuction",
        data
      );
      return auction;
     } catch (error) {
       // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
       return alert(error.response.data.message);
     }
  }

}
export default AuctionService;
