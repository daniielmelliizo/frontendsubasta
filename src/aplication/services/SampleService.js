import { requests } from "../../adapters/ServiciosAdapter";

class SampleService {
  async createSample(data) {
    try {
      const sample = await requests(
        "POST",
        "/sample/sampleCreate",
        data
      );
      return sample;
    } catch (error) {
      return error.response;
    }
  }

  async getSample() {
    try {
      const sample = await requests("GET", "/sample/sampleAll");
      return sample;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      return error.response;
    }
  }

  async getSampleAdmin() {
    try {
      const sample = await requests("GET", "/sample/sampleAllAdmin");
      return sample;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      return error.response;
    }
  }

  async getSampleAllId(id) {
    try {
      const sample = await requests("GET", `/sample/sampleAllId/${id}`);
      return sample;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      return error.response;
    }
  }

  async updateSample(id, data) {
    try {
      const sample = await requests("PUT", `/sample/sampleUpdate/${id}`, data);
      return sample;
    } catch (error) {
      return error.response;
    }
  }

  async createShop(data) {
    try {
     const sample = await requests(
       "POST",
       "/historyShop/historyShopCreate",
       data
     );
     return sample;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      return error.response;
    }
 }

  async deleteUser(id) {
    try {
      const result = await requests("DELETE", `/sample/sampleDelet/${id}`);
      return result;
    } catch (error) {
      return error.response;
    }
  }

  async createCodicoInvitation(data) {
    try {
      const sample = await requests(
        "POST",
        "/code/create",
        data
      );
      return sample;
    } catch (error) {
      return error.response;
    }
  }
}
export default SampleService;
