import { requests } from "src/adapters/ServiciosAdapter";

class BidService {
    async PujaManual(data) {
        try {
            const response = await requests("POST", "/bids/create", data);
            return response;
        } catch (error) {
            return alert(error.response.data.message);
        }
    }
}

export default BidService;
