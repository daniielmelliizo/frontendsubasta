import { requests } from "../../adapters/ServiciosAdapter";

class ProductService {
  async createProduct(data) {
    // Asegurarse de que data esté correctamente formateado como FormData
    if (!(data instanceof FormData)) {
      return Promise.reject(new Error("La información del producto debe ser de tipo FormData."));
    }
  
    // Establecer los encabezados necesarios
    const headers = { "Content-Type": "multipart/form-data" };
  
    // Hacer la petición para crear el producto y manejar la respuesta
    return requests("POST", "/products/createcoofe", data, headers)
      .then((product) => {
        // Retornar el producto creado si la petición es exitosa
        return product;
      })
      .catch((error) => {
  
        // Manejar el error
        if (error.response && error.response.data && error.response.data.message) {
          return error.response
        } else {
          return alert("Ocurrió un error inesperado en el servidor al crear el producto.");
        }
      });
  }

  async getProductById(id) {
    try {
      const product = await requests("GET", `/products/cooffe/${id}`);
      return product;
    } catch (error) {
      return alert(error.response.data.message);
    }
  }

  async updateProduct(id, data) {
    if (!(data instanceof FormData)) {
      return Promise.reject(new Error("La información del producto debe ser de tipo FormData."));
    }
  
    // Establecer los encabezados necesarios
    const headers = { "Content-Type": "multipart/form-data" };
  
    // Hacer la petición para crear el producto y manejar la respuesta
    return requests("PUT", `/products/cooffe/${id}`, data, headers)
      .then((product) => {
        // Retornar el producto creado si la petición es exitosa
        return product;
      })
      .catch((error) => {
  
        // Manejar el error
        if (error.response && error.response.data && error.response.data.message) {
          return error.response
        } else {
          return alert("Ocurrió un error inesperado en el servidor a acutalizar el producto.");
        }
      });
  }


  async getAllProducts() {
    try {
      const products = await requests("GET", "/products/cooffe");
      return products;
    } catch (error) {
      return alert(error.response.data.message);
    }
  }

  async getAllProductsSample() {
    try {
      const products = await requests("GET", "/products/coffeeSample");
      return products;
    } catch (error) {
      return alert(error.response.data.message);
    }
  }

  async deleteProduct(id) {
    try {
      const result = await requests("DELETE", `/products/cooffe/${id}`);
      return result;
    } catch (error) {
      return error.response
    }
  }
}
export default ProductService;