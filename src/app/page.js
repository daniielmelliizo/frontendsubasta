"use client";
import Home from "src/pages/user/Home";

export default function WEB() {
  return (
    <div className="container-fluid p-0">
      <Home />
    </div>
  );
}
