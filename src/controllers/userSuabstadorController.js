class UserSubastaController {
  constructor(userSubastaService) {
    this.userSubastaService = userSubastaService;
  }

  async handleCreateUserSubasta(data) {
    try {
      const user = await this.userSubastaService.createSubastador(data);
      return user;
    } catch (error) {
      console.error('Error en handleCreateUserSubasta:', error);
      return error;
    }
  }

  async handleValidation(data) {
    try {
      const user = await this.userSubastaService.Validation(data);
      return user;
    } catch (error) {
      console.error('Error en handleValidation:', error);
      return error;
    }
  }

  async handleValidationComplet(data) {
    try {
      const user = await this.userSubastaService.ValidationComplet(data);
      return user;
    } catch (error) {
      console.error('Error en handleValidationComplet:', error);
      return error;
    }
  }

  async handleCreateCompany(data) {
    try {
      const user = await this.userSubastaService.createCompany(data);
      return user;
    } catch (error) {
      console.error('Error en handleCreateCompany:', error);
      return error;
    }
  }
}

export default UserSubastaController;
