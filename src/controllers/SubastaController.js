class SubastaController {
  constructor(subastaService) {
    this.subastaService = subastaService;
  }

  async handleGetSubastas() {
    try {
      const auctions = await this.subastaService.getTodasSubastas();
      return auctions;
    } catch (error) {
      return error;
    }
  }

  async handleGetSubastasClient(id) {
    try {
      const auctions = await this.subastaService.getTodasSubastasClient(id);
      return auctions;
    } catch (error) {

      return error;
    }
  }

  async handleGetSubastasSubastador(id) {
    try {
      const auctions = await this.subastaService.getSubastaSubastador(id);
      return auctions;
    } catch (error) {
      console.error('Error fetching auctions: ${error.message}');
      throw new Error('Error fetching auctions: ${error.message}');
    }
  }

  async handleCreateSubasta(data) {
    try {
      const auction = await this.subastaService.createSubasta(data);
      return auction;
    } catch (error) {
      return error;
    }
  }

  async handleUpdateSubasta(id, data) {
    try {
      const auction = await this.subastaService.updateSubasta(id, data);
      return auction;
    } catch (error) {
      return error;
    }
  }
  async handleDeletAuction(id) {
    try {
      const result = await this.subastaService.deleteSubasta(id);
      return result;
    } catch (error) {
      return false;
    }
  }

  async handleGetConfigBid(id) {
    try {
      const response = await this.subastaService.getConfigBid(id);
      return response;
    } catch (error) {
      return error;
    }
  }
  async handleCreateConfigBid(data) {
    try {
      const auction = await this.subastaService.createConfigBid(data);
      return auction;
    } catch (error) {
      return error;
    }
  }

  async handleUpdateConfigBid(id, data) {
    try {
      const auction = await this.subastaService.updateConfigBid(id, data);
      return auction;
    } catch (error) {
      return error;
    }
  }

  async handleGetAuctionByUserMuestra(muestraId, userId) {
    try {
      const auction = await this.subastaService.getAuctionByUserMuestra(muestraId, userId);
      return auction;
    } catch (error) {
      console.error(`Error fetching auction by user: ${error}`);
      return error;
    }
  }

  async handleGetAuctionByMuestraId(muestraId) {
    try {
      const auction = await this.subastaService.getAuctionByMuestraId(muestraId);
      return auction;
    } catch (error) {
      console.error(`Error fetching auction by muestraId: ${error.message}`);
      return error;
    }
  }
  async handleGetSubastasFinalizadas() {
    try {
      const auctions = await this.subastaService.getSubastasFinalizadas();
      return auctions;
    } catch (error) {
      return error;
    }
  }
  
  async handleGetSubastasFinalizadasAdmin() {
    try {
      const auctions = await this.subastaService.getSubastasFinalizadasAdmin();
      return auctions;
    } catch (error) {
      return error;
    }
  }
  async handleGetSubastasFinalizadasclient(id) {
    try {
      const auctions = await this.subastaService.getSubastasFinalizadasCliente(id);
      return auctions;
    } catch (error) {
      return error;
    }
  }

  async handleGetAuctionByAuctionFini(muestraId) {
    try {
      const auction = await this.subastaService.getAuctionByAuctionFinali(muestraId);
      return auction;
    } catch (error) {
      console.error(`Error fetching auction by user: ${error.message}`);
      return error;
    }
  }
}

export default SubastaController;