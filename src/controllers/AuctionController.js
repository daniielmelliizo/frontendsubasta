class AuctionController {
    constructor(auctionService) {
      this.auctionService = auctionService;
    }
    async handleCreateAuction(data) {
      try {
        const auction = await this.auctionService.createAuction(data);
        return auction;
      } catch (error) {
        return error;
      }
    }

  }
  
  export default AuctionController;
  