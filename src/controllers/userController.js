class UserController {
  constructor(userService) {
    this.userService = userService;
  }
  async handleCreateUser(data) {
    try {
      const user = await this.userService.createUser(data);
      return user;
    } catch (error) {
      return error;
    }
  }

  async handleGetUsers() {
    try {
      const users = await this.userService.getUsers();
      return users;
    } catch (error) {
      // console.error("Error al obtener guías:", error.message);
      return [];
    }
  }

  async handleGetUsersId(id) {
    try {
      const user = await this.userService.getUsersId(id);
      return user
    } catch (error) {
      // console.error("Error al obtener guías:", error.message);
      return error;
    }
  }

  async handleGetUsersCargo() {
    try {
      const users = await this.userService.getUsersCargo();
      return users;
    } catch (error) {
      // console.error("Error al obtener guías:", error.message);
      return [];
    }
  }

  async handleUpdateUsers(id, data) {
    try {
      const users = await this.userService.updateUser(id, data);
      return users;
    } catch (error) {
      return error;
    }
  }

  async handleDeleteUser(id) {
    try {
      const result = await this.userService.deleteUser(id);
      return result;
    } catch (error) {
      // console.error("Error al eliminar usuario:", error.message);
      return false;
    }
  }

  async handleValidation(data) {
    try {
      const user = await this.userService.Validation(data);
      return user;
    } catch (error) {
      return error;
    }
  }

  async handleValidationComplet(data) {
    try {
      const user = await this.userService.ValidationComplet(data);
      return user;
    } catch (error) {
      return error;
    }
  }

  async handleGetUsersList(id) {
    try {
      const userlist = await this.userService.getUsersList(id);
      return userlist
    } catch (error) {
      // console.error("Error al obtener guías:", error.message);
      return error;
    }
  }

  async handleUpdatePassword(id, data) {
    try {
      const password = await this.userService.updatePassword(id, data);
      return password;
    } catch (error) {
      return error;
    }
  }

  async handleGetUserId(id) {
    try {
      const user = await this.userService.getUserId(id);
      return user;
    } catch (error) {
      return error;
    }
  }

  // Controlador para editar un usuario
  async handleEditPasswordUser( data) {
    try {
      const updatedUser = await this.userService.editPasswordUser(data);
      return updatedUser;
    } catch (error) {
      return error;
    }
  }

  async handleGetUsersPassword(id, data) {
    try {
      const users = await this.userService.getUsersIdPassword(id, data);
      return users;
    } catch (error) {
      // console.error("Error al obtener guías:", error.message);
      return [];
    }
  }

}

export default UserController;
