class SuscripcionController {
  constructor(suscripcionService) {
    this.suscripcionService = suscripcionService;
  }
  async handleCreateSuscripcion(data) {
    try {
      const suscripcion = await this.suscripcionService.CreateSuscripcion(data);
      return suscripcion;
    } catch (error) {
      return error;
    }
  }

  async handleGetsuscripcion(id) {
    try {
      const suscripcion = await this.suscripcionService.getSuscripcionAuction(id);
      return suscripcion;
    } catch (error) {
      // console.error("Error al obtener guías:", error.message);
      return error;
    }
  }

  async handleUpdatesuscripcion(id, data) {
    try {
      const suscripcion = await this.suscripcionService.updateSuscripcion(id, data);
      return suscripcion;
    } catch (error) {
      // console.error("Error al actualizar usuario:", error.message);
      return error;
    }
  }

  async handleDeletsuscripcion(id, muestraId) {
    try {
      const result = await this.suscripcionService.deletesuscripcion(id, muestraId);
      return result;
    } catch (error) {
      return false;
    }
  }


  async handleGetsuscripcionList(id) {
    try {
      const suscripcionList = await this.suscripcionService.getSuscripcionList(id);
      return suscripcionList;
    } catch (error) {
      // console.error("Error al obtener guías:", error.message);
      return error;
    }
  }
}

export default SuscripcionController;
