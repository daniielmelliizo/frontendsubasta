class SampleController {
    constructor(sampleService) {
      this.sampleService = sampleService;
    }
    async handleCreateSample(data) {
      try {
        const sample = await this.sampleService.createSample(data);
        return sample;
      } catch (error) {
        return error;
      }
    }

    async handleGetSample() {
      try {
        const sample = await this.sampleService.getSample();
        return sample;
      } catch (error) {
        return error;
      }
    }

    async handleGetSampleAdmin() {
      try {
        const sample = await this.sampleService.getSampleAdmin();
        return sample;
      } catch (error) {
        // console.error("Error al obtener guías:", error.message);
        return error;
      }
    }

  
    async handleUpdateSample(id, data) {
      try {
        const sample = await this.sampleService.updateSample(id, data);
        return sample;
      } catch (error) {
        // console.error("Error al actualizar usuario:", error.message);
        return error;
      }
    }

    async handleUShopSample( data) {
      try {
        const sample = await this.sampleService.createShop( data);
        return sample;
      } catch (error) {
        // console.error("Error al actualizar usuario:", error.message);
        return error;
      }
    }
  
    async handleDeleteUser(id) {
      try {
        const result = await this.sampleService.deleteUser(id);
        return result;
      } catch (error) {
        // console.error("Error al eliminar usuario:", error.message);
        return false;
      }
    }

    async handleCreateCodigoSample(data) {
      try {
        const sample = await this.sampleService.createCodicoInvitation(data);
        return sample;
      } catch (error) {
        return error;
      }
    }
  }
  
  export default SampleController;
  