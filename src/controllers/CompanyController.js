class CompanyController {
    constructor(companyService) {
      this.companyService = companyService;
    }
    async handleCreateCompany(data) {
      try {
        const company = await this.companyService.CreateCompany(data);
        return company;
      } catch (error) {
        return error;
      }
    }

    async handleGetCompany() {
      try {
        const company = await this.companyService.getCompanyProduct();
        return company;
      } catch (error) {
        return error;
      }
    }
  
    async handleUpdateCompany(id, data) {
      try {
        const company = await this.companyService.updateCompany(id, data);
        return company;
      } catch (error) {
        // console.error("Error al actualizar usuario:", error.message);
        return error;
      }
    }

    async removeProductFromCompany(company, productId) {
      const updatedCompany = { ...company };
      updatedCompany.productsId = updatedCompany.productsId.filter(p => p.product !== productId);
  
      if (updatedCompany.id) {
        const updateResponse = await this.handleUpdateCompany(updatedCompany.id, updatedCompany);
        return updateResponse;
      } else {
        throw new Error("No se ha seleccionado una empresa válida.");
      }
    }
  
    async handleDeletCompany(id) {
      try {
        const result = await this.companyService.deleteCompany(id);
        return result;
      } catch (error) {
        // console.error("Error al eliminar usuario:", error.message);
        return false;
      }
    }

    async handleGetCompanyClient() {
      try {
        const company = await this.companyService.getCompanyProductClient();
        return company;
      } catch (error) {
        return error;
      }
    }
  }
  
  export default CompanyController;
  