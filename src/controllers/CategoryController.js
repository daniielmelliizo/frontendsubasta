class CategoriaController {
    constructor(categoriaService) {
      this.categoriaService = categoriaService;
    }
  
    async getAllCategorias() {
      try {
        return await this.categoriaService.getAllCategorias();
      } catch (error) {
        console.error("Error fetching categories:", error);
        throw error;
      }
    }

    async handleCreateCategory(data) {
      try {
        const category = await this.categoriaService.createCategory(data);
        return category;
      } catch (error) {
        return error;
      }
    }

    async handleUpdateCategory(id, data) {
      try {
        const category = await this.categoriaService.updateCategory(id, data);
        return category;
      } catch (error) {
        return error;
      }
    }

    async handleDeleteCategory(id) {
      try {
        const category = await this.categoriaService.deleteCategory(id);
        return category;
      } catch (error) {
       console.error("Error al eliminar categoria:", error.message);
        return false;
      }
    }
  }
  
  export default CategoriaController;