class UserInvitedController {
  constructor(userInvitedService ) {
    this.userInvitedService = userInvitedService ;
  }

  async handleCreateUserInvited(data) {
    try {
      const user = await this.userInvitedService.createUser(data);
      return user;
    } catch (error) {
      console.error('Error en handleCreateUserInvited:', error);
      return error;
    }
  }
}

export default UserInvitedController;
