class CompanyController {
  constructor(carstService) {
    this.carstService = carstService;
  }
  async handleCreateCarst(data) {
    try {
      const company = await this.carstService.CreateCarst(data);
      return company;
    } catch (error) {
      return error;
    }
  }

  async handleGetCarstUser(id) {
    try {
      const result = await this.carstService.getCarstUser(id);
      return result;
    } catch (error) {
      return error;
    }
  }

  async handleUpdateCarstProduct(id, data) {
    try {
      const result = await this.carstService.updateCarstProduct(id, data);
      return result;
    } catch (error) {
      return error;
    }
  }

  async handleDeletCarst(id) {
    try {
      const result = await this.carstService.deleteCarst(id);
      return result;
    } catch (error) {
      return false;
    }
  }
}

export default CompanyController;
