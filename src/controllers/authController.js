// src/controllers/authController.js

import AuthenticationService from "src/aplication/services/authService";

class AuthenticationController {
  constructor() {
    this.authenticationService = new AuthenticationService();
  }

  async handleLogin(email, password) {
    try {
      const user = await this.authenticationService.login(email, password);
      return  user /* { success: true, data: user } */;
    } catch (error) {
      return error;
    }
  }
}

export default AuthenticationController;
