class InvitationController {
    constructor(invitationService) {
        this.invitationService = invitationService;
    }

    async handleCheckInvitation(data) {
        try {
            const result = await this.invitationService.checkInvitation(data);
            return result;
        } catch (error) {
            return  error ;
        }
    }
}

export default InvitationController;
