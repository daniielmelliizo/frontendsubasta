class HistoryShopController {
    constructor(historyShopService) {
      this.historyShopService = historyShopService;
    }
    async handleCreateHistoryShop(data) {
      try {
        const company = await this.historyShopService.CreateHistoryShop(data);
        return company;
      } catch (error) {
        return error;
      }
    }

    async handleGetHistoryShopUser(id) {
      try {
        const result = await this.historyShopService.getHistoryShopUser(id);
        return result;
      } catch (error) {
        return error;
      }
    }
    async handleGetHistoryShopAdmin() {
      try {
        const result = await this.historyShopService.getHistoryShopAdmin();
        return result;
      } catch (error) {
        return error;
      }
    }

    async handleUpdateShop(id, data) {
      try {
        const result = await this.historyShopService.updateShopAdmin(id, data);
        return result;
      } catch (error) {
        // console.error("Error al actualizar usuario:", error.message);
        return error;
      }
    }

    async handleGetMaxShop() {
      try {
        const result = await this.historyShopService.getHistoryShopMax();
        return result;
      } catch (error) {
        return error;
      }
    }

  }
  
  export default HistoryShopController;
  