import BidService from "src/aplication/services/BidService";

class BidController {
    constructor() {
        this.bidService = new BidService();
    }

    async handlePujaManual(data) {
        try {
            const puja = await this.bidService.PujaManual(data);
            return puja;
        } catch (error) {
            return error;
        }
    }
}

export default BidController;
