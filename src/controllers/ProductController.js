class ProductController {
    constructor(productService) {
      this.productService = productService;
    }
    async handleCreateProduct(data) {
      try {
        const product = await this.productService.createProduct(data);
        return product;
      } catch (error) {
        return error;
      }
    }

    async getProductById(id) {
      try {
        return await this.productService.getProductById(id);
      } catch (error) {
        console.error("Error fetching product details:", error);
        throw error;
      }
    }

    async handleUpdateProduct(id, data) {
      try {
        const product = await this.productService.updateProduct(id, data);
        return product;
      } catch (error) {
        // console.error("Error al actualizar usuario:", error.message);
        return error;
      }
    }

    async getAllProducts() {
      try {
        return await this.productService.getAllProducts();
      } catch (error) {
        console.error("Error fetching products:", error);
        throw error;
      }
    }

    async HandlegetAllProductsSample() {
      try {
        return await this.productService.getAllProductsSample();
      } catch (error) {
        console.error("Error fetching products:", error);
        throw error;
      }
    }

    async handleDeletProduct(id) {
      try {
        const result = await this.productService.deleteProduct(id);
        return result;
      } catch (error) {
        return false;
      }
    }
   
  }
  
  export default ProductController;
  