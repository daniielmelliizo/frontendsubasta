import axios from 'axios';

const apiClient = axios.create({
  baseURL: 'http://185.164.110.7:5000', 
  timeout: 10000,
});

export default apiClient;