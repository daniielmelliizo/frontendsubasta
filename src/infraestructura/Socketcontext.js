// context/SocketContext.js
import React, { createContext, useContext, useEffect, useState } from 'react';
import { io } from 'socket.io-client';

const SocketContext = createContext();

export const SocketProvider = ({ children }) => {
  const [socket, setSocket] = useState(null);

  useEffect(() => {
    const newSocket = io("http://185.164.110.7:5000", {
        reconnectionAttempts: 5,
        timeout: 10000
    });

    console.log("Conectando al socket...");

    newSocket.on("connect", () => {
        console.log("Conectado al socket!");
    });

    newSocket.on("disconnect", (reason) => {
        console.log("Desconectado del socket:", reason);
    });

    newSocket.on("connect_error", (error) => {
        console.error("Error al conectar al socket:", error);
    });

    setSocket(newSocket);

    return () => {
        newSocket.disconnect();
    };
}, []);

  return (
    <SocketContext.Provider value={socket}>
      {children}
    </SocketContext.Provider>
  );
};

// Hook para acceder al socket
export const useSocket = () => {
  return useContext(SocketContext);
};
